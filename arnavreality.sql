-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 08, 2020 at 10:14 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arnavreality`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_pages`
--

DROP TABLE IF EXISTS `add_pages`;
CREATE TABLE IF NOT EXISTS `add_pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subMenuName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pageTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageDescription` longtext COLLATE utf8mb4_unicode_ci,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaKeyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `add_pages`
--

INSERT INTO `add_pages` (`id`, `pageName`, `subMenuName`, `pageTitle`, `pageDescription`, `metaTitle`, `metaDescription`, `metaKeyword`, `created_at`, `updated_at`) VALUES
(15, '3', NULL, 'About US', '<section _ngcontent-lhr-c5=\"\" class=\"page-title\" style=\"background-image:url(./assets/images/background/1.jpg)\"><div _ngcontent-lhr-c5=\"\" class=\"container\"><div _ngcontent-lhr-c5=\"\" class=\"outer-box\"><h1 _ngcontent-lhr-c5=\"\">About Us</h1><ul _ngcontent-lhr-c5=\"\" class=\"bread-crumb clearfix\"><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\" class=\"current\"><span _ngcontent-lhr-c5=\"\" class=\"fa fa-home\"></span>Home</a></li><li _ngcontent-lhr-c5=\"\" class=\"active\">About Us</li></ul></div></div></section>\r\n\r\n<section _ngcontent-lhr-c5=\"\" class=\"what-we-do sp-three\"><div _ngcontent-lhr-c5=\"\" class=\"container\"><div _ngcontent-lhr-c5=\"\" class=\"row\"><div _ngcontent-lhr-c5=\"\" class=\"col-lg-6\"><div _ngcontent-lhr-c5=\"\" class=\"sec-title\"><h1 _ngcontent-lhr-c5=\"\">What We <span _ngcontent-lhr-c5=\"\">Do?</span></h1></div><div _ngcontent-lhr-c5=\"\" class=\"text\"><p _ngcontent-lhr-c5=\"\">Lorem ipsum dolor sit amet consectetur adipisicing elit sed do ei usmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo con sequat duis aute irure dolor.</p><p _ngcontent-lhr-c5=\"\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo con sequat duis aute irure dolor.</p></div><div _ngcontent-lhr-c5=\"\" class=\"link-btn mb-30 mt-40\"><a _ngcontent-lhr-c5=\"\" class=\"theme-btn btn-style-two\" href=\"#\">Read More</a></div></div><div _ngcontent-lhr-c5=\"\" class=\"col-lg-6 pr-lg-5\"><div _ngcontent-lhr-c5=\"\" class=\"video-image-box style-2 mb-30\"><div _ngcontent-lhr-c5=\"\" class=\"image\"><img _ngcontent-lhr-c5=\"\" alt=\"\" src=\"./assets/images/resource/wel-1.png\"><a _ngcontent-lhr-c5=\"\" class=\"overlay-link play-now\" data-caption=\"\" data-fancybox=\"gallery\" href=\"https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s\"><span _ngcontent-lhr-c5=\"\" class=\"flaticon-play-button\"></span></a><span _ngcontent-lhr-c5=\"\" class=\"ripple\"></span></div></div></div></div></div></section>\r\n\r\n<section _ngcontent-lhr-c5=\"\" class=\"our-experience sp-four grey-light-bg\" style=\"background-image:url(./assets/images/background/about.png);\"><div _ngcontent-lhr-c5=\"\" class=\"container\"><div _ngcontent-lhr-c5=\"\" class=\"sec-title centered\"><h1 _ngcontent-lhr-c5=\"\">About <span _ngcontent-lhr-c5=\"\">Us</span></h1><p _ngcontent-lhr-c5=\"\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo</p></div><div _ngcontent-lhr-c5=\"\" class=\"row\"><div _ngcontent-lhr-c5=\"\" class=\"col-lg-4 col-sm-6 pr-lg-5\"><div _ngcontent-lhr-c5=\"\" class=\"experience-box\"><div _ngcontent-lhr-c5=\"\" class=\"experience-icon\"><i _ngcontent-lhr-c5=\"\" class=\"flaticon-medal\"></i></div><h3 _ngcontent-lhr-c5=\"\" class=\"title\"><a _ngcontent-lhr-c5=\"\" routerlink=\"/about\" routerlinkactive=\"current\" href=\"/jithvar/about\" class=\"current\">Best Company</a><span _ngcontent-lhr-c5=\"\" class=\"experience-no\">01</span></h3><p _ngcontent-lhr-c5=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin pharetra tortor. Phasellus condimentum dui sed hendrerit. </p></div><div _ngcontent-lhr-c5=\"\" class=\"experience-box\"><div _ngcontent-lhr-c5=\"\" class=\"experience-icon\"><i _ngcontent-lhr-c5=\"\" class=\"flaticon-flag\"></i></div><h3 _ngcontent-lhr-c5=\"\" class=\"title\"><a _ngcontent-lhr-c5=\"\" routerlink=\"/about\" routerlinkactive=\"current\" href=\"/jithvar/about\" class=\"current\">Our Mission</a><span _ngcontent-lhr-c5=\"\" class=\"experience-no\">02</span></h3><p _ngcontent-lhr-c5=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin pharetra tortor. Phasellus condimentum dui sed hendrerit. </p></div></div><div _ngcontent-lhr-c5=\"\" class=\"col-lg-4 col-sm-6 pr-lg-5\"><div _ngcontent-lhr-c5=\"\" class=\"experience-box\"><div _ngcontent-lhr-c5=\"\" class=\"experience-icon\"><i _ngcontent-lhr-c5=\"\" class=\"flaticon-rocket-hitting-target\"></i></div><h3 _ngcontent-lhr-c5=\"\" class=\"title\"><a _ngcontent-lhr-c5=\"\" routerlink=\"/about\" routerlinkactive=\"current\" href=\"/jithvar/about\" class=\"current\">Our Vision</a><span _ngcontent-lhr-c5=\"\" class=\"experience-no\">03</span></h3><p _ngcontent-lhr-c5=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin pharetra tortor. Phasellus condimentum dui sed hendrerit. </p></div><div _ngcontent-lhr-c5=\"\" class=\"experience-box\"><div _ngcontent-lhr-c5=\"\" class=\"experience-icon\"><i _ngcontent-lhr-c5=\"\" class=\"flaticon-settings-work-tool\"></i></div><h3 _ngcontent-lhr-c5=\"\" class=\"title\"><a _ngcontent-lhr-c5=\"\" routerlink=\"/about\" routerlinkactive=\"current\" href=\"/jithvar/about\" class=\"current\">What we do</a><span _ngcontent-lhr-c5=\"\" class=\"experience-no\">04</span></h3><p _ngcontent-lhr-c5=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin pharetra tortor. Phasellus condimentum dui sed hendrerit. </p></div></div><div _ngcontent-lhr-c5=\"\" class=\"col-lg-4 pl-lg-5\"><div _ngcontent-lhr-c5=\"\" class=\"experience-image-box wow slideInRight\" data-wow-delay=\"0ms\" data-wow-duration=\"3000ms\"><img _ngcontent-lhr-c5=\"\" alt=\"\" src=\"./assets/images/resource/about-1.png\"></div></div></div></div></section>\r\n\r\n<section _ngcontent-lhr-c5=\"\" class=\"our-team sp-one\"><div _ngcontent-lhr-c5=\"\" class=\"container\"><div _ngcontent-lhr-c5=\"\" class=\"sec-title centered\"><h1 _ngcontent-lhr-c5=\"\">Our <span _ngcontent-lhr-c5=\"\">Commitee</span></h1><p _ngcontent-lhr-c5=\"\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo</p></div><div _ngcontent-lhr-c5=\"\" class=\"row\"><div _ngcontent-lhr-c5=\"\" class=\"team-block-one col-lg-4 col-md-6\"><div _ngcontent-lhr-c5=\"\" class=\"team-item\"><div _ngcontent-lhr-c5=\"\" class=\"team-box\"><div _ngcontent-lhr-c5=\"\" class=\"team-image\"><div _ngcontent-lhr-c5=\"\" class=\"overlay\"></div><img _ngcontent-lhr-c5=\"\" alt=\"\" src=\"./assets/images/resource/team-1.jpg\"><ul _ngcontent-lhr-c5=\"\" class=\"social\"><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-facebook\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-twitter\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-linkedin\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-google-plus\"></i></a></li></ul></div></div><div _ngcontent-lhr-c5=\"\" class=\"designation\"><h4 _ngcontent-lhr-c5=\"\">Mr. Muzad</h4><h5 _ngcontent-lhr-c5=\"\">Advisor</h5></div></div></div><div _ngcontent-lhr-c5=\"\" class=\"team-block-one col-lg-4 col-md-6\"><div _ngcontent-lhr-c5=\"\" class=\"team-item style-2\"><div _ngcontent-lhr-c5=\"\" class=\"team-box\"><div _ngcontent-lhr-c5=\"\" class=\"team-image\"><div _ngcontent-lhr-c5=\"\" class=\"overlay\"></div><img _ngcontent-lhr-c5=\"\" alt=\"\" src=\"./assets/images/resource/team-2.jpg\"><ul _ngcontent-lhr-c5=\"\" class=\"social\"><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-facebook\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-twitter\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-linkedin\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-google-plus\"></i></a></li></ul></div></div><div _ngcontent-lhr-c5=\"\" class=\"designation\"><h4 _ngcontent-lhr-c5=\"\">Robert Khaled</h4><h5 _ngcontent-lhr-c5=\"\">Member</h5></div></div></div><div _ngcontent-lhr-c5=\"\" class=\"team-block-one col-lg-4 col-md-6\"><div _ngcontent-lhr-c5=\"\" class=\"team-item\"><div _ngcontent-lhr-c5=\"\" class=\"team-box\"><div _ngcontent-lhr-c5=\"\" class=\"team-image\"><div _ngcontent-lhr-c5=\"\" class=\"overlay\"></div><img _ngcontent-lhr-c5=\"\" alt=\"\" src=\"./assets/images/resource/team-3.jpg\"><ul _ngcontent-lhr-c5=\"\" class=\"social\"><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-facebook\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-twitter\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-linkedin\"></i></a></li><li _ngcontent-lhr-c5=\"\"><a _ngcontent-lhr-c5=\"\" href=\"#\"><i _ngcontent-lhr-c5=\"\" class=\"fab fa-google-plus\"></i></a></li></ul></div></div><div _ngcontent-lhr-c5=\"\" class=\"designation\"><h4 _ngcontent-lhr-c5=\"\">Albert Mofiz</h4><h5 _ngcontent-lhr-c5=\"\">Member</h5></div></div></div></div></div></section>', NULL, NULL, NULL, '2020-01-07 11:18:54', '2020-01-08 00:48:01'),
(29, '9', NULL, 'Services', '<section _ngcontent-lgn-c7=\"\" class=\"page-title\" style=\"background-image:url(./assets/images/background/1.jpg)\"><div _ngcontent-lgn-c7=\"\" class=\"container\"><div _ngcontent-lgn-c7=\"\" class=\"outer-box\"><h1 _ngcontent-lgn-c7=\"\">Services</h1><ul _ngcontent-lgn-c7=\"\" class=\"bread-crumb clearfix\"><li _ngcontent-lgn-c7=\"\"><a _ngcontent-lgn-c7=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\" class=\"current\"><span _ngcontent-lgn-c7=\"\" class=\"fa fa-home\"></span>Home</a></li><li _ngcontent-lgn-c7=\"\" class=\"active\">Services</li></ul></div></div></section>\r\n\r\n<section _ngcontent-lgn-c7=\"\" class=\"feature-case-section padd-450 sp-ten style-2 grey-light-bg\" style=\"background-image:url(images/background/ser-1.png);\"><div _ngcontent-lgn-c7=\"\" class=\"container\"><div _ngcontent-lgn-c7=\"\" class=\"row\"><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4\"><div _ngcontent-lgn-c7=\"\" class=\"serving-box\"><div _ngcontent-lgn-c7=\"\" class=\"serving-content\"><h3 _ngcontent-lgn-c7=\"\" class=\"title\"><a _ngcontent-lgn-c7=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\" class=\"current\">Financial Analysis</a></h3><p _ngcontent-lgn-c7=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate delectus deserunt dolores eum ipsa molestiae officiis quasi ratione voluptatum. </p></div><i _ngcontent-lgn-c7=\"\" class=\"flaticon-money-1\"></i><div _ngcontent-lgn-c7=\"\" class=\"link-btn mb-30 mt-40\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-five\" href=\"./\">Read More</a></div></div></div><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4\"><div _ngcontent-lgn-c7=\"\" class=\"serving-box style-2\"><div _ngcontent-lgn-c7=\"\" class=\"serving-content\"><h3 _ngcontent-lgn-c7=\"\" class=\"title\"><a _ngcontent-lgn-c7=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\" class=\"current\">Research Managment</a></h3><p _ngcontent-lgn-c7=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate delectus deserunt dolores eum ipsa molestiae officiis quasi ratione voluptatum. </p></div><i _ngcontent-lgn-c7=\"\" class=\"flaticon-data-management-interface-symbol-with-gears-and-binary-code-numbers\"></i><div _ngcontent-lgn-c7=\"\" class=\"link-btn mb-30 mt-40\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-five\" href=\"./\">Read More</a></div></div></div><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4\"><div _ngcontent-lgn-c7=\"\" class=\"serving-box\"><div _ngcontent-lgn-c7=\"\" class=\"serving-content\"><h3 _ngcontent-lgn-c7=\"\" class=\"title\"><a _ngcontent-lgn-c7=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\" class=\"current\">Investment Trade</a></h3><p _ngcontent-lgn-c7=\"\" class=\"description\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate delectus deserunt dolores eum ipsa molestiae officiis quasi ratione voluptatum. </p></div><i _ngcontent-lgn-c7=\"\" class=\"flaticon-sell\"></i><div _ngcontent-lgn-c7=\"\" class=\"link-btn mb-30 mt-40\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-five current\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/jithvar/\">Read More</a></div></div></div></div></div></section>\r\n\r\n<section _ngcontent-lgn-c7=\"\" class=\"pricing-section sp-six\"><div _ngcontent-lgn-c7=\"\" class=\"container\"><div _ngcontent-lgn-c7=\"\" class=\"sec-title centered\"><h1 _ngcontent-lgn-c7=\"\">Company <span _ngcontent-lgn-c7=\"\">Packages</span></h1><p _ngcontent-lgn-c7=\"\">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Perspiciatis unde omnis iste natus error sit.</p></div><div _ngcontent-lgn-c7=\"\" class=\"row\"><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4 col-md-6 pricing-block-one\"><div _ngcontent-lgn-c7=\"\" class=\"inner-box hvr-float-shadow\"><div _ngcontent-lgn-c7=\"\" class=\"category\">Basic Plan</div><div _ngcontent-lgn-c7=\"\" class=\"price\">$39</div><div _ngcontent-lgn-c7=\"\" class=\"text\">Excepteur sint occaecat cupidatat non proi dent sunt in culpa.</div><div _ngcontent-lgn-c7=\"\" class=\"lower-box\"><ul _ngcontent-lgn-c7=\"\"><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">1 Bathroom Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">Up to 3 Bedrooms Cleaning</strong></li><li _ngcontent-lgn-c7=\"\">1 Livingroom Cleaning</li><li _ngcontent-lgn-c7=\"\">Kitchen Cleaning</li></ul><div _ngcontent-lgn-c7=\"\" class=\"link-btn\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-two current\" routerlink=\"\" routerlinkactive=\"current\" href=\"/jithvar/\">Read More</a></div></div></div></div><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4 col-md-6 pricing-block-one standard\"><div _ngcontent-lgn-c7=\"\" class=\"inner-box hvr-float-shadow\"><div _ngcontent-lgn-c7=\"\" class=\"star\"><span _ngcontent-lgn-c7=\"\" class=\"fas fa-star\"></span></div><div _ngcontent-lgn-c7=\"\" class=\"category\">Standard Plan</div><div _ngcontent-lgn-c7=\"\" class=\"price\">$45</div><div _ngcontent-lgn-c7=\"\" class=\"text\">Excepteur sint occaecat cupidatat non proi dent sunt in culpa.</div><div _ngcontent-lgn-c7=\"\" class=\"lower-box\"><ul _ngcontent-lgn-c7=\"\"><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">1 Bathroom Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">Up to 3 Bedrooms Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">1 Livingroom Cleaning</strong></li><li _ngcontent-lgn-c7=\"\" class=\"light\">Kitchen Cleaning</li></ul><div _ngcontent-lgn-c7=\"\" class=\"link-btn\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-sixteen current\" routerlink=\"\" routerlinkactive=\"current\" href=\"/jithvar/\">Read More</a></div></div></div></div><div _ngcontent-lgn-c7=\"\" class=\"col-lg-4 col-md-6 pricing-block-one\"><div _ngcontent-lgn-c7=\"\" class=\"inner-box hvr-float-shadow\"><div _ngcontent-lgn-c7=\"\" class=\"category\">Advanced Plan</div><div _ngcontent-lgn-c7=\"\" class=\"price\">$69</div><div _ngcontent-lgn-c7=\"\" class=\"text\">Excepteur sint occaecat cupidatat non proi dent sunt in culpa.</div><div _ngcontent-lgn-c7=\"\" class=\"lower-box\"><ul _ngcontent-lgn-c7=\"\"><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">1 Bathroom Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">Up to 3 Bedrooms Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">1 Livingroom Cleaning</strong></li><li _ngcontent-lgn-c7=\"\"><strong _ngcontent-lgn-c7=\"\">Kitchen Cleaning</strong></li></ul><div _ngcontent-lgn-c7=\"\" class=\"link-btn\"><a _ngcontent-lgn-c7=\"\" class=\"theme-btn btn-style-two current\" routerlink=\"\" routerlinkactive=\"current\" href=\"/jithvar/\">Read More</a></div></div></div></div></div></div></section>', NULL, NULL, NULL, '2020-01-08 16:25:40', '2020-01-08 16:30:33'),
(30, '8', NULL, 'Company History', NULL, NULL, NULL, NULL, '2020-01-08 16:33:29', '2020-01-08 16:33:29'),
(32, '11', NULL, 'Software Development', '<section _ngcontent-ows-c4=\"\" class=\"page-title\" style=\"background-image:url(./assets/images/background/1.jpg)\"><div _ngcontent-ows-c4=\"\" class=\"container\"><div _ngcontent-ows-c4=\"\" class=\"outer-box\"><h1 _ngcontent-ows-c4=\"\">Service Details</h1><ul _ngcontent-ows-c4=\"\" class=\"bread-crumb clearfix\"><li _ngcontent-ows-c4=\"\"><a _ngcontent-ows-c4=\"\" routerlink=\"/\" routerlinkactive=\"current\" href=\"/\" class=\"current\"><span _ngcontent-ows-c4=\"\" class=\"fa fa-home\"></span>Home</a></li><li _ngcontent-ows-c4=\"\" class=\"active\">Service Details</li></ul></div></div></section>\r\n<section _ngcontent-ows-c4=\"\" class=\"service-details sp-12\"><div _ngcontent-ows-c4=\"\" class=\"container\"><div _ngcontent-ows-c4=\"\" class=\"row\"><div _ngcontent-ows-c4=\"\" class=\"col-lg-4 sidebar\"><div _ngcontent-ows-c4=\"\" class=\"sidebar-widget category-widget\"><div _ngcontent-ows-c4=\"\" class=\"sidebar-title\"><h4 _ngcontent-ows-c4=\"\">Our Services</h4></div><ul _ngcontent-ows-c4=\"\"><li _ngcontent-ows-c4=\"\" class=\"active\"><a _ngcontent-ows-c4=\"\" href=\"#\">Invest Planning</a></li><li _ngcontent-ows-c4=\"\"><a _ngcontent-ows-c4=\"\" href=\"#\">Financial Analysis</a></li><li _ngcontent-ows-c4=\"\"><a _ngcontent-ows-c4=\"\" href=\"#\">Market Research</a></li><li _ngcontent-ows-c4=\"\"><a _ngcontent-ows-c4=\"\" href=\"#\">Investment Trade</a></li><li _ngcontent-ows-c4=\"\"><a _ngcontent-ows-c4=\"\" href=\"#\">Business Plan</a></li></ul></div><div _ngcontent-ows-c4=\"\" class=\"broucher\"><a _ngcontent-ows-c4=\"\" class=\"theme-btn\" href=\"#\"><span _ngcontent-ows-c4=\"\" class=\"flaticon-acrobat\"></span>Download Application <br _ngcontent-ows-c4=\"\">Online Form</a><a _ngcontent-ows-c4=\"\" class=\"theme-btn\" href=\"#\"><span _ngcontent-ows-c4=\"\" class=\"flaticon-acrobat\"></span>Download Clearence <br _ngcontent-ows-c4=\"\">Online Form</a></div><div _ngcontent-ows-c4=\"\" class=\"sidebar-widget tag-widget\"><div _ngcontent-ows-c4=\"\" class=\"sidebar-title\"><h4 _ngcontent-ows-c4=\"\">Tags</h4></div><a _ngcontent-ows-c4=\"\" href=\"#\">Business</a><a _ngcontent-ows-c4=\"\" href=\"#\">Insurance</a><a _ngcontent-ows-c4=\"\" href=\"#\">Inevestor</a><a _ngcontent-ows-c4=\"\" href=\"#\">Finance</a><a _ngcontent-ows-c4=\"\" href=\"#\">Marketing</a><a _ngcontent-ows-c4=\"\" href=\"#\">Account &amp; Profit</a></div></div><div _ngcontent-ows-c4=\"\" class=\"col-lg-8\"><div _ngcontent-ows-c4=\"\" class=\"content\"><div _ngcontent-ows-c4=\"\" class=\"image mb-50\"><img _ngcontent-ows-c4=\"\" alt=\"\" src=\"./assets/images/resource/service-14.jpg\"></div><h2 _ngcontent-ows-c4=\"\">Financial Analysis</h2><div _ngcontent-ows-c4=\"\" class=\"text\"><p _ngcontent-ows-c4=\"\">Lorem ipsum dolor sit amet consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum.</p></div><blockquote _ngcontent-ows-c4=\"\" class=\"block-quote\"><div _ngcontent-ows-c4=\"\" class=\"text\">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod tempor in cididunt labore dolore magna aliqua. enim minim veniam quis nostrud exercitation ullamco</div></blockquote><div _ngcontent-ows-c4=\"\" class=\"text mb-80\">Quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum.</div><div _ngcontent-ows-c4=\"\" class=\"row mb-80\"><ul _ngcontent-ows-c4=\"\" class=\"accordion-box\"><li _ngcontent-ows-c4=\"\" class=\"accordion block\"><div _ngcontent-ows-c4=\"\" class=\"acc-btn\"><div _ngcontent-ows-c4=\"\" class=\"icon-outer\"><span _ngcontent-ows-c4=\"\" class=\"icon icon_plus fa fa-angle-down\"></span><span _ngcontent-ows-c4=\"\" class=\"icon icon_minus fa fa-angle-down\"></span></div>How do I find analyst reports ?</div><div _ngcontent-ows-c4=\"\" class=\"acc-content\"><div _ngcontent-ows-c4=\"\" class=\"content\"><div _ngcontent-ows-c4=\"\" class=\"text mb-30\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum. </div></div></div></li><li _ngcontent-ows-c4=\"\" class=\"accordion block\"><div _ngcontent-ows-c4=\"\" class=\"acc-btn active\"><div _ngcontent-ows-c4=\"\" class=\"icon-outer\"><span _ngcontent-ows-c4=\"\" class=\"icon icon_plus fa fa-angle-down\"></span><span _ngcontent-ows-c4=\"\" class=\"icon icon_minus fa fa-angle-down\"></span></div>Where should I incorporate my business?</div><div _ngcontent-ows-c4=\"\" class=\"acc-content current\"><div _ngcontent-ows-c4=\"\" class=\"content\"><div _ngcontent-ows-c4=\"\" class=\"text mb-30\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum. </div></div></div></li><li _ngcontent-ows-c4=\"\" class=\"accordion block\"><div _ngcontent-ows-c4=\"\" class=\"acc-btn\"><div _ngcontent-ows-c4=\"\" class=\"icon-outer\"><span _ngcontent-ows-c4=\"\" class=\"icon icon_plus fa fa-angle-down\"></span><span _ngcontent-ows-c4=\"\" class=\"icon icon_minus fa fa-angle-down\"></span></div>What is the meaning of tax entity?</div><div _ngcontent-ows-c4=\"\" class=\"acc-content\"><div _ngcontent-ows-c4=\"\" class=\"content\"><div _ngcontent-ows-c4=\"\" class=\"text mb-30\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum. </div></div></div></li><li _ngcontent-ows-c4=\"\" class=\"accordion block\"><div _ngcontent-ows-c4=\"\" class=\"acc-btn\"><div _ngcontent-ows-c4=\"\" class=\"icon-outer\"><span _ngcontent-ows-c4=\"\" class=\"icon icon_plus fa fa-angle-down\"></span><span _ngcontent-ows-c4=\"\" class=\"icon icon_minus fa fa-angle-down\"></span></div>How many of your customers reach their goals ?</div><div _ngcontent-ows-c4=\"\" class=\"acc-content\"><div _ngcontent-ows-c4=\"\" class=\"content\"><div _ngcontent-ows-c4=\"\" class=\"text mb-30\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum. </div></div></div></li></ul></div><h3 _ngcontent-ows-c4=\"\">Conclusion</h3><div _ngcontent-ows-c4=\"\" class=\"text mb-30\">Consectetur adipisicing elit sed do ei usmod tempor incididunt.enim minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo conse in quat duis aute irure dolor. excepteur sint occaecat cupidatat non proident, sunt culpa qui officia deserunt mollit anim id est laborum.</div></div></div></div></div></section>', NULL, NULL, NULL, '2020-01-09 00:39:48', '2020-01-09 00:39:48'),
(33, '12', NULL, 'Website Development', NULL, NULL, NULL, NULL, '2020-01-09 00:50:53', '2020-01-09 00:50:53'),
(34, '13', NULL, 'Apps Development', NULL, NULL, NULL, NULL, '2020-01-09 00:51:16', '2020-01-09 00:51:16'),
(35, '14', NULL, 'SEO', NULL, NULL, NULL, NULL, '2020-01-09 00:51:32', '2020-01-09 00:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `add_sub_menuses`
--

DROP TABLE IF EXISTS `add_sub_menuses`;
CREATE TABLE IF NOT EXISTS `add_sub_menuses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subMenuName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageNameId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `add_sub_menuses`
--

INSERT INTO `add_sub_menuses` (`id`, `subMenuName`, `pageNameId`, `created_at`, `updated_at`) VALUES
(2, 'Website Development', '9', '2020-01-07 04:58:24', '2020-01-07 04:58:24'),
(3, 'Apps Development', '9', '2020-01-07 04:58:34', '2020-01-07 04:58:34'),
(4, 'Software Development', '9', '2020-01-07 04:58:44', '2020-01-07 04:58:44'),
(5, 'ERP', '9', '2020-01-07 04:58:54', '2020-01-07 04:58:54'),
(6, 'CRM', '9', '2020-01-07 04:59:00', '2020-01-07 04:59:00'),
(8, 'SEO', '9', '2020-01-07 04:59:35', '2020-01-07 04:59:35');

-- --------------------------------------------------------

--
-- Table structure for table `callback`
--

DROP TABLE IF EXISTS `callback`;
CREATE TABLE IF NOT EXISTS `callback` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `callback`
--

INSERT INTO `callback` (`id`, `name`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Arun Pradhan', 'er.aru2010@gmail.com', 'test', 'Hello Sir,', '2020-01-03 20:42:36', '2020-01-03 20:42:36'),
(2, 'Arun Pradhan', 'er.aru2010@gmail.com', 'fsdf', 'sdfds', '2020-01-03 20:45:38', '2020-01-03 20:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

DROP TABLE IF EXISTS `contactus`;
CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(75, 'Arun Pradhan', 'er.aru2010@gmail.com', '6263898440', 'dfdsf', 'dsfsd', '2019-12-31 21:13:33', '2019-12-31 21:13:33'),
(76, 'Arun Pradhan', 'er.aru2010@gmail.com', '6263898440', 'tesy', 'hello', '2019-12-31 21:15:52', '2019-12-31 21:15:52'),
(77, 'Arun Pradhan', 'er.aru2010@gmail.com', '6263898440', 'jhgdsf', 'jkdsbfs', '2019-12-31 21:18:50', '2019-12-31 21:18:50'),
(78, 'Arun Pradhan', 'er.aru2010@gmail.com', '6263898440', 'fgdf', 'fdgfgf', '2019-12-31 21:19:49', '2019-12-31 21:19:49'),
(79, 'ankit', 'ankit.das@jithvar.com', '6263898440', 'test', 'Hello', '2020-01-02 06:13:24', '2020-01-02 06:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `getaquote`
--

DROP TABLE IF EXISTS `getaquote`;
CREATE TABLE IF NOT EXISTS `getaquote` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `getaquote`
--

INSERT INTO `getaquote` (`id`, `name`, `email`, `country`, `phone`, `service`, `budget`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Arun Pradhan', 'er.aru2010@gmail.com', 'India', '6263898440', 'E-Commerce Development', '1.5 Lac', 'Hello Sir, i want to get a quote for e-commerce website & apps on android.', '2020-01-04 00:10:24', '2020-01-04 00:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `master`
--

DROP TABLE IF EXISTS `master`;
CREATE TABLE IF NOT EXISTS `master` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parant_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master`
--

INSERT INTO `master` (`id`, `parant_id`, `name`, `abbr`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, NULL, 'Residential Property', 'property-type', '1', '2020-02-04 04:16:44', '2020-02-04 04:16:44', NULL, '1'),
(2, NULL, 'Commercial Property', 'property-type', '1', '2020-02-04 04:16:56', '2020-02-04 04:16:56', NULL, '1'),
(3, NULL, 'PG/Hostels', 'property-type', '1', '2020-02-04 04:17:08', '2020-02-04 04:17:08', NULL, '1'),
(4, 1, 'Apartment Type', 'subproperty-type', '1', '2020-02-04 04:17:31', '2020-02-04 04:17:31', NULL, '1'),
(5, 1, 'House Villa', 'subproperty-type', '1', '2020-02-04 04:19:53', '2020-02-04 04:19:53', NULL, '1'),
(6, 1, 'Other', 'subproperty-type', '1', '2020-02-04 04:20:06', '2020-02-04 04:20:06', NULL, '1'),
(7, 2, 'Office', 'subproperty-type', '1', '2020-02-04 04:20:17', '2020-02-04 04:20:17', NULL, '1'),
(8, 2, 'Retails', 'subproperty-type', '1', '2020-02-04 04:20:29', '2020-02-04 04:20:29', NULL, '1'),
(9, 2, 'Land', 'subproperty-type', '1', '2020-02-04 04:20:38', '2020-02-04 04:20:38', NULL, '1'),
(10, 2, 'Other', 'subproperty-type', '1', '2020-02-04 04:20:48', '2020-02-04 04:20:48', NULL, '1'),
(11, 4, 'Residential Apartment', 'resi-commer-type', '1', '2020-02-04 04:35:35', '2020-02-04 04:35:35', NULL, '1'),
(12, 4, 'Independent/Builder Floor', 'resi-commer-type', '1', '2020-02-04 04:36:00', '2020-02-04 04:36:00', NULL, '1'),
(13, 4, 'Service Apartment', 'resi-commer-type', '1', '2020-02-04 04:36:17', '2020-02-04 04:36:17', NULL, '1'),
(14, 5, 'Independent House/Villa', 'resi-commer-type', '1', '2020-02-04 04:36:44', '2020-02-04 04:36:44', NULL, '1'),
(15, 5, 'Farm House', 'resi-commer-type', '1', '2020-02-04 04:36:54', '2020-02-04 04:36:54', NULL, '1'),
(16, 7, 'Commericial Office Space', 'resi-commer-type', '1', '2020-02-04 04:37:19', '2020-02-04 04:37:19', NULL, '1'),
(17, 7, 'Office in IT Park', 'resi-commer-type', '1', '2020-02-04 04:37:42', '2020-02-04 04:37:42', NULL, '1'),
(18, 7, 'Office in Business Park', 'resi-commer-type', '1', '2020-02-04 04:37:53', '2020-02-04 04:37:53', NULL, '1'),
(19, 7, 'Business center', 'resi-commer-type', '1', '2020-02-04 04:38:05', '2020-02-04 04:38:05', NULL, '1'),
(20, 7, 'Time Share', 'resi-commer-type', '1', '2020-02-04 04:38:14', '2020-02-04 04:38:14', NULL, '1'),
(21, 8, 'Commercial Shops', 'resi-commer-type', '1', '2020-02-04 04:38:38', '2020-02-04 04:38:38', NULL, '1'),
(22, 8, 'Commercial Showrooms', 'resi-commer-type', '1', '2020-02-04 04:38:47', '2020-02-04 04:38:47', NULL, '1'),
(23, 8, 'Space in Retail Mall', 'resi-commer-type', '1', '2020-02-04 04:38:59', '2020-02-04 04:38:59', NULL, '1'),
(24, 9, 'Commercial Land/Inst. Land', 'resi-commer-type', '1', '2020-02-04 04:39:16', '2020-02-04 04:39:16', NULL, '1'),
(25, 9, 'Agricultural/Farm Land', 'resi-commer-type', '1', '2020-02-04 04:39:28', '2020-02-04 04:39:28', NULL, '1'),
(26, 9, 'Industrial Lands/Plots', 'resi-commer-type', '1', '2020-02-04 04:39:39', '2020-02-04 04:39:39', NULL, '1'),
(27, 1, '1 BHK', 'room-type', '1', '2020-02-04 04:50:59', '2020-02-04 04:50:59', NULL, '1'),
(28, 1, '2 BHK', 'room-type', '1', '2020-02-04 04:51:11', '2020-02-04 04:51:11', NULL, '1'),
(29, 1, '3 BHK', 'room-type', '1', '2020-02-04 04:51:19', '2020-02-04 04:51:19', NULL, '1'),
(30, 3, 'Single', 'room-type', '1', '2020-02-04 04:51:37', '2020-02-04 04:51:37', NULL, '1'),
(31, 3, '2 Seater', 'room-type', '1', '2020-02-04 04:51:48', '2020-02-04 04:51:48', NULL, '1'),
(32, NULL, 'Demo', 'resi-commer-type', '1', '2020-02-04 05:01:37', '2020-02-04 05:01:37', NULL, '1'),
(33, NULL, 'Wooden', 'kitchen-type', '1', '2020-02-04 05:15:22', '2020-02-04 05:15:22', NULL, '1'),
(34, NULL, 'Modular', 'kitchen-type', '1', '2020-02-04 05:16:16', '2020-02-04 05:16:16', NULL, '1'),
(35, NULL, 'Western', 'bathroom-type', '1', '2020-02-04 05:33:42', '2020-02-04 05:33:42', NULL, '1'),
(36, 1, 'Pooja Room', 'other-rooms-type', '1', '2020-02-04 05:41:40', '2020-02-04 05:41:40', NULL, '1'),
(37, 1, 'Store Room', 'other-rooms-type', '1', '2020-02-04 05:41:52', '2020-02-04 05:41:52', NULL, '1'),
(38, NULL, 'Vitrified', 'flooring-type', '1', '2020-02-04 05:46:11', '2020-02-04 05:46:11', NULL, '1'),
(39, NULL, 'East', 'facing-type', '1', '2020-02-04 05:48:09', '2020-02-04 05:48:09', NULL, '1'),
(40, 1, 'Fully Furnished', 'furnished-type', '1', '2020-02-04 05:51:08', '2020-02-04 05:51:08', NULL, '1'),
(41, 40, 'Bed', 'furniture-type', '1', '2020-02-04 05:53:16', '2020-02-04 05:53:16', NULL, '1'),
(42, 40, 'Wordorbe', 'furniture-type', '1', '2020-02-04 05:53:30', '2020-02-04 05:53:30', NULL, '1'),
(46, NULL, 'Amenities', 'additional-feature-type', '1', '2020-02-05 05:10:41', '2020-02-05 05:10:41', NULL, '1'),
(47, NULL, 'Property Feature', 'additional-feature-type', '1', '2020-02-05 05:11:18', '2020-02-05 05:11:18', NULL, '1'),
(48, NULL, 'Society Feature', 'additional-feature-type', '1', '2020-02-05 05:11:26', '2020-02-05 05:11:26', NULL, '1'),
(49, NULL, 'Some features about your property', 'additional-feature-type', '1', '2020-02-05 05:11:47', '2020-02-05 05:11:47', NULL, '1'),
(50, NULL, 'Property Highlights', 'additional-feature-type', '1', '2020-02-05 05:11:59', '2020-02-05 05:11:59', NULL, '1'),
(51, 46, 'Water Storage', 'additional-feature-sub-type', '1', '2020-02-05 05:14:13', '2020-02-05 05:14:13', NULL, '1'),
(52, 46, 'Visitor Parking', 'additional-feature-sub-type', '1', '2020-02-05 05:14:27', '2020-02-05 05:14:27', NULL, '1'),
(53, 46, 'Maintenance Staff', 'additional-feature-sub-type', '1', '2020-02-05 05:16:10', '2020-02-05 05:16:10', NULL, '1'),
(54, 46, 'Temple', 'additional-feature-sub-type', '1', '2020-02-05 05:16:46', '2020-02-05 05:16:46', NULL, '1'),
(55, 46, 'Park-1', 'additional-feature-sub-type', '1', '2020-02-05 05:16:56', '2020-02-05 05:16:56', NULL, '1'),
(56, 46, 'Waste Disposal', 'additional-feature-sub-type', '1', '2020-02-05 05:17:06', '2020-02-05 05:17:06', NULL, '1'),
(57, 47, 'Security / Fire Alarm', 'additional-feature-sub-type', '1', '2020-02-05 05:17:21', '2020-02-05 05:17:21', NULL, '1'),
(58, 47, 'Centrally Air Conditioned', 'additional-feature-sub-type', '1', '2020-02-05 05:17:33', '2020-02-05 05:17:33', NULL, '1'),
(59, 47, 'Private Garden / Terrace', 'additional-feature-sub-type', '1', '2020-02-05 05:17:46', '2020-02-05 05:17:46', NULL, '1'),
(60, 47, 'Intercom Facility', 'additional-feature-sub-type', '1', '2020-02-05 05:18:01', '2020-02-05 05:18:01', NULL, '1'),
(61, 47, 'Internet/wi-fi connectivity', 'additional-feature-sub-type', '1', '2020-02-05 05:18:10', '2020-02-05 05:18:10', NULL, '1'),
(62, 48, 'Swimming Pool', 'additional-feature-sub-type', '1', '2020-02-05 05:18:21', '2020-02-05 05:18:21', NULL, '1'),
(63, 48, 'Lift(s)', 'additional-feature-sub-type', '1', '2020-02-05 05:18:34', '2020-02-05 05:18:34', NULL, '1'),
(64, 48, 'Fitness Centre / GYM', 'additional-feature-sub-type', '1', '2020-02-05 05:18:43', '2020-02-05 05:18:43', NULL, '1'),
(65, 48, 'Club house / Community Center', 'additional-feature-sub-type', '1', '2020-02-05 05:18:52', '2020-02-05 05:18:52', NULL, '1'),
(66, 48, 'Security Personnel', 'additional-feature-sub-type', '1', '2020-02-05 05:19:00', '2020-02-05 05:19:00', NULL, '1'),
(67, 49, 'Swimming Pool', 'additional-feature-sub-type', '1', '2020-02-05 05:19:12', '2020-02-05 05:19:12', NULL, '1'),
(68, 49, 'Lift(s)', 'additional-feature-sub-type', '1', '2020-02-05 05:19:25', '2020-02-05 05:19:25', NULL, '1'),
(69, 49, 'Fitness Centre / GYM', 'additional-feature-sub-type', '1', '2020-02-05 05:19:38', '2020-02-05 05:19:38', NULL, '1'),
(70, 50, '24*7 Water', 'additional-feature-sub-type', '1', '2020-02-05 05:19:48', '2020-02-05 05:19:48', NULL, '1'),
(71, 50, 'Visitor Parking Available', 'additional-feature-sub-type', '1', '2020-02-05 05:19:58', '2020-02-05 05:19:58', NULL, '1'),
(72, 50, 'Close to School', 'additional-feature-sub-type', '1', '2020-02-05 05:20:10', '2020-02-05 05:20:10', NULL, '1'),
(73, 50, 'On-Call Maintenance Staff', 'additional-feature-sub-type', '1', '2020-02-05 05:20:19', '2020-02-05 05:20:19', NULL, '1'),
(74, 50, 'Close to Hospital', 'additional-feature-sub-type', '1', '2020-02-05 05:20:28', '2020-02-05 05:20:28', NULL, '1'),
(75, 50, 'Close to Railway Station', 'additional-feature-sub-type', '1', '2020-02-05 05:20:37', '2020-02-05 05:20:37', NULL, '1'),
(76, NULL, 'Uttar Pradesh', 'state', '1', '2020-02-05 05:29:23', '2020-02-05 05:29:23', NULL, '1'),
(77, 76, 'Lucknow', 'city', '1', '2020-02-05 05:41:22', '2020-02-05 05:41:22', NULL, '1'),
(78, 77, 'Gomti Nagar.', 'area', '1', '2020-02-05 05:42:45', '2020-02-05 05:42:45', NULL, '1'),
(79, 77, 'Faizabad Road', 'area', '1', '2020-02-05 05:42:58', '2020-02-05 05:42:58', NULL, '1'),
(80, 77, 'Vibhuti Khand', 'area', '1', '2020-02-05 05:43:11', '2020-02-05 05:43:11', NULL, '1'),
(81, 77, 'Aliganj', 'area', '1', '2020-02-05 05:43:21', '2020-02-05 05:43:21', NULL, '1'),
(82, NULL, 'sq.ft', 'unit', '1', '2020-02-05 05:46:13', '2020-02-05 05:46:13', NULL, '1'),
(83, NULL, 'Sq. Yards', 'unit', '1', '2020-02-05 05:47:44', '2020-02-05 05:47:44', NULL, '1'),
(84, NULL, 'Wooden', 'kitchen-type', '1', '2020-02-05 05:56:56', '2020-02-05 06:09:31', '2020-02-05 06:09:31', '1'),
(85, NULL, 'Basement', 'floor-type', '1', '2020-02-05 05:58:21', '2020-02-05 05:58:21', NULL, '1'),
(86, 1, 'Semi-Furnished', 'furnished-type', '1', '2020-02-07 02:28:42', '2020-02-07 02:28:42', NULL, '1'),
(87, 1, 'None', 'furnished-type', '1', '2020-02-07 02:29:14', '2020-02-07 02:29:14', NULL, '1'),
(88, 86, 'Fan', 'furniture-type', '1', '2020-02-07 02:36:20', '2020-02-07 02:36:20', NULL, '1'),
(89, 40, 'Fans', 'furniture-type', '1', '2020-02-07 03:30:42', '2020-02-07 03:30:42', NULL, '1'),
(90, 40, 'Cooler', 'furniture-type', '1', '2020-02-07 03:32:37', '2020-02-07 03:32:37', NULL, '1'),
(91, 40, 'Television', 'furniture-type', '1', '2020-02-07 03:32:49', '2020-02-07 03:32:49', NULL, '1'),
(92, NULL, '1', 'floor-type', '1', '2020-02-08 02:37:59', '2020-02-08 02:37:59', NULL, '1'),
(93, NULL, '1 BHK', 'floor-type', '1', '2020-02-08 02:38:09', '2020-02-08 02:57:01', '2020-02-08 02:57:01', '1'),
(94, NULL, '1 BHK', 'floor-type', '1', '2020-02-08 02:38:16', '2020-02-08 02:56:59', '2020-02-08 02:56:59', '1'),
(95, NULL, '1 BHK', 'floor-type', '1', '2020-02-08 02:39:33', '2020-02-08 02:56:56', '2020-02-08 02:56:56', '1'),
(96, NULL, '2 BHK', 'floor-type', '1', '2020-02-08 02:46:07', '2020-02-08 02:56:54', '2020-02-08 02:56:54', '1'),
(97, NULL, '1 BHK', 'floor-type', '1', '2020-02-08 02:53:50', '2020-02-08 02:56:51', '2020-02-08 02:56:51', '1'),
(98, NULL, '2', 'floor-type', '1', '2020-02-08 02:57:09', '2020-02-08 02:57:09', NULL, '1'),
(99, NULL, '3', 'floor-type', '1', '2020-02-08 02:57:17', '2020-02-08 02:57:17', NULL, '1'),
(100, NULL, '4', 'floor-type', '1', '2020-02-08 02:57:22', '2020-02-08 02:57:22', NULL, '1'),
(101, NULL, '5', 'floor-type', '1', '2020-02-08 02:57:27', '2020-02-08 02:57:27', NULL, '1'),
(102, NULL, '6', 'floor-type', '1', '2020-02-08 02:57:33', '2020-02-08 02:57:33', NULL, '1'),
(103, NULL, '1', 'floor-type', '1', '2020-02-08 03:07:06', '2020-02-08 03:16:37', '2020-02-08 03:16:37', '1'),
(104, NULL, 'an', 'floor-type', '1', '2020-02-08 03:08:26', '2020-02-08 03:16:34', '2020-02-08 03:16:34', '1');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_26_095410_create_tasks_table', 1),
(4, '2019_12_26_101320_create_contact_us_table', 2),
(5, '2019_12_26_204937_create_items_table', 3),
(6, '2019_12_27_083700_create_items_table', 4),
(8, '2020_01_04_022617_create_subscribe_table', 5),
(9, '2020_01_04_052820_create_getaquote_table', 6),
(10, '2020_01_04_073922_create_students_table', 7),
(12, '2020_01_06_055117_create_pages_table', 8),
(13, '2020_01_06_110636_create_add_pages_table', 9),
(14, '2020_01_07_074020_create_website_masters_table', 10),
(15, '2020_01_07_092128_create_add_sub_menuses_table', 11),
(16, '2020_01_07_103136_create_teams_table', 12),
(17, '2020_01_09_045142_create_sliders_table', 13),
(18, '2020_01_24_044229_role_permission', 14),
(19, '2020_01_25_104336_create_master_table', 14),
(20, '2020_01_25_115403_add_status_to_master_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subMenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pageLink` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageStatus` enum('inactive','active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `pageName`, `subMenu`, `pageLink`, `pageStatus`, `created_at`, `updated_at`) VALUES
(3, 'Aboutus', NULL, 'aboutus', 'active', '2020-01-06 03:14:49', '2020-01-29 06:36:02'),
(8, 'Company History', NULL, 'company-history', 'active', '2020-01-07 04:56:49', '2020-01-07 04:56:49'),
(9, 'Services', NULL, 'services', 'active', '2020-01-07 04:56:58', '2020-01-07 04:56:58'),
(10, 'Career', NULL, 'career', 'active', '2020-01-07 04:57:54', '2020-01-08 14:33:18'),
(11, 'Software Development', '9', 'software-development', 'active', '2020-01-08 03:46:37', '2020-01-08 03:58:43'),
(12, 'Website Development', '9', 'website-development', 'active', '2020-01-08 03:59:20', '2020-01-08 03:59:20'),
(13, 'App Development', '9', 'app-development', 'active', '2020-01-08 14:21:26', '2020-01-08 14:22:19'),
(14, 'SEO', '9', 'seo', 'active', '2020-01-09 00:46:31', '2020-01-09 00:49:55'),
(15, 'CRM', '9', 'crm', 'active', '2020-01-09 00:46:39', '2020-01-09 00:50:05'),
(16, 'ERP', '9', 'erp', 'active', '2020-01-09 00:46:51', '2020-01-09 00:46:51');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('arun.pradhan@jithvar.com', '$2y$10$xUOldPfu7MgfzmuriRlAYuXXKDfej5z494d8SbsUkoc/XYlYVqiDm', '2019-12-28 00:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `content`, `created_at`, `updated_at`) VALUES
(2, '1578547965.jpg', 'A satisfied customer is the best business strategy of all.', '2020-01-09 00:02:45', '2020-01-09 00:19:04'),
(3, '1578547976.jpg', 'A satisfied customer is the best business strategy of all.', '2020-01-09 00:02:56', '2020-01-09 00:02:56'),
(4, '1578547991.jpg', 'A satisfied customer is the best business strategy of all.', '2020-01-09 00:03:11', '2020-01-09 00:03:11'),
(5, '1578548006.jpg', 'A satisfied customer is the best business strategy of all.', '2020-01-09 00:03:26', '2020-01-09 00:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jobpost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`id`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'er.aru2010@gmail.com', 1, '2020-01-03 21:17:38', '2020-01-03 21:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_amenities`
--

DROP TABLE IF EXISTS `tbl_amenities`;
CREATE TABLE IF NOT EXISTS `tbl_amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE IF NOT EXISTS `tbl_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `controller_name` varchar(50) NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`id`, `group_id`, `name`, `controller_name`, `action_name`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(3, 3, 'show', 'UserController', 'show', 1, 1, '2020-01-23 11:01:45', NULL, NULL),
(2, 3, 'view', 'RoleController', 'index', 1, 1, '2020-01-23 10:35:39', NULL, '2020-01-23 03:47:14'),
(4, 4, 'index', 'RoleController', 'index', 1, 1, '2020-01-23 11:02:19', NULL, '2020-01-23 05:14:16'),
(5, 5, 'index', 'PermissionController', 'index', 1, 1, '2020-01-23 11:02:50', NULL, '2020-01-23 05:15:50'),
(6, 6, 'index', 'PermissionGroupController', 'index', 1, 1, '2020-01-23 11:02:40', NULL, '2020-01-23 05:17:21'),
(7, 7, 'index', 'RolePermissionController', 'index', 1, 1, '2020-01-23 11:04:18', NULL, '2020-01-23 05:18:14'),
(8, 3, 'index', 'UserController', 'index', 1, 1, '2020-01-23 11:01:51', NULL, '2020-01-23 05:19:06'),
(12, 8, 'add-pages', 'PagesController', 'add-pages', 1, 1, '2020-01-23 06:04:17', NULL, '2020-01-23 06:04:17'),
(10, 8, 'page list', 'PagesController', 'page-list', 1, 1, '2020-01-23 11:28:24', NULL, '2020-01-23 05:58:24'),
(11, 8, 'menu-list', 'PagesController', 'index', 1, 1, '2020-01-23 05:59:30', NULL, '2020-01-23 05:59:30'),
(9, 4, 'Create', 'RoleController', 'index', 1, 1, '2020-01-24 05:30:33', NULL, '2020-01-24 00:00:33'),
(13, 8, 'teams', 'PagesController', 'teams', 1, 1, '2020-01-23 06:05:01', NULL, '2020-01-23 06:05:01'),
(14, 8, 'sliders', 'PagesController', 'sliders', 1, 1, '2020-01-24 06:04:08', NULL, '2020-01-24 00:34:08'),
(15, 8, 'website-master', 'PagesController', 'website-master', 1, 1, '2020-01-23 06:05:55', NULL, '2020-01-23 06:05:55'),
(17, 3, 'Test', 'PageController', 'new', 1, 1, '2020-01-24 02:14:22', NULL, '2020-01-24 02:14:22'),
(18, 9, 'Master-index', 'MasterController', 'index', 1, 1, '2020-01-29 00:58:38', NULL, '2020-01-29 00:58:38'),
(19, 9, 'Master-create', 'MasterController', 'create', 1, 1, '2020-01-29 00:59:14', NULL, '2020-01-29 00:59:14'),
(20, 9, 'Master-edit', 'MasterController', 'edit', 1, 1, '2020-01-29 00:59:44', NULL, '2020-01-29 00:59:44'),
(21, 9, 'Master-delete', 'MasterController', 'delete', 1, 1, '2020-01-29 01:00:06', NULL, '2020-01-29 01:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_group`
--

DROP TABLE IF EXISTS `tbl_permission_group`;
CREATE TABLE IF NOT EXISTS `tbl_permission_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(100) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_permission_group`
--

INSERT INTO `tbl_permission_group` (`id`, `permission_name`, `status`, `updated_at`, `created_at`) VALUES
(3, 'User Managment', 1, '2020-01-23 03:44:41', '2020-01-23 03:44:41'),
(4, 'Role Management', 1, '2020-01-23 05:30:03', '2020-01-23 05:30:03'),
(5, 'Permission Management', 1, '2020-01-23 05:33:25', '2020-01-23 05:33:25'),
(6, 'Permission Group Management', 1, '2020-01-23 05:33:42', '2020-01-23 05:33:42'),
(7, 'Role Permission Management', 1, '2020-01-23 05:33:56', '2020-01-23 05:33:56'),
(8, 'CMS Management', 1, '2020-01-23 05:35:10', '2020-01-23 05:35:10'),
(9, 'Master Management', 1, '2020-01-29 00:57:06', '2020-01-29 00:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

DROP TABLE IF EXISTS `tbl_property`;
CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person` varchar(20) DEFAULT NULL,
  `property_for` varchar(20) DEFAULT NULL,
  `property_type` int(11) NOT NULL DEFAULT '0',
  `property_sub_type` int(11) NOT NULL DEFAULT '0',
  `property_sub_sub_type` int(11) NOT NULL DEFAULT '0',
  `willing_to` varchar(20) DEFAULT NULL,
  `agreement_type` varchar(20) DEFAULT NULL,
  `plot_area` decimal(10,2) DEFAULT NULL,
  `plot_unit` varchar(20) DEFAULT NULL,
  `buildup_area` decimal(10,2) DEFAULT NULL,
  `buildup_unit` varchar(20) DEFAULT NULL,
  `carpet_area` decimal(10,2) DEFAULT NULL,
  `carpet_unit` varchar(20) DEFAULT NULL,
  `bedroom_type` int(11) DEFAULT '0',
  `bathrooms` int(11) NOT NULL DEFAULT '0',
  `balconies` int(11) NOT NULL DEFAULT '0',
  `kitchen_type` int(11) NOT NULL DEFAULT '0',
  `bathroom_type` int(11) NOT NULL DEFAULT '0',
  `facing_type` int(11) NOT NULL DEFAULT '0',
  `flooring_type` int(11) NOT NULL DEFAULT '0',
  `power_backup` varchar(100) DEFAULT NULL,
  `total_floor` int(11) NOT NULL DEFAULT '0',
  `property_floor` int(11) NOT NULL DEFAULT '0',
  `furnished_type` int(11) NOT NULL DEFAULT '0',
  `parking_place` varchar(10) DEFAULT NULL,
  `age_of_property` varchar(100) DEFAULT NULL,
  `available` date DEFAULT NULL,
  `description` text,
  `state` int(11) NOT NULL DEFAULT '0',
  `city` int(11) NOT NULL DEFAULT '0',
  `area` int(11) NOT NULL DEFAULT '0',
  `address` text,
  `expected_rent` int(11) NOT NULL DEFAULT '0',
  `electricity_water_excluded` varchar(5) NOT NULL DEFAULT '0',
  `security_deposite` varchar(100) DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `negotiable` varchar(5) NOT NULL DEFAULT '0',
  `rent_aggrement` date DEFAULT NULL,
  `months_of_notice` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_on` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `person`, `property_for`, `property_type`, `property_sub_type`, `property_sub_sub_type`, `willing_to`, `agreement_type`, `plot_area`, `plot_unit`, `buildup_area`, `buildup_unit`, `carpet_area`, `carpet_unit`, `bedroom_type`, `bathrooms`, `balconies`, `kitchen_type`, `bathroom_type`, `facing_type`, `flooring_type`, `power_backup`, `total_floor`, `property_floor`, `furnished_type`, `parking_place`, `age_of_property`, `available`, `description`, `state`, `city`, `area`, `address`, `expected_rent`, `electricity_water_excluded`, `security_deposite`, `price`, `negotiable`, `rent_aggrement`, `months_of_notice`, `created_by`, `created_on`, `status`) VALUES
(1, 'owner', 'sell', 2, 4, 12, 'single_women', '6', '5.00', '82', '6.00', '82', NULL, NULL, 27, 1, 1, 33, 35, 39, 38, 'P', 85, 85, 40, 'yes', '26', '2020-02-05', NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(2, 'Select', 'Select', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(3, 'Select', 'Select', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(4, 'Select', 'Select', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(5, 'owner', 'rent', 1, 4, 11, 'family', 'any', '25.00', '82', '25.00', '82', '63.00', '82', 29, 2, 2, 33, 35, 39, 38, 'N', 85, 85, 40, 'yes', '26', '2020-02-14', 'test', 76, 77, 78, 'test', 2000, 'on', 'Fixed', 20000, '0', '2020-02-07', 23, 1, NULL, 1),
(6, 'owner', 'rent', 1, 4, 11, 'family', 'any', '25.00', '82', '25.00', '82', '63.00', '82', 29, 2, 2, 33, 35, 39, 38, 'N', 85, 85, 40, 'yes', '26', '2020-02-14', NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(7, 'dealer', 'rent', 1, 5, 14, 'family', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(8, NULL, NULL, 0, 0, 0, NULL, NULL, '33.00', '83', '3.00', '83', '3.00', '83', 28, 1, 1, 34, 35, 39, 38, 'P', 85, 85, 40, 'yes', '33', '2020-02-07', NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(9, 'dealer', 'rent', 1, 5, 14, 'single_men', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(10, 'owner', 'rent', 1, 4, 11, 'family', 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(11, 'owner', 'rent', 1, 5, 14, 'family', 'company_lease', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(12, 'owner', 'rent', 1, 5, 15, 'single_women', 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(13, 'owner', 'rent', 1, 4, 11, 'family', NULL, '33.00', '82', '44.00', '83', '4.00', '82', 27, 2, 1, 34, 35, 39, 38, 'N', 85, 85, 40, 'yes', '26', '2020-02-12', 'today 8 feb', 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(14, 'owner', 'rent', 1, 5, 14, 'family', 'any', '56.00', '82', '7.00', '82', '7.00', '82', 27, 2, 1, 34, 35, 39, 38, 'P', 85, 85, 40, 'yes', '4', '2020-02-22', NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(15, 'owner', 'sell', 2, 7, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(16, 'owner', 'sell', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(17, 'Select', 'rent', 1, 4, 11, NULL, 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(18, 'owner', 'rent', 1, 4, 11, 'family', 'company_lease', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(19, 'dealer', 'rent', 1, 4, 11, 'family', 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(20, 'dealer', 'sell', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(21, 'owner', 'sell', 1, 4, 11, NULL, NULL, '23.00', '82', '4.00', '82', '4.00', '83', 27, 2, 1, 33, 35, 39, 38, 'N', 85, 85, 40, 'yes', '34', '2020-01-30', NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(22, 'owner', 'sell', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(23, 'owner', 'sell', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(24, 'owner', 'sell', 2, 8, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(25, 'owner', 'sell', 1, 4, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(26, 'owner', 'rent', 2, 7, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(27, 'owner', 'rent', 1, 5, 14, 'family', 'company_lease', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(28, 'owner', 'rent', 1, 5, 15, 'family', 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(29, 'owner', 'sell', 1, 5, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(30, 'owner', 'sell', 2, 8, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(31, 'owner', 'sell', 1, 5, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(32, 'owner', 'sell', 1, 5, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(33, 'dealer', 'sell', 2, 7, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(34, 'dealer', 'rent', 1, 5, 15, 'family', 'company_lease', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(35, 'dealer', 'rent', 1, 5, 15, 'single_men', 'any', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(36, 'dealer', 'sell', 1, 4, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(37, 'owner', 'sell', 1, 4, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(38, 'owner', 'rent', 2, 7, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(39, 'owner', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(40, 'owner', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(41, 'dealer', 'sell', 1, 5, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(42, 'dealer', 'rent', 2, 8, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(43, 'dealer', 'sell', 2, 8, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(44, 'owner', 'sell', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(45, 'dealer', 'rent', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(46, 'owner', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(47, 'owner', 'rent', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(48, 'owner', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(49, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(50, 'dealer', 'rent', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(51, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(52, 'owner', 'sell', 1, 5, 15, NULL, NULL, '2.00', '83', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(53, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(54, 'owner', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(55, 'dealer', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(56, 'dealer', 'sell', 2, 8, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(57, 'owner', 'sell', 2, 7, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(58, 'owner', 'sell', 1, 5, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(59, 'owner', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(60, 'dealer', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(61, 'dealer', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(62, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(63, 'dealer', 'rent', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(64, 'owner', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(65, 'owner', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(66, 'dealer', 'sell', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(67, 'owner', 'sell', 2, 8, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(68, 'owner', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(69, 'dealer', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(70, 'dealer', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(71, 'owner', 'rent', 2, 9, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(72, 'owner', 'sell', 2, 9, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(73, 'owner', 'rent', 2, 9, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(74, 'dealer', 'rent', 2, 9, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(75, 'owner', 'sell', 1, 4, 11, NULL, NULL, '34.00', '82', '4.00', '82', '4.00', '83', 28, 2, 2, 0, 0, 0, 0, '0', 0, 0, 0, '0', NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1),
(76, 'Select', 'Select', 1, 4, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, '0', NULL, 0, '0', NULL, 0, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_additional_features`
--

DROP TABLE IF EXISTS `tbl_property_additional_features`;
CREATE TABLE IF NOT EXISTS `tbl_property_additional_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `additional_sub_feature_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_property_additional_features`
--

INSERT INTO `tbl_property_additional_features` (`id`, `additional_sub_feature_id`, `property_id`) VALUES
(12, 54, 5),
(11, 51, 5),
(10, 74, 33),
(9, 73, 33),
(8, 71, 33),
(7, 70, 33),
(13, 57, 5),
(14, 60, 5),
(15, 51, 13),
(16, 54, 13),
(17, 57, 13),
(18, 60, 13),
(19, 63, 13),
(20, 66, 13),
(21, 70, 13),
(22, 73, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_furniture`
--

DROP TABLE IF EXISTS `tbl_property_furniture`;
CREATE TABLE IF NOT EXISTS `tbl_property_furniture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `furniture_type` int(11) NOT NULL,
  `no_of_furniture` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_property_furniture`
--

INSERT INTO `tbl_property_furniture` (`id`, `furniture_type`, `no_of_furniture`, `property_id`) VALUES
(1, 41, 4, 29),
(2, 42, 5, 29),
(3, 89, 6, 29),
(4, 90, 2, 29),
(5, 91, 4, 29),
(15, 91, 6, 32),
(14, 90, 6, 32),
(13, 89, 5, 32),
(12, 42, 4, 32),
(11, 41, 10, 32),
(16, 41, 4, 33),
(17, 42, 6, 33),
(18, 89, 7, 33),
(19, 90, 7, 33),
(20, 91, 7, 33),
(21, 41, 4, 34),
(22, 42, 1, 34),
(26, 42, 9, 35),
(25, 41, 7, 35),
(27, 89, 9, 35),
(28, 41, 3, 36),
(29, 42, 4, 36),
(30, 89, 4, 36),
(31, 90, 5, 36),
(32, 91, 53, 36),
(33, 41, 3, 37),
(34, 42, 3, 37),
(35, 89, 4, 37),
(36, 90, 5, 37),
(37, 91, 5, 37),
(38, 41, 3, 38),
(39, 42, 4, 38),
(40, 89, 5, 38),
(41, 90, 6, 38),
(42, 91, 7, 38),
(43, 41, 4, 39),
(44, 42, 4, 39),
(45, 41, 5, 40),
(46, 42, 5, 40),
(47, 41, 4, 41),
(48, 42, 4, 41),
(49, 41, 5, 42),
(50, 42, 1, 42),
(51, 41, 2, 43),
(52, 42, 4, 43),
(53, 41, 4, 44),
(54, 42, 4, 44),
(55, 89, 4, 44),
(56, 90, 4, 44),
(57, 91, 4, 44),
(58, 41, 4, 45),
(59, 42, 4, 45),
(60, 89, 4, 45),
(61, 90, 4, 45),
(62, 41, 4, 46),
(63, 42, 4, 46),
(64, 89, 4, 46),
(65, 90, 4, 46),
(66, 91, 4, 46),
(71, 41, 6, 1),
(72, 42, 6, 1),
(73, 89, 6, 1),
(74, 41, 22, 5),
(75, 42, 2, 5),
(76, 89, 2, 5),
(77, 90, 2, 5),
(78, 91, 2, 5),
(79, 41, 22, 6),
(80, 42, 2, 6),
(81, 89, 2, 6),
(82, 90, 2, 6),
(83, 91, 2, 6),
(84, 41, 6, 12),
(85, 42, 8, 12),
(86, 89, 1, 12),
(87, 90, 76, 12),
(88, 91, 5, 12),
(89, 41, 4, 13),
(90, 42, 4, 13),
(91, 89, 4, 13),
(92, 90, 4, 13),
(93, 91, 5, 13),
(94, 41, 6, 14),
(95, 42, 6, 14),
(96, 89, 8, 14),
(97, 41, 5, 19),
(98, 42, 5, 19),
(99, 89, 5, 19),
(100, 90, 1, 19),
(101, 91, 3, 19),
(102, 41, 4, 21),
(103, 42, 4, 21),
(104, 88, 9, 25);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_image`
--

DROP TABLE IF EXISTS `tbl_property_image`;
CREATE TABLE IF NOT EXISTS `tbl_property_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_property_image`
--

INSERT INTO `tbl_property_image` (`id`, `image`, `property_id`) VALUES
(1, '1581148354.Tulips.jpg', 74),
(2, '1581148435.Tulips.jpg', 75);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_parking`
--

DROP TABLE IF EXISTS `tbl_property_parking`;
CREATE TABLE IF NOT EXISTS `tbl_property_parking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_type` varchar(20) NOT NULL,
  `no_of_parking` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_property_parking`
--

INSERT INTO `tbl_property_parking` (`id`, `parking_type`, `no_of_parking`, `property_id`) VALUES
(55, 'open_parking', 0, 47),
(3, 'open_parking', 6, 1),
(5, 'open_parking', 2, 5),
(7, 'open_parking', 2, 6),
(9, 'open_parking', 0, 12),
(11, 'open_parking', 5, 13),
(13, 'open_parking', 4, 14),
(15, 'open_parking', 5, 15),
(17, 'open_parking', 0, 18),
(19, 'open_parking', 5, 19),
(21, 'open_parking', 4, 21),
(23, 'open_parking', 0, 22),
(25, 'open_parking', 0, 23),
(27, 'open_parking', 0, 24),
(29, 'open_parking', 0, 25),
(31, 'open_parking', 0, 26),
(33, 'open_parking', 0, 27),
(35, 'open_parking', 0, 28),
(37, 'open_parking', 0, 29),
(39, 'open_parking', 0, 30),
(41, 'open_parking', 0, 34),
(43, 'open_parking', 0, 35),
(45, 'open_parking', 0, 36),
(47, 'open_parking', 0, 40),
(49, 'open_parking', 0, 41),
(51, 'open_parking', 0, 42),
(53, 'open_parking', 0, 43),
(57, 'open_parking', 0, 48),
(59, 'open_parking', 0, 50),
(61, 'open_parking', 0, 52),
(63, 'open_parking', 0, 54),
(65, 'open_parking', 0, 55),
(67, 'open_parking', 0, 56),
(69, 'open_parking', 0, 57),
(71, 'open_parking', 0, 58),
(73, 'open_parking', 0, 59),
(75, 'open_parking', 0, 60),
(77, 'open_parking', 0, 61),
(79, 'open_parking', 0, 63),
(81, 'open_parking', 0, 64),
(83, 'open_parking', 0, 65),
(85, 'open_parking', 0, 66),
(87, 'open_parking', 0, 67),
(89, 'open_parking', 0, 68),
(91, 'open_parking', 0, 69),
(93, 'open_parking', 0, 70),
(95, 'open_parking', 0, 71),
(97, 'open_parking', 0, 72),
(99, 'open_parking', 0, 73),
(101, 'open_parking', 0, 74),
(103, 'open_parking', 0, 75);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE IF NOT EXISTS `tbl_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` smallint(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Admin', 1, '2020-01-24 07:05:46', '2020-01-23 18:30:00'),
(2, 'User', 1, '2020-01-23 07:34:11', '2020-01-23 07:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_permission`
--

DROP TABLE IF EXISTS `tbl_role_permission`;
CREATE TABLE IF NOT EXISTS `tbl_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_role_permission`
--

INSERT INTO `tbl_role_permission` (`id`, `permission_id`, `role_id`, `status`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(121, 19, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(120, 18, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(119, 15, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(118, 14, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(117, 13, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(116, 11, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(115, 10, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(114, 12, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(113, 7, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(112, 6, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(111, 5, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(110, 4, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(109, 8, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(73, 4, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(72, 2, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(71, 3, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(74, 5, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(75, 6, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(76, 7, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(77, 12, 2, 1, 1, '2020-01-23 23:59:36', '2020-01-23 23:59:36', NULL),
(108, 2, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(107, 3, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(122, 20, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL),
(123, 21, 1, 1, 1, '2020-01-29 01:36:07', '2020-01-29 01:36:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profilePhoto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gplus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `position`, `profilePhoto`, `facebook`, `twitter`, `linkedin`, `gplus`, `created_at`, `updated_at`) VALUES
(1, 'Denial Mustak', 'Advisor', '1578398655.jpg', NULL, NULL, NULL, NULL, '2020-01-07 05:37:10', '2020-01-07 06:34:15'),
(2, 'Jone Hasu', 'Member', '1578395279.jpg', NULL, NULL, NULL, NULL, '2020-01-07 05:37:59', '2020-01-07 05:37:59'),
(4, 'Kida Alom', 'Member', '1578398722.jpg', NULL, NULL, NULL, NULL, '2020-01-07 06:35:22', '2020-01-07 06:35:22'),
(6, 'Denial Naim', 'Member', '1578398759.jpg', NULL, NULL, NULL, NULL, '2020-01-07 06:35:59', '2020-01-07 06:35:59'),
(8, 'Robert Khaled', 'Advisor', '1578513395.jpg', NULL, NULL, NULL, NULL, '2020-01-08 14:26:35', '2020-01-08 14:26:35'),
(9, 'Albert Korim', 'Member', '1578513743.jpg', NULL, NULL, NULL, NULL, '2020-01-08 14:27:48', '2020-01-08 14:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'arun', 'admin@gmail.com', NULL, '$2y$10$6qzzwEMZ/RKml8LuLXl1ouPeBH5KXwd7kH/9Y91gEn1pkGcMxHDra', '', NULL, '2019-12-26 04:27:09', '2020-01-29 04:04:55'),
(5, 2, 'ankita das', 'ankita10204@mailinator.com', NULL, '$2y$10$gbO6f6erW.RmBjJMttGWM.xFj.RtGbI2sMKumF2o2tKg5eawKThBW', 'active', 'xX16CIbwa76rFnH4RYjyDZAVsfEspCcxYZ0bNX3ZoPrIP4tAC6F1BmNZ3N73', '2020-01-23 07:35:27', '2020-01-27 07:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `website_masters`
--

DROP TABLE IF EXISTS `website_masters`;
CREATE TABLE IF NOT EXISTS `website_masters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phoneFirst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondaryEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noreplyEmail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gplus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employeeLogin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientLogin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `website_masters`
--

INSERT INTO `website_masters` (`id`, `phoneFirst`, `phoneSecond`, `primaryEmail`, `secondaryEmail`, `noreplyEmail`, `facebook`, `linkedin`, `twitter`, `gplus`, `address`, `employeeLogin`, `clientLogin`, `created_at`, `updated_at`) VALUES
(1, '790-5590-238', NULL, 'hello@jithvar.com', NULL, 'noreply@jithvar.com', 'https://www.facebook.com/jithvar/', 'https://www.linkedin.com/company/jithvar/', NULL, NULL, 'C-219, Sector C Rd, Sector C, Shahpura, Bhopal, Madhya Pradesh 462039', 'https://office.jithvar.com/', 'https://project.jithvar.com/', '2020-01-06 18:30:00', '2020-01-07 03:47:34');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
