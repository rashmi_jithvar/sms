@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Fees Master List</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Fees Master List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
              <div class="pull-left"> <h5>Fees Master List</h5></div>
               <div class="pull-right"> 
                <a class="btn btn-primary m-t-5"  href="{{ url('add-fees') }}">Add Fees</a>
              </div>
            </div>
            <div class="card-body table-border-style">
              
                <div class="table-responsive">
                  
                   <table class="table">
                     <thead>
                       <tr>
                         <th>#</th>
                        
                         <th>Class</th>
                         <th>Category</th>
                         <th>Total Fees</th>                    
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        @php $i = 0
                        @endphp
                        @if(!empty($properties))
                        @foreach($properties as $property)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          
                          <td>{{$property->hasClass ? $property->hasClass->name:''}}</td>
                          <td>{{$property->hasCategory ? $property->hasCategory->name:''}}</td>
                          <td>{{$property ? $property->total_fees:''}}</td>
                         
                          <td>
                          @if($property->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                              <a href="{{url('fees-type/edit',$property->id)}}" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <a href="{{url('fees-type/show',$property->id)}}" title="show"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a href="{{url('fees-type/delete',$property->id)}}" title="delete"><i class="fa fa-trash" aria-hidden="true" Onclick="return ConfirmDelete();"></i></a>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection