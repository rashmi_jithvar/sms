@extends('layouts.datatable')

@section('content')
<?php
use App\Models\Master;
 ?>
<style>
  .card {
    padding:20px;
  }

</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('fees-master') }}">Fee Master List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Fee Detail</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-md-12">
    <div class="profile-content">
        <div class="row">
            <div class="card-body">
                <div class="card-topline-aqua">
                    <header></header>
                </div>
                <div class="white-box">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane fontawesome-demo show active" id="customer">
                            <div id="biography">
                                <div class="customer-view">
                                   <!-- <p>
                                        <a class="btn btn-success" href="/app/customer/create">New Customer</a>&nbsp;<a class="btn btn-primary" href="/app/customer/update?id=5470">Update</a>&nbsp; <a class="btn btn-warning" href="javascript:createShopify();">Create Shopify</a>
                                        <a class="btn btn-danger pull-right" href="javascript:createComment();">
                                            <i class="fa fa-comment"></i>
                                        </a>
                                    </p>-->

                                    <div class="card">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <small class="text-muted"></small>
                                                <small class="text-muted"></small>
                                                <p style="font-weight: bold;">
                                                    Fee Detail
                                                    <!--<label class="pull-right badge badge-success">Default</label>
                                                    <label class="pull-right badge badge-success">Active</label>-->
                                                </p>
                                                <hr>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 >Class: </h6>
                                                <p class="m-b-0">{{$fees_master ? $fees_master->hasClass->name:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Category: </h6>
                                                <p class="m-b-0">
                                                    {{$fees_master ? $fees_master->hasCategory->name:''}}</p>
                                            </div>
                                        </div>

                                       <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                        <table id="" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Fee Type</strong></th>
                                                    <th><strong>Fee ({{$currency_name ? $currency_name->name:''}})</strong></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                                @php $i = 0;
                                                $total = 0;
                                                @endphp

                                                @foreach($fees_type as $val)
                                                @php $i++
                                                @endphp
                                                <tr>

                                                    <td>{{$i}}</td>
                                                    <?php  $name = Master::where('id',$val->fees_type)->first();?>
                                                    <td>{{$name ? $name->name:''}}</td>
                                                    <td>{{number_format($val ? $val->value:'0.00',2)}}</td>
                                                    <?php $total+= $val->value;  ?>
                                                </tr>
                                                @endforeach                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-right"></th>
                                                    <th>Total: {{$total ? $total:'0.00'}}({{$currency_name ? $currency_name->name:''}})</th>
                                                    <th ></th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div> 
</div>
@endsection