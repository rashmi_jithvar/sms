@extends('layouts.app') @section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .table thead th{
        background:white;
    }
</style>
<?php
if($fees_master->id)
{
       $key_id = $fees_master->id;
}

else{
       $key_id = Session::get('key_id');
} 

?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Add Fees</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('fees-master') }}">Fees Master</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div id="coverScreen"  class="LockOn" style="display: none;"></div>  
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            <form id="propertytype_form" method="POST" action="{{ route('fees-type.fees-store') }}">
                                @csrf @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }} @php Session::forget('message'); @endphp
                                </div>
                                @endif
                        <input type="hidden" class="form-control" name="key_id" id="key_id" value="{{$key_id}}">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Class</label>
                                        <select class="form-control" id="class" name="class">
                                            <?php echo $class ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('class') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Category</label>
                                        <select class="form-control" id="category" name="category">
                                            <?php echo $category ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('category') }}</span>
                                    </div>
                                </div>
                                <h6>Add Fees</h6>
                     <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Fees Type</label>
                                        <select class="form-control" id="fees_type" name="fees_type">
                                            <?php echo $fees ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('first_name') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Fee ({{$currency_name ? $currency_name->name:''}})</label>
                                        <input type="number" class="form-control" name="value" id="value" placeholder="Fee" min="0">
                                        <span class="text-danger name">{{ $errors->first('value') }}</span>

                                    </div>

                                     <div class="col-md-3">
                                         <br>
                                     <button type="button" class="btn btn-success icon icon-plus2" onclick="addCart();" id="add_new_item1">+</button>
                                 </div>
                                </div>  
                               <br>
                                </hr>    

                                <div class="form-group row">
                                     <div class="col-md-8">
                                       <div id="cart-table">

                                        <table class="table table-striped" id="cart-table-body">

                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Fee Type</strong></th>
                                                    <th><strong>Fee ({{$currency_name ? $currency_name->name:''}})</strong></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-right"></th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

loadCart({{$key_id}});

function loadCart($key_id)
{
    $.ajax({
        type : 'get',
        url : "{{URL::to('load-cart')}}",
        data:{'key_id':$key_id},
        success:function(data){

        $('#cart-table-body').html(data);
       
        }
        });
  }    

    function addCart()
    {
        var fees_type = $('#fees_type').val();
        var value = $("#value").val();
        var key_id = $("#key_id").val();
                if(this.fees_type.value == "") {
                      alert("Please Select Fee Type");
                      this.fees_type.focus();
                      return false;
                    }
                if(this.value.value == "") {
                      alert("Please enter Value");
                      this.value.focus();
                      return false;
                    }
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");       
                $.ajax({
                        type : 'post',
                        url : "{{route('add-cart')}}",
                        data:{ "_token": "{{ csrf_token() }}",'fees_type':fees_type,'value':value,'key_id':key_id},

                        success:function(data){ 
                        loadCart('<?= $key_id; ?>');
                         if (data == '0') {
                            swal("Oopss!", "You clicked the button!", "error");
                        } 
                        else {
                                //swal("Added Successfully!", "", "success");
                                $("#value").val("");
                                $('#fees_type').val("0");    
                        }
                         $("#coverScreen").hide();
                }
        });
    }
function removeCart(url) {
 
    var x = confirm("Are you sure you want to delete?");
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");      
      if (x)
            $.ajax({
                    type : 'get',
                    url : url,
                    success:function(data){
                     $("#coverScreen").show();    
                     $("#coverScreen").html("loading..");                         
                        loadCart('<?= $key_id; ?>');
                           if (data == '1') {
                        
                               //swal("Deleted Successfully!", "", "success");
                            } else {
                                swal("Oopss!", "You clicked the button!", "error");

                            }
                        $("#coverScreen").hide();
                    }
            });
      else
        return false;
        }
</script>

