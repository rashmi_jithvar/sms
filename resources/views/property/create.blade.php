
@extends('layouts.step_form_app', ['title' => 'Page List'])
@section('content')
<style>
  .form-control{
    padding:5px 20px;
  }
  .unit{
    margin-left:-33px;
  }
</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="">Sell/Rent Property</a></li>
                    
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Sell/Rent Property</h4>
                          <form id="formClass" method="POST" action="">
                            <div class="col-sm-12">
                               <div class="card">
                                  <div class="card-header">
                                     <h5>Sell/Rent Property</h5>
                                  </div>
                                  <div class="card-body">
                                     <div id="smartwizard-left" class="sw-vertical-left">
                                        <ul>
                                           <li>
                                              <a href="#step-l-1">
                                                 <h6>Basic Detail</h6>
                                                 <p class="m-0">
  
                                                 </p>
                                              </a>
                                           </li>
                                           <li>
                                              <a href="#step-l-2">
                                                 <h6>Property Detail</h6>
                                                 <p class="m-0"></p>
                                              </a>
                                           </li>
                                           <li>
                                              <a href="#step-l-3">
                                                 <h6>Features</h6>
                                                 <p class="m-0"></p>
                                              </a>
                                           </li>                                           
                                           <li>
                                              <a href="#step-l-4">
                                                 <h6>Location & Pricing</h6>
                                                 
                                              </a>
                                           </li>

                                        </ul>
                                        <div>
<!----Basic Details Form-------->                                            
                                           <div id="step-l-1">
                                              <h5>Basic Details</h5>
                                              <hr>
                    
                                              <p>
                                                <div class="form-group row">
                                
                                                    <div class="col">
                                                        <label>You Are:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" id="ddl">
                                                        <option value="owner">Owner</option>
                                                        <option value="dealer">Dealer</option>
                                                    </select>                                                            
                                                    </div>
                                                    <div class="col">
                                                        <label>Property For:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12">
                                                        <option value="sell">Sell</option>
                                                        <option value="rent">Rent</option>
                                                    </select>                             
                                                    </div>                                                 
                                                    <div class="col"></div>                           
                                                </div>                                                
                                                   
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>Property Type:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name='property_type'>
                                                            <?php echo $property_type ?>
                                                    </select>                                          
                                                    </div>

                                                    <div class="col">
                                                        <label>Property Residential/Commericial Type:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name='subproperty_type'>
                                                            <?php echo $property_sub_type ?>
                                                    </select>                                 
                                                    </div>
                                                    <div class="col">
                                                        <label>Property Apartment/Flat Type:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name='sub_subproperty_type'>
                                                            <?php echo $property_sub_sub_type ?>
                                                    </select>                           
                                                    </div>                                            
                                                </div>
                                                 <h6 class="mt-3">Willing to rent out to:*</h6>
                                                    <hr>
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>
                                                         <input type="checkbox" id="cn-p-1" >
                                                         Family</label>
                                                     </div>
                                                    <div class="col">    
                                                        <label>
                                                            <input type="checkbox" id="cn-p-1" >
                                                        Single Men</label>  
                                                    </div>
                                                    <div class="col"> 
                                                        <label>
                                                        <input type="checkbox" id="cn-p-1" >
                                                        Single Women</label>
                                                    </div>    
                                                     <div class="col"></div>
                                                      <div class="col"></div>
                                                      <div class="col"></div>                                        
                                                </div>
                                                <h6 class="mt-3">Preferred Agreement Type:</h6>
                                                <hr>                                              
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>
                                                         <input type="radio" id="agreementType" name="agreementType">
                                                         Company Lease Agreement</label>
                                                     </div>
                                                    <div class="col">
                                                    <label >    
                                                            <input type="radio" id="agreementType" name="agreementType">
                                                        Any</label>  
                                                    </div>
                                                      
                                                     
                                                      <div class="col"></div>                                         
                                                </div>
                                            </p>
                                            
                                           </div>
<!----Property Details Form-------->   
                                           <div id="step-l-2">
                                              <h5>Property Details</h5>
                                              <hr>
                                              <h6>Plot Detail</h6>
<!----1 row------->
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>Plot Area:</label>
                                                        <input type="text" id="cn-p-1" name="agreementType" class="form-control" placeholder="Enter Plot">
                                                    </div>

                                                    <div class="col unit">
                                                        <label>Unit:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="plot_unit" >
                                                          <?php echo $unit; ?>
                                                    </select>                             
                                                    </div>                                                 
                                                    <div class="col">
                                                        <label>Build-up Area:</label>
                                                        <input type="text" id="cn-p-1" name="agreementType" class="form-control" placeholder="Enter Build-up">
                                                    </div>  
                                                    <div class="col unit">
                                                        <label>Unit:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="buildup_unit">
                                                              <?php echo $unit; ?>
                                                    </select>                             
                                                    </div>                                                 
                                                    <div class="col">
                                                        <label>Carpet Area:</label>
                                                        <input type="text" id="cn-p-1" name="agreementType" class="form-control" placeholder="Enter Carpet">
                                                    </div>  
                                                    <div class="col unit">
                                                        <label>Unit:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="carpet_unit">
                                                              <?php echo $unit; ?>
                                                    </select>                             
                                                    </div>                                                                            
                                                </div> 
                                                <br>  
                                             
<!----2 Row-------->                                              
                                              <div class="form-group row">
                                                    <div class="col">
                                                        <label>BedRooms:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="bedroom_type">
                                                                <?php echo $bedroom_type; ?>
                                                    </select>                             
                                                    </div>    
                                                    <div class="col">
                                                        <label>Bathrooms:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="bathroom">
                                                            <?php echo $bathroom; ?>
                                                    </select>                             
                                                    </div>                                                 

                                                    <div class="col">
                                                       <label>Balconies:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="balcony">
                                                      <option value="1bhk">select</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select>                             
                                                    </div>                                            
                                              </div>
                                              <br>
<!------Row----------------->
                                              <div class="form-group row">
                                                    <div class="col">
                                                        <label>Kitchen:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="kitchen_type">
                                                          <?php echo $kitchen_type; ?>
                                                    </select>                             
                                                    </div>    
                                                    <div class="col">
                                                        <label>Bathrooms Type:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="bathroom_type">
                                                      <?php echo $bathroom_type; ?>
                                                    </select>                             
                                                    </div> 
                                                     <div class="col"></div>                                                 
                                         
                                              </div>  
<!------Row-----------------><br>
                                              <div class="form-group row">

                                                 <div class="col">
                                                        <label>House Facing:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="facing_type">
                                                      <?php echo $facing_type; ?>
                                                    </select>                                                     
                                                 </div>                                                 
                                                    <div class="col">
                                                        <label>Type Of Flooring:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="flooring_type">
                                                          <?php echo $flooring_type; ?>
                                                    </select>                             
                                                    </div>    
                                                    <div class="col">
                                                      <label>Power Backup:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12">
                                                      <option value="" label="Select" selected="selected">Select</option>
                                                      <option value="N" label="None">None</option>
                                                      <option value="P" label="Partial">Partial</option>
                                                      <option value="F" label="Full">Full</option>
                                                    </select>                             
                                                    </div>                                            
                                              </div>                                                 
                                              <br>                                          
<!----3 Row------->
                                       <label>Other Rooms</label>
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>
                                                         <input type="checkbox" id="cn-p-1" >
                                                         Pooja Room</label>
                                                     </div>
                                                    <div class="col">    
                                                        <label>
                                                            <input type="checkbox" id="cn-p-1" >
                                                        Study Room</label>  
                                                    </div>
                                                    <div class="col"> 
                                                        <label>
                                                        <input type="checkbox" id="cn-p-1" >
                                                        Storage Room</label>
                                                    </div>    
                                                     <div class="col">
                                                        <label>
                                                        <input type="checkbox" id="cn-p-1" >
                                                        Servent Room</label>                                                       
                                                     </div>
                                                      <div class="col">
                                                        <label>
                                                        <input type="checkbox" id="cn-p-1" >
                                                        Other</label> 
                                                      </div>
                                                      <div class="col"></div>
                                                </div> 
                                                <br>
<!--------4 row-------------------------->
                                              <div class="form-group row">
                                                    <div class="col">
                                                        <label>Total Floor:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="floor_type">
                                                              <?php echo $floor_type; ?>
                                                    </select>                             
                                                    </div>    
                                                    <div class="col">
                                                        <label>Property Floor:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" name="floor_type">

                                                          <?php echo $floor_type; ?>    
                                                        </select>               
                                                    </div>                                                 

                                                    <div class="col">
                                                       <label>Furnished:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12" name="furnished_type">
                                                            <?php echo $furnished_type ?>
                                                    </select>                             
                                                    </div>                                            
                                              </div> 
<!----3 Row------->
                                       <label>Furnishing Item</label>
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label> Fans
                                                         <input type="number" name="fans" id="fans" value="0" style="width: 100px;"  max="100" class="form-control" />
                                                         </label>
                                                     </div>
                                                    <div class="col">    
                                                        <label>Lights
                                                            <input type="number" name="light" id="light" value="0" style="width: 100px;"  max="100" class="form-control" />
                                                        </label>  
                                                    </div>
                                                    <div class="col"> 
                                                        <label>Wardrobes
                                                          <input type="number" name="wardrobes" id="wardrobes" value="0" style="width: 100px;"  max="100" class="form-control" /></label>
                                                    </div> 
                                                    <div class="col">
                                                        <label>Beds
                                                          <input type="number" name="beds" id="beds" value="0" style="width: 100px;"  max="100" class="form-control" />
                                                        </label>                                     
                                                     </div>
                                                                                                        
                                                      <div class="col">
                                                        <label>Fridge
                                                        <input type="number" name="beds" id="beds" value="0" style="width: 100px;"  max="100" class="form-control" /></label> 
                                                      </div>
                                                      <div class="col">Other
                                                        <input type="number" name="beds" id="beds" value="0" style="width: 100px;"  max="100" class="form-control" /></div>

                                                  </div>

                                                <br>

                                          
<!--------4 row-------------------------->
                                                <div class="form-group row">
                                                    <div class="col">
                                                        <label>Parking Place:</label>
                                                     <select class="js-example-placeholder-multiple col-sm-12">
                                                        <option value="">select</option>
                                                        <option value="yes">Yes</option>
                                                        <option value="no">No </option>
                                                    </select>                             
                                                    </div>    
                                                    <div class="col">
                                                        <label>Closed Parking
                                                            <input type="number" name="close-parking" id="close-parking" value="0" style="width: 100px;"  max="100" class="form-control" />
                                                        </label>  
                                                      &nbsp;&nbsp;&nbsp;
                                                    
                                                        <label>Open Parking
                                                          <input type="number" name="open-parking" id="open-parking" value="0" style="width: 100px;"  max="100" class="form-control" /></label>
                                                    </div>
                                                    <div class="col"></div>               
                                                    </div>
<!----1 row------->
                                                <div class="form-group row">
                                               
                                                    <div class="col">
                                                        <label>Age of Property:</label>
                                                        <input type="text" id="cn-p-1" name="agreementType" class="form-control" placeholder="Enter Age Of Property">
                                                    </div>                                                    
                                                    <div class="col">
                                                        <label>Avaiable From:</label>
                                                        <input type="date" id="cn-p-1" name="agreementType" class="form-control">
                                                    </div>

                                                 
                                                                                                   
                                                    <div class="col"></div>  
                                                                                                                      
                                                </div> 
                                                <br>                                                     
                                                  </div>                       
                                                                                         
<!----Additional Features-------->                                            
                                           <div id="step-l-3" >
                                              <h5>Additional Features</h5>
                                              <hr>
                                              @foreach($additional_feauture as $feature)
                                              <h6>{{$feature->name}}:</h6>
                                                <div class="form-group row">
                                                  @foreach($additional_sub_feature as $sub_feature)
                                                  
                                                    <div class="col-md-4">
                                                        <label>
                                                         <input type="checkbox" id="cn-p-1" >
                                                         {{$sub_feature->name}}</label>
                                                     </div>
                                                    
                                                  @endforeach 
                                                </div>  
                                                @endforeach                                            
                                                  

                                              <h6>Upload Image</h6>
                                                <div class="form-group row">
                                                    <div class="col">
                                                        
                                                         <input type="file" id="cn-p-1" >
                                                     
                                                     </div>
                                                    <div class="col">    
                                                         
                                                    </div>
                                                    <div class="col"> 
                                                      
                                                    </div> 
                                                  </div>  
                                              <h6>Description</h6>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        
                                                         <textarea name="description" class="form-control"></textarea>
                                                     </div>
                                                 
                                                    
                                                  </div>                                                   <p><strong></strong> Lorem IpsumLorem IpsumLorem Ipsum is simply dummy text and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>                                                                                                                                                   
                                           </div>
<!----Location & Price-------->                                             
                                           <div id="step-l-4" >
                                              <h5>Location & Price</h5>
                                              <hr>
                                              <h6></h6>
                                                  <div class="form-group row">
                                                    <div class="col">
                                                        <label>State:</label>
                                                        <input type="text" id="cn-p-1" class="form-control" placeholder="Enter State">                             
                                                    </div>    
                                                    <div class="col">
                                                        <label>City:</label>
                                                       <input type="text" id="cn-p-1" class="form-control"
                                                       placeholder="Enter City">             
                                                    </div>                                                 

                                                    <div class="col">
                                                       <label>Area:</label>
                                                       <input type="text" id="cn-p-1" class="form-control"
                                                       placeholder="Enter Area">                             
                                                    </div>                                            
                                              </div>
                                                  <div class="form-group row">
                                                    <div class="col">
                                                        <label>Address:</label>
                                                      <input type="text" class="form-control" name="address" placeholder="Enter Address">                          
                                                    </div>    
                                                    <div class="col">               
                                                    </div>                                                 

                                                    <div class="col">                         
                                                    </div>                                            
                                              </div>                                              
                                              <h6 class="mt-3">Pricing</h6>
                                            <div class="form-group row">
                                                    <div class="col">
                                                        <label>Expected Rent:</label>
                                                      <input type="text" class="form-control" name="expected_rent" placeholder="Enter Expected Rent"/>        
                                                    </div>    
                                                    <div class="col">
                                                      <br>
                                                      
                                                        <label>Electricity & Water Charge Excluded &nbsp;&nbsp;&nbsp;
                                                         <input type="checkbox" id="cn-p-1" >
                                                         </label>           
                                                    </div> 
                                                    <div class="col">
                                                      <br>
                                                       <label>Price Negotiable &nbsp;&nbsp;&nbsp;
                                                         <input type="checkbox" id="cn-p-1" >
                                                         </label> 
                                                    </div>                                                
                                              </div> 
                                            <div class="form-group row">
                                                    <div class="col">
                                                        <label>Security Deposite:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12">
                                                        <option value="1bhk">select</option>
                                                        <option value="Fixed">Fixed</option>
                                                        <option value="multipleRent">Multiple-Rent</option>
                                                 
                                                        </select>      
                                                    </div>    
                                                    <div class="col">
                                           
                                                        <label>Price</label>      
                                                         <input type="text" id="cn-p-1" class="form-control" placeholder="Enter Price">
                                                             
                                                    </div> 
                                                    <div class="col"></div>                                                
                                              </div>    
                                            <div class="form-group row">
                                                    <div class="col">
                                                        <label>Duration of Rent Aggrement:</label>
                                                          <input type="date" id="cn-p-1" class="form-control">   
                                                    </div>    
                                                    <div class="col">
                                           
                                                        <label>Months Of Notice </label>      
                                                         <input type="text" id="cn-p-1" class="form-control" placeholder="Enter No. Of Months">
                                                             
                                                    </div> 
                                                    <div class="col"></div>                                                
                                              </div>                                                                                                                                         
                                              <p><strong></strong> Lorem IpsumLorem IpsumLorem Ipsum is simply dummy text and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                                           </div>

                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection



