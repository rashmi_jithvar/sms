@extends('layouts.step_form_app', ['title' => 'Page List']) @section('content')
<!-- END HEAD -->

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('rent-sell') }}">Property List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Property create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" id="basic">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <div class="card-head">
                                            <header>Basic Wizard</header>
                                        </div>
                                        <div class="card-body">
                                            <div id="wizard">
                                                <h1>First Step</h1>
                                                <div>First Content</div>
                                                <h1>Second Step</h1>
                                                <div>Second Content</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- wizard with validation-->

                            <!-- Verticle Steps Wizard -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <div class="card-head">
                                            <header>Post Property</header>
                                        </div>
                    <div id="card-body">
                            <form id="example-advanced-form" enctype="multipart/form-data">
                                       
                                            <h3>Basic Detail</h3>

                                            <fieldset>                                               
                                                <div class="form-group row">                                                    
                                                    <div class="col-md-4 has-error">
                                                        <label>Owner Name:</label>
                                                        <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Enter Owner Name" required>
                                                        <span class="text-danger">{{ $errors->first('customer_name') }}</span>

                                                    </div>

                                                    <div class="col-md-4">
                                                        <label>Contact No:</label>
                                                            <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact no." required="" maxlength="12">
                                                            <span class="text-danger">{{ $errors->first('contact_no') }}</span>
                                                            
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Email :</label>
                                                        <input type="email" class="form-control" name="email" id="email" placeholder=" Enter Email" required="">
                                                        <span class="error">{{ $errors->first('email') }}</span> 
                                                    </div>
                                                </div>                                   
                                                <div class="form-group row">

                                                    <div class="col-md-4">
                                                        <label >You Are:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" id="you_are" required>
                                                            <option value="0">Select</option>
                                                            <option value="owner">Owner</option>
                                                            <option value="dealer">Dealer</option>
                                                        </select>
                                                        <span class="text-danger">{{ $errors->first('you_are') }}</span> 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Property For:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" id="property_for" required="">
                                                            <option value="0">Select</option>
                                                            <option value="sell">Sell</option>
                                                            <option value="rent">Rent</option>
                                                        </select>
                                                        <span class="text-danger">{{ $errors->first('property_for') }}</span> 
                                                    </div>
                                                    
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Property Type:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name='property_type' id="property_type" required="">
                                                            <?php echo $property_type ?>
                                                        </select>
                                                        <span class="text-danger">{{ $errors->first('property_type') }}</span> 

                                                    </div>

                                                    <div class="col-md-4">
                                                        <label id="subproperty_type_label">Property Sub-Type:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name='subproperty_type' id="subproperty_type" required="">
                                                            
                                                            <span class="text-danger">{{ $errors->first('subproperty_type') }}</span> 
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label id="sub_subproperty_type_label">Type:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name='sub_subproperty_type' id="sub_subproperty_type">
                                                             <span class="text-danger">{{ $errors->first('sub_subproperty_type') }}</span> 
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="residentialRentTo" style="display: none;">
                                                <h5 class="mt-3"><b>Willing to rent out to:*</b></h5>
                                                <hr>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="checkbox" id="willing_to" name="family"  value="family"> Family
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="checkbox" id="willing_to" name="single_men"  value="bachelor_man">Bachelor Man</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="checkbox" id="willing_to" name="single_women"  value="bachelor_woman">Bachelor Woman</label>
                                                    </div>
                                                </div>
                                                <h6 class="mt-3">Preferred Agreement Type:</h6>
                                                <hr>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="radio"  name="agreementType" name="agreement_type" id="agreement_type" value="company_lease"> Company Lease Agreement</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="radio" name="agreementType" id="agreement_type" value="any" > Any
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                                <div id="CommercialRentTo" style="display: none;">
                                                <h5 class="mt-3"><b>Multiple Property Units:*</b></h5>
                                                <hr>
                                                <div class="form-group row">
                                                    <div class="col-md-3">
                                                        <label>
                                                            <input type="radio" id="multiplePropertyUnit" name="multiple_property_unit"  value="yes"> Yes
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>
                                                            <input type="radio" id="multiplePropertyUnit" name="multiple_property_unit"  value="no"> No</label>
                                                    </div>
                                               
                                                </div>

                                            </div>                                            
                                                <div id="otherRentTo" style="display: none;">
                                                <h5 class="mt-3"><b>Willing to rent out to:*</b></h5>
                                                <hr>
                                                <div class="form-group row">
                                                    <div class="col-md-3">
                                                        <label>
                                                            <input type="radio" id="willing_to" name="willing_to"  value="boys"> Boys
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>
                                                            <input type="radio" id="willing_to" name="willing_to"  value="girls"> Girls</label>
                                                    </div>
                                                   
                                                </div>

                                            </div> 
                                            </fieldset>
                                            <h3>Property Details</h3>
                                            <fieldset>
                                                <h5>Property Details</h5>
                                                <hr>
                                             
                                                <!----1 row------->
                                                <div class="form-group row">
                                                    <input type="hidden" id="property_id" name="id" class="form-control" value="">
                                                  
                                                    <div class="col-md-2">
                                                        <label>Plot Area:</label>
                                                        <input type="text" id="plot_area" name="agreementType" class="form-control" placeholder="Enter Plot" required="">
                                                    </div>

                                                    <div class="col-md-2">
                                                        <label>Unit:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="plot_unit" id="plot_unit" required="">
                                                           
                                                            <?php echo $unit; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Build-up Area:</label>
                                                        <input type="text" id="buildup_area" name="agreementType" class="form-control" placeholder="Enter Build-up" required="">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Unit:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="buildup_unit" id="buildup_unit" required="">

                                                            <?php echo $unit; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Carpet Area:</label>
                                                        <input type="text" id="carpet_area" name="agreementType" class="form-control" placeholder="Enter Carpet" required="">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Unit:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="carpet_unit" id="carpet_unit" required="">
                                                            <?php echo $unit; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                               

                                                <!----2 Row-------->
                                                <div class="form-group row">
                                                    <div class="col-md-3" id="bedroom_col" >
                                                        <label id="labelBedroom">:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="bedroom_type" id="bedroom_type">
                                                          
                                                            <?php echo $bedroom_type; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label id="labelBathroom">:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="bathroom" id="bathroom">
                                                            <option value="0">Select</option>
                                                            <option value="1">Common</option>
                                                            <option value="2">1</option>
                                                            <option value="2">2</option>
                                                            <option value="2">3</option>
                                                            <option value="2">4</option>
                                                            <option value="2">5</option>
                                                            <option value="2">6</option>
                                                            <option value="2">7</option>
                                                            <option value="2">8</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Bathrooms Type:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="bathroom_type" id="bathroom_type">
                                                            <?php echo $bathroom_type; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3" id="kitchen_col">
                                                        <label id="labelKitchen">:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="kitchen_type" id="kitchen_type">
                                                            <?php echo $kitchen_type; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                                <!------Row----------------->
                                                <div class="form-group row">

                                                    <div class="col-md-3" id="balcony_col">
                                                        <label>Balconies:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="balcony" id="balcony">
                                                            <option value="0">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>House Facing:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="facing_type" id="facing_type">
                                                            <?php echo $facing_type; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Type Of Flooring:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="flooring_type" id="flooring_type">
                                                            <?php echo $flooring_type; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Power Backup:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" id="power_backup">
                                                            <option value="" label="Select" selected="selected">Select</option>
                                                            <option value="N" label="None">None</option>
                                                            <option value="P" label="Partial">Partial</option>
                                                            <option value="F" label="Full">Full</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!------Row----------------->
                                              
                                                <!----3 Row------->
                                                <label id="labelOtherRoom" style="display: none;"><h4>Other Rooms</h4><hr></label>
                                               
                                                <div class="form-group row" id="other_room">                                                                         
                                                </div>
         
                                                <!--------4 row-------------------------->
                                                <div class="form-group row">
                                                    <div class="col-md-4" id="floor_label" style="display: none;">
                                                        <label>Total Floor:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" name="floor_type" id="floor_type">
                                                            <?php echo $floor_type; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4" id="property_floor_label" style="display: none;">
                                                        <label>Property Floor:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="property_floor" id="property_floor">
                                                            <?php echo $floor_type; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label>Furnished:</label>
                                                        <select class="js-example-placeholder-multiple col-md-12" name="furnished_type" id="furnished_type">
                                                        
                                                        </select>
                                                    </div>
                                                </div>
                                                <!----3 Row------->
                                                <label id="furnished_item" style="display: none;">
                                                    <h4>Furnishing Item</h4><hr>
                                                </label>

                                                <div class="form-group row" id="furniture_type">
                                            
                                                </div>

                                                <!--------4 row-------------------------->
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Parking Place:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" id="parking_place">
                                                            <option value="">Select</option>
                                                            <option value="yes">Yes</option>
                                                            <option value="no">No </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Closed Parking
                                                            <input type="number" id="close_parking" name="close_parking"  value="0" style="width: 100px;" max="100" min="0" class="form-control" />
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;

                                                        <label>Open Parking
                                                            <input type="number" id="open_parking" name="open_parking"  value="0" style="width: 100px;" max="100" min="0" class="form-control" />
                                                        </label>
                                                    </div>
                                                    
                                                </div>
                                                <!----1 row------->
                                                <div class="form-group row">

                                                    <div class="col-md-4">
                                                        <label>Age of Property:</label>
                                                        <input type="text" id="age" name="age" class="form-control" placeholder="Enter Age Of Property">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Avaiable From:</label>
                                                        <input type="text" id="avaiable_from" name="avaiable_from" class="form-control " >
                                                    </div>

                                                    <div class="col-md-4"></div>

                                                </div>
                                               
                                            </fieldset>
                                            <h3>Features</h3>
                                            <fieldset>
                                                <h4>Additional Features</h4>
                                                
                                                <hr> @foreach($additional_feauture as $feature)
                                                <h4><b>{{$feature->name}}:</b></h4>
                                                <div class="form-group row">

                                                    @foreach ($additional_sub_feature as $key => $value)
                                                       @foreach ($value->children as $key => $val) 
                                                        @if($val->parant_id == $feature->id)
                                                    <div class="col-md-4">
                                                        <label>
                                                            <input type="checkbox" id="additional_features" value="{{$val->id}}" name="additional_feauture[]"> {{$val->name}}
                                                        </label>
                                                    </div>
                                                          @endif
                                                        @endforeach
                                                    @endforeach
                                                </div>
                                                @endforeach
                                                <h5><b>Upload Image</b></h5>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <input type="file" id="image" name="image" multiple>
                                                    </div>
                                           
                                                </div>
                                                <h5><b>Description</b></h5>
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <textarea id="description" name="description" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h3>Location & Pricing</h3>
                                            <fieldset>
                                             
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>State:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" name="state" id="state" required="">
                                                            <?php echo $state ?>
                                                        </select>                                 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>City:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" name="city" id="city" required="">
                                                        </select>                                        
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label>Area:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" name="area" id="area" >
                                                        </select>                                                          
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Address:</label>
                                                        <input type="text"  id="address" class="form-control" name="address" placeholder="Enter Address" required="">
                                                    </div>
                                                    <div class="col-md-4">
                                                    </div>

                                              
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Expected Price:</label>
                                                        <input type="text" id="expected_rent" class="form-control" name="expected_rent" placeholder="Enter Expected Rent" />
                                                    </div>
                                                    <div class="col-md-4">
                                                      

                                                        <label>Electricity & Water Charge Excluded &nbsp;&nbsp;
                                                            <input type="checkbox" id="charge_exclude" name="charge_exclude">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                       
                                                        <label>Price Negotiable &nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" id="price_negotiable" name="price_negotiable">
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Security Deposite:</label>
                                                        <select class="js-example-placeholder-multiple col-sm-12" id="security_deposite">
                                                            <option value="1bhk">Select</option>
                                                            <option value="Fixed">Fixed</option>
                                                            <option value="multipleRent">Multiple-Rent</option>

                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">

                                                        <label>Price</label>
                                                        <input type="text" id="price" class="form-control" placeholder="Enter Price">

                                                    </div>
                                                    
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Duration of Rent Aggrement:</label>
                                                        <input type="text" id="rent_aggreement d_week" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">

                                                        <label>Months Of Notice </label>
                                                        <input type="text" id="notice" class="form-control" placeholder="Enter No. Of Months">

                                                    </div>
                                                    
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    </form>
                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection