@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Post Property List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
          <div class="card-header">
               <a class="btn btn-primary m-t-5"  href="{{ url('post-property') }}">Post Property</a>
            </div>
            <div class="card-body table-border-style">
              <h4>Property List</h4>
                <div class="table-responsive">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>#</th>
                        
                         <th>Property no.</th>
                         <th>Customer Name</th>
                         <th>Contact No.</th>
                         <th>Email</th>
                         <th>Posted By</th>
                         <th>Property For</th>
                         <th>Property Type</th>
                         
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        @php $i = 0
                        @endphp
                        @if(!empty($properties))
                        @foreach($properties as $property)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          
                          <td>{{$property ? $property->property_unique_id:''}}</td>
                          <td>{{$property ? $property->customerDetail->customer_name:''}}</td>
                          <td>{{$property ? $property->customerDetail->contact_no:''}}</td>

                          <td>{{$property ? $property->customerDetail->email:''}}</td>
                          <td>{{$property ? $property->person : ''}}</td>
                          <td>{{$property ? $property->property_for:''}}</td>
                          <td>{{$property ? $property->PropertyType->name:''}}<br></td>
                         
            
                          <td>
                          @if($property->status == '1')
                            <label class="badge badge-success">Underscreeings</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>

                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection