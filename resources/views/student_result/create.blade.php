@extends('layouts.app') @section('content')

<style>
    .table thead th{
        background:white;
    }
    #external_field,#tb_ext{
        display: none;
    }
</style>
<?php
if($marks->id)
{
       $result_key = $marks->id;
}

else{
       $result_key = Session::get('result_key');
} 

?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Add Student Result</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('student-result-report') }}">Student Result</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div id="coverScreen"  class="LockOn" style="display: none;"></div>  
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            <form id="marks-master" method="POST" action="{{ route('student-marks.store') }}">
                                @csrf @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }} @php Session::forget('message'); @endphp
                                </div>
                                @endif
                        <input type="hidden" class="form-control" name="result_key" id="result_key" value="{{$result_key}}">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Exam Type</label>
                                        <select class="form-control" id="exam_type" name="exam_type">
                                            <?php echo $exam_type ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('exam_type') }}</span>
                                    </div>                                    
                                    <div class="col-md-3">
                                        <label>Class</label>
                                        <select class="form-control" id="class" name="class">
                                            <?php echo $class ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('class') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Section</label>
                                        <select class="form-control" id="section" name="section">
                                            <?php echo $section ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('section') }}</span>
                                    </div> 
                                    <div class="col-md-3">
                                        <label>Student</label>
                                        <select class="form-control" id="student" name="student">
                
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('student') }}</span>
                                    </div>                                                                       

                                                                     
                                </div>
                                <h6>Add Marks</h6>
                     <div class="form-group row">
                                    <div class="col-md-2">
                                        <label>Subject</label>
                                        <select class="form-control" id="subject" name="subject">
                                        </select>
                                        <input type="hidden" class="form-control" name="all_subject_total_marks" id="all_subject_total_marks" placeholder="Total Max Marks" min="0" readonly="" tabindex="-1">
                                        <span class="text-danger parant_id">{{ $errors->first('subject') }}</span>
                                    </div>
                                    <!--<div class="col-md-2" id="max_marks_fields">
                                        <label>Max Marks</label>
                                        <input type="number" class="form-control" name="max_marks" id="max_marks" placeholder="Max Marks" min="0">
                                        <span class="text-danger name">{{ $errors->first('max_marks') }}</span>

                                    </div>---->
                                    <div class="col-md-2">
                                        <label>Max Internal Marks</label>
                                        <input type="number" class="form-control" name="internal_marks" id="internal_marks" placeholder="Internal Marks" min="0" readonly="" tabindex="-1">
                                        <span class="text-danger name">{{ $errors->first('internal_marks') }}</span>
                                    </div>                                    
                                    <div class="col-md-2"  >
                                        <label>Max External Marks</label>
                                        <input type="number" class="form-control" name="external_marks" id="external_marks" placeholder="External Marks" min="0" readonly="" tabindex="-1">
                                        <span class="text-danger name">{{ $errors->first('external_marks') }}</span>
                                       
                                    </div>
                                  
                                    <div class="">
                                        <label>Total Max Marks</label>
                                        <input type="number" class="form-control" name="total_int_marks" id="total_int_marks" placeholder="Total Max Marks" min="0" readonly="" tabindex="-1">
                                    </div>
                                    <div class="col-md-2"  >
                                        <label>Obtained Marks</label>
                                        <input type="number" class="form-control" name="obtained_marks" id="obtained_marks" placeholder="Obtained Marks" min="0" >
                                        <span class="text-danger name">{{ $errors->first('obtained_marks') }}</span>
                                    </div>                                    
                                    <div class="col-md-2" style="display: none;" >
                                        <label>Total Obtained Marks</label>
                                        <input type="number" class="form-control" name="total_marks" id="total_marks" placeholder="Total Marks" min="0" readonly="" tabindex="-1">
                                        <span class="text-danger name">{{ $errors->first('total_marks') }}</span>
                                    </div>                                    
                                    
                                     <div class="col-md-1">
                                         <br>
                                     <button type="button" class="btn btn-success icon icon-plus2" onclick="addStudentMarks();" id="add_new_item1">+</button>
                                 </div>
                                </div>  
                                <br>
                                </hr>    

                                <div class="form-group row">
                                     <div class="col-md-8">
                                       <div id="cart-table">
                                           
                                        <table class="table table-striped" id="cart-table-body" >
                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Subject</strong></th>
                                                    <!---<th><strong>Max Marks</strong></th>-->
                                                    
                                                    <th><strong>Internal Marks</strong></th>
                                                    <th> <strong>External Marks</strong></th>
                                                    <th> <strong>Total Marks</strong></th>
                                                    <th> <strong>Obtained Marks</strong></th>
                                                    <th> <strong>Total Obtained Marks</strong></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-right"></th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    loadStudentMarks('{{$result_key}}');    
    function loadStudentMarks($result_key)
    {
       $("#coverScreen").show();    
       $("#coverScreen").html("loading..");   
        $.ajax({
            type : 'get',
            url : "{{URL::to('load-student-marks')}}",
            data:{'result_key':$result_key},
            success:function(data){
                $('#cart-table-body').html(data);
                $("#coverScreen").hide();
              //$('#tab').html("sdferewr");
            }

            });

      }    

    function addStudentMarks()
    {

        $subject =  $internal_marks = $external_marks =  $obtained_marks =  $total_max_marks = $total_obtained_marks = 0;
        if($('#subject').val()!=''){
            $subject = $('#subject').val();            
        }
        /*if($("#max_marks").val()!=''){
            $max_marks = $("#max_marks").val();            
        }*/
        if($("#internal_marks").val()!=''){
            $internal_marks = $("#internal_marks").val();            
        }
        if($("#external_marks").val()!=''){
            $external_marks = $("#external_marks").val();            
        } 
        if($("#obtained_marks").val()!=''){
            $obtained_marks = $("#obtained_marks").val();            
        }
        if($("#total_marks").val()!=''){
            $total_obtained_marks = $("#total_marks").val();            
        }    
        if($("#total_int_marks").val()!=''){
            $total_max_marks = $("#total_int_marks").val();            
        }                            
        $result_key = $("#result_key").val();

                $.ajax({
                        type : 'post',
                        url : "{{route('add-student-marks')}}",
                        data:{ "_token": "{{ csrf_token() }}",'subject':$subject,'result_key':$result_key,'internal_marks':$internal_marks,'external_marks':$external_marks,'obtained_marks':$obtained_marks,'total_obtained_marks':$total_obtained_marks,'total_max_marks':$total_max_marks},
                        success:function(data){ 
                        loadStudentMarks('<?= $result_key; ?>');
                         if (data == '0') {
                            swal("Oopss!", "You clicked the button!", "error");
                        } 
                        else {
                                //swal("Added Successfully!", "", "success");
                                $("#subject").val("0");
                                $("#total_marks").val("0");
                                $('#obtained_marks').val("0");
                                $("#external_marks").val("0");
                                $("#internal_marks").val("0");
                               // $('#max_marks').val("0");  
                        }
                       
                }
        });
    }
function removeMarks(url) {
 
    var x = confirm("Are you sure you want to delete?");
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");      
      if (x)
            $.ajax({
                    type : 'get',
                    url : url,
                    success:function(data){
                     $("#coverScreen").show();    
                     $("#coverScreen").html("loading..");                         
                        loadStudentMarks('<?= $result_key; ?>');
                           if (data == '1') {
                               //swal("Deleted Successfully!", "", "success");
                            } else {
                                swal("Oopss!", "You clicked the button!", "error");

                            }
                        $("#coverScreen").hide();
                    }
            });
      else
        return false;
        }
/* Get Student List */
        $(document).on("change", "#class,#section", function() {
            $class= 0;
            $section = 0;
            if($('#class').val()!=''){
                $class = $('#class').val();               
            }
            if($('#section').val()!=''){
                $section = $('#section').val();               
            }
            if($class!=0 && $section !=0){
                $.ajax({
                    type : 'post',
                    url : "{{route('get-student')}}",
                    data:{ "_token": "{{ csrf_token() }}",'class':$class,'section':$section},
                    success:function(data){
                        $('#student').html(data);
                        }
                    });
                }
                /*else{
                    alert("Please Select Section");
                    this.section.focus();
                    return false;
                }*/
          }); 
/* get Subject List */   
        $(document).on("change", "#exam_type,#class", function() {
            $class= 0;
            $exam_type = 0;
            if($('#class').val()!=''){
                $class = $('#class').val();               
            }
            if($('#exam_type').val()!=''){
                $exam_type = $('#exam_type').val();               
            }
            if($class!=0 && $exam_type !=0){
                $.ajax({
                    type : 'post',
                    url : "{{route('get-class-subject')}}",
                    data:{ "_token": "{{ csrf_token() }}",'class':$class,'exam_type':$exam_type},
                    success:function(data){
                        $('#subject').html(data);
                        }
                    });
                /* get total subject Marks */
                $.ajax({
                    type : 'post',
                    url : "{{route('get-total-subject-marks')}}",
                    data:{ "_token": "{{ csrf_token() }}",'class':$class,'exam_type':$exam_type},
                    success:function(result){
                      
                        $('#all_subject_total_marks').val(result);
                        }
                    });                
                }
                /*else{
                    alert("Please Select Class");
                    this.exam_type.focus();
                    return false;
                }*/
          });      
/* is external */         
        $(document).on("click", "#is_external", function() {
                  $('#external_field').toggle();
                  $('#max_marks_fields').toggle();
                  $('#tb_ext').toggle();
                  $('#tb_int').toggle();
                  if($(this).is(':checked')==true){
                    $('#lbl_max_marks').html('Max Internal Marks')
                  }
                  else{
                    $('#lbl_max_marks').html('Max Marks')
                  }

          });         

/* Form Submit */
    $(document).ready( function() {
   if ($("#marks-master").length > 0) {
    $("#marks-master").validate({
        rules: {
            class: {
                required: true,
                //lettersonly: true
            },
            exam_type: {
                required: true,
                //lettersonly: true
            }
        },

    class: {

      class: {
        required: "Please Select Class.",
      },

      exam_type: {
        required: "Please Select Exam Type.",
      },            
    },
    })
  }    
});    
/* get Marks */
    $(document).ready( function() {
            $( "#subject" ).change(function() {
                $subject = $class =  $exam_type = 0;
                if($( "#subject option:selected" )){
                     $subject = $('#subject').val();
                }
                else{
                    $("#subject").focus(); 
                    alert('Select Subject');
                }
                if($('#class').val() !=0){
                     $class = $('#class').val();
                }
                else{
                    $("#class").focus(); 
                    alert('Select Class Marks');
                }                  
                if($('#exam_type').val() !=0){
                     $exam_type = $('#exam_type').val();
                }
                else{
                    $("#exam_type").focus(); 
                    alert('Select External Marks');
                }
              
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");                      
                if($subject &&  $class && $exam_type !=0){
                        $.ajax({
                                type : 'post',
                                url : "{{route('get-marks')}}",
                                dataType: 'json',
                                data:{ "_token": "{{ csrf_token() }}",'subject':$subject,'class':$class,'exam_type':$exam_type},
                                success:function(data){ 
                                      if(data!=0){
                                        $('#internal_marks').val(data.internal_marks);
                                        $('#external_marks').val(data.external_marks);
                                        $('#total_int_marks').val(data.total_marks);
                                        //$('#fee').val(data.value);
                                        //$('#total').val(data.value);

                                      }  
                                }
                        });
                    $("#coverScreen").hide();                          
                }
            });

        $(document).on("change keyup blur", "#obtained_marks", function() {
            $obtain_marks = $('#obtained_marks').val();
            $('#total_marks').val($obtain_marks);
      
          });
        });  
</script>

