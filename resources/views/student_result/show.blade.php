@extends('layouts.app')
@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Student Report Card</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                     <li class="breadcrumb-item"><a href="{{ url('student-result-report') }}">Student Result Detail</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Student Report Card</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-sm-12">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <h5>Report Card</h5>
                </div>
                  <div class="pull-right">
                      <a href="{{ url('student-result/print',$list->id) }}" type="button" class="btn btn-sm btn-primary" title="Print" style="color:white">Print <i class="fa fa-print" aria-hidden="true"></i>
                    </a> 
                  </div>
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-4">
                          Logo
                        </div>

                        <div class="col-sm-6">
                            <h5 class="mb-3 text-left" ><strong><?= $schoolInfo ? $schoolInfo->school_name :'School Name'?></strong></h5>
                        </div>
                    </div>
<!----Student Details --->
                    <div class="row mb-4">
                      <table class="table table-striped table-bordered">
                        <tr>
                          <th>Student Name:</th>
                          <td >{{$list->studentInfo ? $list->studentInfo->first_name:""}}&nbsp;{{$list->studentInfo ? $list->studentInfo->last_name:""}}</td>
                          <th>Class/Section:</th>
                          <td>{{$list->studentInfo->hasClass ? $list->studentInfo->hasClass->name:""}} &nbsp;&nbsp;({{$list->studentInfo->hasSection ? $list->studentInfo->hasSection->name:""}})</td>                          
                        </tr>
                        <tr>
                          <th>Roll No.:</th>
                          <td>{{$list->studentInfo ? $list->studentInfo->roll_no:""}}</td>
                          <th>Date Of Birth</th>
                          <td>{{date("d-m-Y ",strtotime($list->studentInfo ? $list->studentInfo->dob:'' ))}}</td>
                        </tr>                        
                        <tr>
                          <th>Gender</th>
                          <td>{{$list->studentInfo ? ucfirst($list->studentInfo->gender):""}}</td>                          
                          <th>Exam:</th>
                          <td>{{$list->hasExamType ? $list->hasExamType->name:""}}</td>
                        </tr>                                                
                      </table>
                    </div>
                    <div class="row mb-4">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Subject</th>
                                    <th>Total Marks</th>
                                    <th>Marks Obtained</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            @php $i = 0
                            @endphp                              
                            @foreach($list->hasSubjectMarks as $marks)
                            @php $i++
                            @endphp                            
                                <tr>
                                  
                                  <td>{{$i}}</td>
                                    <td class="text-left">{{ $marks->getSubject ? $marks->getSubject->name:''}}</td>
                                    <td class="text-left">{{$marks->total_max_marks ? $marks->total_max_marks:''}}</td>
                                    <td class="text-left">{{$marks->obtained_marks ? $marks->obtained_marks:''}}</td>
                                    
                                </tr>
                          @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th colspan="2" class="text-right">Total</th>
                                  <td colspan="">{{$list ? $list->total_max_marks:""}}</td>
                                  <td>{{$list ? $list->total_obtained_marks:""}}</td>
                                  
                                </tr>  
                                <tr>
                                  <th colspan="2" class="text-right">Percentage</th>
                                  <td>{{$list ? number_format($list->percent,2):""}}%</td>
                                  <td></td>
                                  
                                </tr>  
                                <tr>
                                  <th colspan="2" class="text-right">Grade</th>
                                  <td>{{$list->getGrade ? $list->getGrade->name:""}}</td>
                                  <td class="text-left"><span style="font-weight:bold;">Remark</span>:&nbsp;&nbsp;{{$list ? $list->remark:""}}</td>
                                </tr>                                                              
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready( function() {
        $(document).on("change keyup", "#paid_amount", function() {

            var main = $('#payable_amount').val();
            var disc = $('#paid_amount').val();
            var due = main - disc;
            var paid = $('#paid_amount').val();
      
              
              $('#due_amount').val(due);
              $('#final_amount').val(paid);
              if (due < 0) {
                  alert('Paid Amount should be less or equal to Payable Amount!');
                    $(this).val('');
                    $('#due_amount').val('');
                    return false;
              }
          });
        });
  </script>
  <script type="text/javascript">
    $(document).ready( function() {
   if ($("#payment").length > 0) {
    $("#payment").validate({
        rules: {
            paid_amount: {
                required: true,
                
            },

        },

    messages: {

      paid_amount: {
        required: "Please Enter Paid Amount",
      },
          
    },
    })
  }    
});
</script>