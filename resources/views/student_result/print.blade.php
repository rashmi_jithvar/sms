<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<style type="text/css">
.error{
  color:red;
}
</style>
<style type="text/css">
@media print {
    #printbtn {
        display :  none;
    }
    #back{
         display :  none;
    }
}
</style>

<div class="col-sm-12">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <!--<h5>Report Card</h5>-->
                </div>
                  <div class="pull-right">
                    <a  id="printbtn"  type="button" class="btn btn-sm btn-primary printPage" title="Pay Now">Print <i class="fa fa-print" aria-hidden="true"></i></a> 
                     <a href="{{ url('student-result/show',$list->id) }}" id="back" type="button" class="btn btn-sm btn-info back" title="Back">Back </a> 
                    </a> 
                  </div>
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-4">
                          Logo
                        </div>

                        <div class="col-sm-6">
                            <h5 class="mb-3 text-left" ><strong><?= $schoolInfo ? $schoolInfo->school_name :'School Name'?></strong></h5>
                        </div>
                    </div>
<!----Student Details --->
                    <div class="row mb-4">
                      <table class="table table-striped table-bordered">
                        <tr>
                          <th>Student Name:</th>
                          <td >{{$list->studentInfo ? $list->studentInfo->first_name:""}}&nbsp;{{$list->studentInfo ? $list->studentInfo->last_name:""}}</td>
                          <th>Class/Section:</th>
                          <td>{{$list->studentInfo->hasClass ? $list->studentInfo->hasClass->name:""}} &nbsp;&nbsp;({{$list->studentInfo->hasSection ? $list->studentInfo->hasSection->name:""}})</td>                          
                        </tr>
                        <tr>
                          <th>Roll No.:</th>
                          <td>{{$list->studentInfo ? $list->studentInfo->roll_no:""}}</td>
                          <th>Date Of Birth</th>
                          <td>{{date("d-m-Y ",strtotime($list->studentInfo ? $list->studentInfo->dob:'' ))}}</td>
                        </tr>                        
                        <tr>
                          <th>Gender</th>
                          <td>{{$list->studentInfo ? ucfirst($list->studentInfo->gender):""}}</td>                          
                          <th>Exam:</th>
                          <td>{{$list->hasExamType ? $list->hasExamType->name:""}}</td>
                        </tr>                                                
                      </table>
                    </div>
                    <div class="row mb-4">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Subject</th>
                                    <th>Total Marks</th>
                                    <th>Marks Obtained</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            @php $i = 0
                            @endphp                              
                            @foreach($list->hasSubjectMarks as $marks)
                            @php $i++
                            @endphp                            
                                <tr>
                                  
                                  <td>{{$i}}</td>
                                    <td class="text-left">{{ $marks->getSubject ? $marks->getSubject->name:''}}</td>
                                    <td class="text-left">{{$marks->total_max_marks ? $marks->total_max_marks:''}}</td>
                                    <td class="text-left">{{$marks->obtained_marks ? $marks->obtained_marks:''}}</td>
                                   
                                </tr>
                          @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th colspan="2" class="text-right">Total</th>
                                  <td colspan="">{{$list ? $list->total_max_marks:""}}</td>
                                  <td>{{$list ? $list->total_obtained_marks:""}}</td>
                                 
                                </tr>  
                                <tr>
                                  <th colspan="2" class="text-right">Percentage</th>
                                  <td>{{$list ? number_format($list->percent,2):""}}%</td>
                                  <td></td>
                                  
                                </tr> 
                                <tr>
                                  <th colspan="2" class="text-right">Grade</th>
                                  <td>{{$list->getGrade ? $list->getGrade->name:""}}</td>
                                  <td class="text-left"><span style="font-weight:bold;">Remark</span>:&nbsp;&nbsp;{{$list ? $list->remark:""}}</td>
                                
                                </tr>                                                              
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $('a.printPage').click(function(){
           window.print();
           $('.printPage').hide();
           return false;
});
</script>