<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Forgot Password Email | Student Advise</title>
  </head>
  <body>
    <table cellpadding="0" cellspacing="0" border="0" width="640" style="padding:20px 20px 0 20px; background-color: #dcdce2;">
      <tr>
        <td style="background-color: #ffffff; width:100%;">
          <table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
            <tr>
              <td colspan="2" style="font-family: sans-serif; font-size: 28px; color: #2b2f3e; padding: 30px 10px 0; text-align: center;">Password Reset</td>
            </tr>
            <tr>
              <td colspan="2" style="padding: 20px 20px;">
                <p style="line-height:24px; color:#333333; font-family: sans-serif; font-size: 16px; margin:0;">Please click on 'below link' to reset password<br/>
                <span style="color:#200eed;">{{ $url.'/password/reset/'.$token.'?email='.$email }} </span>
                <br/>
                </p>
              </td>
            </tr>

            <tr>
              <td colspan="2" style="padding-bottom: 30px; text-align: center;"><a href="{{ $url.'/password/reset/'.$token.'?email='.$email }}" style="text-decoration:none; background-color: #37a000; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 14px; line-height: 24px; padding: 10px 35px;">PASSWORD RESET</a></td>
             
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>