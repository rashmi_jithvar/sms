@extends('layouts.app') @section('content')

<style type="text/css">
.error{
  color:red;
}
</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Fee Invoice</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                     <li class="breadcrumb-item"><a href="{{ url('student/show',$student_monthly_fees->studentInfo->id) }}">Student Detail</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Invoice Detail</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-sm-12">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <h5>Invoice</h5>
                </div>
                  <div class="pull-right">
                    <a href="" type="button" class="btn btn-sm btn-success" title="Pay Now" data-toggle="modal" data-target="#Pay">Pay Now</a> 
                                        <a href="{{ url('student/print-monthly-invoice',$student_monthly_fees ? $student_monthly_fees->id:'') }}" type="button" class="btn btn-sm btn-primary" title="Print" style="color:white">Print <i class="fa fa-print" aria-hidden="true"></i>
                    </a> 
                  </div>
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">

                            <h6 class="mb-3"><br></h6>

                            <div>
                          
                            <div>
                                <strong></strong>
                            </div>
                              <div>School Name: &nbsp;{{$schoolInfo ? $schoolInfo->school_name:'' }}</div>
                              <div></div>
                              <div>Contact No.:&nbsp; {{$schoolInfo ? $schoolInfo->contact_no:'' }}</div>
                              
                             
                               <div>Address: {{$schoolInfo ? $schoolInfo->address:'' }}<br>
                                {{$schoolInfo ? $schoolInfo->pin_code:'' }}
                              </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <h6 class="mb-3"><strong>Invoice No:&nbsp;&nbsp;{{$student_monthly_fees ? $student_monthly_fees->invoice_no:'' }}</strong></h6>
                            <h6 class="mb-3"><strong>Month: &nbsp;
                                     @foreach($month as $key => $mon) 
                                            @if($student_monthly_fees->month_id == $key)
                                            {{$mon}}
                                            @endif
                                     @endforeach 
                                   </strong>
                                </h6>
                            <div>
                                <strong></strong>
                            </div>
                            <div>Student's Name: &nbsp;{{$student_monthly_fees->studentInfo ? $student_monthly_fees->studentInfo->first_name:'' }}&nbsp;{{$student_monthly_fees->studentInfo ? $student_monthly_fees->studentInfo->last_name:'' }}</div>
                            <div></div>
                            <div>Class:&nbsp; {{$student_monthly_fees->studentInfo->hasClass ? $student_monthly_fees->studentInfo->hasClass->name:'' }}</div>
                            <div>Section:&nbsp; {{$student_monthly_fees->studentInfo->hasSection ? $student_monthly_fees->studentInfo->hasSection->name:'' }}</div>
                                  
                             <div>Phone: {{!empty($student_monthly_fees->studentInfo->student_parent[0])? $student_monthly_fees->studentInfo->student_parent[0]->mobile : (!empty($student_monthly_fees->studentInfo->student_parent[1])? $student_monthly_fees->studentInfo->student_parent[1]->mobile : '')}}
                            
                        
                           <!-- @if(isset($student_monthly_fees->studentInfo->student_parent))
                                @foreach($student_monthly_fees->studentInfo->student_parent as $parant)
                                    &nbsp;&nbsp;{{$parant->mobile}} 
                                   
                                @endforeach
                            @endif-->
                            </div>

                        </div>



                    </div>

                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Fee Type</th>
                                    <th>Frequency</th>
                                    <th>Fee (INR)</th>
                                    <th>Discount (INR)</th>
                                    <th>Total Fees(INR)</th>
                                </tr>
                            </thead>
                            <tbody>
                              @php $i = 0
                              @endphp  
                              @if(isset($student_fee_item))
                                @foreach($student_fee_item as $fees)
                              @php $i++
                              @endphp                              
                                <tr>
                                  <td>{{$i}}</td>
                                    <td class="text-left">{{$fees->getFees ? $fees->getFees->name : ''}}</td>
                                    <td class="text-left">{{$fees->getLabel ? $fees->getLabel->label : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->fee : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->discount : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->total : ''}}</td>
                                </tr>
                                 @endforeach
                             @endif   
                            </tbody>

                        </table>

                    </div>

                    <div class="row">

                        <div class="col-lg-4 col-sm-5">

                        </div>

                        <div class="col-lg-4 col-sm-5 ml-auto">

                            <table class="table table-clear">

                                <tbody>

                                    <tr>

                                        <td class="left">

                                            <strong>Total amount:&nbsp;&nbsp;</strong>
                                            
                                        </td>

                                        <td class="right">
                                            {{$student_monthly_fees ? $student_monthly_fees->total_fees:''}}
                                        </td>

                                    </tr>
                                    <tr>

                                        <td class="left">

                                            <strong>Paid Amount:&nbsp;&nbsp;</strong>
                                            
                                        </td>

                                        <td class="right">{{$student_monthly_fees ? $student_monthly_fees->paid_amount:''}}</td>

                                    </tr>

                                    <tr>

                                        <td class="left">

                                            <strong>Due Amount:&nbsp;&nbsp;</strong>
                                            
                                        </td>

                                        <td class="right">{{$student_monthly_fees ? $student_monthly_fees->due_amount:''}}</td>

                                    </tr>

                                  <!--  <tr>

                                        <td class="left">

                                            <strong>Final Total:&nbsp;&nbsp;</strong>
                                            {{$student_monthly_fees ? $student_monthly_fees->final_amount:''}}
                                        </td>

                                        <td class="right">

                                            <strong></strong>

                                        </td>

                                    </tr>--->

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!---Model -------------->
 <div class="modal fade" id="Pay" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <div align="left">
            <h6>Invoice No. {{$student_monthly_fees ? $student_monthly_fees->invoice_no:'' }}</h6>
          </div>
          <div align="right"> <button type="button" class="close" data-dismiss="modal">&times;</button></div>
        </div>
        <div class="modal-body">
          <form method="get" action="{{ route('student.payment-monthly') }}" id="payment">
                <div class="row">
                  <div class="col-md-6">
                      <lable>Payable Amount:</lable>
                      <input type="text" name="payable_amount" id="payable_amount" class="form-control" value="{{$student_monthly_fees ? $student_monthly_fees->due_amount:''}}" readonly="">
                  </div>
                  <div class="col-md-6">
                      <lable>Paid Amount:</lable>
                      <input type="text" name="paid_amount" id="paid_amount" class="form-control">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <lable>Due Amount:</lable>
                    <input type="number" name="due_amount" id="due_amount" class="form-control" readonly="">
                    <input type="hidden" name="id" id="id" class="form-control" value="{{$student_monthly_fees ? $student_monthly_fees->id:''}}">
                  </div>
                </div>
                <br>
                  <div class="col-md-4">
                  <input type="submit" class="form-control btn btn-success">
                </div> 
          </form>                      
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready( function() {
        $(document).on("change keyup", "#paid_amount", function() {

            var main = $('#payable_amount').val();
            var disc = $('#paid_amount').val();
            var due = main - disc;
            var paid = $('#paid_amount').val();
      
              
              $('#due_amount').val(due);
              $('#final_amount').val(paid);
              if (due < 0) {
                  alert('Paid Amount should be less or equal to Payable Amount!');
                    $(this).val('');
                    $('#due_amount').val('');
                    return false;
              }
          });
        });
  </script>
  <script type="text/javascript">
    $(document).ready( function() {
   if ($("#payment").length > 0) {
    $("#payment").validate({
        rules: {
            paid_amount: {
                required: true,
                
            },

        },

    messages: {

      paid_amount: {
        required: "Please Enter Paid Amount",
      },
          
    },
    })
  }    
});
</script>