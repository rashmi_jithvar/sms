@extends('layouts.app') @section('content')
<style>
  .card {
    padding:20px;
  }

</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('student') }}">Student List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Student Detail</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-md-12">
    <div class="profile-content">
        <div class="row">
            <div class="card-body">
                <div class="card-topline-aqua">
                    <header></header>
                </div>
                <div class="white-box">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane fontawesome-demo show active" id="customer">
                            <div id="biography">
                                <div class="customer-view">
                                   <!-- <p>
                                        <a class="btn btn-success" href="/app/customer/create">New Customer</a>&nbsp;<a class="btn btn-primary" href="/app/customer/update?id=5470">Update</a>&nbsp; <a class="btn btn-warning" href="javascript:createShopify();">Create Shopify</a>
                                        <a class="btn btn-danger pull-right" href="javascript:createComment();">
                                            <i class="fa fa-comment"></i>
                                        </a>
                                    </p>-->

                                    <div class="card">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <small class="text-muted"></small>
                                                <small class="text-muted"></small>
                                                <p style="font-weight: bold;">
                                                    Student Detail
                                                    <!--<label class="pull-right badge badge-success">Default</label>
                                                    <label class="pull-right badge badge-success">Active</label>-->
                                                </p>
                                                <hr>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 >Student's Name: </h6>
                                                <p class="m-b-0">{{$student ? $student->first_name:''}}&nbsp;&nbsp;{{$student ? $student->last_name:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Gender: </h6>
                                                <p class="m-b-0">
                                                    {{$student ? $student->gender:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Date Of Birth: </h6>
                                                <p class="m-b-0">
                                                    {{date("d-m-Y ",strtotime($student ? $student->dob:'' ))}} </p>
                                            </div>                                            

                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6>Class: </h6>
                                                <p class="m-b-0">{{$student->hasClass ? $student->hasClass->name:'' }}</p>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Section: </h6>
                                                <p class="m-b-0">{{$student->hasSection ? $student->hasSection->name:'' }}</p>
                                            </div>



                                            <div class="col-md-4">
                                                <h6>Category: </h6>
                                                <p class="m-b-0">{{$student->hasCategory ? $student->hasCategory->name:'' }}</p>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <h6>Father's Name: </h6>
                                                        <p class="m-b-0">{{$father ? $father->name:''}}
                                                        </p>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h6>Father Contact no: </h6>
                                                        <p class="m-b-0">{{$father ? $father->mobile:''}}
                                                        </p>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h6>Father's Email: </h6>
                                                        <p class="m-b-0">{{$father ? $father->email:''}}
                                                             </p>
                                                    </div>

                                                </div>
                                             

                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h6>Father's Occupation: </h6>
                                                        <p class="m-b-0">{{$father ? $father->occupation:''}}
                                                             </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h6>Mother's Name: </h6>
                                                        <p class="m-b-0">{{$mother ? $mother->name:''}}
                                                        </p>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h6>Mother's Contact no: </h6>
                                                        <p class="m-b-0">{{$mother ? $mother->mobile:''}}
                                                        </p>
                                                    </div>

                                                    

                                                </div>
                                                <hr>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                        <h6>Mother's Email: </h6>
                                                        <p class="m-b-0">{{$mother ? $mother->email:''}}
                                                            </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h6>Mother's Occupation: </h6>
                                                        <p class="m-b-0">{{$mother ? $mother->occupation:''}}
                                                           </p>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h6>State:</h6>
                                                        <p class="m-b-0">{{$student ? $student->state:''}}
                                                        </p>
                                                    </div>



                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h6>City: </h6>
                                                        <p class="m-b-0">{{$student ? $student->city:''}}
                                                            </p>
                                                    </div>                                                  
                                                    <div class="col-md-4">
                                                        <h6>Pincode: </h6>
                                                        <p class="m-b-0">{{$student ? $student->pincode:''}}
                                                           </p>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <h6>Address:</h6>
                                                        <p class="m-b-0">{{$student ? $student->address:''}}
                                                        </p>
                                                    </div>
                                                </div>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
    <div class="row">
         @foreach($monthly_fees as $fees)
            <div class="col-sm-4">
                <?php 
                    if($fees->status == '1'){
                        $style = "border:2px solid #97c76d";
                        $font = "badge-success";
                        
                        $status ='Paid';
                    }
                    else{
                        $style = "border:2px solid #e68e87";
                        $font = "badge-danger";
                        $status ='UnPaid';
                    }
                ?>  
                <a href="{{ url('student/view-invoice',$fees->id) }}">
                <div class="card text-white bg-light" style="<?=$style; ?>">
                        <div class="card-body" style="padding: 1px 1px;">
                            <div class="pull-left">
                                <h6 class="card-title">Month: 
                                   
                                     @foreach($month as $key => $mon) 
                                            @if($fees->month_id == $key)
                                            {{$mon}}
                                            @endif
                                     @endforeach 
                                    
                                </h6>
                                <p style="color: black">Due Amount : {{$fees ? $fees->total_fees:''}}</p>
                            </div>
                            <div class="pull-right">
                                <h6 class="">Fee Amount&nbsp;:&nbsp;{{$fees ? $fees->total_fees:''}}</h6>
                                <p style="color: black">Status:&nbsp;&nbsp;
                                <span class="badge <?=$font;?>">
                                    <strong></strong>
                                    {{$fees->status ? 'Paid':'Unpaid'}}
                                </span>
                               </p>
                            </div>
                        </div>
                        <div class="card-footer" style="padding: 3px 1px;">
                            @if($fees->status == '1')  
                            <div class="pull-left">
                                <p style="color: black">Payment On : {{date("d-m-Y ",strtotime($fees ? $fees->updated_at:'' ))}}</p>
                            </div>
                            @endif
                            <div class="pull-right">
                                @if($fees->status == '0')   
                                <p><a href="{{ url('student/view-invoice',$fees->id) }}" type="button" class="btn btn-sm btn-success" title="Pay Now">Pay Now</a> </p>
                                @else
                                    <b style="color:black;">{{$fees ? $fees->paid_amount:''}}</b>
                                @endif    
                            </div>
                            </div>                                                     
                    </div>
                </a>
            </div> 
            @endforeach 

    </div>  
</div>
@endsection