@extends('layouts.app')
@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }}  @endisset List</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    
                    <li class="breadcrumb-item"><a href="javascript:void(0)">@isset($title) {{ $title }}  @endisset  List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
              <div class="pull-left"> <h5>@isset($title) {{ $title }}  @endisset List</h5></div>
               <!--<div class="pull-right"> 
                <a class="btn btn-primary m-t-5"  href="{{ url('add-student-enquiry') }}">Add Enquiry</a>
              </div>   -->           
            </div>
            <div class="card-body table-border-style">
           
              <?php if(isset($abbr)){ ?>
            <div class="row">
        
            <div class="col-md-3">
                      <label>Addmission Start Date:</label>
                                <input type="text" class="form-control" name="dob" id="start_date_addmission">
                              
                                    <span class="text-danger name"></span> 
            </div>
            <div class="col-md-3">
                      <label> Addmission End Date:</label>
                                <input type="text" class="form-control" name="end_date" id="end_date_addmission">
                                    <span class="text-danger name"></span> 
            </div>  
            </div>   
          <?php } ?>
            <br>            
                <div class="table-responsive">
                    <div id="coverScreen"  class="LockOn" style="display: none;">
                    </div>  
                   <table id="lang-dt" class="table table-striped table-bordered nowrap">
                     <thead>
                       <tr>
                         <th>#</th>
                        
                         <th>Student Name</th>
                         <th>Roll No.</th>
                         <th>DOB</th>
                         <th>Gender</th>
                         <th>Class</th>
                         <th>Category</th>  
                         <th>Created At</th>                        
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                       <?php if(isset($abbr)){ ?>
                       <tr>
                         <th></th>
                         <th>
                            <select class="form-control" id="student" name="student">
                                          <?php echo $student_ddl; ?>
                            </select>
                         </th>
                         <th><input type="" class="form-control" name="roll_no" id="roll_no" ></th>
                         <th></th>

                         <th>
                            <select class="form-control"  id="gender" name="gender">
                                            <option value="">Select</option> 
                                            <option {{ ( old('gender') == 'male') ? 'selected' : '' }} value="male">Male</option>
                                            <option {{ ( old('gender') == 'female') ? 'selected' : '' }}  value="female">Female</option>
                            </select>                           
                         </th>
                         <th>
                            <select class="form-control" id="class" name="class">
                                          <?php echo $classddl; ?>
                            </select>
                         </th>                         
                         <th>
                            <select class="form-control" id="category" name="category">
                                          <?php echo $categoryddl; ?>
                            </select>                           
                         </th>

                         <th></th>
                         <th></th>
                         <th></th>
                        
                   
                  
                       </tr>
                       <?php } ?>                       
                    </thead>
                    <tbody id="report">
                        @php $i = 0
                        @endphp
                        @if(!empty($student))
                        @foreach($student as $stu)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          
                          <td><a href="{{url('student/show',$stu->id)}}" title="delete">{{$stu ? $stu->first_name:''}}&nbsp;&nbsp;{{$stu ? $stu->last_name:''}}</td>
                          <td>{{$stu ? $stu->roll_no:''}}</a></td>
                          <td>{{date("d-m-Y ",strtotime($stu ? $stu->dob:'' ))}}</td>
                          <td>
                             {{$stu ? $stu->gender:''}}
                          </td>                          
                          <td>
                             {{$stu->hasClass ? $stu->hasClass->name:''}}
                          </td>
                          <td>
                              {{$stu->hasCategory ? $stu->hasCategory->name:''}}
                          </td>
                          <td>{{date("d-m-Y ",strtotime($stu ? $stu->created_at:'' ))}}</td>                         
                          <td>
                          @if($stu->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                              <!---<a href="{{url('student/edit',$stu->id)}}" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <!--<a href="{{url('enquiry/show',$stu->id)}}" title="show"><i class="fa fa-eye" aria-hidden="true"></i></a>-->
                              <!--<a href="{{url('student/delete',$stu->id)}}" title="delete"><i class="fa fa-trash" aria-hidden="true" Onclick="return ConfirmDelete();"></i></a>-->
                              <a href="{{url('student/show',$stu->id)}}" title="Show"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a href="{{url('student/total-fees',$stu->id)}}" title="Invoice"><i class="fa fa-files-o" aria-hidden="true"></i>
</a>
                            
                          </td>
                          <?php 
                                if($stu->gender =='male'){
                                  $total_male[] = $stu->gender;
                                }
                                if($stu->gender =='female'){
                                  $total_female[] = $stu->gender;
                                }                                
                               
                               ?>
                        </tr>

                      @endforeach
                      @endif
                        <tr>
                          
                            <th colspan="7" class="text-right">Total Male: &nbsp; &nbsp; &nbsp;@if(isset($total_male)){{$total_male ? count($total_male):'0'}}@endif</th>
                            
                            <th colspan="2" class="text-left">Total Female:&nbsp; &nbsp; &nbsp;@if(isset($total_female)){{$total_female ? count($total_female):'0'}}@endif</th> 
                            <th></th>
                           
                      </tr>                        
                      </tbody>

                       <tbody id="filter"></tbody>   
                      
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
          $(document).on("change keyup blur", "#student,#class,#roll_no,#gender,#category", function() {
                $student = $class= $roll_no = $month =  $category = $gender = 0;
                if($('#student').val() !=''){
                  $student = $('#student').val();

                }
                if($('#class').val() !=''){
                  $class = $('#class').val();
                
                }  
                if($('#roll_no').val() !=''){
                  $roll_no = $('#roll_no').val();
                
                }  
                if($('#gender').val() !=''){
                  $gender = $('#gender').val();
                
                }  

                if($('#category').val() !=''){
                  $category = $('#category').val();
                
                }
                $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");    
                        $.ajax({
                                type : 'post',
                                url : "{{route('search-addmission')}}",
                                data:{ "_token": "{{ csrf_token() }}",'student':$student,'class':$class,'roll_no':$roll_no,'category':$category,'gender':$gender},
                                success:function(data){ 
                                 
                                    $('#filter').html(data);
                                     $('#report').hide(); 
                                    //$('#foot1').hide();
                                    $("#coverScreen").hide();  
                                }
                        });                                                              
          });
</script>
