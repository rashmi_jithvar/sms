@extends('layouts.app') @section('content')

<style type="text/css">
.error{
  color:red;
}
</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Fee Invoice</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                     <li class="breadcrumb-item"><a href="{{ url('student/show',$student_info->id) }}">Student </a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Invoice Detail</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-sm-12">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <h5>Invoice</h5>
                </div>
                  <div class="pull-right">
                    <!--<a href="" type="button" class="btn btn-sm btn-success" title="Pay Now" data-toggle="modal" data-target="#Pay">Pay Now</a> -->
                    <a href="{{ url('student/print-report',$student_info ? $student_info->id:'') }}" type="button" class="btn btn-sm btn-primary" title="Pay Now">Print <i class="fa fa-print" aria-hidden="true"></i>
</a> 
                  </div>
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">

                            <h6 class="mb-3"><br></h6>

                            <div>
                          
                            <div>
                                <strong></strong>
                            </div>
                              <div>School Name: &nbsp;{{$schoolInfo ? $schoolInfo->school_name:'' }}</div>
                              <div></div>
                              <div>Contact No.:&nbsp; {{$schoolInfo ? $schoolInfo->contact_no:'' }}</div>
                              
                             
                               <div>Address: {{$schoolInfo ? $schoolInfo->address:'' }}<br>
                                {{$schoolInfo ? $schoolInfo->pin_code:'' }}
                              </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <h6 class="mb-3"><strong>Invoice:</strong></h6>
                          

                                   </strong>
                                </h6>
                            <div>
                                <strong></strong>
                            </div>
                            <div>Student's Name: &nbsp;{{$student_info ? $student_info->first_name:'' }}&nbsp;{{$student_info ? $student_info->last_name:'' }}</div>
                            <div></div>
                            <div>Class:&nbsp; {{$student_info->hasClass ? $student_info->hasClass->name:'' }}</div>
                            <div>Section:&nbsp; {{$student_info->hasSection ? $student_info->hasSection->name:'' }}</div>
                           
                             <div>Phone: {{!empty($student_info->parent[0])? $student_info->parent[0]->mobile : (!empty($student_info->parent[1])? $student_info->parent[1]->mobile : '') }}
                            
      
                            </div>

                        </div>



                    </div>

                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Month</th>
                                    <th>Total Fees (INR)</th>
                                    <th>Discount (INR)</th>
                                    <th>Paid Fees(INR)</th>
                                    <th>Due Fees(INR)</th>
                                </tr>
                            </thead>
                            <tbody>
                              @php $i = 0
                              @endphp  
                              @if(isset($student_monthly_fees))
                                @foreach($student_monthly_fees as $monthly_fees)
                              @php $i++
                              @endphp                              
                                <tr>
                                  <td>{{$i}}</td>
                                    <td class="text-left">
                                        @foreach($month as $key => $mon) 
                                          @if($monthly_fees->month_id == $key)
                                            {{$mon}}
                                          @endif
                                     @endforeach 
                                    </td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->total_fees : '0.00'}}</td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->discount : '0.00'}}</td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->paid_amount : '0.00'}}
                                    </td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->due_amount : '0.00'}}
                                    </td>
                                </tr>

                                 @endforeach

                             @endif   
                            </tbody>

                        </table>

                    </div>

                    <div class="row">

                        <div class="col-lg-4 col-sm-5">
                        </div>

                        <div class="col-lg-4 col-sm-5 ml-auto">

                            <table class="table table-clear">

                                <tbody>

                                    <tr>

                                        <td class="left">

                                            <strong>Total Amount:&nbsp;&nbsp;</strong>
                                            <?= $total_fees ? number_format($total_fees,2) :'0.00'?>
                                        </td>

                                        <td class="right"></td>

                                    </tr>
                                    <tr>

                                        <td class="left">

                                            <strong>Total Discount:&nbsp;&nbsp;</strong>
                                            <?= $total_fees ? number_format($discount,2) :'0.00'?>
                                        </td>

                                        <td class="right"></td>

                                    </tr>                                    
                                    <tr>

                                        <td class="left">

                                            <strong>Paid Amount:&nbsp;&nbsp;
                                              <?= $paid_fees ? number_format($paid_fees,2):'0.00' ?></strong>
                                            
                                        </td>

                                        <td class="right"></td>

                                    </tr>

                                    <tr>

                                        <td class="left">

                                            <strong>Due Amount:&nbsp;&nbsp;
                                            <?= $due_fees ? number_format($due_fees,2):'0.00' ?></strong>
                                           
                                        </td>

                                        <td class="right"></td>

                                    </tr>

                                  <!--  <tr>

                                        <td class="left">

                                          
                                        </td>

                                        <td class="right">

                                            <strong></strong>

                                        </td>

                                    </tr>--->

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!---Model -------------->
 <div class="modal fade" id="Pay" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <div align="left">
            <h6>Invoice No.</h6>
          </div>
          <div align="right"> <button type="button" class="close" data-dismiss="modal">&times;</button></div>
        </div>
        <div class="modal-body">
          <form method="get" action="{{ route('student.payment-monthly') }}" id="payment">
                <div class="row">
                  <div class="col-md-6">
                      <lable>Payable Amount:</lable>
                      
                  </div>
                  <div class="col-md-6">
                      <lable>Paid Amount:</lable>
                      <input type="text" name="paid_amount" id="paid_amount" class="form-control">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <lable>Due Amount:</lable>
                    <input type="number" name="due_amount" id="due_amount" class="form-control" readonly="">
                  </div>
                  <div class="col-md-6">
                    <lable>Final Amount:</lable>
                    <input type="text" name="final_amount" id="final_amount" class="form-control" readonly="">
                    <input type="hidden" name="id" id="id" class="form-control" value="">
                  </div> 
                </div>
                <br>
                  <div class="col-md-4">
                  <input type="submit" class="form-control btn btn-success">
                </div> 
          </form>                      
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
@endsection
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">

    $(document).ready( function() {
        $(document).on("change keyup", "#paid_amount", function() {

            var main = $('#payable_amount').val();
            var disc = $('#paid_amount').val();
            var due = main - disc;
            var paid = $('#paid_amount').val();
      
              
              $('#due_amount').val(due);
              $('#final_amount').val(paid);
              if (due < 0) {
                    swal("Oopss!", "Paid Amount should be less or equal to Payable Amount!", "error");
                    $(this).val('');
                    $('#due_amount').val('');
                    $('#final_amount').val('');
                    return false;
              }
          });
        });
  </script>
  <script type="text/javascript">
    $(document).ready( function() {
   if ($("#payment").length > 0) {
    $("#payment").validate({
        rules: {
            paid_amount: {
                required: true,
                
            },

        },

    messages: {

      paid_amount: {
        required: "Please Enter Paid Amount",
      },
          
    },
    })
  }    
});
</script>