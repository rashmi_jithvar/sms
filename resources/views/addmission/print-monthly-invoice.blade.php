<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<style type="text/css">
.error{
  color:red;
}
</style>
<style type="text/css">
@media print {
    #printbtn {
        display :  none;
    }
    #back{
         display :  none;
    }
}
</style>
<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>
<br>
<div class="col-sm-12">
    <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <h5>Invoice&nbsp;&nbsp;{{$student_monthly_fees ? $student_monthly_fees->invoice_no:'' }}</h5>
                </div>
                  <div class="pull-right" style="float:right;">
                    <a  id="printbtn"  type="button" class="btn btn-sm btn-primary printPage" title="Pay Now">Print <i class="fa fa-print" aria-hidden="true"></i></a> 
                     <a href="{{ url('student/view-invoice',$student_monthly_fees->id) }}" id="back" type="button" class="btn btn-sm btn-info back" title="Back">Back </a> 
                  </div>
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">

                            <h6 class="mb-3"><br></h6>

                            <div>
                          
                            <div>
                                <strong></strong>
                            </div>
                              <div>School Name: &nbsp;{{$schoolInfo ? $schoolInfo->school_name:'' }}</div>
                              <div></div>
                              <div>Contact No.:&nbsp; {{$schoolInfo ? $schoolInfo->contact_no:'' }}</div>
                              
                             
                               <div>Address: {{$schoolInfo ? $schoolInfo->address:'' }}<br>
                                {{$schoolInfo ? $schoolInfo->pin_code:'' }}
                              </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <h6 class="mb-3"><strong>Invoice No:&nbsp;&nbsp;{{$student_monthly_fees ? $student_monthly_fees->invoice_no:'' }}</strong></h6>
                            <h6 class="mb-3"><strong>Month: &nbsp;
                                     @foreach($month as $key => $mon) 
                                            @if($student_monthly_fees->month_id == $key)
                                            {{$mon}}
                                            @endif
                                     @endforeach 
                                   </strong>
                                </h6>
                            <div>
                                <strong></strong>
                            </div>
                            <div>Student's Name: &nbsp;{{$student_monthly_fees->studentInfo ? $student_monthly_fees->studentInfo->first_name:'' }}&nbsp;{{$student_monthly_fees->studentInfo ? $student_monthly_fees->studentInfo->last_name:'' }}</div>
                            <div></div>
                            <div>Class:&nbsp; {{$student_monthly_fees->studentInfo->hasClass ? $student_monthly_fees->studentInfo->hasClass->name:'' }}</div>
                            <div>Section:&nbsp; {{$student_monthly_fees->studentInfo->hasSection ? $student_monthly_fees->studentInfo->hasSection->name:'' }}</div>
                           
                             <div>Phone: 
                            
                        
                           <!-- @if(isset($student_monthly_fees->studentInfo->student_parent))
                                @foreach($student_monthly_fees->studentInfo->student_parent as $parant)
                                    &nbsp;&nbsp;{{$parant->mobile}} 
                                   
                                @endforeach
                            @endif-->
                            </div>

                        </div>



                    </div>

                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Fee Type</th>
                                    <th>Frequency</th>
                                    <th>Fee (INR)</th>
                                    <th>Discount (INR)</th>
                                    <th>Total Fees(INR)</th>
                                </tr>
                            </thead>
                            <tbody>
                              @php $i = 0
                              @endphp  
                              @if(isset($student_fee_item))
                                @foreach($student_fee_item as $fees)
                              @php $i++
                              @endphp                              
                                <tr>
                                  <td>{{$i}}</td>
                                    <td class="text-left">{{$fees->getFees ? $fees->getFees->name : ''}}</td>
                                    <td class="text-left">{{$fees->getLabel ? $fees->getLabel->label : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->fee : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->discount : ''}}</td>
                                    <td class="text-left">{{$fees ? $fees->total : ''}}</td>
                                </tr>
                                 @endforeach
                             @endif   
                            </tbody>

                        </table>

                    </div>

                    <div class="row">

                        <div class="col-lg-4 col-sm-5">

                        </div>

                        <div class="col-lg-4 col-sm-5 ml-auto">

                            <table class="table table-clear">

                                <tbody>

                                    <tr>

                                        <td class="left">

                                            <strong>Total Amount:&nbsp;&nbsp;</strong>
                                            
                                        </td>

                                        <td class="right">{{$student_monthly_fees ? $student_monthly_fees->total_fees:''}}</td>

                                    </tr>
                                    <tr>

                                        <td class="left">

                                            <strong>Paid Amount:&nbsp;&nbsp;</strong>
                                            
                                        </td>

                                        <td class="right">{{$student_monthly_fees ? $student_monthly_fees->paid_amount:''}}</td>

                                    </tr>

                                    <tr>

                                        <td class="left">
                                            <strong>Due Amount:&nbsp;&nbsp;</strong>
                                        </td>

                                        <td class="right">{{$student_monthly_fees ? $student_monthly_fees->due_amount:''}}</td>

                                    </tr>

                                  <!--  <tr>

                                        <td class="left">

                                            <strong>Final Total:&nbsp;&nbsp;</strong>
                                            {{$student_monthly_fees ? $student_monthly_fees->final_amount:''}}
                                        </td>

                                        <td class="right">

                                            <strong></strong>

                                        </td>

                                    </tr>--->

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $('a.printPage').click(function(){
           window.print();
           $('.printPage').hide();
           return false;
});
</script>