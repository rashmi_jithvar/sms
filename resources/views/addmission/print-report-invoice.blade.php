
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

<style type="text/css">
@media print {
    #printbtn {
        display :  none;
    }
    #back{
         display :  none;
    }    
}
</style>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>
<br>

<div class="col-sm-12">
    <div class="content-wrapper">

        <div class="card">

            <div class="card-header">
                <div class="pull-left">
                    <h5>Invoice</h5>
                </div>
                  <div class="pull-right" style="float:right;">
                    <!--<a href="" type="button" class="btn btn-sm btn-success" title="Pay Now" data-toggle="modal" data-target="#Pay">Pay Now</a> -->
                    <a  id="printbtn"  type="button" class="btn btn-sm btn-primary printPage" title="Pay Now">Print <i class="fa fa-print" aria-hidden="true"></i></a> 
                    <a href="{{ url('student') }}"  type="button" id="back" class="btn btn-sm btn-info back" title="Back">Back </a> 
                  </div>
                                  
              
                </div>
            <div class="container">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">

                            <h6 class="mb-3"><br></h6>

                            <div>
                          
                            <div>
                                <strong></strong>
                            </div>
                              <div>School Name: &nbsp;{{$schoolInfo ? $schoolInfo->school_name:'' }}</div>
                              <div></div>
                              <div>Contact No.:&nbsp; {{$schoolInfo ? $schoolInfo->contact_no:'' }}</div>
                              
                             
                               <div>Address: {{$schoolInfo ? $schoolInfo->address:'' }}<br>
                                {{$schoolInfo ? $schoolInfo->pin_code:'' }}
                              </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <h6 class="mb-3"><strong>Invoice:</strong></h6>
                          

                                   </strong>
                                </h6>
                            <div>
                                <strong></strong>
                            </div>
                            <div>Student's Name: &nbsp;{{$student_info ? $student_info->first_name:'' }}&nbsp;{{$student_info ? $student_info->last_name:'' }}</div>
                            <div></div>
                            <div>Class:&nbsp; {{$student_info->hasClass ? $student_info->hasClass->name:'' }}</div>
                            <div>Section:&nbsp; {{$student_info->hasSection ? $student_info->hasSection->name:'' }}</div>
                           
                             <div>Phone: {{!empty($student_info->parent[0])? $student_info->parent[0]->mobile : (!empty($student_info->parent[1])? $student_info->parent[1]->mobile : '') }}
                            
      
                            </div>

                        </div>



                    </div>

                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Month</th>
                                    <th>Total Fees (INR)</th>
                                    <th>Discount (INR)</th>
                                    <th>Paid Fees(INR)</th>
                                    <th>Due Fees(INR)</th>
                                </tr>
                            </thead>
                            <tbody>
                              @php $i = 0
                              @endphp  
                              @if(isset($student_monthly_fees))
                                @foreach($student_monthly_fees as $monthly_fees)
                              @php $i++
                              @endphp                              
                                <tr>
                                  <td>{{$i}}</td>
                                    <td class="text-left">
                                        @foreach($month as $key => $mon) 
                                          @if($monthly_fees->month_id == $key)
                                            {{$mon}}
                                          @endif
                                     @endforeach 
                                    </td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->total_fees : '0.00'}}</td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->discount : '0.00'}}</td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->paid_amount : '0.00'}}
                                    </td>
                                    <td class="text-left">{{$monthly_fees ? $monthly_fees->due_amount : '0.00'}}
                                    </td>
                                </tr>

                                 @endforeach

                             @endif   
                            </tbody>

                        </table>

                    </div>

                    <div class="row">

                        <div class="col-lg-4 col-sm-5">
                        </div>

                        <div class="col-lg-4 col-sm-5 ml-auto">

                            <table class="table table-clear">

                                <tbody>

                                    <tr>

                                        <td class="left">

                                            <strong>Total Amount:&nbsp;&nbsp;</strong>
                                            <?= $total_fees ? number_format($total_fees,2) :'0.00'?>
                                        </td>

                                        <td class="right"></td>

                                    </tr>
                                    <tr>

                                        <td class="left">

                                            <strong>Total Discount:&nbsp;&nbsp;</strong>
                                            <?= $total_fees ? number_format($discount,2) :'0.00'?>
                                        </td>

                                        <td class="right"></td>

                                    </tr>                                    
                                    <tr>

                                        <td class="left">

                                            <strong>Paid Amount:&nbsp;&nbsp;
                                              <?= $paid_fees ? number_format($paid_fees,2):'0.00' ?></strong>
                                            
                                        </td>

                                        <td class="right"></td>

                                    </tr>

                                    <tr>

                                        <td class="left">

                                            <strong>Due Amount:&nbsp;&nbsp;
                                            <?= $due_fees ? number_format($due_fees,2):'0.00' ?></strong>
                                           
                                        </td>

                                        <td class="right"></td>

                                    </tr>

                                  <!--  <tr>

                                        <td class="left">

                                          
                                        </td>

                                        <td class="right">

                                            <strong></strong>

                                        </td>

                                    </tr>--->

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $('a.printPage').click(function(){
           window.print();
           $('.printPage').hide();
           return false;
});
</script>