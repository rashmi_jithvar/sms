@extends('layouts.app')
@section('content')
<?php 
use App\Models\Master;
?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Paid Fees Report List</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Paid Fees Report  List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
              <div class="pull-left"> <h5>Paid Fees Report List</h5></div>
               <!--<div class="pull-right"> 
                <a class="btn btn-primary m-t-5"  href="{{ url('add-student-enquiry') }}">Add Enquiry</a>
              </div>   -->           
            </div>
            <div class="card-body table-border-style">
              <div id="coverScreen"  class="LockOn" style="display: none;">
              </div>              
            <br>            
                <div class="table-responsive">
                   <table id="lang-dt" class="table table-striped table-bordered nowrap">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Student Name</th>
                         <th>Roll No.</th>
                         <th>Class</th>
                         <th>Gender</th>
                         <th>Category</th>
                         <th>Due Fees</th>   
                         <th>Month</th>  
                         <th>Amount</th>                        
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                       <tr>
                         <th></th>
                         <th>
                            <select class="form-control" id="student" name="student">
                                          <?php echo $student_ddl; ?>
                            </select>
                         </th>
                         <th><input type="" class="form-control" name="roll_no" id="roll_no" ></th>
                         <th>
                            <select class="form-control" id="class" name="class">
                                          <?php echo $classddl; ?>
                            </select>
                         </th>
                         <th>
                            <select class="form-control"  id="gender" name="gender">
                                            <option value="">Select</option> 
                                            <option {{ ( old('gender') == 'male') ? 'selected' : '' }} value="male">Male</option>
                                            <option {{ ( old('gender') == 'female') ? 'selected' : '' }}  value="female">Female</option>
                            </select>                           
                         </th>
                         <th>
                            <select class="form-control" id="category" name="category">
                                          <?php echo $categoryddl; ?>
                            </select>                           
                         </th>

                         <th></th>

                         <th>
                            <select class="form-control" id="month" name="month">
                                          <?php echo $monthddl; ?>
                            </select>                           
                         </th>
                         <th></th>
                         <th></th>
                         <th></th>
                       </tr>
                    </thead>
                    <tbody id="report">
                        @php $i = 0;
                        $paid_amount = 0;
                        $total_amount = 0;
                        @endphp
                        @if(!empty($student))
                        @foreach($student as $stu)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          
                          <td><a href="{{url('student/show',$stu->id)}}" title="delete">{{$stu ? $stu->first_name:''}}&nbsp;&nbsp;{{$stu ? $stu->last_name:''}}</td>
                          <td>{{$stu ? $stu->roll_no:''}}</a></td>
                          <td>
                             {{$stu ? $stu->class_name:''}}
                          </td>
                          <td>
                             {{$stu ? $stu->gender:''}}
                          </td>
                          <td>
                            <?php $category = Master::where('id',$stu->category)->first(); ?>
                             {{$category ? $category->name:''}}
                          </td>                           
                          <td>
                            {{$stu ? $stu->paid_amount:''}}
                          </td>  
                          <td>
                            
                          </td>
                          <td>
                            {{$stu ? $stu->total_amount:''}}
                          </td>
                          <td>

                          @if($stu->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                              <a href="{{url('student/show',$stu->id)}}" title="Show"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a href="{{url('student/total-fees',$stu->id)}}" title="Invoice"><i class="fa fa-files-o" aria-hidden="true"></i>
                              </a>
                              <?php $paid_amount+= $stu->paid_amount;
                                $total_amount+= $stu->total_amount;
                                if($stu->gender =='male'){
                                  $total_male[] = $stu->gender;
                                }
                                if($stu->gender =='female'){
                                  $total_female[] = $stu->gender;
                                }    

                                ?>
                            
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                                            <tfoot>
                                                <tr>
                                                  
                                                    <th colspan="7" class="text-right">Total Paid Amount: &nbsp; &nbsp; &nbsp;{{$paid_amount ? $paid_amount:'0.00'}}</th>
                                                    
                                                    <th colspan="2" class="text-left">Total Due Amount:&nbsp; &nbsp; &nbsp;{{$total_amount ? $total_amount:'0.00'}}</th> 
                                                    <th></th>
                                                    <th></th> 
                                                </tr>
                                                <tr>
                                                  
                                                    <th colspan="7" class="text-right">Total Male: &nbsp; &nbsp; &nbsp;{{$total_male ? count($total_male):'0'}}</th>
                                                    
                                                    <th colspan="2" class="text-left">Total Female:&nbsp; &nbsp; &nbsp;{{$total_female ? count($total_female):'0'}}</th> 
                                                    <th></th>
                                                    <th></th> 
                                                </tr>                                                

                                            </tfoot>                      
                      <tbody id="filter"></tbody>     
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
          $(document).on("change keyup blur", "#student,#class,#roll_no,#month,#gender,#category", function() {
                  $student = $class= $roll_no = $month =  $category = $gender = 0;
                if($('#student').val() !=''){
                  $student = $('#student').val();

                }
                if($('#class').val() !=''){
                  $class = $('#class').val();
                
                }  
                
                if($('#roll_no').val() !=''){
                  $roll_no = $('#roll_no').val();
                
                }  

                if($('#month').val() !=''){
                  $month = $('#month').val();
                
                }  

                if($('#gender').val() !=''){
                  $gender = $('#gender').val();
                
                }  

                if($('#category').val() !=''){
                  $category = $('#category').val();
                
                }
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");                    
                        $.ajax({
                                type : 'post',
                                url : "{{route('search-paid-amount')}}",
                               data:{ "_token": "{{ csrf_token() }}",'student':$student,'class':$class,'roll_no':$roll_no,'month':$month,'category':$category,'gender':$gender},
                                success:function(data){ 
                                    
                                    $('#filter').html(data);
                                    $('#report').hide();  
                                    $("#coverScreen").hide();  
                                }
                        });                                                              
          });
</script>

