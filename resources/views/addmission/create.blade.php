@extends('layouts.app') @section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .table thead th{
        background:white;
    }
</style>
<?php
if($addmission->id)
{
       $fee_temp_id = $fees_master->id;
}

else{
       $fee_temp_id = Session::get('fee_temp_id');
} 

?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Add Student</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('student') }}">Student List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

    <div class="main-panel">
        <div class="content-wrapper">
                <div id="coverScreen"  class="LockOn" style="display: none;"></div>  
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            <form id="addmission" method="POST" action="{{ route('student.store') }}">
                                @csrf @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }} @php Session::forget('message'); @endphp
                                </div>
                                @endif
                                <div class="form-group row">
                                    <div class="col-md-3">
                                            <h6><b>Student's Name:</b>&nbsp;&nbsp;{{$student_enquiry ?  $student_enquiry->first_name:''}} &nbsp;{{$student_enquiry ?  $student_enquiry->last_name:''}}</h6>
                                    </div>
                                        <div class="col-md-3">
                                           <h6><b>Category:</b>&nbsp;&nbsp;{{$student_enquiry->hasCategory ?  $student_enquiry->hasCategory->name:''}}</h6>
                                    </div>
                                        <div class="col-md-3">
                                             <h6><b>Class:</b>&nbsp;&nbsp;{{$student_enquiry->hasClass ?  $student_enquiry->hasClass->name:''}}</h6>
                                    </div>
                                  
                                </div>                       
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Section</label>
                                            <select class="form-control" id="section" name="section">
                                                <?php echo $section ?>
                                            </select>
                                            <span class="text-danger parant_id">{{ $errors->first('section') }}</span>
                                    </div>                                                                                                        

 
                                    <div class="col-md-4">
                                        <input type="hidden" name="category" id="category" value="{{$student_enquiry ?  $student_enquiry->category:''}}">
                                         <input type="hidden" name="class" id="class" value="{{$student_enquiry ?  $student_enquiry->admission_seeking:''}}">
                                         <input type="hidden" name="fee_temp_id" id="fee_temp_id" value="{{$fee_temp_id ? $fee_temp_id:''}}">
                                         <input type="hidden" name="student_id" id="student_id" value="{{$student_enquiry ? $student_enquiry->id:''}}">
                                    </div>                                   
                                </div>
                                <div class="form-group row">                                                                 

                                </div>                                
                                <h6>Add Fees</h6>
                                <div class="form-group row">

                                    <div class="col-md-3">
                                        <label>Fees Type</label>
                                        <select class="js-example-basic-single form-control" id="fees_type" name="fees_type">
                                            <?php  echo $fees_type ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('fees_type') }}</span>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Frequency</label>
                                            <input type="text" class="form-control" name="frequency" id="frequency" placeholder="Frequency" readonly="">
                                            <input type="hidden" class="form-control" name="frequency_id" id="frequency_id" placeholder="Frequency" readonly="">
                                            <span class="text-danger name">{{ $errors->first('frequency') }}</span>

                                    </div> 
                                    <div class="col-md-2">
                                        <label>Fee ({{$currency_name ? $currency_name->name:''}})</label>
                                            <input type="text" class="form-control" name="fee" id="fee" placeholder="Fee" readonly="">
                                            <span class="text-danger name">{{ $errors->first('frequency') }}</span>

                                    </div>   
                                    <div class="col-md-2">
                                        <label>Discount ({{$currency_name ? $currency_name->name:''}})</label>
                                            <input type="text" class="form-control" name="discount" id="discount" placeholder="Discount" value="0">
                                            <span class="text-danger name">{{ $errors->first('Discount') }}</span>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Total</label>
                                            <input type="text" class="form-control" name="total" id="total" placeholder="Total" readonly="">
                                            <span class="text-danger name">{{ $errors->first('total') }}</span>
                                    </div>       
                                    <div class="col-md-1">
                                        <br>
                                       <button type="button" class="btn btn-success icon icon-plus2" onclick="addFeesTemp();" id="add_new_item1">+</button>
                                    </div>                                                                                                                                                                                                                                                       
                                </div>  
                               <br>
                                </hr>                                  
                                <div class="form-group row">

                                     <div class="col-md-12">
                                    
                                    <div id="cart-table">
                                        <table class="table table-striped" id="cart-table-body">
                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Fees Type</strong></th>
                                                    <th><strong>Frequency </strong></th>
                                                    <th><strong>Fee ({{$currency_name ? $currency_name->name:''}})</strong></th>
                                                    <th><strong>Discount </strong></th>
                                                    <th><strong>Total </strong></th>

                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-right"></th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--<script type="text/javascript">
$(document).ready( function() {
   if ($("#addmission").length > 0) {
    $("#addmission").validate({
        rules: {
            section:{
                required:function () {
                       return ($("#section option:selected").val() != "0");
                   }
            }
        },
    messages: {
      section: {
        required: "Please Select Section",
      },  
    },
    })
  }    
}); 
</script>-->
<script type="text/javascript">
    $(document).ready( function() {
            loadFees({{$fee_temp_id}});
            $( "#fees_type" ).change(function() {
                $fees_type = $class =  $category = 0;
                if($( "#fees_type option:selected" )){
                     $fees_type = $('#fees_type').val();
                }
                else{
                    $("#fees_type").focus(); 
                    alert('Select Fees Type');
                }
                if($('#category').val() !=0){
                     $category = $('#category').val();
                }
                else{
                    $("#category").focus(); 
                    alert('Select Category');
                }                  
                if($('#class').val() !=0){
                     $class = $('#class').val();
                }
                else{
                    $("#class").focus(); 
                    alert('Select Class');
                }
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");                      
                if($fees_type &&  $class && $category !=0){
                        $.ajax({
                                type : 'post',
                                url : "{{route('get-fees')}}",
                                dataType: 'json',
                                data:{ "_token": "{{ csrf_token() }}",'category':$category,'class':$class,'fees_type':$fees_type},
                                success:function(data){ 
                                      if(data!=0){
                                        $('#frequency').val(data.label);
                                        $('#frequency_id').val(data.label_id);
                                        $('#fee').val(data.value);
                                        $('#total').val(data.value);

                                      }  
                                }
                        });
                    $("#coverScreen").hide();                          
                }
            });

        $(document).on("change keyup blur", "#discount", function() {
            var main = $('#fee').val();
            var disc = $('#discount').val();
            var discont = main - disc;
            if(discont!=''){
                 $('#total').val(discont);
            }
            else{
                 $('#total').val(main);
            }
          });
        });
    function addFeesTemp()
    {
        var fees_type = $('#fees_type').val();
        var frequency_id = $("#frequency_id").val();

        var fee = $('#fee').val();
        var discount = $('#discount').val();
        var total = $('#total').val();
        var fee_temp_id = $("#fee_temp_id").val();
                if(this.fees_type.value == "") {
                      alert("Please Select Fee Type");
                      this.fees_type.focus();
                      return false;
                    }                                     
                $.ajax({
                        type : 'post',
                        url : "{{route('add-fees-temp')}}",
                        data:{ "_token": "{{ csrf_token() }}",'fees_type':fees_type,'frequency_id':frequency_id,'fee':fee,'discount':discount,'total':total,'fee_temp_id':fee_temp_id},
                        success:function(data){ 
                        loadFees('<?= $fee_temp_id; ?>');
                         if (data == '0') {
                            swal("Oopss!", "You clicked the button!", "error");
                        } 
                        else {
                               //swal("Added Successfully!", "", "success");
                                $("#frequency_id").val("");
                                $("#fee").val("");
                                $("#discount").val("0");
                                $("#total").val("0");
                                $('#frequency').val("")
                                $('#fees_type').val("0");    
                        }
                }
        });
    }
    function loadFees($fee_temp_id)
    {
        $.ajax({
            type : 'get',
            url : "{{URL::to('load-fees')}}",
            data:{'fee_temp_id':$fee_temp_id},
            success:function(data){
            $('#cart-table-body').html(data);
           
            }
            });
      }    
function removeCart(url) {
    var x = confirm("Are you sure you want to delete?");
      if (x)
            $.ajax({
                    type : 'get',
                    url : url,
                    success:function(data){
                        loadFees('<?= $fee_temp_id; ?>');
                           if (data == '1') {
                        
                               //swal("Deleted Successfully!", "", "success");
                            } else {
                                swal("Oopss!", "You clicked the button!", "error");

                            }
                    }
            });
      else
        return false;
        } 
                        
</script>
