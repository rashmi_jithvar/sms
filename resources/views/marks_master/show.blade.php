@extends('layouts.datatable')

@section('content')
<?php
use App\Models\Master;
 ?>
<style>
  .card {
    padding:20px;
  }

</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('marks-master') }}">Marks List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Marks  Detail</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-md-12">
    <div class="profile-content">
        <div class="row">
            <div class="card-body">
                <div class="card-topline-aqua">
                    <header></header>
                </div>
                <div class="white-box">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane fontawesome-demo show active" id="customer">
                            <div id="biography">
                                <div class="customer-view">
                                   <!-- <p>
                                        <a class="btn btn-success" href="/app/customer/create">New Customer</a>&nbsp;<a class="btn btn-primary" href="/app/customer/update?id=5470">Update</a>&nbsp; <a class="btn btn-warning" href="javascript:createShopify();">Create Shopify</a>
                                        <a class="btn btn-danger pull-right" href="javascript:createComment();">
                                            <i class="fa fa-comment"></i>
                                        </a>
                                    </p>-->

                                    <div class="card">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <small class="text-muted"></small>
                                                <small class="text-muted"></small>
                                                <p style="font-weight: bold;">
                                                    Marks Detail
                                                    <!--<label class="pull-right badge badge-success">Default</label>
                                                    <label class="pull-right badge badge-success">Active</label>-->
                                                </p>
                                                <hr>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 >Class: </h6>
                                                <p class="m-b-0">{{$list ? $list->className->name:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Exam Type: </h6>
                                                <p class="m-b-0">
                                                    {{$list ? $list->examType->name:''}}</p>
                                            </div>
                                        </div>

                                       <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                        <table id="" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Subject</strong></th>
                                                    <!--<th><strong>Max Marks</strong></th>-->
                                                    <th><strong>Internal Marks</strong></th>
                                                    <th><strong>External</strong></th> 
                                                    <th><strong>Total</strong></th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                                @php $i = 0;
                                                $total = 0;
                                                @endphp

                                                @foreach($list->subjectHasMarks as $val)
                                                @php $i++
                                                @endphp
                                                <tr>

                                                    <td>{{$i}}</td>
                                                   
                                                    <td>{{$val->getSubject ? $val->getSubject->name:''}}</td>
                                                    <!--<td>{{$val ? $val->max_mark:''}}</td>-->
                                                    <td>{{$val ? $val->internal_marks:''}}</td>
                                                    <td>{{$val ? $val->external_marks:''}}</td>
                                                    <td>{{$val ? $val->total_marks:''}}</td>

        <?php  $total+= $val->total_marks; ?> 
                                                    
                                                @endforeach     

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                
                                             
                                                    <th class="text-right"></th>
                                                    <th></th>
                                                    <th>Total: &nbsp;&nbsp;{{$total ? $total:''}}</th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div> 
</div>
@endsection