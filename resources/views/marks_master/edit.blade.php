@extends('layouts.app') @section('content')

<style>
    .table thead th{
        background:white;
    }
    #external_field,#tb_ext{
        display: none;
    }
</style>
<?php
if($marks->id)
{
       $subject_key = $marks->id;
}

else{
       $subject_key = Session::get('subject_key');
} 

?>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Edit Marks</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('marks-master') }}">Marks Master</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div id="coverScreen"  class="LockOn" style="display: none;"></div>  
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            <form id="propertytype_form" method="POST" action="{{ route('marks-master.update',$marks->id) }}">
                                @csrf @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }} @php Session::forget('message'); @endphp
                                </div>
                                @endif
                        <input type="hidden" class="form-control" name="subject_key" id="subject_key" value="{{$subject_key}}">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Class</label>
                                        <select class="form-control" id="class" name="class">
                                            <?php echo $class ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('class') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Exam Type</label>
                                        <select class="form-control" id="exam_type" name="exam_type">
                                            <?php echo $exam_type ?>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('exam_type') }}</span>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <br> <br> 
                                         <label>Has External Marks?
                                            <input type="checkbox" class="form-control" name="is_external" id="is_external"></label>
                                        <span class="text-danger">{{ $errors->first('is_external') }}</span>
                                    </div>                                    
                                </div>
                                <h6>Add Master</h6>
                     <div class="form-group row">
                                    <div class="col-md-2">
                                        <label>Subject</label>
                                        <select class="form-control" id="subject" name="subject">
                                           
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('subject') }}</span>
                                    </div>
                                   <!-- <div class="col-md-2">
                                        <label>Max Marks</label>
                                        <input type="number" class="form-control" name="max_marks" id="max_marks" placeholder="Max Marks" min="0">
                                        <span class="text-danger name">{{ $errors->first('max_marks') }}</span>

                                    </div>-->
                                    <div class="col-md-2" id="external_field">
                                        <label>External Marks</label>
                                        <input type="number" class="form-control" name="external_marks" id="external_marks" placeholder="External Marks" min="0">
                                        <span class="text-danger name">{{ $errors->first('external_marks') }}</span>
                                    </div>
                                    <div class="col-md-2">
                                        <label id="lbl_max_marks">Max Marks</label>
                                        <input type="number" class="form-control" name="internal_marks" id="internal_marks" placeholder="Internal Marks" min="0">
                                        <span class="text-danger name">{{ $errors->first('internal_marks') }}</span>
                                    </div>   
                                                                     
                                     <div class="col-md-2">
                                         <br>
                                     <button type="button" class="btn btn-success icon icon-plus2" onclick="addMarks();" id="add_new_item1">+</button>
                                 </div>
                                </div>  
                               <br>
                                </hr>    

                                <div class="form-group row">
                                     <div class="col-md-8">
                                       <div id="cart-table">
                                           
                                        <table class="table table-striped" id="cart-table-body" >

                                            <thead>
                                                <tr>
                                                    <th><strong>#</strong></th>
                                                    <th><strong>Subject</strong></th>
                                                    <!---<th><strong>Max Marks</strong></th>-->
                                                    <th id="tb_ext"> <strong>External Marks</strong></th>
                                                    <th><strong>Internal Marks</strong></th>
                                                    
                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="cart-body">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th class="text-right"></th>
         
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php  $class_id = $marks ? $marks->class_id:0  ?>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    loadMarks({{$subject_key}});    
    getClass({{$class_id}});
    function getClass($class_id){
            
                $.ajax({
                    type : 'post',
                    url : "{{route('get-subject')}}",
                    data:{ "_token": "{{ csrf_token() }}",'class':$class_id},
                    success:function(data){
                    $('#subject').html(data);
                    }
                });
    }
        function loadMarks($subject_key)
    {
      
        $.ajax({
            type : 'get',
            url : "{{URL::to('load-marks')}}",
            data:{'subject_key':$subject_key},
            success:function(data){
                $('#cart-table-body').html(data);
              //$('#tab').html("sdferewr");
            }
            });
      }    

    function addMarks()
    {

        $subject = $max_marks = $internal_marks = $external_marks =  $is_external  = 0;
        if($('#subject').val()!=''){
            $subject = $('#subject').val();            
        }
        if($("#internal_marks").val()!=''){
            $internal_marks = $("#internal_marks").val();            
        }
        if($("#external_marks").val()!=''){
            $external_marks = $("#external_marks").val();            
        } 
        if($("#is_external").val().checked){
            $is_external = $("#is_external").val();            
        } 
                     
        $subject_key = $("#subject_key").val();

                $.ajax({
                        type : 'post',
                        url : "{{route('add-marks')}}",
                        data:{ "_token": "{{ csrf_token() }}",'subject':$subject,'subject_key':$subject_key,'internal_marks':$internal_marks,'external_marks':$external_marks,'is_external':$is_external},
                        success:function(data){ 
                        loadMarks('<?= $subject_key; ?>');
                         if (data == '0') {
                            swal("Oopss!", "You clicked the button!", "error");
                        } 
                        else {
                                //swal("Added Successfully!", "", "success");
                                $("#subject").val("0");
                                $("#is_external").val("0");
                                $("#external_marks").val("0");
                                $("#internal_marks").val("0");
                        }
                       
                }
        });
    }
function removeMarks(url) {
 
    var x = confirm("Are you sure you want to delete?");
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading..");      
      if (x)
            $.ajax({
                    type : 'get',
                    url : url,
                    success:function(data){
                     $("#coverScreen").show();    
                     $("#coverScreen").html("loading..");                         
                        loadMarks('<?= $subject_key; ?>');
                           if (data == '1') {
                               //swal("Deleted Successfully!", "", "success");
                            } else {
                                swal("Oopss!", "You clicked the button!", "error");

                            }
                        $("#coverScreen").hide();
                    }
            });
      else
        return false;
        }
/* get subject */
        $(document).on("change", "#class", function() {
            $class= 0;
            if($('#class').val()!=''){
                $class = $('#class').val();               
            }

            if($class!=0){
                $.ajax({
                    type : 'post',
                    url : "{{route('get-subject')}}",
                    data:{ "_token": "{{ csrf_token() }}",'class':$class},
                    success:function(data){
                        $('#subject').html(data);
                        }
                    });
                }
                else{
                    alert("Please Select Class");
                    this.class.focus();
                    return false;
                }
          }); 
/* is external */         
        $(document).on("click", "#is_external", function() {

                  $('#external_field').toggle();
                  $('#max_marks_fields').toggle();
                  $('#tb_ext').toggle();
                  $('#tb_int').toggle();
                  if($(this).is(':checked')==true){
                    $('#lbl_max_marks').html('Max Internal Marks')
                  }
                  else{
                    $('#lbl_max_marks').html('Max Marks')
                  }

          });          

      
</script>

