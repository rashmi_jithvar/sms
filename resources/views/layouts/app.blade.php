
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@isset($title) {{ $title }}  @endisset {{ config('app.name', 'School Management') }}</title>
    <!-- Favicon -->
    <link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('assets/fonts/fontawesome/css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/animation/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/notification/css/notification.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/plugins/animation/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/multi-select/css/multi-select.css') }}">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <script type="text/javascript">siteURL="{{URL::to('/')}}";</script>
    <style type="text/css">
        .none{display:none;}
        .required-icon {color:#FF0000;}
        .alert-empty { border: 1px solid #FF0000;}
        .none {display:none}
    </style>
    <style type="text/css">
      .LockOn {
        display: block;
        visibility: visible;
        position: absolute;
        z-index: 999;
        top: 0px;
        left: 0px;
        width: 105%;
        height: 105%;
        background-color:white;
        vertical-align:bottom;
        padding-top: 20%; 
        filter: alpha(opacity=75); 
        opacity: 0.75; 
        font-size:large;
        color:blue;
        font-style:italic;
        font-weight:400;
        background-image: url("../assets/plugins/lightbox2-master/images/loading.gif");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        text-align: center;
    }
    </style>    
    <style type="text/css">.error{
        color:red;
    }</style>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-lite.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> 
</head>

<body>
    <div id="app">
        @if (Auth::check())
        <!-- NAVIGATION -->
        @include('layouts.navigation')
        <!-- HEADER -->
        @include('layouts.header')
        <!-- header-user-list & header chat -->
        @include('layouts.rightNav')        
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- breadcrumb -->
                                @include('layouts.breadcrumb')
                                <!-- Page -->
                                <div class="row">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <main class="py-4">
            @yield('content')
        </main>
        @endif
    </div>
    <!-- Scripts -->
    <script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('assets/js/menu-setting.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/js/jquery.quicksearch.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-select-custom.js') }}"></script>
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/core.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/charts.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/animated.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/maps.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/worldLow.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/plugins/chart-am4/js/continentsLow.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/js/pages/dashboard-analytics.js') }}"></script> -->
    <script src="{{ asset('assets/js/script.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <!-- include libraries(jQuery, bootstrap) -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
    <script>
    $('#summernote').summernote({height: 250});
    </script>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-validation.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
   
    <script>
        function ConfirmDelete()
        {
          var x = confirm("Are you sure you want to delete?");
          if (x)
              return true;
          else
            return false;
        }
    </script> 
    <!--<script>
        function ConfirmContact()
        {
           var father_contact_no = $('#father_contact_no').val();
           var mother_contact_no = $('#mother_contact_no').val();
           alert(father_contact_no);
           if(father_contact_no || mother_contact_no !=''){
               return true;
           }
           else{

               alert('Contact no. Required..!!');
               return false;
           }
        }
    </script>   -->    
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready( function() {
  $( "#dob,#start_date_addmission,#end_date_addmission,#end_date,#start_date" ).datepicker({ 
    dateFormat: "d/m/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '-100:+0',


  });
$('#end_date_addmission,#start_date_addmission').datepicker().on('change keyup', function(e) {
            var start_date = 0;
            var end_date = 0;
             if($('#start_date_addmission').val() !=""){
               var start_date = $('#start_date_addmission').val();
             }
            if($('#end_date_addmission').val() !=""){
              var end_date = $('#end_date_addmission').val();
            }
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading.."); 
                      $.ajax({
                              type : 'post',
                              url : "{{route('search-addmission')}}",
                              data:{ "_token": "{{ csrf_token() }}",'start_date':start_date,'end_date':end_date},
                              success:function(data){ 
                              
                               $('#report').hide();
                               $('#filter').html(data);
                               $("#coverScreen").hide();  
                      }
              });
});
$('#end_date,#start_date').datepicker().on('change keyup', function(e) {
            var start_date = 0;
            var end_date = 0;
             if($('#start_date').val() !=""){
               var start_date = $('#start_date').val();
             }
            if($('#end_date').val() !=""){
              var end_date = $('#end_date').val();
            }
                 $("#coverScreen").show();    
                 $("#coverScreen").html("loading.."); 
                      $.ajax({
                              type : 'post',
                              url : "{{route('search-enquiry')}}",
                              data:{ "_token": "{{ csrf_token() }}",'start_date':start_date,'end_date':end_date},
                              success:function(data){ 
                              
                               $('#report').hide();
                               $('#filter').html(data);
                                $("#coverScreen").hide();  
                      }
              });
});
});

</script>



    @yield('script')
</body>

</html>