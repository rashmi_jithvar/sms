<?php 
 use App\User;
 $routeArray = User::permission();
 //dd($routeArray);
 $routeArray = (!empty($routeArray))? $routeArray : [];
?>
<style>
  li{
    text-decoration-style: none;
  }
</style>
<nav class="pcoded-navbar menupos-fixed">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="{{ route('home') }}" class="b-brand">
                    <div class="b-bg">
                        SM
                    </div>
                    <span class="b-title">School Management</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
            </div>
            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">
                    <li class="nav-item pcoded-menu-caption">
                        <label></label>
                    </li>
                    <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="{{ route('home') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-home"></i></span> Dashboard</a>
                    </li>
                    
                    <!--<li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="#!" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-file"></i></span><span class="pcoded-mtext">CMS</span></a>
                        <ul class="pcoded-submenu">
                          @if(in_array("PagesController@page-list", $routeArray)) 
                          <li class=""><a href="{{ route('page-list') }}" class="">Page List</a></li>
                           
                          @endif

                          @if(in_array("PagesController@index", $routeArray))
                           <li class=""><a href="{{ route('pages') }}" class="">Add Menu</a></li>  
                           @endif
                             <li class=""><a href="{{ route('add-sub-menus') }}" class="">Add Sub Menu</a></li> 
                           @if(in_array("PagesController@add-pages", $routeArray)) 
                            <li class=""><a href="{{ route('add-pages') }}" class="">Add Pages</a></li>
                            @endif

                            @if(in_array("PagesController@teams", $routeArray))
                            <li class=""><a href="{{ route('teams') }}" class="">Team</a></li>
                            @endif

                            @if(in_array("PagesController@sliders", $routeArray))
                            <li class=""><a href="{{ route('sliders') }}" class="">Slider</a></li>
                            @endif

                        </ul>
                    </li>-->
                   

                   @if(in_array("UserController@show", $routeArray)) 
                    <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="#" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-users"></i></span><span class="pcoded-mtext">User</span></a>
                        <ul class="pcoded-submenu">
                           
                                  <li class="nav-item">
                                      @if(in_array("RoleController@index", $routeArray))
                                      <a href="{{ url('roles') }}" class="nav-link ">
                                          <span class="title">Roles</span>
                                      </a>
                                      @endif  
                                  </li>
                                  <li class="nav-item">
                                      @if(in_array("PermissionGroupController@index", $routeArray))
                                      <a href="{{ url('permission_group') }}" class="nav-link ">
                                          <span class="title">Permission Group</span>
                                      </a>
                                     @endif
                                  </li>
                                  <li class="nav-item">
                                    @if(in_array("PermissionController@index", $routeArray))
                                      <a href="{{ url('permission') }}" class="nav-link ">
                                          <span class="title">Permission</span>
                                      </a>
                                    @endif
                                  </li>
                                <li class="nav-item">
                                    @if(in_array("RolePermissionController@index", $routeArray))
                                      <a href="{{ url('role_permission/create') }}" class="nav-link ">
                                          <span class="title">Role Permission</span>
                                      </a>
                                     @endif
                                  </li>
                                  <li class="nav-item">
                                      @if(in_array("UserController@index", $routeArray))
                                      <a href="{{ url('user') }}" class="nav-link">
                                          <span class="title">User</span>
                                      </a>
                                       @endif
                                  </li>
                              </ul>
                    </li>
                    <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="#" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-users"></i></span><span class="pcoded-mtext">Master</span></a>
                        <ul class="pcoded-submenu">
                           
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'class']) }}" class="nav-link ">
                                          <span class="title">Class</span>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                     
                                      <a href="{{ route('master',['abbr'=>'section']) }}" class="nav-link ">
                                          <span class="title">Section</span>
                                      </a>
                                    
                                  </li>
                                  <li class="nav-item">
                                    <a href="{{ route('master',['abbr'=>'session']) }}" class="nav-link ">
                                          <span class="title">Session</span>
                                      </a>
                                  
                                  </li>
                                <li class="nav-item">
                                    <a href="{{ route('master',['abbr'=>'subject']) }}" class="nav-link ">
                                          <span class="title">Subject</span>
                                      </a>
                                    
                                  </li>
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'fees']) }}" class="nav-link">
                                          <span class="title">Fees Type</span>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'category']) }}" class="nav-link">
                                          <span class="title">Category</span>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'grade']) }}" class="nav-link">
                                          <span class="title">Grade</span>
                                      </a>
                                  </li>    
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'discount']) }}" class="nav-link">
                                          <span class="title">Discount Type</span>
                                      </a>
                                  </li>   
                                  <li class="nav-item">
                                      <a href="{{ route('master',['abbr'=>'exam-type']) }}" class="nav-link">
                                          <span class="title">Exam Type</span>
                                      </a>
                                  </li>                                      
                                  <li class="nav-item">
                                      <a href="{{ route('fees-master') }}" class="nav-link">
                                          <span class="title">Fee Master</span>
                                      </a>
                                  </li> 
                                  <li class="nav-item">
                                      <a href="{{ route('subject-master') }}" class="nav-link">
                                          <span class="title">Subject Master</span>
                                      </a>
                                  </li> 
                                  <li class="nav-item">
                                      <a href="{{ route('marks-master') }}" class="nav-link">
                                          <span class="title">Marks Master</span>
                                      </a>
                                  </li>  
                                  <li class="nav-item">
                                      <a href="{{ route('grade-master') }}" class="nav-link">
                                          <span class="title">Grade Master</span>
                                      </a>
                                  </li>                                                                                                                                                                                                                    
                              </ul>
                    </li>                          

                  <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item">
                        <a href="{{ route('student-enquiry') }}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-list"></i></span><span class="pcoded-mtext">Addmission Enquiry</span></a>
        
                  </li> 
                  <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item">
                        <a href="{{ route('student') }}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-list"></i></span><span class="pcoded-mtext">Manage Student</span></a>
        
                  </li> 
                  <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item">
                        <a href="{{ route('student-result-report') }}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-list"></i></span><span class="pcoded-mtext">Manage Student Result </span></a>
        
                  </li>                   
                    <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="#" class="nav-link"><span class="pcoded-micon"><i class="fa fa-bar-chart" aria-hidden="true"></i>
                            </span><span class="pcoded-mtext">Reports</span></a>
                        <ul class="pcoded-submenu">
                           
                                  <li class="nav-item">
                                      <a href="{{ route('enquiry-report',['abbr'=>'report']) }}" class="nav-link ">
                                          <span class="title">Enquiry Report</span>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                     
                                      <a href="{{ route('enquiry-addmission-report' ,['abbr'=>'report']) }}" class="nav-link ">
                                          <span class="title">Addmission Report</span>
                                      </a>
                                    
                                  </li>
                                  <li class="nav-item">
                                    <a href="{{ route('due-fee-report') }}" class="nav-link ">
                                          <span class="title">Due Fees Report</span>
                                      </a>
                                  
                                  </li>
                                <li class="nav-item">
                                    <a href="{{ route('paid-fee-report') }}" class="nav-link ">
                                          <span class="title">Total Fees Report</span>
                                      </a>
                                    
                                  </li>
                                                                                         
                              </ul>
                    </li>                                    
                @endif                                                 
                 </ul>

                   <!-- <li data-username="dashboard default ecommerce sales Helpdesk ticket CRM analytics crypto project"
                        class="nav-item pcoded-hasmenu">
                        <a href="#!" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-home"></i></span><span class="pcoded-mtext">Property Management</span></a>
                        <ul class="pcoded-submenu">
                           
                                  <li class="nav-item">
                                      
                                      <a href="{{ url('rent-sell') }}" class="nav-link ">
                                          <span class="title">Post Property</span>
                                      </a>
                                     
                                  </li>
                              </ul>
                          </li>   ---->                    
            </div>
        </div>
    </nav>