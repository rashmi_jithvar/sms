<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.phoenixcoded.net/elite-able/bootstrap/default/dt_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Mar 2019 12:30:36 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@isset($title) {{ $title }}  @endisset {{ config('app.name', 'School Management') }}</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="Elite Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
<meta name="keywords" content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, Elite Able, Elite Able bootstrap admin template">
<meta name="author" content="Phoenixcoded" />

<link rel="icon" href="http://html.phoenixcoded.net/elite-able/bootstrap/assets/images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/fontawesome-all.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/plugins/animation/css/animate.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/plugins/data-tables/css/datatables.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="">
	    <div id="app">
        @if (Auth::check())
        <!-- NAVIGATION -->
        @include('layouts.navigation')
        <!-- HEADER -->
        @include('layouts.header')
        <!-- header-user-list & header chat -->
        @include('layouts.rightNav')        
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- breadcrumb -->
                                @include('layouts.breadcrumb')
                                <!-- Page -->
                                <div class="row">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <main class="py-4">
            @yield('content')
        </main>
        @endif
    </div>
<script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
<script src="{{ asset('assets/js/menu-setting.min.js') }}"></script>

<script src="{{ asset('assets/plugins/data-tables/js/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/data-basic-custom.js') }}"></script>

</body>

<!-- Mirrored from html.phoenixcoded.net/elite-able/bootstrap/default/dt_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Mar 2019 12:30:37 GMT -->
</html>
