<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{'Arnav Reality'}}</title>
    <link rel="icon" href="./assets/images/favicon.ico" type="image/x-icon">
    <link href="{{ asset('assets/fonts/fontawesome/css/fontawesome-all.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/plugins/notification/css/notification.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/multi-select/css/multi-select.css') }}">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <script type="text/javascript">
        siteURL = "{{URL::to('/')}}";
    </script>
    <style type="text/css">
        .none {
            display: none;
        }
        
        .required-icon {
            color: #FF0000;
        }
        
        .alert-empty {
            border: 1px solid #FF0000;
        }
        
        .none {
            display: none
        }
        
        #basic {
            display: none;
        }
        label {
            font-weight: bold;
        }
        
        .js-example-placeholder-multiple {
            position: fixed;
        }
        body{
              
              overflow-x: hidden;
        }
        .error{
            color:red;
        }
    </style>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-lite.min.css" rel="stylesheet">
    <!-- steps -->
    <link rel="stylesheet" href="{{ asset('step-form/assets/css/pages/steps.css') }}">
    <link href="{{ asset('step-form/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('step-form/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('step-form/assets/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('step-form/assets/plugins/material/material.min.css') }}">
    <link rel="stylesheet" href="{{ asset('step-form/assets/css/material_style.css') }}">
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" />
    <style type="text/css">

    </style>
</head>

<body>
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <div id="app">
        @if (Auth::check())
        <!-- NAVIGATION -->
        @include('layouts.navigation')
        <!-- HEADER -->
        @include('layouts.header')
        <!-- header-user-list & header chat -->
        @include('layouts.rightNav')
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- breadcrumb -->
                                @include('layouts.breadcrumb')
                                <!-- Page -->
                                <div class="row">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <main class="py-4">
            @yield('content')
        </main>
        @endif
    </div>
    <!-- Scripts -->
    <script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('assets/js/menu-setting.min.js') }}"></script>
    <script src="{{ asset('assets/js/script.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- include libraries(jQuery, bootstrap) -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
    <script>
        $('#summernote').summernote({
            height: 250
        });
    </script>

    <script src="{{ asset('step-form/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
        <!-- Material -->
    <script src="{{ asset('step-form/assets/plugins/material/material.min.js') }}"></script>
    <!-- steps -->
    <script src="{{ asset('step-form/assets/plugins/steps/jquery.steps.js') }}"></script>
    <script src="{{ asset('step-form/assets/js/pages/steps/steps-data.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/js/jquery.quicksearch.js') }}"></script>
    <script src="{{ asset('assets/plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <script src="{{ asset('assets/js/pages/form-select-custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript">
        $('a[href="#next"]').click(function() {
            $step =$("#example-advanced-form").steps("getCurrentIndex");

            $you_are = $property_for = $property_type = $property_type = $subproperty_type =$sub_subproperty_type = $agreementType = $multiplePropertyUnit = 0;
            $you_are = $('#you_are').val();
            $property_for = $('#property_for').val();
            $property_type = $('#property_type').val();
            $subproperty_type =$('#subproperty_type').val();
            $sub_subproperty_type = $('#sub_subproperty_type').val();
            $willing_to = $('#willing_to').val();
            $customer_name = $('#customer_name').val();
            $contact_no = $('#contact_no').val();
            if($('#multiplePropertyUnit').val() !=''){
                $multiplePropertyUnit = $('#multiplePropertyUnit').val();
            }
            $email = $('#email').val();

            if($('#property_id').val()!=''){
                $property_id = $('#property_id').val();
            }
            else{
                $property_id = 0;
            }
            var $willing_to = new Array();
              var n1 = jQuery("#willing_to:checked").length;
              if (n1 > 0){
                  jQuery("#willing_to:checked").each(function(){
                      $willing_to.push($(this).val()); 
                  });
              } 
            $agreementType = $("#agreement_type:checked").val();  

            if($step==1){                                                                                           $('#you_are').attr('required', true); 

                    $.ajax({
                        type : 'post',
                        url : "store-basic-property",
                        data:{ "_token": "{{ csrf_token() }}",'you_are':$you_are,'property_for':$property_for,'property_type':$property_type,'subproperty_type':$subproperty_type,'sub_subproperty_type':$sub_subproperty_type,'willing_to':$willing_to,'agreementType':$agreementType,'property_id':$property_id,'customer_name':$customer_name,'contact_no':$contact_no,'email':$email,'multiplePropertyUnit':$multiplePropertyUnit},
                        success:function(data){ 
                         
                             $('#property_id').val(data);  
                            
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 

                        }
                });     
            }

            $plot_area = $plot_unit = $buildup_area= $buildup_unit = $carpet_area = $carpet_unit = 
            $bedroom_type= $bathroom = $balcony = $kitchen_type = $bathroom_type = $facing_type = 
            $flooring_type= $power_backup = $floor_type= $property_floor = $parking_place = $age = 
            $avaiable_from= $furniture_type = $close_parking =$furnished_type = $open_parking = 0;
            $plot_area = $('#plot_area').val();
            $plot_unit = $('#plot_unit').val();
            $buildup_area = $('#buildup_area').val();
            $buildup_unit = $('#buildup_unit').val();
            $carpet_area = $('#carpet_area').val();
            $carpet_unit = $('#carpet_unit').val();

            if($('#bedroom_type').val()!=''){
                $bedroom_type = $('#bedroom_type').val();
            }
            
            if($('#bathroom').val() !=''){
                $bathroom = $('#bathroom').val();
            }
            
            if($('#balcony').val() !=''){
                $balcony = $('#balcony').val();
            }
            
            if($('#kitchen_type').val() !=''){
                $kitchen_type = $('#kitchen_type').val();
            }
            if($('#bathroom_type').val()!=''){
                $bathroom_type = $('#bathroom_type').val();
            }
            if($('#facing_type').val()!=''){
                $facing_type = $('#facing_type').val();
            }
            if($('#flooring_type').val()!=''){
                $flooring_type =$('#flooring_type').val();
            }
            if($('#power_backup').val()!=''){
               $power_backup = $('#power_backup').val();  
            }
            if($('#floor_type').val() !=''){
                $floor_type = $('#floor_type').val();
            }
            if($('#property_floor').val()!=''){
                $property_floor = $('#property_floor').val();
            }
            if($('#furnished_type').val()!=''){
                $furnished_type = $('#furnished_type').val();
            }
            if($('#parking_place').val()!=''){
                $parking_place = $('#parking_place').val();
            }
            
            $age = $('#age').val();
            $avaiable_from = $('#avaiable_from').val();

              var $furniture_type = new Array();
              $("#furniture_type").find("input").each(function(){
                   if($(this).attr("data-id")){
                   // alert($(this).attr("data-id"));
                   if($(this).val()){
                     var type = $(this).attr("data-id") +'-'+ $(this).val(); 
                      $furniture_type.push(type);
                   }
                }
               });                

                   
           $close_parking =$('#close_parking').val();
           $open_parking =$('#open_parking').val(); 
            if($step==2 && $property_id!=0){
                    $.ajax({
                        type : 'post',
                        url : "store-basic-property",
                        data:{ "_token": "{{ csrf_token() }}",'plot_area':$plot_area,'plot_unit':$plot_unit,'buildup_area':$buildup_area,'buildup_unit':$buildup_unit,'carpet_area':$carpet_area,'carpet_unit':$carpet_unit,'bedroom_type':$bedroom_type,'bathroom':$bathroom,'balcony':$balcony,'kitchen_type':$kitchen_type,'bathroom_type':$bathroom_type,'facing_type':$facing_type,'flooring_type':$flooring_type,'power_backup':$power_backup,'floor_type':$floor_type,'property_floor':$property_floor,'furnished_type':$furnished_type,'parking_place':$parking_place,'age':$age,'avaiable_from':$avaiable_from,'property_id':$property_id,'furniture_type':$furniture_type,'close_parking':$close_parking,'open_parking':$open_parking},
                        success:function(data){ 
                           
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                    
                        }
                });
            } 
            $description = 0;
            $description = $('#description').val();
            //$image = $('#image').val(); 
            var $additional_features = new Array();
              var n2 = jQuery("#additional_features:checked").length;
              if (n2 > 0){
                  jQuery("#additional_features:checked").each(function(){
                      $additional_features.push($(this).val()); 
                  });
              } 
            if($step==3){
                alert('helllo');
                    $.ajax({
                        type : 'post',
                        url : "store-basic-property",

                        data:{ "_token": "{{ csrf_token() }}",'description':$description,'property_id':$property_id,'additional_features':$additional_features}, 
                        success:function(data){ 
                       
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                      
                        }
                });
            } 
                                   
        });
        $('a[href="#finish"]').click(function() {
         
            $location_price = $('#location_price').val();
            $state = $('#state').val();
            $city = $('#city').val();
            $area =$('#area').val();
            $address = $('#address').val();
            $expected_rent = $('#expected_rent').val();
            $charge_exclude = $('#charge_exclude:checked').val();

            $price_negotiable = $('#price_negotiable:checked').val();
            $security_deposite = $('#security_deposite').val();
            $price= $('#price').val();
            $rent_aggreement = $('#rent_aggreement').val();
            $notice = $('#notice').val();
           
                    $.ajax({
                        type : 'post',
                        url : "store-basic-property",
                        data:{ "_token": "{{ csrf_token() }}",'state':$state,'city':$city,'area':$area,'address':$address,'expected_rent':$expected_rent,'charge_exclude':$charge_exclude,'price_negotiable':$price_negotiable,'security_deposite':$security_deposite,'price':$price,'rent_aggreement':$rent_aggreement,'notice':$notice,'property_id':$property_id},
                        success:function(data){ 
                            
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                else{
                                      Swal.fire({
                                           icon: 'success',
                                           title: 'Success',
                                           text: 'Saved Successfully!',
                                          
                                      })  
                                      window.location.href = "http://localhost/arnav_reality/public/rent-sell";
                                }
                        }
                });
            
        }); 

        $('#furnished_type').on('change', function (e) {
               $furnished_type = $('#furnished_type').val();
               $abbr =  'furniture-type';

                    $.ajax({
                        type : 'post',
                        url : "get-furniture",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$furnished_type,'abbr':$abbr},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                else{
                                       $('#furniture_type').html(data);
                                       $('#furnished_item').show();

                                }
                        }
                });               
        });
        $('#property_type,#property_for').on('change', function (e) {

               $property_type = $('#property_type').val();
               $property_for = $('#property_for').val();
               $abbr = 'subproperty-type';
               $room_type = "room-type";
               $other_room_type = "other-rooms-type";
               $furnished = "furnished-type";
               $type =$('#property_type').val();

                $.ajax({
                        type : 'post',
                        url : "get-type-label",
                        data:{ "_token": "{{ csrf_token() }}",'type':$type},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                }
                                var master_type = data;
                                $('#subproperty_type_label').html(master_type +' Type');

                    /* Hide show Fields */ 
                                if(master_type=='Residential' && $property_for =='rent'){
                                   
                                    $('#residentialRentTo').show();
                                    $('#CommercialRentTo').hide();
                                    $('#otherRentTo').hide();
                                    $('#labelBedroom').html('Bedrooms Type');
                                    $('#balcony_col').show();
                                    $('#labelBathroom').html('Bathrooms');
                                    $('#labelKitchen').html('Kitchen Type');
                                }
                                if(master_type=='Residential' && $property_for !='rent'){
                                    $('#residentialRentTo').hide();
                                    $('#CommercialRentTo').hide();
                                    $('#otherRentTo').hide();
                                    $('#labelBedroom').html('Bedrooms Type');
                                    $('#balcony_col').show();
                                    $('#labelBathroom').html('Bathrooms');
                                    $('#labelKitchen').html('Kitchen Type');
                                }
                                if(master_type=='PG/Hostels' && $property_for =='rent' ){
                                    
                                    $('#otherRentTo').show();
                                    $('#CommercialRentTo').hide();
                                    $('#residentialRentTo').hide();
                                    $('#labelBedroom').html('Cabins Type');
                                    $('#balcony_col').hide();
                                    $('#labelBathroom').html('WashRooms');
                                    $('#labelKitchen').html('Pantry Type');
                                }
                                if(master_type=='PG/Hostels' && $property_for !='rent'){
                                    $('#residentialRentTo').hide();
                                    $('#CommercialRentTo').hide();
                                    $('#otherRentTo').hide();
                                    $('#labelBedroom').html('Cabins Type');  
                                    $('#labelBathroom').html('WashRooms');  
                                    $('#balcony_col').hide();
                                    $('#labelKitchen').html('Pantry Type');

                                }                                                                
                                if(master_type=='Commercial'){
                                    $('#residentialRentTo').hide();
                                    $('#CommercialRentTo').show();
                                    $('#otherRentTo').hide();
                                    $('#labelBedroom').html('Room Type');
                                    $('#labelBathroom').html('Bathrooms');
                                    $('#balcony_col').hide();
                                    $('#labelKitchen').html('Pantry Type');

                                }                                                      
                        }
                    });                
                    $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$abbr},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                         })
                                } 
                                $('#subproperty_type').html(data);
                                $('#labelOtherRoom').show();
                                
                        }   
                });

                $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$room_type},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                         })
                                } 
                                $('#bedroom_type').html(data);
                        }
                });
                $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$furnished},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                         })
                                } 
                              
                                $('#furnished_type').html(data);
                        }
                });                
                $.ajax({
                        type : 'post',
                        url : "get-other-room",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$other_room_type},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                         })
                                } 
                                  
                                $('#other_room').html(data);
                        }
                });                                                
        });
        $('#subproperty_type').on('change', function (e) {
               $property_type = $('#subproperty_type').val();
               
               $abbr = 'resi-commer-type';
               $type =$('#subproperty_type').val();
               $floor_abbr = 'have-floor';
           
                $.ajax({
                        type : 'post',
                        url : "get-type-label",
                        data:{ "_token": "{{ csrf_token() }}",'type':$type},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                $('#sub_subproperty_type_label').html(data +' Type');
                        }
                    }); 
                $.ajax({
                        type : 'post',
                        url : "get-master-abbr",
                        data:{ "_token": "{{ csrf_token() }}",'property_type':$property_type,'abbr':$floor_abbr},
                        success:function(data){ 
                           
                                if (data =='1') {   
                                    $('#floor_label').show();
                                    $('#property_floor_label').show();
                                } 
                                else if(data =='0') {
                                    $('#floor_label').hide();
                                    $('#property_floor_label').hide();
                                }
                                
                        }
                    });                                    
                    $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$abbr},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                
                               
                                $('#sub_subproperty_type').html(data);
                        }
                });               
        }); 
        $('#state').on('change', function (e) {
               $property_type = $('#state').val();
               $abbr = 'city';
                    $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$abbr},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                $('#city').html(data);
                        }
                });               
        });
        $('#city').on('change', function (e) {
               $property_type = $('#city').val();
               $abbr = 'area';
                    $.ajax({
                        type : 'post',
                        url : "get-master",
                        data:{ "_token": "{{ csrf_token() }}",'parent_id':$property_type,'abbr':$abbr},
                        success:function(data){ 
                                if (data =='0') {   
                                          Swal.fire({
                                           icon: 'error',
                                           title: 'Oops...',
                                           text: 'Somethings Went Wrong!',
                                          
                                         })
                                } 
                                $('#area').html(data);
                        }
                });               
        });
 $.ajaxSetup({
     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
 });

$('#image').change(function () {
    $id = $property_id;
    event.preventDefault();
    let image_upload = new FormData();
    let TotalImages = $('#image')[0].files.length;  //Total Images
    let images = $('#image')[0];  
    
    for (let i = 0; i < TotalImages; i++) {
        image_upload.append('images', images.files[i]);
    }

    image_upload.append('TotalImages', TotalImages);
    image_upload.append('property_id', $id);

    $.ajax({
        method: 'POST',
        url: 'store-basic-property',
        data: image_upload,
        contentType: false,
        processData: false,
        success: function (images) {
            console.log(`ok ${images}`)
        },
        error: function () {
          console.log(`Failed`)
        }
    })

})                
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready( function() {
  $( "#avaiable_from" ).datepicker({ dateFormat: "d/m/yy" });
});
</script>

</body>

</html>