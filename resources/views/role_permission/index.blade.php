@extends('layouts.app')

@section('content')

<div class="col-sm-12">
     @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
       </div>
     @endif

  <div class="card">
    <div class="card-header">
            <a class="btn btn-primary m-t-5"  href="{{ url('roles/create') }}">Create Role</a>
        </div>
  </div>
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
          <div class="card-header">
                <h5>Roles List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                   <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Permission </th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @foreach($roles as $role)
                        <tr>
                            <td>{{$role->id}}</td>
                            <td>{{$role->action_name}}</td>
                            <td>{{$role->name}}</td>
                            <td>
                         @if($role->status == '1')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                        
                            </td>
                            <td>
            
                    <form action="{{action('RolePermissionController@destroy', $role->id)}}" method="post">
                    {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                           <!--   <a href="{{action('RolePermissionController@edit',$role->id)}}" class="btn btn-info" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
                        <button class="btn btn-danger" type="submit" Onclick="return ConfirmDelete();"><i class="fa fa-trash" ></i></button>
                    </form>
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<a href="{{action('PermissionGroupController@edit',$role->id)}}" title="Update"><i class="feather icon-edit" aria-hidden="true"></i></a>
                              <a href="{{action('PermissionGroupController@destroy', $role->id)}}"  ><i class="feather icon-trash-2" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>