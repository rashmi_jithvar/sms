@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Role Permission</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

  @if (isset($role))  
  @endif                
  @if (isset($rolepermission))                  
  @if($rolepermission)
  
        @foreach ($rolepermission as $perKey => $perm_val) 
            @php $perm_array[] = $perm_val->permission_id;
            @endphp
  @endforeach

  @endif
  @endif    
                   
<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body" >
                            <h4 class="card-title">Add Role Permission </h4>
                          <form method="POST" action="{{ url('role_permission/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                              <div class="col-4">              
                                <div class="form-group">
                                      <label for="">Role</label>

                                      <select name="role_id" id="role_id" class="form-control" onchange="loadRole(this.value)">
                                         <option disabled selected>Select Role</option>
                                        @if (isset($roledropdown))
                                      @foreach($roledropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}" {{ $role == $key ? 'selected':'' }}>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                     <span class="text-danger">{{ $errors->first('role_id') }}</span> 

                                </div> 
                            </div>   
           
                          @if (isset($pergroup))
                            @foreach($pergroup as $key => $name)
                          <div class="row" >
                                <div class="col-md-12 m-b-20">  
                              @if($name->permission_name) 
                                <h4><u>{{$name->permission_name}} </u></h4>
                                <br>
                                <div class="row" >
                                  @foreach($name->permission as $key => $permission)
                                     @php $data_checked = ''
                                      @endphp
                                         @if (isset($perm_array))
                                        @if($perm_array)
                                        
                                          @if(in_array($permission->id, $perm_array))
                                            @php $data_checked = "checked";
                                            @endphp
                                          @endif
                                       @endif
                                    @endif       
                                  <div class="col-md-2"> 
                                  <label> <input type="checkbox" name="permission_id[]" value="{{$permission->id}}" {{$data_checked}}>

                                    {{$permission->name}}</label>
                                  </div>
                                  
                          
                                @endforeach

                              </div>

                              @endif
                        </div>
                      </div>
                      <hr/>
                            @endforeach
                          @endif

                              <!--  <div class="form-group row">
                                    <div class="col-4">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" required>
                                            <option disabled selected>Select Status</option>
                                            <option value="1" >Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>
<span class="text-danger">{{ $errors->first('status') }}</span> 
                                </div>-->
                                
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    function loadRole(value)
    {
     
        window.location = "{{ url('role_permission/create') }}?role="+value;
    }
</script>
@endsection