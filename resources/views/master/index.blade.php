@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $title ? $title :''}} </a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif

</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
          <div class="card-header">
              <div class="pull-left"> <h5>{{ $title ? $title:'' }} List</h5></div>
               <div class="pull-right"> 
                <a class="btn btn-primary m-t-5"  href="{{ route('master.create',['abbr'=>request()->get('abbr')]) }}">Create {{$title ? $title:""}}</a>
              </div>
              
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>#</th>
                         @if(request()->get('abbr')!='subject' && request()->get('abbr') !='section' && request()->get('abbr') !='grade'  && request()->get('abbr') !='class' && request()->get('abbr') !='session' && request()->get('abbr') !='category' && request()->get('abbr') !='discount' && request()->get('abbr') !='exam-type')
                         <th>{{$subtitle ? $subtitle:""}}</th>
                         @endif
                         <th>{{$title ? $title:""}}</th>
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        @php $i = 0
                        @endphp
                        @if(!empty($properties))
                        @foreach($properties as $property)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          @if(!empty($property->parant_id) && request()->get('abbr') !='subject' && request()->get('abbr') !='section' && request()->get('abbr') !='class'  && request()->get('abbr') !='grade' && request()->get('abbr') !='session' && request()->get('abbr') !='category' && request()->get('abbr') !='discount' && request()->get('abbr') !='exam-type')
                          @if(request()->get('abbr') != 'fees')
                            @foreach($property->parent as $parent)
                            <td>{{$parent->name}}</td>
                            @endforeach
                          @else
                            @foreach($property->getLabel as $label)
                              <td>{{$label->label}}</td>
                            @endforeach 
                          @endif                 
                          @elseif(request()->get('abbr') !='subject' && request()->get('abbr') !='session' && request()->get('abbr') !='class' && request()->get('abbr') !='grade'  && request()->get('abbr') !='section' && request()->get('abbr') !='category' && request()->get('abbr') !='discount' && request()->get('abbr') !='exam-type')
                          <td>empty</td>
                          @endif

                          <td>{{$property->name}}</td>
                          <td>
                          @if($property->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                           <form id="my_form" action="{{route('master.delete',['id'=>$property->id ,'abbr'=>request()->get('abbr')])}}" method="post">
                            @method('DELETE')
                           {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                           @if(permissionGate('edit'))
                             <a href="{{route('master.edit',['id'=>$property->id,'abbr'=>$property->abbr])}}"  role="button"><i class="feather icon-edit" aria-hidden="true"></i></a>
                           @endif
                           @if(permissionGate('delete'))
                           <button style="background: none!important;border: none;padding: 0!important;color: #566673;" Onclick="return ConfirmDelete();"><i class="feather icon-trash-2" ></i></button>
                           @endif
                          </form>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection