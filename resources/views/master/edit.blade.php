@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                   <li class="breadcrumb-item"><a href="{{ route('master',['abbr'=>strtolower($title ? $title:'class')]) }}">@isset($title) {{ $title }} @endisset List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit </h4>
                            <form id="propertytype_form" method="POST" action="{{ route('master.update',['id'=>$property_exist->id,'abbr'=>$property_exist->abbr]) }}">
                                @method('PATCH')
                                @csrf
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }}
                                    @php
                                    Session::forget('message');
                                    @endphp
                                </div>
                                @endif
                                @if(!empty($property_type))
                                <div class="form-group row">
                                    <div class="col">
                                        <label>{{$subtitle ? $subtitle :''}}</label>
                                        <select class="form-control" id="parant_id" name="parant_id">
                                          <?php echo $property_type ?>
                                        
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('name') }}</span> 
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                       
                                    <div class="col">
                                        <label>{{$title ? $title :''}}:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Name" name="name"value="{{old('name',isset($property_exist)?$property_exist->name:'')}}" >
                                        <span class="text-danger name">{{ $errors->first('name') }}</span> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label >Status</label>
                                        @php 
                                          $arr = ['Inctive','Active'];
                                         @endphp
                                        <select class="form-control"  id="status" name="status" >
                                           <option value="">Select</option> 
                                            @foreach($arr as $key=>$row)
                                           <option value="{{$key}}" {{isset($property_exist)?($property_exist->status==$key ? 'selected':''):''}}>{{$row}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status') }}</span> 
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection