@extends('layouts.app')
@section('content')

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
  .toggle.ios .toggle-handle { border-radius: 20rem; }
</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ route('master',['abbr'=>strtolower($title ? $title:'class')]) }}">@isset($title) {{ $title }} @endisset List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            <h4 class="card-title">Add {{$title ? $title :''}}</h4>
                            <form id="propertytype_form" method="POST" action="{{ route('master.store',['abbr'=>request()->get('abbr')]) }}">
                                @csrf
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }}
                                    @php
                                    Session::forget('message');
                                    @endphp
                                </div>
                                @endif
                                <div class="form-group row">
                                    <div class="col">
                                        <label>{{$title ? $title :''}}:</label>
                                        <input type="text" class="form-control"  placeholder="Enter {{$title ? $title :''}}" name="name" >
                                        <span class="text-danger name">{{ $errors->first('name') }}</span> 
                                    </div>
                                </div>
                                @if(!empty($property_type))
                                <div class="form-group row">
                                    <div class="col">
                                        <label>{{$subtitle ? $subtitle :''}}</label>
                                        <select class="form-control" id="parant_id" name="parant_id">
                                          <?php echo $property_type ?>
                                        
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('name') }}</span> 
                                    </div>
                                </div>
                                @endif                                
                                <div class="form-group row">
                                    <div class="col">
                                        <label >Status</label>
                                        @php 
                                          $arr = ['Inactive','Active'];
                                         @endphp
                                        <select class="form-control"  id="status" name="status" >
                                            <option value="">Select</option> 
                                            @foreach($arr as $key=>$row)
                                           <option value="{{$key}}">{{$row}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status') }}</span> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="switch switch-primary d-inline m-r-10">
                                    <input type="checkbox" id="switch-p-1" checked="">
                                    <label for="switch-p-1" class="cr"></label>
                                    </div>
                                    <label>Checked</label>
                                    </div>
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection