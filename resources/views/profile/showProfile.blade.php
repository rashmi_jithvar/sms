@extends('layouts.app') @section('content')
<style>
  .card {
    padding:20px;
  }

</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
</div>

<div class="col-md-12">
    <div class="profile-content">
        <div class="row">
            <div class="card-body">
                <div class="card-topline-aqua">
                    <header></header>
                </div>
                <div class="white-box">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane fontawesome-demo show active" id="customer">
                            <div id="biography">
                                <div class="customer-view">
                                   <!-- <p>
                                        <a class="btn btn-success" href="/app/customer/create">New Customer</a>&nbsp;<a class="btn btn-primary" href="/app/customer/update?id=5470">Update</a>&nbsp; <a class="btn btn-warning" href="javascript:createShopify();">Create Shopify</a>
                                        <a class="btn btn-danger pull-right" href="javascript:createComment();">
                                            <i class="fa fa-comment"></i>
                                        </a>
                                    </p>-->

                                    <div class="card">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <small class="text-muted"></small>
                                                <small class="text-muted"></small>
                                                <p style="font-weight: bold;">
                                                    School Profile
                                                    <!--<label class="pull-right badge badge-success">Default</label>
                                                    <label class="pull-right badge badge-success">Active</label>-->
                                                </p>
                                                <hr>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 >School Name: </h6>
                                                <p class="m-b-0">{{$webMaster ? $webMaster->school_name:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Contact No.: </h6>
                                                <p class="m-b-0">
                                                    {{$webMaster ? $webMaster->contact_no:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Email: </h6>
                                                <p class="m-b-0">
                                                   {{$webMaster ? $webMaster->email:''}} </p>
                                            </div>                                            

                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 >Address: </h6>
                                                <p class="m-b-0">{{$webMaster ? $webMaster->address:''}}<br>
                                                    {{$webMaster ? $webMaster->pin_code:''}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <h6>Registration No.: </h6>
                                                <p class="m-b-0">{{$webMaster ? $webMaster->registration_no:''}}</p>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Website: </h6>
                                                <p class="m-b-0">{{$webMaster ? $webMaster->wesite:'' }}</p>
                                            </div>                                                                              
                                        </div>                                   


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
</div>
@endsection