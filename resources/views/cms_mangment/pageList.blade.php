@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Page List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Page List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Page Name</th>
                                <th>Page Title</th>
                                <th>Meta Title</th>
                                <th>Meta Description</th>
                                <th>Meta Keywords</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($pageListDetails as $pld)
                            <tr class="row_{{$pld->id}}">
                                <td>{{ $i }}</td>
                                <td>
                                    @foreach($pageLists as $pl)
                                        @if($pld->pageName == $pl->id) {{$pl->pageName}} @endif
                                    @endforeach
                                </td>
                                <td>{{ $pld->pageTitle }}</td>
                                <td>@if(!empty($pld->metaTitle)) YES @else NO @endif</td>
                                <td>@if(!empty($pld->metaDescription)) YES @else NO @endif</td>
                                <td>@if(!empty($pld->metaKeyword)) YES @else NO @endif</td>
                                <td>
                                    <a href="{{action('AddPagesController@edit',$pld->id)}}"><span class="feather icon-edit"></span></a> | 
                                    <a href="javascript:void(0);" onclick="removeItem('{{$pld->id}}')"><span class="feather icon-trash-2"></span></a>
                                </td>
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <input type="hidden" id="tableName" value="{{ $tableName }}"/> 
                    <input type="hidden" id="fieldItem" value="id"/> 
                    {{csrf_field()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection