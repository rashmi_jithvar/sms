@extends('layouts.app')

@section('content')

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Add Sub Menu</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-8 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="addsubMenuForm">
                        {{csrf_field()}}
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Menu Name<span class="required-icon">*</span></label>
                                <select id="pageName" name="pageName" class="form-control col-sm-12">
                                    <option value="">-- SELECT --</option>
                                    @foreach($pageList as $pl)
                                    <option value="{{$pl->id}}">{{ $pl->pageName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Sub Menu Name<span class="required-icon">*</span></label>
                                <input type="text" class="form-control" name="subMenuName" id="subMenuName"
                                    placeholder="Sub Menu Name">
                            </div>
                        </div>
                        
                        <div class="col-md-6 column">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="col-sm-12">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Sub Menu List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sub Menu Name</th>
                                <th>Menu Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($submenuList as $sl)
                            <tr class="row_{{$sl->id}}">
                                <td>{{ $i }}</td>
                                <td>{{ $sl->subMenuName }}</td>
                                <td>
                                    @foreach($pageList as $pl)
                                        @if($pl->id == $sl->pageNameId) {{$pl->pageName}} @endif
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{action('AddSubMenusController@edit',$sl->id)}}"><span class="feather icon-edit"></span></a>
                                    <a href="javascript:void(0);" onclick="removeItem('{{$sl->id}}')"><span class="feather icon-trash-2"></span></a>
                                </td>
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <input type="hidden" id="tableName" value="{{ $tableName }}"/> 
                    <input type="hidden" id="fieldItem" value="id"/> 
                    {{csrf_field()}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection