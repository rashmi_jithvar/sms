@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Teams</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    
    <div class="card">
        <div class="card-header">
            <a class="btn btn-primary m-t-5" data-toggle="collapse" href="#collapseExample" role="button"
                aria-expanded="false" aria-controls="collapseExample">Add New Team Member</a>
        </div>
        <div class="collapse" id="collapseExample">
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="addteamForm" enctype="multipart/form-data">
                        <div class="row">
                            {{csrf_field()}}
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label>Team Member Name<span class="required-icon">*</span></label>
                                    <input type="text" class="form-control" name="name" id="name"
                                        placeholder="Team Member Name">
                                        <span id="for_name" class="none">This field is required.</span>
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label>Position Name<span class="required-icon">*</span></label>
                                    <input type="text" class="form-control" name="position" id="position"
                                        placeholder="Position Name">
                                        <span id="for_position" class="none">This field is required.</span>
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label>Profile Photo<span class="required-icon"></span></label>
                                    <input type="file" class="form-control" name="profilePhoto" id="profilePhoto">
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label for="aboutDetails">Facebook</label>
                                    <input type="text" class="form-control" id="facebook" name="facebook" />
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label for="aboutDetails">Linkedin</label>
                                    <input type="text" class="form-control" id="linkedin" name="linkedin" />
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label for="aboutDetails">Twitter</label>
                                    <input type="text" class="form-control" id="twitter" name="twitter" />
                                </div>
                            </div>
                            <div class="col-md-3 column">
                                <div class="form-group">
                                    <label for="aboutDetails">Google Plus</label>
                                    <input type="text" class="form-control" id="gplus" name="gplus" />
                                </div>
                            </div>

                            <div class="col-md-12 column">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>


</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Manage Team Member</h5>
        </div>
        <div class="card-body table-border-style">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Team Member Name</th>
                            <th>Position Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($teamList as $sl)
                        <tr class="row_{{$sl->id}}">
                            <td>{{ $i }}</td>
                            <td>{{ $sl->name }}</td>
                            <td>{{ $sl->position }}</td>
                            <td>
                                <a title="Edit" href="{{action('TeamsController@edit',$sl->id)}}"><span
                                        class="feather icon-edit"></span></a> | 
                                <a title="Delete" href="javascript:void(0);" onclick="removeItem('{{$sl->id}}')"><span
                                        class="feather icon-trash-2"></span></a>
                            </td>
                        </tr>
                        @php $i++ @endphp
                        @endforeach
                    </tbody>
                </table>
                <input type="hidden" id="tableName" value="{{ $tableName }}" />
                <input type="hidden" id="fieldItem" value="id" />
                {{csrf_field()}}
            </div>
        </div>
    </div>
</div>

@endsection