@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('menu') }}">Menu</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit Menu</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Update Menu</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="updateMenuForm">
                        <div class="row">
                        {{csrf_field()}}
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Page Name</label>
                                <input type="text" class="form-control" name="pageName" id="pageName"
                                    placeholder="Page Name" value="{{$menuDetails->pageName}}">
                                    <span id="for_pageName" class="none">This field is required.</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Parent Name</label>
                                <select class="form-control" name="subMenuName" id="subMenuName">
                                    <option value="">-- SELECT --</option>
                                    @foreach($pageList as $pl)
                                        <option value="{{$pl->id}}" @if($pl->id == $menuDetails->subMenu) selected @endif>{{$pl->pageName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Status</label><br/>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio1" name="status" class="custom-control-input"
                                        value="active" 
                                        @if($menuDetails->pageStatus == "active") checked @endif
                                        >
                                    <label class="custom-control-label" for="customRadio1">Active</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio2" name="status" class="custom-control-input"
                                        value="inactive" @if($menuDetails->pageStatus == "inactive") checked @endif>
                                    <label class="custom-control-label" for="customRadio2">Inactive</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input type="hidden" id="recordId" name="recordId" value={{$menuDetails->id}} />
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection