@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ route('page-list') }}">Page List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Add page</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Add Pages Details</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="addPageInformation">
                    {{csrf_field()}}
                        <div class="row">
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Menu Name<span class="required-icon">*</span></label>
                                <select id="pageName" name="pageName" class="form-control col-sm-12">
                                    <option value="">-- SELECT --</option>
                                    @foreach($pageList as $pl)
                                    <option value="{{$pl->id}}">{{ $pl->pageName}}</option>
                                    @endforeach
                                </select>
                                <span id="for_pageName" class="none">This field is required</span>
                            </div>
                        </div>

                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Page Title<span class="required-icon">*</span></label>
                                <input type="text" class="form-control" name="pageTitle" id="pageTitle"/>
                                <span id="for_pageTitle" class="none">This field is required</span>
                            </div>
                        </div>
                        <div class="col-md-12 column">
                            <div class="form-group">
                                <label for="aboutDetails">Page Description<span class="required-icon"></span></label>
                                <textarea id="summernote" name="editor"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Meta Title</label>
                                <input type="text" class="form-control" name="metaTitle" id="metaTitle"/>
                            </div>
                        </div>
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label for="aboutDetails">Meta Keyword's</label>
                                <input type="text" class="form-control" id="metaKeyword" name="metaKeyword" />
                            </div>
                        </div>
                        <div class="col-md-12 column">
                            <div class="form-group">
                                <label for="aboutDetails">Meta Description</label>
                                <textarea class="form-control" name="metaDescription" id="metaDescription"></textarea>
                            </div>
                        </div>
                        <div class="col-md-2 column">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-10 column">
                            <div class="form-group">
                                <div class="alert alert-danger none" role="alert"></div>
                                <div class="alert alert-success none" role="alert"></div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection