@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Profile Details</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="websiteInformation" enctype="multipart/form-data">
                    {{csrf_field()}}
                        <div class="row">
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label>First Phone</label>
                                <input type="text" class="form-control" name="phoneFirst" id="phoneFirst" value="{{$webMaster->phoneFirst}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label>Second Phone</label>
                                <input type="text" class="form-control" name="phoneSecond" id="phoneSecond" value="{{$webMaster->phoneSecond}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label>Primary Email</label>
                                <input type="text" class="form-control" name="primaryEmail" id="primaryEmail" value="{{$webMaster->primaryEmail}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Secondary Email</label>
                                <input type="text" class="form-control" id="secondaryEmail" name="secondaryEmail" value="{{$webMaster->secondaryEmail}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Noreply Email</label>
                                <input type="text" class="form-control" id="noreplyEmail" name="noreplyEmail" value="{{$webMaster->noreplyEmail}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Facebook</label>
                                <input type="text" class="form-control" id="facebook" name="facebook" value="{{$webMaster->facebook}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Linkedin</label>
                                <input type="text" class="form-control" id="linkedin" name="linkedin" value="{{$webMaster->linkedin}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Twitter</label>
                                <input type="text" class="form-control" id="twitter" name="twitter" value="{{$webMaster->twitter}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Google Plus</label>
                                <input type="text" class="form-control" id="gplus" name="gplus" value="{{$webMaster->gplus}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Address</label>
                                <input type="text" class="form-control" id="address" name="address" value="{{$webMaster->address}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Employee Login Url</label>
                                <input type="text" class="form-control" id="employeeLogin" name="employeeLogin" value="{{$webMaster->employeeLogin}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <label for="aboutDetails">Client Login</label>
                                <input type="text" class="form-control" id="clientLogin" name="clientLogin" value="{{$webMaster->clientLogin}}"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">UPDATE</button>
                                <input type="hidden" id="recordId" name="recordId" value={{$webMaster->id}} />                                
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection