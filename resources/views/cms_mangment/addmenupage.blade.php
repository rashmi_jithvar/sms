@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Menu</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <a class="btn btn-primary m-t-5" data-toggle="collapse" href="#collapseExample" role="button"
                aria-expanded="false" aria-controls="collapseExample">Add New Menu</a>
        </div>
        <div class="collapse" id="collapseExample">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="alert alert-danger none" role="alert"></div>
                        <div class="alert alert-success none" role="alert"></div>
                        <form id="addMenuForm">
                            <div class="row">
                            {{csrf_field()}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Page Name<span class="required-icon">*</span></label>
                                    <input type="text" class="form-control" name="pageName" id="pageName"
                                        placeholder="Page Name">
                                    <span id="for_pageName" class="none">This field is required.</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Parent Name</label>
                                    <select class="form-control" name="subMenuName" id="subMenuName">
                                        <option value="">-- SELECT --</option>
                                        @foreach($pageList as $pl)
                                            <option value="{{$pl->id}}">{{$pl->pageName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status</label><br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadio1" name="status" class="custom-control-input"
                                            value="active">
                                        <label class="custom-control-label" for="customRadio1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadio2" name="status" class="custom-control-input"
                                            value="inactive" checked>
                                        <label class="custom-control-label" for="customRadio2">Inactive</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Manage Menu List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($pageList as $pl)
                            <tr class="row_{{$pl->id}}">
                                <td>{{ $i }}</td>
                                <td>{{ $pl->pageName }}</td>
                                <td>{{ $pl->pageStatus }}</td>
                                <td>
                                    <a href="{{action('PagesController@edit',$pl->id)}}"><span class="feather icon-edit"></span></a> | 
                                    <a href="javascript:void(0);" onclick="removeItem('{{$pl->id}}')"><span class="feather icon-trash-2"></span></a>
                                </td>
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <input type="hidden" id="tableName" value="{{ $tableName }}"/> 
                    <input type="hidden" id="fieldItem" value="id"/> 
                    {{csrf_field()}}
                </div>
            </div>
        </div>
</div>

@endsection