@extends('layouts.app')

@section('content')

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Add Sub Menu</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-8 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="updateSubMenuForm">
                        {{csrf_field()}}
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Menu Name<span class="required-icon">*</span></label>
                                <select id="pageName" name="pageName" class="form-control col-sm-12">
                                    <option value="">-- SELECT --</option>
                                    @foreach($pageList as $pl)
                                    <option value="{{$pl->id}}" @if($pl->id == $submenuInfo->pageNameId) selected @endif>{{ $pl->pageName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 column">
                            <div class="form-group">
                                <label>Sub Menu Name<span class="required-icon">*</span></label>
                                <input type="text" class="form-control" name="subMenuName" id="subMenuName"
                                    placeholder="Sub Menu Name" value="{{$submenuInfo->subMenuName}}"/>
                            </div>
                        </div>
                        
                        <div class="col-md-6 column">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input type="hidden" name="recordId" id="recordId" value="{{$submenuInfo->id}}"/>
                        <div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection