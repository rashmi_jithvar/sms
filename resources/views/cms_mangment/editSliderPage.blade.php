@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ route('sliders') }}">Sliders</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit Sliders</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="updateslideForm" enctype="multipart/form-data">
                        <div class="row">
                            {{csrf_field()}}
                            <div class="col-md-6 column">
                                <div class="form-group">
                                    <label>Slide Content<span class="required-icon">*</span></label>
                                    <input type="text" class="form-control" name="content" id="content"
                                        placeholder="Slide Content" maxlength="100" value="{{$slideInfo->content}}">
                                        <span id="for_content" class="none">This field is required.</span>
                                </div>
                            </div>
                            <div class="col-md-6 column">
                                <div class="form-group">
                                    <label>Slide Image<span class="required-icon">*</span></label>
                                    <input type="file" class="form-control" name="photo" id="photo">
                                    <span id="for_photo" class="none">This field is required.</span>
                                </div>
                            </div>
                            <div class="col-md-12 column">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update Slide</button>
                                    <input type="hidden" name="oldSlide" id="oldSlide" value="{{$slideInfo->image}}"/>
                                    <input type="hidden" name="recordId" id="recordId" value="{{$slideInfo->id}}"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection