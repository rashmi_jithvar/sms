@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('teams') }}">Team</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit Team</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Update Team Member</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="alert alert-danger none" role="alert"></div>
                    <div class="alert alert-success none" role="alert"></div>
                    <form id="updateteamForm" enctype="multipart/form-data">
                        <div class="row">
                        {{csrf_field()}}
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label>Team Member Name<span class="required-icon">*</span></label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Team Member Name" value="{{$memberInfo->name}}"/>
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label>Position Name<span class="required-icon">*</span></label>
                                <input type="text" class="form-control" name="position" id="position"
                                    placeholder="Position Name" value="{{$memberInfo->position}}"/>
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label>Profile Photo<span class="required-icon"></span></label>
                                <input type="file" class="form-control" name="profilePhoto" id="profilePhoto">
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label for="aboutDetails">Facebook</label>
                                <input type="text" class="form-control" id="facebook" name="facebook" value="{{$memberInfo->facebook}}"/>
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label for="aboutDetails">Linkedin</label>
                                <input type="text" class="form-control" id="linkedin" name="linkedin" value="{{$memberInfo->linkedin}}"/>
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label for="aboutDetails">Twitter</label>
                                <input type="text" class="form-control" id="twitter" name="twitter" value="{{$memberInfo->twitter}}"/>
                            </div>
                        </div>
                        <div class="col-md-3 column">
                            <div class="form-group">
                                <label for="aboutDetails">Google Plus</label>
                                <input type="text" class="form-control" id="gplus" name="gplus" value="{{$memberInfo->gplus}}"/>
                            </div>
                        </div>
                        
                        <div class="col-md-12 column">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">UPDATE</button>
                                <input type="hidden" class="form-control" id="recordId" name="recordId" value="{{$memberInfo->id}}"/>
                                <input type="hidden" class="form-control" id="oldPic" name="oldPic" value="{{$memberInfo->profilePhoto}}"/>
                            </div>
                        </div>
</div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection