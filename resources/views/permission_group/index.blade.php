@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Permission Group</a></li>
                   
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
     @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
       </div>
     @endif

  <div class="card">
    <div class="card-header">
            <a class="btn btn-primary m-t-5"  href="{{ url('/permission_group/create') }}">Add Permission Group</a>
        </div>
  </div>
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
          <div class="card-header">
                <h5>Roles List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                                        <table id="example1" class="display" style="width:100%;">
                      <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i = 0
                         @endphp
                           @foreach($roles as $role)
                          @php $i++
                         @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$role->permission_name}}</td>
                            <td>
                         @if($role->status == '1')
                               <label class="badge badge-success">Active</label>
                          
                          @else
                                <label class="badge badge-danger">Pending</label>
                           @endif
                              
                        
                            </td>
                            <td>
            

                       
                              <a href="{{action('PermissionGroupController@edit',$role->id)}}" title="Update"><i class="feather icon-edit" aria-hidden="true"></i></a>
                              <a href="{{action('PermissionGroupController@destroy', $role->id)}}"  ><i class="feather icon-trash-2" aria-hidden="true" title="Delete" Onclick="return ConfirmDelete();"></i></a>
                    
                
                              
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection