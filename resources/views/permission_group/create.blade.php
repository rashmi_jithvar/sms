@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('permission_group') }}">Permission Group</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Permission Group create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Permission Group</h4>
                          <form id="permission_form" method="POST" action="{{ url('permissiongroup/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Permission:</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" >
                                        <span class="text-danger name">{{ $errors->first('name') }}</span> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" >
                                            <option value="">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status') }}</span> 
                                    </div>
                                    
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

@endsection