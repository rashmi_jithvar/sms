@extends('layouts.app')
@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Add Enquiry</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('student-enquiry') }}">Enquiry</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                           
                            <form id="enquiry" method="POST" action="{{ route('enquiry.store')}}">
                                @csrf
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }}
                                    @php
                                    Session::forget('message');
                                    @endphp
                                </div>
                                @endif
                               
                               
                                <h5>General Detail</h5>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label>First Name</label>
                                        <input type="text" class="form-control"  placeholder="Enter First Name" name="first_name" id="first_name" value="{{ old('first_name') }}">
                                        <span class="text-danger parant_id">{{ $errors->first('first_name') }}</span> 
                                    </div>

                                    <div class="col-md-4">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control"  placeholder="Enter Last Name" name="last_name" id="last_name" value="{{ old('last_name') }}">
                                        <span class="text-danger parant_id">{{ $errors->first('last_name') }}</span> 
                                    </div>  
                                    <div class="col-md-4">
                                        <label>Gender</label>
                                            <select class="form-control"  id="gender" name="gender">
                                            <option value="">Select</option> 
                                            <option {{ ( old('gender') == 'male') ? 'selected' : '' }} value="male">Male</option>
                                            <option {{ ( old('gender') == 'female') ? 'selected' : '' }}  value="female">Female</option>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('gender') }}</span> 
                                    </div>                                                                    
                                </div>
                               
                                <div class="form-group row">

                                    <div class="col-md-4">
                                        <label>Date Of Birth:</label>
                                        <input type="text" class="form-control" name="dob" id="dob">
                                        <span class="text-danger name">{{ $errors->first('dob') }}</span> 
                                    </div>
                                    <div class="col-md-4">
                                        <label>Category:</label>
                                        <select class="form-control" id="category" name="category">
                                          <?php echo $category ?>
                                        </select>
                                        <span class="text-danger name">{{ $errors->first('category') }}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Addmission Seeking Class:</label>
                                        <select class="form-control" id="admission_seeking" name="admission_seeking">
                                          <?php echo $class; ?>
                                        </select>
                                        <span class="text-danger name">{{ $errors->first('admission_seeking') }}</span> 
                                    </div>                                                                        
                                </div>
                                 <h5>Guardian Detail</h5>
  

                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Father's Name:</label>
                                        <input type="text" class="form-control" name="father_name" id="father_name" placeholder="Father's Name" value="{{ old('father_name') }}">
                                        <span class="text-danger name">{{ $errors->first('father_name') }}</span>                                            
                                    </div>
                                    <div class="col-md-3">
                                        <label>Father's Contact No.:</label>
                                        <input type="text" class="form-control phone-group" name="father_contact_no" id="father_contact_no" placeholder="Enter Contact No" maxlength="10">
                                        <span class="text-danger name" value="{{ old('father_contact_no') }}">{{ $errors->first('father_contact_no') }}</span>                                            
                                    </div>
                                    <div class="col-md-3">
                                        <label>Father's Email:</label>
                                        <input type="email" class="form-control" name="father_email" id="father_email" placeholder="Enter Email" value="{{ old('father_email') }}">
                                        <span class="text-danger name">{{ $errors->first('father_email') }}</span>                                            
                                    </div>  
                                    <div class="col-md-3">
                                        <label>Father's Occupation:</label>
                                        <input type="text" class="form-control" name="occupation_father" id="occupation_father" placeholder="Enter Occupation" value="{{ old('occupation_father') }}">
                                        <span class="text-danger name">{{ $errors->first('occupation_father') }}</span>                                            
                                    </div>                                                                  
                                </div>   
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Mother's Name:</label>
                                        <input type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Mother's Name" value="{{ old('mother_name') }}">
                                        <span class="text-danger name">{{ $errors->first('mother_name') }}</span>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label>Mother's Contact No:</label>
                                        <input type="text" class="form-control phone-group" name="mother_contact_no" id="mother_contact_no" placeholder="Enter Contact No" maxlength="10">
                                        <span class="text-danger name" value="{{ old('mother_contact_no') }}">{{ $errors->first('mother_contact_no') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Mother's Email:</label>
                                        <input type="email" class="form-control" name="mother_email" id="mother_email" placeholder="Enter Email"  value="{{ old('mother_email') }}">
                                        <span class="text-danger name">{{ $errors->first('mother_email') }}</span>
                                    </div> 
                                    <div class="col-md-3">
                                        <label>Mother's Occupation:</label>
                                        <input type="text" class="form-control" name="occupation_mother" id="occupation_mother" placeholder="Enter Occupation" value="{{ old('occupation_mother') }}">
                                        <span class="text-danger name">{{ $errors->first('occupation_mother') }}</span>
                                    </div>                                                                                          
                                </div>   

                                <h5>Address</h5>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label>State:</label>
                                        <input type="text" class="form-control" name="state" id="state" placeholder="Enter State" value="{{ old('state') }}">
                                        <span class="text-danger name">{{ $errors->first('state') }}</span>                                            
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label>City:</label>
                                        <input type="text" class="form-control" name="city" id="city" placeholder="Enter City" value="{{ old('city') }}">
                                        <span class="text-danger name">{{ $errors->first('city') }}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pincode:</label>
                                        <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Enter Pincode" value="{{ old('pincode') }}" maxlength="6">
                                        <span class="text-danger name">{{ $errors->first('pincode') }}</span>
                                    </div>                                                          
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label>Address:</label>
                                        <textarea class="form-control" name="address" id="address" placeholder="Enter Address">{{ old('address') }}</textarea>
                                        <span class="text-danger name">{{ $errors->first('address') }}</span>                                            
                                    </div>                  
                                </div>                                                                 
                                <button type="submit" class="btn btn-primary mr-2" Onclick="return ConfirmContact();">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



<script type="text/javascript">
    $(document).ready( function() {
   if ($("#enquiry").length > 0) {
    $("#enquiry").validate({
        rules: {
            first_name: {
                required: true,
                lettersonly: true
            },
            last_name: {
                required: true,
                lettersonly: true
            },
            gender:{
                required:function () {
                       return ($("#gender option:selected").val() != "0");
                   }
            },
            dob:{
                required:true
            },
            select:{
                required: true,
                required:function () {
                       return ($("#category option:selected").val() == "0");
                   }
            },
            admission_seeking:{
                required:function () {
                       return ($("#admission_seeking option:selected").val() == "0");
                   }
            },
            father_name:{
                required:true,
                lettersonly: true
            },
            father_contact_no:{  
                required: {
                    depends: function(element) {
                        return $("#mother_contact_no").val() == ''
                    }
                },                   
                  number: true,
                  minlength:10,
                  maxlength:10,      
            },
            /*father_email:{
                required:true,
                email:true
            },
            occupation_father:{
                required:true
            },*/
            mother_name:{
                required:true,
                lettersonly: true

            },
           mother_contact_no:{
                required: {
                    depends: function(element) {
                        return $("#father_contact_no").val() == ''
                    }
                },            
                  number: true,
                  minlength:10,
                  maxlength:10,   
            },
           /* mother_email:{
                required:true,
                email:true
            },
            occupation_mother:{
                required:true
            },*/
            state:{
                required:true
            },
            city:{
                required:true
            },
            pincode:{
                required:true,
                number: true,
                maxlength: 6                
            },
            address:{
                required:true
            }
        },

    messages: {

      first_name: {
        required: "Please Enter the First Name",
      },

      last_name: {
        required: "Please Enter the Last Name",
      },

      gender: {
        required: "Please Select Gender",
      },  

      dob: {
        required: "Please Enter Date Of Birth",
      },

      category: {
        required: "Please Select Category",
      }, 

      admission_seeking: {
        required: "Please Select Class",
      },

      father_name: {
        required: "Please Enter Father's Name",
      },  

      mother_name: {
        required: "Please Enter Mother Name",
      },  
      state: {
        required: "Please Enter State",
      },  

      city: {
        required: "Please Enter City",
      }, 
      pincode: {
        required: "Please Enter Pincode",
      }, 
      address: {
        required: "Please Enter Address",
      },             
    },
    })
  }    
});
</script>   
