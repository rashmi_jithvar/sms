@extends('layouts.app')
@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Edit Enquiry</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('student-enquiry') }}">Enquiry</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Edit</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                           
                            <form id="enquiry" method="POST" action="{{ route('enquiry.update',$student->id)}}">
                                @csrf
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }}
                                    @php
                                    Session::forget('message');
                                    @endphp
                                </div>
                                @endif
                               
                               
                                <h5>General Detail</h5>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label>First Name</label>
                                        <input type="text" class="form-control"  placeholder="Enter First Name" name="first_name" id="first_name" value="{{ old('first_name') || $student ? $student->first_name:''}}">
                                        <span class="text-danger parant_id">{{ $errors->first('first_name') }}</span> 
                                    </div>

                                    <div class="col-md-4">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control"  placeholder="Enter Last Name" name="last_name" id="last_name" value="{{ old('last_name') || $student ? $student->last_name:''}}">
                                        <span class="text-danger parant_id">{{ $errors->first('last_name') }}</span> 
                                    </div>  
                                    <div class="col-md-4">
                                        <label>Gender</label>
                                            <select class="form-control"  id="gender" name="gender">
                                            <option value="">Select</option> 
                                            <option {{ ( old('gender') == 'male') ? 'selected' : ''  ||  ( $student->gender == 'male') ? 'selected' : ''  }} value="male">Male</option>
                                            <option {{ ( old('gender') == 'female') ? 'selected' : '' ||  ( $student->gender == 'female') ? 'selected' : ''   }}  value="female">Female</option>
                                        </select>
                                        <span class="text-danger parant_id">{{ $errors->first('gender') }}</span> 
                                    </div>                                                                    
                                </div>
                               
                                <div class="form-group row">

                                    <div class="col-md-4">
                                        <label>Date Of Birth:</label>
                                        <input type="text" class="form-control" name="dob" id="dob" 
                                        value="{{date('d-m-Y ',strtotime($student ? $student->dob:'' ))  }}">
                                        <span class="text-danger name">{{ $errors->first('dob') }}</span> 
                                    </div>
                                    <div class="col-md-4">
                                        <label>Category:</label>
                                        <select class="form-control" id="category" name="category">
                                          <?php echo $category ?>
                                        </select>
                                        <span class="text-danger name">{{ $errors->first('category') }}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Addmission Seeking Class:</label>
                                        <select class="form-control" id="admission_seeking" name="admission_seeking">
                                          <?php echo $class; ?>
                                        </select>
                                        <span class="text-danger name">{{ $errors->first('admission_seeking') }}</span> 
                                    </div>                                                                        
                                </div>
                                 <h5>Guardian Detail</h5>
  

                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Father's Name:</label>
                                        <input type="hidden" class="form-control" name="father_id" id="father_name" placeholder="" value="{{ $father ? $father->id:''}}">
                                        <input type="text" class="form-control" name="father_name" id="father_name" placeholder="Father's Name" value="{{ old('father_name') || $father ? $father->name:''}}">
                                        <span class="text-danger name">{{ $errors->first('father_name') }}</span>                                            
                                    </div>
                                    <div class="col-md-3">
                                        <label>Contact No.:</label>
                                        <input type="text" class="form-control" name="father_contact_no" id="father_contact_no" placeholder="Enter Contact No" maxlength="10" value="{{ old('father_contact_no') || $father ? $father->mobile:''}}">
                                        <span class="text-danger name" >{{ $errors->first('father_contact_no') }}</span>                                            
                                    </div>
                                    <div class="col-md-3">
                                        <label>Email:</label>
                                        <input type="email" class="form-control" name="father_email" id="father_email" placeholder="Enter Email" value="{{ old('father_email')  || $father ? $father->email:'' }}">
                                        <span class="text-danger name">{{ $errors->first('father_email') }}</span>                                            
                                    </div>  
                                    <div class="col-md-3">
                                        <label>Occupation:</label>
                                        <input type="text" class="form-control" name="occupation_father" id="occupation_father" placeholder="Enter Occupation" value="{{ old('occupation_father') || $father ? $father->occupation:''  }}">
                                        <span class="text-danger name">{{ $errors->first('occupation_father') }}</span>                                            
                                    </div>                                                                  
                                </div>   
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Mother's Name:</label>
                                        <input type="hidden" class="form-control" name="mother_id" id="father_name" placeholder="" value="{{ $mother ? $mother->id:''}}">
                                        <input type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Mother's Name" value="{{ old('mother_name') || $mother ? $mother->name:''  }}">
                                        <span class="text-danger name">{{ $errors->first('mother_name') }}</span>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label>Contact No:</label>
                                        <input type="text" class="form-control" name="mother_contact_no" id="mother_contact_no" placeholder="Enter Contact No" maxlength="10" value="{{ old('mother_contact_no') || $mother ? $mother->mobile:''}}">
                                        <span class="text-danger name" >{{ $errors->first('mother_contact_no') }}</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Email:</label>
                                        <input type="email" class="form-control" name="mother_email" id="mother_email" placeholder="Enter Email"  value="{{ old('mother_email') || $mother ? $mother->email:'' }}">
                                        <span class="text-danger name">{{ $errors->first('mother_email') }}</span>
                                    </div> 
                                    <div class="col-md-3">
                                        <label>Occupation:</label>
                                        <input type="text" class="form-control" name="occupation_mother" id="occupation_mother" placeholder="Enter Occupation" value="{{ old('occupation_mother')  || $mother ? $mother->occupation:''  }}">
                                        <span class="text-danger name">{{ $errors->first('occupation_mother') }}</span>
                                    </div>                                                                                          
                                </div>   

                                <h5>Address</h5>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label>State:</label>
                                        <input type="text" class="form-control" name="state" id="state" placeholder="Enter State" value="{{ old('state') || $student ? $student->state:''   }}">
                                        <span class="text-danger name">{{ $errors->first('state') }}</span>                                            
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label>City:</label>
                                        <input type="text" class="form-control" name="city" id="city" placeholder="Enter City" value="{{ old('city') || $student ? $student->city:''   }}">
                                        <span class="text-danger name">{{ $errors->first('city') }}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pincode:</label>
                                        <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Enter Pincode" value="{{ old('pincode') || $student ? $student->pincode:''  }}">
                                        <span class="text-danger name">{{ $errors->first('pincode') }}</span>
                                    </div>                                                          
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label>Address:</label>
                                        <textarea class="form-control" name="address" id="address" placeholder="Enter Address">{{ old('address') || $student ? $student->address:'' }}</textarea>
                                        <span class="text-danger name">{{ $errors->first('address') }}</span>                                            
                                    </div>                  
                                </div>                                                                 
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

