@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{url('user')}}">User</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">User edit</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User </h4>
                          <form id="user_form" method="POST" action="{{ action('UserController@update',$id) }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:</label>
                                        <input type="text"  id="name" class="form-control"   name="name" value="{{$user->name}}" >
                                         <span class="text-danger name">{{ $errors->first('name') }}</span> 
                                    </div>
                                    <div class="col">
                                        <label>Email:</label>
                                        <input type="email" id="email" class="form-control"  value="{{$user->email}}"name="email" >
                                         <span class="text-danger email">{{ $errors->first('email') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label for="exampleSelectGender">Role Id</label>
                                      <select name="role_id" id="role_id" class="form-control">
                                        <option disabled selected >Select RoleId</option>
                                        @if (isset($roledropdown))
                                      @foreach($roledropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}" {{ $user->role_id == $key ? 'selected="selected"' : '' }}>{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                       <span class="text-danger role_id">{{ $errors->first('role_id') }}</span>  
                                    </div>
                                </div>
                              
                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger">{{ $errors->first('username') }}</span>   
                                        
                                    </div>-->
                                <div class="col">
                                        <label>Password:</label>
                                        <input type="password" class="form-control" id="edit_password" placeholder="New Password ( if you want to change it )" name="password" >
                                      <span class="text-danger password">{{ $errors->first('password') }}</span>    
                                </div>
                                <div class="col">
                                        <label>Confirm Password:</label>
                                        <input type="password" class="form-control" id="edit_conf_password" placeholder="Confirm Password ( if you want to change it )" name="confirm_password" >
                                    <span class="text-danger conf_password">{{ $errors->first('confirm_password') }}</span>      
                                </div>
                                <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control" id="status" name="status" >
                                            <option disabled selected>Select Status</option>
                                            <option value="active" {{ $user->status == 'active' ? 'selected':'' }}>Active</option>
                                            <option value="inactive" {{ $user->status == 'inactive' ? 'selected':'' }}>Inactive</option>
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status') }}</span> 
                                    </div>
                                    
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                              
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
  </div>
</div>
</div>

@endsection