@extends('layouts.app')

@section('content')
            <div class="col-md-12">
                <div class="page-content">
                     <div class="row">
                      <div class="col-sm-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>View User</header>
                                 </div>
                                 <div class="card-body ">
                                 <div class="table-scrollable">
                                  <table id="mainTable" class="table table-striped">
                                  <thead>

                                  </thead>
                                  <tbody>
                                      <tr>
                                          <th>Name</th>
                                          <td>{{$user->name}}</td>
                                      </tr>
                                      <tr>
                                          <th>Email</th>
                                          <td>{{$user->email}}</td>
                                      </tr>
                                      <tr>
                                          <th>Role</th>
                                          <td>{{$role_name->name}}</td>
                                      </tr>
                                    <tr>
                                          <th>Status</th>
                                      <td>                           
                                             @if($user->status == 'Active')
                                                   <label class="badge badge-success">Active</label>
                                              
                                              @else
                                                    <label class="badge badge-danger">Pending</label>
                                               @endif
                                      </td>
                                      </tr>
                                      </tr></tr>                                        
                                      <tr>
                                          <th>Created At</th>
                                          <td> {{date("d-m-Y",strtotime($user->created_at))}}</td>
                                      </tr>                                       
                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>
                              </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
@endsection
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                