@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{url('user')}}">User</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">User create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                          <form id="user_form" method="POST" action="{{ url('user/store') }}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>FullName:<span class="required-mark">*</span></label>
                                        <input type="text"  id="name" class="form-control"  placeholder="Enter Fullname" name="name" >
                                         <span class="text-danger name">{{ $errors->first('name') }}</span> 
                                    </div>
                                    <div class="col">
                                        <label>Email:<span class="required-mark">*</span></label>
                                        <input type="email" id="email" class="form-control"  placeholder="Enter Email" name="email" >
                                         <span class="text-danger email">{{ $errors->first('email') }}</span>
                                        
                                    </div>
                                    <div class="col">
                                        <label >Role Id:<span class="required-mark">*</span></label>
                                      <select name="role_id" id="role_id" class="form-control">
                                        <option value="">Select RoleId</option>
                                        @if (isset($roledropdown))
                                      @foreach($roledropdown as $key => $dropdownGroup)
                                      <option value ="{{$key}}">{{ $dropdownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                                       <span class="text-danger role_id">{{ $errors->first('role_id') }}</span>  
                                    </div>
                                </div>
                               <!-- <div class="form-group row" id="client" style="display: none;">
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col">
                                    <label >ClientId:<span class="required-mark">*</span></label>
                                      <select name="client_id" id="client_id" class="form-control" required="">
                                        <option value='0'>Select Client</option>
                                        @if (isset($clientdropdown))
                                      @foreach($clientdropdown as $key => $cliedownGroup)
                                      <option value ="{{$key}}">{{ $cliedownGroup }} </option>
                                       @endforeach
                                       @endif
                                    </select>
                               
                                    </div>
                                </div>-->
                                <div class="form-group row">
                                 <!-- <div class="col">
                                        <label>Username:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Username" name="username" >
                                       <span class="text-danger">{{ $errors->first('username') }}</span>   
                                        
                                    </div>-->
                                <div class="col">
                                        <label>Password:<span class="required-mark">*</span></label>
                                        <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" >
                                      <span class="text-danger password">{{ $errors->first('password') }}</span>    
                                </div>
                                <div class="col">
                                        <label>Confirm Password:<span class="required-mark">*</span></label>
                                        <input type="password" class="form-control" id="conf_password" placeholder="Enter Confirm Password" name="confirm_password" >
                                    <span class="text-danger conf_password">{{ $errors->first('confirm_password') }}</span>      
                                </div>
                                <div class="col">
                                        <label for="exampleSelectGender">Status:<span class="required-mark">*</span></label>
                                        <select class="form-control select2" id="status" name="status">
                                            <option value="">Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status_a') }}</span> 
                              </div>
                                    
                              </div>
                              <div class="form-group row">
     
                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">

</script>
<script>
   if ($("#customerform").length > 0) {
    $("#customerform").validate({
     
    rules: {
    name: {
        required: true,
        maxlength: 150
      },
    email: {
        required: true,
        email: true,
      },
    role_id: {
        required: true,
      },
   
    password: {
          required: true,
          maxlength: 50,

    }, 
    confirm_password: {
        required: true,
       equalTo: "#password"
      
      },
   
    status_a: {
          required: true,
         
    },    
  },
    messages: {
       
      name: {
        required: "Please Enter First name",
        maxlength: "Your last name maxlength should be 150 characters long."
      },


    email: {
        required: "Please Enter Email address",
          email: "Please enter valid email",
        
      },
    role_id: {
        required: "Please Select Role id",
       
      }, 
    password: {
          required: "Please Enter Password",
          
        },
    status_a: {
          required: "Please select status",
          
      },
       
    },
    })
  }
</script>
@endsection