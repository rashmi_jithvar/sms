@extends('layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="col-xl-8 col-md-12">
    <div class="row">

        <div class="col-md-6">
            <div class="card table-card">
                <div class="row-table">
                    <div class="col-sm-6 card-body-big br">
                         <a href="{{url('student')}}">
                        <div class="row">
                           
                            <div class="col-sm-4">
                                <i class="icon feather icon-users text-c-green mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{ count($total_student ? $total_student:'' )}}</h5>
                                <span>Students</span>
                            </div>
                            
                        </div>
                        </a>
                    </div>
                    <div class="col-sm-6 card-body-big">
                        <a href="{{url('student-enquiry')}}">
                        <div class="row">
                            <div class="col-sm-4">
                                <i class="icon feather icon-search text-c-red mb-1 d-block"></i>
                            </div>
                            <div class="col-sm-8 text-md-center">
                                <h5>{{ count($total_enquiry ? $total_enquiry:'' )}}</h5>
                                <span>Enquiry</span>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>



    </div>
</div>


@endsection