@extends('layouts.app', ['title' => 'Page List'])

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">SubProperty type</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
  <div class="card">
    <div class="card-header">
            <a class="btn btn-primary m-t-5"  href="{{ url('subproperty_type/create') }}">Create Property</a>
        </div>
  </div>
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
          <div class="card-header">
                <h5>Roles List</h5>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                   <table id="example1" class="display" style="width:100%;">
                     <thead>
                       <tr>
                         <th>Sr.No. </th>
                         <th>Property Type</th>
                         <th>Name</th>
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        @php $i = 0
                        @endphp
                        @if(!empty($properties))
                        @foreach($properties as $property)
                        @php $i++
                        @endphp
                        <tr>
                          <td>{{$i}}</td>
                          @foreach($property->parent as $parent)
                          <td>{{$parent->name}}</td>
                          @endforeach
                          <td>{{$property->name}}</td>
                          <td>
                          @if($property->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                           <form id="my_form" action="{{action('RolesController@destroy', $property->id)}}" method="get">
                           {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                             <a href="{{route('subproperty_type.edit',['id'=>$property->id])}}"  role="button"><i class="feather icon-edit" aria-hidden="true"></i></a>
                           <button style="background: none!important;border: none;padding: 0!important;color: #566673;" Onclick="return confirm();"><i class="feather icon-trash-2" ></i></button>
                          </form>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection