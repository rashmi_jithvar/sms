@extends('layouts.app')
@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }}  @endisset List</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    
                    <li class="breadcrumb-item"><a href="javascript:void(0)">@isset($title) {{ $title }}  @endisset  List</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
   @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
   @endif
</div>

<div class="col-sm-12">
   <div class="content-wrapper">
        <div class="card">
            <div class="card-header">
              <div class="pull-left"> <h5>@isset($title) {{ $title }}  @endisset List</h5></div>
               <div class="pull-right"> 
                <a class="btn btn-primary m-t-5"  href="{{ url('add-subject-master') }}">Add Subject Master</a>
              </div>            
            </div>
            <div class="card-body table-border-style">
            <br>            
                <div class="table-responsive">
                    <div id="coverScreen"  class="LockOn" style="display: none;">
                    </div>  
                   <table id="lang-dt" class="table table-striped table-bordered nowrap">
                     <thead>
                       <tr>
                         <th>#</th>
                        
                         <th>Class </th>
                         <th>Subjects</th>                       
                         <th>Status</th>
                         <th>Actions</th>
                       </tr>                  
                    </thead>
                    <tbody>
                        @php $i = 0
                        @endphp
                        @if(!empty($list))
                        @foreach($list as $li)
                        @php $i++
                        @endphp
                        <?php if(count($li->SubjectMaster) > 0){?>
                        <tr>
                          <td>{{$i}}</td>
                          <td>{{$li ? $li->name:""}}</td> 
                          <td>@if(isset($li->SubjectMaster))
                                @foreach($li->SubjectMaster as $sub)
                                      {{$sub->getSubject->name}}
                                      <br>
                                @endForeach
                              @endif
                          </td>                      
                          <td>
                          @if($li->status == '1')
                            <label class="badge badge-success">Active</label>
                          @else
                            <label class="badge badge-danger">Inactive</label>
                          @endif
                          </td>
                          <td>
                              <a href="{{url('subject-master/edit',$li->id)}}" title="update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
  
                              <a href="{{url('subject-master/delete',$li->id)}}" title="delete"><i class="fa fa-trash" aria-hidden="true" Onclick="return ConfirmDelete();"></i></a>
                          
                          </td>

                        </tr>
                      <?php } ?>

                      @endforeach
                      @endif
                 
                      </tbody>

                       <tbody id="filter"></tbody>   
                      
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

