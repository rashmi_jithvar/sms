@extends('layouts.app')
@section('content')

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
  .toggle.ios .toggle-handle { border-radius: 20rem; }
</style>
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ route('subject-master') }}">@isset($title) {{ $title }} @endisset List</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Create</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">

                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            <h4 class="card-title">Add {{$title ? $title :''}}</h4>
                            <form id="propertytype_form" method="POST" action="{{ route('subject-master.store') }}">
                                @csrf
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{ Session::get('message') }}
                                    @php
                                    Session::forget('message');
                                    @endphp
                                </div>
                                @endif
                               

                                
                                <div class="form-group row">
                                    <div class="col">
                                        <label>Class</label>
                                        <select class="form-control js-example-basic-multiple" id="e1 class_id" name="class_id">
                                          <?php echo $class ?>
                                        
                                        </select>
                                        <span class="text-danger class_id">{{ $errors->first('class_id') }}</span> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label>Subject</label>
                                        <select class="form-control js-example-basic-multiple" id="e1 subject_id" name="subject_id[]" multiple="multiple">
                                          <?php echo $subject ?>
                                        
                                        </select>
                                        <span class="text-danger subject_id">{{ $errors->first('subject_id') }}</span> 
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="switch switch-primary d-inline m-r-10">
                                        <input type="checkbox" id="switch-p-1" checked="" name="status">
                                        <label for="switch-p-1" class="cr"></label>
                                        </div>
                                        <label>Status</label>
                                                                   
                                </div>
                                
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
@endsection
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script type="text/javascript">
    //var selectedValuesTest = ["WY", "AL", "NY"];
$(document).ready(function() {
  $("#e1").select2({
    multiple: true,
  });
  //$('#e1').val(selectedValuesTest).trigger('change');
});
</script>