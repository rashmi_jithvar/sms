@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>@isset($title) {{ $title }} @endisset</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="{{ url('permission') }}">Permission</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Permission edit</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
              

<div class="main-panel">
    <div class="content-wrapper">
       
            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Update Permission </h4>
                          <form id="permissiongroup_form" method="POST" action="{{action('PermissionController@update', $id)}}">
                                            @csrf
                                            @if(Session::has('message'))
                                                    <div class='alert alert-success'>
                                                    {{ Session::get('message') }}
                                                    @php
                                                    Session::forget('message');
                                                    @endphp
                                                    </div>
                                            @endif
                                <div class="form-group row">
                                    <div class="col">
                                      <label for="City">Choose GroupName</label>
                                      <select name="group_id" class="form-control" id="groupname">
                                        <option value="">Select Permission Group</option>
                                        @if (isset($groupdropdown))
                                    @foreach($groupdropdown as $key => $dropdownGroup)

                                      <option value ="{{$key}}" @if($permission->group_id==$key) selected='selected' @endif>{{ $dropdownGroup }} </option>
                                      @endforeach
                                       @endif
                                    </select>
                                    <span class="text-danger groupname">{{ $errors->first('group_id') }}</span> 
                                    </div>
                
                                    <div class="col">
                                        <label>Name:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Roles" name="name" value="{{$permission->name}}" id="name">
                                        <span class="text-danger name">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                
                                    <div class="col">
                                        <label>Controller Name:</label>
                                        <input type="text" class="form-control"  placeholder="Enter Roles" name="controller_name" value="{{$permission->controller_name}}" id="controller_name">
                                        <span class="text-danger controller_name">{{ $errors->first('controller_name') }}</span>
                                    </div>
                                
                
                                    <div class="col">
                                        <label>Action Name:</label>
                                        <input type="text" class="form-control" id="action_name" placeholder="Enter Roles" name="action_name" value="{{$permission->action_name}}" >
                                         <span class="text-danger action_name">{{ $errors->first('action_name') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleSelectGender">Status</label>
                                        <select class="form-control"  name="status" id="status">
                                            <option value="">Select Status</option>
                                            <option value="1" @if($permission->status=='1') selected='selected' @endif>Active</option>
                                            <option value="0" @if($permission->status == '0' ) 'selected':'' @endif>Inactive</option>
                                        </select>
                                        <span class="text-danger status">{{ $errors->first('status') }}</span>
                                    </div>

                                </div>
                               <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</div>

@endsection