<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use Master;

class User extends Authenticatable
{
    use Notifiable;

    const Admin = '1';
    const User = '2';

    //status
    const ACTIVE = 'Active';
    const INACTIVE = 'Inactive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        public function updateUser($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->name = $data['name'];
        $ticket->email = $data['email'];
        if($data['password']){
            $password = $data['password'];
        $ticket->password = \Hash::make($password);
        }
        $ticket->role_id = $data['role_id'];
        $ticket->status = $data['status'];
        $ticket->save();
        return 1;

    }

    public function AddUser($data,$password)
  {

   
    $user = new user();
        $user->name = $data['first_name'];
        $user->email = $data['email'];
        $user->password = Hash::make($password);
        $user->role_id = user::Client;
        $user->status = user::ACTIVE;
        $user->remember_token = request('_token');
        $user->save();
        return $user->id;
   }

   protected function permission(){
     $roles = \DB::select("SELECT * FROM `tbl_role_permission` as role_permission inner join tbl_permission as permission on role_permission.permission_id=permission.id where role_id='".auth()->user()->role_id."'");
   
     if(!empty($roles)){
       foreach ($roles as $key => $value) {
         $routeArray[] =$value->controller_name .'@'.$value->action_name;
       }
      return $routeArray;
     }
   }

   /**
     * Send the password reset notification.
     * @note: This override Authenticatable methodology
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }   




}


