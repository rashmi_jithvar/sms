<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Requests;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliderList = Slider::get();
        $tableName = "sliders";
        return view('cms_mangment.addSliderPage', compact('sliderList', 'tableName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->HasFile('photo')) {
            $imageName = time().'.'.request()->photo->getClientOriginalExtension();
            request()->photo->move(public_path('slider'), $imageName);
        } else {
            $imageName = NULL;
        }
            $posted = new Slider;
            $posted->image = $imageName;
            $posted->content = $request->content;
            $posted->save();

            return response()->json([
                "message" => "Slide Added Successfully!"
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Slider $slider)
    {
        $slideInfo = Slider::where('id', $request->id)
                        ->first();

        return view('cms_mangment.editSliderPage', compact('slideInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $Chk = Slider::where('id', $request->recordId)->first();
        if(!empty($Chk))
        {
            if($request->HasFile('photo')) {

                if(\File::exists(public_path('slider/'.$request->oldSlide))){

                    \File::delete(public_path('slider/'.$request->oldSlide));
                
                  }
                
                $imageName = time().'.'.request()->photo->getClientOriginalExtension();
                request()->photo->move(public_path('team'), $imageName);
            } else {
                $imageName = $request->oldSlide;
            }
            $form_data = array(
            'content' => $request->content,
            'image' => $imageName
            );
  
            Slider::whereId($request->recordId)->update($form_data);
            return response()->json([
                "message" => "Slide Updated Successfully!"
            ], 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        //
    }
}
