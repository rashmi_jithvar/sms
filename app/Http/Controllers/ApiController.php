<<<<<<< .mine
<?php
/**
 * Class and Function List:
 * Function list:
 * - forgotPassword()
 * - (()
 * - ConfirmOtp()
 * - AuditSource()
 * - Replication()
 * - AuditScore()
 * - AuditAnswer()
 * - QuestionCategory()
 * - delete()
 * - View()
 * - AuditType()
 * - QuestionGroup()
 * - Question()
 * - Organization()
 * - Department()
 * - Sites()
 * - Team()
 * - Template()
 * - AuditPlanAuditDetail()
 * - AuditPlanSchedule()
 * - AuditPlanResponsibility()
 * - SignAuditUpdateStatus()
 * - SignAuditSummary()
 * - SignAuditSummaryStrength()
 * - SignAuditSummaryAuditFinding()
 * - SignAuditSummaryAuditImprovement()
 * Classes list:
 * - ApiController extends Controller
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use App\Models\Audit_Source;
use App\Models\Replication;
use App\Models\Audit_Scoring;
use App\Models\Audit_Answer;
use App\Models\Question_category;
use App\Models\Question;
use App\Models\Question_group;
use App\Models\Audit_type;
use App\Models\Sites;
use App\Models\QuestionAnswer;
use App\Models\Organization;
use App\Models\Template;
use App\Models\Department;
use App\Models\Team;
use App\Models\AuditPlanAuditDetail;
use App\Models\SignAuditUpdateStatus;
use App\Models\AuditPlanSchedule;
use App\Models\AuditPlanResponsibility;
use App\Models\SignAuditSummaryStrength;
use App\Models\SignAuditSummaryAuditFinding;
use App\Models\SignAuditSummaryAuditImprovement;
use App\Models\SignAuditQuestionCompliant;
use App\Models\SignAuditCorrectiveActions;
use App\Models\SignAuditSummary;
use App\Models\AuditSign;
use App\Models\AuditPlanPerformQA;
use App\Models\SendMail;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PDF;
use Auth;
use DB;

class ApiController extends Controller
{
    /**
     * Forgot Password.
     * @param string $email
     */
    public function forgotPassword(Request $request)
    {

        $validation = Validator::make($request->all() , ['email' => 'required|email']);

        if ($validation->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_SUCCESS_OK,
                'data' => null,
                'message' => 'Please enter valid email id',
                'errors' => $validation->errors() ,
            ) , RESPONSE_CODE_SUCCESS_OK);
        }
        else
        {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $result = '';
            for ($i = 0;$i < 8;$i++) $result .= $characters[mt_rand(0, 61) ];

            $user = Users::where('email', $request->email)
                ->first();
            $user->updateToken($result, $user->id);
            /**
             * Account Details of User
             */
            //$name = $user->name;
            $email = $user->email;
            $id = $user->id;
            //$apiKey = urlencode('orisGPoTsWY-9nmLyhbGE8S4UVGrgfXxYpdo4Ci1bS');

            /**
             * Message Details Send Message
             */
            //$numbers = $user->mobile;
            //$sender = urlencode('TXTLCL');
            $otp_message = 'Your OTP for login into academic tracker is ' . $result . '. Please don\'t share your OTP with anyone.';

            /**
             * Send Mail
             * @param $otp_message,$email
             */
            Mail::send([], array(
                'yourValue' => 'OTP(One Time Password)'
            ) , function ($message) use ($otp_message, $email)
            {
                $message->setBody($otp_message, 'text/html');
                $message->subject('OTP(One Time Password)');
                $message->to($email)->cc($email);
            });
            if (empty($message->errors))
            {
                $data = 1;
            }
            else
            {
                $data = 0;
            }
            /**
             * Response
             */
            if ($data == 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => null,
                    'message' => 'An OTP has send to your mail, Please use it as password',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => null,
                    'message' => 'Something went wrong, Please try again.',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

        }
    }

    /**
     * Confirm Otp
     *
     * @return mixed
     */
    public function ConfirmOtp(Request $request)
    {
        $validator = Validator::make($request->all() , ['id' => 'required', 'otp' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            $user = Users::where('otp', $request->otp)
                ->Where('id', $request->id)
                ->first();

            if (count($user) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $user,
                    'message' => 'User login Successfully',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
        }
    }

    /**
     * CURD Audit Source
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditSource(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Audit_Source();
                            return $deleteQuery->deleteAuditSource($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Audit_Source();
                            return $checkId->updateAuditSource($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Audit_Source();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Audit_Source();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_Source();
                    return $indexQuery->indexAuditSource($request,$user->company_id);
                }
            }                

            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }




    /**
     * Add Replication Fields
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Replication(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',
        //'status' =>'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Replication();
                            return $deleteQuery->deleteReplication($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Replication();
                            return $checkId->updateReplication($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Replication();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Replication();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Replication();
                    return $indexQuery->indexReplication($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }
    
    /**
     * Audit Score Fields
     * For Add
     * @param audit_type,name,min,max &  abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditScore(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Audit_Scoring();
                            return $deleteQuery->deleteAuditScoring($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Audit_Scoring();
                            return $checkId->updateAuditScoring($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Audit_Scoring();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Audit_Scoring();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_Scoring();
                    return $indexQuery->indexAuditScoring($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Audit Answer Fields
     * For Add
     * @param  name, compliance,status &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditAnswer(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Audit_Answer();
                            return $deleteQuery->deleteAuditAnswer($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Audit_Answer();
                            return $checkId->updateAuditAnswer($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Audit_Answer();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Audit_Answer();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Audit_Answer();
                    return $indexQuery->indexAuditAnswer($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question Category Fields
     * For Add
     * @param basic_risk,category,status & abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionCategory(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        //'status' =>'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE3CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question_category();
                            return $deleteQuery->deleteQuestionCategory($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question_category();
                            return $checkId->updateQuestionCategory($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question_category();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question_category();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Question_category();
                    return $indexQuery->indexQuestionCategory($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     *Audit type CRUD
     * For Add
     * @param name,description,site& abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditType(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Audit_type();
                            return $deleteQuery->deleteAuditType($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Audit_type();
                            return $checkId->updateAuditType($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Audit_type();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Audit_type();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_type();
                    return $indexQuery->indexAuditType($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

     /**
     *Question Group Fields
     * For Add
     * @param audit_type,question_group_name,manditory& abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionGroup(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question_group();
                            return $deleteQuery->deleteQuestionGroup($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question_group();
                            return $checkId->updateQuestionGroup($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question_group();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question_group();
                      return $AddQuery->add($request);    
                    }
                    else if($request->abbr == 'get_answer')
                    {   
                      $AddQuery = new Question_group();
                      return $AddQuery->getAnswered($request,$user->company_id);    
                    }                      
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Question_group();
                    return $indexQuery->indexQuestionGroup($request,$user->company_id);
                }
          }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Question(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question();
                            return $deleteQuery->deleteQuestion($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question();
                            return $checkId->updateQuestion($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Question();
                    return $indexQuery->indexQuestion($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question CRUD
     * For Add
     * @param name ,score,abbr = add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionAnswer(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new QuestionAnswer();
                            return $deleteQuery->deleteQuestionAnswer($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new QuestionAnswer();
                            return $checkId->updateQuestionAnswer($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new QuestionAnswer();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new QuestionAnswer();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new QuestionAnswer();
                    return $indexQuery->indexQuestionAnswer($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }    
    /**
     * Organization CRUD
     * For Add
     * @param string company_name, abbr= add
     * @param Int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Organization(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Organization();
                                return $deleteQuery->deleteOrganization($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Organization();
                                return $checkId->updateOrganization($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Organization();
                                return $view->View($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Organization();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Organization();
                    return $indexQuery->indexOrganization($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Department CRUD
     * For Add
     * @param String department_manager_name,department_manager_email_id,department_name abbr= add
     * @param int organization_id,status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Department(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Department();
                                return $deleteQuery->deleteDepartment($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Department();
                                return $checkId->updateDepartment($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Department();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Department();
                      return $AddQuery->add($request,$user->company_id);    
                    }
                    else if($request->abbr == 'getDepartment')
                    {
                      $AddQuery = new Department();
                      return $AddQuery->getDepartment($request,$user->company_id);    
                    }                       
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Department();
                    return $indexQuery->indexDepartment($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Sites CRUD
     * For Add
     * @param String company_name,location_name,site_name abbr= add
     * @param int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Sites(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Sites();
                                return $deleteQuery->deleteSites($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Sites();
                                return $checkId->updateSites($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Sites();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Sites();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Sites();
                    return $indexQuery->indexSites($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Team CRUD
     * For Add
     * @param String username,user_email,user_phone abbr= add
     * @param int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Team(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Team();
                                return $deleteQuery->deleteTeam($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Team();
                                return $checkId->updateTeam($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Team();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Team();
                      return $AddQuery->add($request,$user->company_id);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Team();
                    return $indexQuery->indexTeam($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Add/Update Template
     * For Update
     * @param integer $id
     * @return response
     */
    /**
     * Template CRUD
     * For Add
     * @param String template abbr= add
     * @param int audit_type
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Template(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Template();
                                return $deleteQuery->deleteTemplate($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Template();
                                return $checkId->updateTemplate($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Template();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Template();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Template();
                    return $indexQuery->indexTemplate($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-audit-detail
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanAuditDetail(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'reference' => 'required', 'audit_QA' => 'required', 'title' => 'required', 'scope' => 'required', 'audit_source' => 'required', 'department' => 'required', 'department_manager' => 'required', 'audit_status' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                 if($request->abbr=='update')
                {
                    $checkId = new AuditPlanAuditDetail();
                    return $checkId->updateAuditPlanAuditDetail($request);

                }
                else
                {
                    $templateQuery = new AuditPlanAuditDetail();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-schedule
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanSchedule(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'planned_date' => 'required', 'start_date' => 'required', 'completed_date' => 'required', 'close_date' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                 if($request->abbr=='update')
                {
                    $checkId = new AuditPlanSchedule();
                    return $checkId->updateAuditPlanSchedule($request);

                }
                else
                {
                    $templateQuery = new AuditPlanSchedule();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-responsibility
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanResponsibility(Request $request)
    {
        
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'auditee' => 'required', 'lead_auditor' => 'required', 'auditor' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new AuditPlanResponsibility();
                    return $checkId->updateAuditPlanResponsibility($request);

                }
                else
                {
                    $templateQuery = new AuditPlanResponsibility();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-update-status
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditUpdateStatus(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'audit_status' => 'required', 'result' => 'required', 'plan_audit_detail_id' => 'required','status'=>'required'

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new SignAuditUpdateStatus();
                    return $checkId->updateSignAuditUpdateStatus($request);

                }
                else
                {
                    $templateQuery = new SignAuditUpdateStatus();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummary(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'summary' => 'required', 'plan_audit_detail_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new SignAuditSummary();
                    return $checkId->updateSignAuditSummary($request);

                }
                else
                {
                    $templateQuery = new SignAuditSummary();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-strength
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryStrength(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'strenghts' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                   
                    $checkId = new SignAuditSummaryStrength();
                    return $checkId->updateSignAuditSummaryStrength($request);

                }
                else
                {
                    $templateQuery = new SignAuditSummaryStrength();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-auditFinding
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryAuditFinding(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'audit_id'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {
                                $deleteQuery = new SignAuditSummaryAuditFinding();
                                return $deleteQuery->deleteAuditFinding($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new SignAuditSummaryAuditFinding();
                                return $checkId->updateAuditFinding($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new SignAuditSummaryAuditFinding();
                                return $view->viewAuditFinding($request);
                            }
                            else if($request->abbr == 'uploadImage')
                            {
                   
                              $AddQuery = new SignAuditSummaryAuditFinding();
                              return $AddQuery->upload($request);    
                            }                              
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditSummaryAuditFinding();
                      return $AddQuery->add($request);    
                    }
                  
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditSummaryAuditFinding();
                    return $indexQuery->indexAuditFinding($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-audit-improvement
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryAuditImprovement(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','audit_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {
                                $deleteQuery = new SignAuditSummaryAuditImprovement();
                                return $deleteQuery->deleteAuditImprovement($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new SignAuditSummaryAuditImprovement();
                                return $checkId->updateAuditImprovement($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new SignAuditSummaryAuditImprovement();
                                return $view->viewAuditImprovement($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditSummaryAuditImprovement();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditSummaryAuditImprovement();
                    return $indexQuery->indexAuditImprovement($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function SignAuditQuestionCompliant(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','audit_id'=>'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new SignAuditQuestionCompliant();
                            return $deleteQuery->deleteQuestionCompliant($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new SignAuditQuestionCompliant();
                            return $checkId->updateQuestionCompliant($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new SignAuditQuestionCompliant();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditQuestionCompliant();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditQuestionCompliant();
                    return $indexQuery->indexQuestionCompliant($request);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 

    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function SignAuditCorrectiveActions(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new SignAuditCorrectiveActions();
                            return $deleteQuery->deleteCorrectiveActions($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new SignAuditCorrectiveActions();
                            return $checkId->updateCorrectiveActions($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new SignAuditCorrectiveActions();
                            return $view->view($request);
                        }
                        else if($request->abbr=='get_last_id')
                        {

                            $view = new SignAuditCorrectiveActions();
                            return $view->getLastId($request);
                        }                        
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditCorrectiveActions();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditCorrectiveActions();
                    return $indexQuery->indexCorrectiveActions($request);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }  

    /*Team List*/
    public function TeamList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
           $list = Team::join('users', 'tbl_team.created_by', '=', 'users.id')->where('users.company_id' ,'=',$user->company_id)->pluck('tbl_team.username', 'tbl_team.id');

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }  
    public function PlanScheduleList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
       
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
         /* FULL details */       
        /*    $audit_info = DB::table('tbl_audit_plan_audit_details')
            ->select("tbl_audit_plan_audit_details.*","tbl_audit_plan_schedule.id as schedule_id","tbl_audit_plan_schedule.planned_date","tbl_audit_plan_schedule.start_date","tbl_audit_plan_schedule.completed_date","tbl_audit_plan_schedule.close_date","tbl_plan_audit_responsibility.id as responsibility_id","tbl_plan_audit_responsibility.auditee","tbl_plan_audit_responsibility.lead_auditor","tbl_plan_audit_responsibility.auditor","tbl_department.department_name")
            ->join('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->join('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
           // ->join('tbl_sign_audit_summary', 'tbl_sign_audit_summary.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_sign_audit_summary_strength', 'tbl_sign_audit_summary_strength.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_sign_audit_update_status', 'tbl_sign_audit_update_status.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_audit_sign', 'tbl_audit_sign.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            ->join('users', 'tbl_audit_plan_audit_details.created_by', '=', 'users.id')
            ->join('tbl_department', 'tbl_department.id', '=', 'tbl_audit_plan_audit_details.department')
            ->where('users.company_id' ,'=',$user->company_id)
            ->get();

            foreach($audit_info as $val){
                    $audit_id=$val->id;
            $allover_summary =DB::table('tbl_sign_audit_summary')
                        ->where('tbl_sign_audit_summary.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();  
            $summary_strength =DB::table('tbl_sign_audit_summary_strength')
                        ->where('tbl_sign_audit_summary_strength.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();    
            $audit_update_status =DB::table('tbl_sign_audit_update_status')
                        ->where('tbl_sign_audit_update_status.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();  
            $audit_signup =DB::table('tbl_audit_sign')
                        ->where('tbl_audit_sign.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();                 
            $audit_finding =DB::table('tbl_sign_audit_summary_audit_finding')
                        ->where('tbl_sign_audit_summary_audit_finding.audit_summary_id' ,'=',$audit_id)
                        ->get();
                        
            $audit_improvement =DB::table('tbl_sign_audit_summary_audit_improvement')
                        ->where('tbl_sign_audit_summary_audit_improvement.audit_summary_id' ,'=',$audit_id)
                        ->get();

            $audit_corrective_actions =DB::table('tbl_sign_audit_corrective_actions')
                        ->where('tbl_sign_audit_corrective_actions.sign_audit_id' ,'=',$audit_id)
                        ->get();                         
            $list[] = array('sign_audit_info' =>$val ,'allover_summary'=>$allover_summary,'summary_strength'=>$summary_strength,'audit_update_status'=>$audit_update_status,'$audit_signup'=>$audit_signup,'audit_finding' =>$audit_finding ,'audit_improvement' =>$audit_improvement ,'audit_corrective_actions' =>$audit_corrective_actions);                    
            }*/
            if(empty($list)){
            $list = DB::table('tbl_audit_plan_audit_details')
            ->select("tbl_audit_plan_audit_details.*","tbl_audit_plan_schedule.id as schedule_id","tbl_audit_plan_schedule.planned_date","tbl_audit_plan_schedule.start_date","tbl_audit_plan_schedule.completed_date","tbl_audit_plan_schedule.close_date","tbl_plan_audit_responsibility.id as responsibility_id","tbl_plan_audit_responsibility.auditee","tbl_plan_audit_responsibility.lead_auditor","tbl_plan_audit_responsibility.auditor","tbl_department.department_name")
            ->leftJoin('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->leftJoin('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            ->join('users', 'tbl_audit_plan_audit_details.created_by', '=', 'users.id')
            ->join('tbl_department', 'tbl_department.id', '=', 'tbl_audit_plan_audit_details.department')
            ->where('users.company_id' ,'=',$user->company_id)
            ->where('tbl_audit_plan_audit_details.created_by' ,'=',$request->user_id)
            ->get();
            }

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    public function AuditSignList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
            $list = DB::table('tbl_audit_plan_audit_details')
            ->join('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->join('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')->get();

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }    

    public function TeamList1(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
            $list = Team::pluck('username', 'id');

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditSign(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'username' => 'required', 'password' => 'required', 'user_sign_as' => 'required', 'plan_audit_detail_id' => 'required|unique:tbl_audit_sign',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
             $CheckUser = Users::with('hasType')->where('email', '=', $request->username)->first();

             if (!empty($CheckUser)) {
                $id= $CheckUser->id;
                if (password_verify($request->password, $CheckUser->password)) {  
                    
                    $checkId = new AuditSign();
                    return $checkId->add($request,$id);
                        if($CheckUser)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => $checkId,
                                    'message' => 'User logged successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                     else {

                        if ($CheckUser->status_enum == ENUM_STATUS_INACTIVE) {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'User status Inactive. Please contact admin.',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                        }
                    }
                }
                else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Incorrect password',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                }
              }
            else {
                return response()->json(
                    array(
                        'status' => false,
                        'code' => RESPONSE_CODE_ERROR_BAD,
                        'data' => null,
                        'message' => 'Email not registered',
                        'errors' => null,
                    ), RESPONSE_CODE_ERROR_BAD
                );
            }              
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 
    /**
     * Delete Plan Schedule
     * For Delete
     * @param Request $Request
     * @return response
     */
    public function DeletePlanSchedule(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'id' => 'required', 

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
             $audit_detail = AuditPlanAuditDetail::where('id', '=', $request->id)->first();
             if(!empty($audit_detail)){
                $planSchudule = AuditPlanSchedule::where('plan_audit_detail_id', '=', $request->id)->first();
                if($planSchudule){
                   $audit_detail->delete();                    
                }
                $responsibility = AuditPlanResponsibility::where('plan_audit_detail_id', '=', $request->id)->first();
                if($responsibility){
                    $responsibility->delete();          
                }
                $planSchudule->delete();
                $data =1;
             }
             else{
                $data = 0;
             }
             if($data==1)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => null,
                                    'message' => 'Delete Successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                    else 
                    {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'Try Again..!',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                    }
            }
                else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Invalid Login',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                   }
            } 
    }

    /**
     * View
     *
     * @return response
     */
    public function ViewAuditPlan(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'abbr' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();

            if (!empty($user))
            {
                if ($user->token == $request->token)
                {
                        if ($request->abbr == 'audit_detail')
                        {
                            $query = AuditPlanAuditDetail::all();
                            
                            if (empty($query))
                            {
                                $data = 0;
                            }

                        }
                        else if ($request->abbr == 'schedule')
                        {
                            $query = AuditPlanSchedule::all();

                            if (empty($query))
                            {

                                $data = 0;
                            }
                        }

                        else if ($request->abbr == 'responsibilty')
                        {
                            $query = AuditPlanResponsibility::all();
                            if (empty($query))
                            {

                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'summary')
                        {
                            $query = SignAuditSummary::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'summary_strength')
                        {
                            $query = SignAuditSummaryStrength::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_status')
                        {
                            $query = SignAuditUpdateStatus::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_sign')
                        {
                            $query = AuditSign::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_finding')
                        {
                            $query = SignAuditSummaryAuditFinding::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_improvement')
                        {
                            $query = SignAuditSummaryAuditImprovement::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_question_compliant')
                        {
                            $query = SignAuditQuestionCompliant::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_corrective_actions')
                        {
                            $query = SignAuditCorrectiveActions::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr != '')
                        {

                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => null,
                                'message' => 'Table not found.',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                        if (isset($data))
                        {
                            return response()->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => 'null',
                                'message' => 'Data not Found!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                        else
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => $query,
                                'message' => 'Data  Found!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                    }
                
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 
    /**
     * Send Mail
     *
     * @return mixed
     */
    public function SendMail(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','subject' =>'required','sent_to' =>'required','content' =>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
            /* 
            *   Send Mail 
            */             
                $subject = $request->subject;
                Mail::send([], array(
                    'yourValue' => $subject
                ) , function ($message) use ($request)
                {
                    $body = file_get_contents('../mail.html');
                    $body = str_replace('$$content$$', $request->content, $body);
                    $body = str_replace('$$user_name$$', $request->sent_to, $body);
                    //$body = str_replace('$$form$$', $form, $body);
                    if($request->attachment){
                       $message->attachData($request->attachment, 'File',[ 'as' => 'file.zip','mime' => 'application/pdf/doc']);
                    }
                    $message->setBody($body, 'text/html');
                    $message->to($request->sent_to);
                    if($request->cc){
                        $message->cc($request->cc);
                    }
                    if($request->bcc){
                        $message->bcc($request->bcc);
                    }   
                   
                  $message->subject('Mail');
                   
                    
                });
                if (empty($message->errors))
                {
                    $data = 1;
                }
                else
                {
                    $data = 0;
                }
          $send_mail = new SendMail();
          $send_mail->add($request,$user->email);      
              
            if ($data = 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $send_mail,
                    'message' => 'Email Send Successfully..!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
          }
        else {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                    'data' => null,
                    'message' => 'Invalid Login',
                    'errors' => null,
                    ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                );
            }
        }
    } 

    /**
     * CURD Audit Source
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditPlanPerformQA(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new AuditPlanPerformQA();
                            return $deleteQuery->deleteAuditPlanPerformQA($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new AuditPlanPerformQA();
                            return $checkId->updateAuditPlanPerformQA($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new AuditPlanPerformQA();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new AuditPlanPerformQA();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new AuditPlanPerformQA();
                    return $indexQuery->indexAuditPlanPerformQA($request,$user->company_id);
                }
            }                

            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /*
        Desc : to route a request based on different audit type
        created_by : sv 10122019
    */

    public function downloadPDF()
    {
        $type = request()->segment(3);
        $user_id = request()->segment(4);

        $request = new \Illuminate\Http\Request();

        $request->replace(['user_id' => $user_id, 'download' => 'pdf']);

            if($type == 'auditSource') {

                return $this->pdfAuditSource($request);

            } elseif($type == 'auditStatus') {

                return $this->pdfAuditStatus($request);

            } elseif($type == 'auditSite') {

                return $this->pdfAuditSite($request);

            } elseif($type == 'auditType') {

                return $this->pdfAuditType($request);

            } elseif($type == 'auditDepartment') {

                return $this->pdfAuditDepartment($request);

            } else {

                return response()
                    ->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Oops! Something went wrong, please try later.',
                    'errors' => '' ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }

    }                          

    public function pdfAuditSource(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_source', 'tbl_audit_plan_audit_details.audit_source', '=', 'tbl_audit_source.id')
                    ->where('tbl_audit_plan_audit_details.created_by', $request->user_id)
                    //->where('tbl_audit_source.created_by', $request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_audit_source.name as source_name')
                    ->get();
                 
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_source');
                    return $pdf->download('pdf_audit_source.pdf');
                }
        }
    } 

    public function pdfAuditStatus(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->where('tbl_audit_plan_audit_details.created_by', $request->user_id)
                    ->select('tbl_audit_plan_audit_details.*')
                    ->orderBy('tbl_audit_plan_audit_details.audit_status', 'Asc')
                    ->get();
                   
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_status');
                    return $pdf->download('pdf_audit_status.pdf');
                }
              
        }
    }


    public function pdfAuditSite(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
    
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_type', 'tbl_audit_plan_audit_details.audit_QA', '=', 'tbl_audit_type.id')
                    ->join('tbl_sites', 'tbl_sites.id', '=', 'tbl_audit_type.site_name')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_sites.site_name as site_name')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_site');
                    return $pdf->download('pdf_audit_site.pdf');
                }
            
        }
    }  

    public function pdfAuditType(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
    
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_type', 'tbl_audit_plan_audit_details.audit_QA', '=', 'tbl_audit_type.id')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_audit_type.name as audit_type')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_type');
                    return $pdf->download('pdf_audit_type.pdf');
                }
        }
    }

    public function pdfAuditDepartment(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
        
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_department', 'tbl_audit_plan_audit_details.department', '=', 'tbl_department.id')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_department.department_name as department_name')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_department');
                    return $pdf->download('pdf_audit_department.pdf');
                }
            }
        }
    /**
     * Send Mail
     *
     * @return mixed
     */
    public function SendReport(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','subject' =>'required','sent_to' =>'required','type' =>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
            /* 
            *   Send Mail 
            */
            
            $getPdfPath = New  SendMail();
             $pdffile = $getPdfPath->getPdfPath($request);
             $pdf_path =  asset('pdf').'/'.$pdffile;      
             $subject = $request->subject;
                Mail::send([], array(
                    'yourValue' => $subject
                ) , function ($message) use ($request,$pdf_path)
                {
                    $body = file_get_contents('../sendmail.html');
                    $body = str_replace('$$content$$', $request->content, $body);
                    $body = str_replace('$$user_name$$', $request->sent_to, $body);
                    $body = str_replace('$$file_name$$', $pdf_path, $body);
                   
                    //$body = str_replace('$$form$$', $form, $body);
                    if($request->attachment){
                       $message->attachData($pdfpath, 'File',[ 'as' => 'file.zip','mime' => 'application/pdf/doc']);
                    }
                    $message->setBody($body, 'text/html');
                    $message->to($request->sent_to);
                    if($request->cc){
                        $message->cc($request->cc);
                    }
                    if($request->bcc){
                        $message->bcc($request->bcc);
                    }   
                   
                  $message->subject($request->subject);
                   
                    
                });
                if (empty($message->errors))
                {
                    $data = 1;
                }
                else
                {
                    $data = 0;
                }
                $send_mail = new SendMail();
                $send_mail->add($request,$user->email);      
              
            if ($data = 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => "",
                    'message' => 'Email Send Successfully..!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
          }
        else {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                    'data' => null,
                    'message' => 'Invalid Login',
                    'errors' => null,
                    ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                );
            }
        }
    }
    public function test(){
        echo "test";
    }    
}

=======
<?php
/**
 * Class and Function List:
 * Function list:
 * - forgotPassword()
 * - (()
 * - ConfirmOtp()
 * - AuditSource()
 * - Replication()
 * - AuditScore()
 * - AuditAnswer()
 * - QuestionCategory()
 * - delete()
 * - View()
 * - AuditType()
 * - QuestionGroup()
 * - Question()
 * - Organization()
 * - Department()
 * - Sites()
 * - Team()
 * - Template()
 * - AuditPlanAuditDetail()
 * - AuditPlanSchedule()
 * - AuditPlanResponsibility()
 * - SignAuditUpdateStatus()
 * - SignAuditSummary()
 * - SignAuditSummaryStrength()
 * - SignAuditSummaryAuditFinding()
 * - SignAuditSummaryAuditImprovement()
 * Classes list:
 * - ApiController extends Controller
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use App\Models\Audit_Source;
use App\Models\Replication;
use App\Models\Audit_Scoring;
use App\Models\Audit_Answer;
use App\Models\Question_category;
use App\Models\Question;
use App\Models\Question_group;
use App\Models\Audit_type;
use App\Models\Sites;
use App\Models\QuestionAnswer;
use App\Models\Organization;
use App\Models\Template;
use App\Models\Department;
use App\Models\Team;
use App\Models\AuditPlanAuditDetail;
use App\Models\SignAuditUpdateStatus;
use App\Models\AuditPlanSchedule;
use App\Models\AuditPlanResponsibility;
use App\Models\SignAuditSummaryStrength;
use App\Models\SignAuditSummaryAuditFinding;
use App\Models\SignAuditSummaryAuditImprovement;
use App\Models\SignAuditQuestionCompliant;
use App\Models\SignAuditCorrectiveActions;
use App\Models\SignAuditSummary;
use App\Models\AuditSign;
use App\Models\AuditPlanPerformQA;
use App\Models\SendMail;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PDF;
use Auth;
use DB;

class ApiController extends Controller
{
    /**
     * Forgot Password.
     * @param string $email
     */
    public function forgotPassword(Request $request)
    {

        $validation = Validator::make($request->all() , ['email' => 'required|email']);

        if ($validation->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_SUCCESS_OK,
                'data' => null,
                'message' => 'Please enter valid email id',
                'errors' => $validation->errors() ,
            ) , RESPONSE_CODE_SUCCESS_OK);
        }
        else
        {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $result = '';
            for ($i = 0;$i < 8;$i++) $result .= $characters[mt_rand(0, 61) ];

            $user = Users::where('email', $request->email)
                ->first();
            $user->updateToken($result, $user->id);
            /**
             * Account Details of User
             */
            $name = $user->name;
            $email = $user->email;
            $id = $user->id;
            $apiKey = urlencode('orisGPoTsWY-9nmLyhbGE8S4UVGrgfXxYpdo4Ci1bS');

            /**
             * Message Details Send Message
             */
            $numbers = $user->mobile;
            $sender = urlencode('TXTLCL');
            $otp_message = 'Your OTP for login into academic tracker is ' . $result . '. Please don\'t share your OTP with anyone.';

            /**
             * Send Mail
             * @param $otp_message,$email
             */
            Mail::send([], array(
                'yourValue' => 'OTP(One Time Password)'
            ) , function ($message) use ($otp_message, $email)
            {
                $message->setBody($otp_message, 'text/html');
                $message->subject('OTP(One Time Password)');
                $message->to($email)->cc($email);
            });
            if (empty($message->errors))
            {
                $data = 1;
            }
            else
            {
                $data = 0;
            }
            /**
             * Response
             */
            if ($data == 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => null,
                    'message' => 'An OTP has send to your mail, Please use it as password',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => null,
                    'message' => 'Something went wrong, Please try again.',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

        }
    }

    /**
     * Confirm Otp
     *
     * @return mixed
     */
    public function ConfirmOtp(Request $request)
    {
        $validator = Validator::make($request->all() , ['id' => 'required', 'otp' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            $user = Users::where('otp', $request->otp)
                ->Where('id', $request->id)
                ->first();

            if (count($user) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $user,
                    'message' => 'User login Successfully',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
        }
    }

    /**
     * CURD Audit Source
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditSource(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Audit_Source();
                            return $deleteQuery->deleteAuditSource($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Audit_Source();
                            return $checkId->updateAuditSource($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Audit_Source();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Audit_Source();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_Source();
                    return $indexQuery->indexAuditSource($request,$user->company_id);
                }
            }                

            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }




    /**
     * Add Replication Fields
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Replication(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',
        //'status' =>'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Replication();
                            return $deleteQuery->deleteReplication($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Replication();
                            return $checkId->updateReplication($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Replication();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Replication();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Replication();
                    return $indexQuery->indexReplication($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }
    
    /**
     * Audit Score Fields
     * For Add
     * @param audit_type,name,min,max &  abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditScore(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new Audit_Scoring();
                            return $deleteQuery->deleteAuditScoring($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new Audit_Scoring();
                            return $checkId->updateAuditScoring($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new Audit_Scoring();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new Audit_Scoring();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_Scoring();
                    return $indexQuery->indexAuditScoring($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Audit Answer Fields
     * For Add
     * @param  name, compliance,status &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditAnswer(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Audit_Answer();
                            return $deleteQuery->deleteAuditAnswer($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Audit_Answer();
                            return $checkId->updateAuditAnswer($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Audit_Answer();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Audit_Answer();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Audit_Answer();
                    return $indexQuery->indexAuditAnswer($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question Category Fields
     * For Add
     * @param basic_risk,category,status & abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionCategory(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        //'status' =>'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE3CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question_category();
                            return $deleteQuery->deleteQuestionCategory($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question_category();
                            return $checkId->updateQuestionCategory($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question_category();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question_category();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Question_category();
                    return $indexQuery->indexQuestionCategory($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     *Audit type CRUD
     * For Add
     * @param name,description,site& abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditType(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Audit_type();
                            return $deleteQuery->deleteAuditType($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Audit_type();
                            return $checkId->updateAuditType($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Audit_type();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Audit_type();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new Audit_type();
                    return $indexQuery->indexAuditType($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

     /**
     *Question Group Fields
     * For Add
     * @param audit_type,question_group_name,manditory& abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionGroup(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question_group();
                            return $deleteQuery->deleteQuestionGroup($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question_group();
                            return $checkId->updateQuestionGroup($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question_group();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question_group();
                      return $AddQuery->add($request);    
                    }
                    else if($request->abbr == 'get_answer')
                    {   
                      $AddQuery = new Question_group();
                      return $AddQuery->getAnswered($request,$user->company_id);    
                    }                      
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Question_group();
                    return $indexQuery->indexQuestionGroup($request,$user->company_id);
                }
          }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Question(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new Question();
                            return $deleteQuery->deleteQuestion($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new Question();
                            return $checkId->updateQuestion($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new Question();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Question();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Question();
                    return $indexQuery->indexQuestion($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question CRUD
     * For Add
     * @param name ,score,abbr = add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function QuestionAnswer(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new QuestionAnswer();
                            return $deleteQuery->deleteQuestionAnswer($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new QuestionAnswer();
                            return $checkId->updateQuestionAnswer($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new QuestionAnswer();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new QuestionAnswer();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new QuestionAnswer();
                    return $indexQuery->indexQuestionAnswer($request,$user->company_id);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }    
    /**
     * Organization CRUD
     * For Add
     * @param string company_name, abbr= add
     * @param Int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Organization(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Organization();
                                return $deleteQuery->deleteOrganization($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Organization();
                                return $checkId->updateOrganization($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Organization();
                                return $view->View($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Organization();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Organization();
                    return $indexQuery->indexOrganization($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Department CRUD
     * For Add
     * @param String department_manager_name,department_manager_email_id,department_name abbr= add
     * @param int organization_id,status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Department(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Department();
                                return $deleteQuery->deleteDepartment($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Department();
                                return $checkId->updateDepartment($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Department();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Department();
                      return $AddQuery->add($request,$user->company_id);    
                    }
                    else if($request->abbr == 'getDepartment')
                    {
                      $AddQuery = new Department();
                      return $AddQuery->getDepartment($request,$user->company_id);    
                    }                       
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Department();
                    return $indexQuery->indexDepartment($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Sites CRUD
     * For Add
     * @param String company_name,location_name,site_name abbr= add
     * @param int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Sites(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Sites();
                                return $deleteQuery->deleteSites($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Sites();
                                return $checkId->updateSites($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Sites();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Sites();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Sites();
                    return $indexQuery->indexSites($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Team CRUD
     * For Add
     * @param String username,user_email,user_phone abbr= add
     * @param int status
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Team(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Team();
                                return $deleteQuery->deleteTeam($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Team();
                                return $checkId->updateTeam($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Team();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Team();
                      return $AddQuery->add($request,$user->company_id);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Team();
                    return $indexQuery->indexTeam($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /**
     * Add/Update Template
     * For Update
     * @param integer $id
     * @return response
     */
    /**
     * Template CRUD
     * For Add
     * @param String template abbr= add
     * @param int audit_type
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function Template(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {

                                $deleteQuery = new Template();
                                return $deleteQuery->deleteTemplate($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new Template();
                                return $checkId->updateTemplate($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new Template();
                                return $view->view($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new Template();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new Template();
                    return $indexQuery->indexTemplate($request,$user->company_id);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-audit-detail
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanAuditDetail(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'reference' => 'required', 'audit_QA' => 'required', 'title' => 'required', 'scope' => 'required', 'audit_source' => 'required', 'department' => 'required', 'department_manager' => 'required', 'audit_status' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                 if($request->abbr=='update')
                {
                    $checkId = new AuditPlanAuditDetail();
                    return $checkId->updateAuditPlanAuditDetail($request);

                }
                else
                {
                    $templateQuery = new AuditPlanAuditDetail();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-schedule
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanSchedule(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'planned_date' => 'required', 'start_date' => 'required', 'completed_date' => 'required', 'close_date' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                 if($request->abbr=='update')
                {
                    $checkId = new AuditPlanSchedule();
                    return $checkId->updateAuditPlanSchedule($request);

                }
                else
                {
                    $templateQuery = new AuditPlanSchedule();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update audit-plan-responsibility
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditPlanResponsibility(Request $request)
    {
        
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'auditee' => 'required', 'lead_auditor' => 'required', 'auditor' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new AuditPlanResponsibility();
                    return $checkId->updateAuditPlanResponsibility($request);

                }
                else
                {
                    $templateQuery = new AuditPlanResponsibility();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-update-status
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditUpdateStatus(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'audit_status' => 'required', 'result' => 'required', 'plan_audit_detail_id' => 'required','status'=>'required'

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new SignAuditUpdateStatus();
                    return $checkId->updateSignAuditUpdateStatus($request);

                }
                else
                {
                    $templateQuery = new SignAuditUpdateStatus();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummary(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'summary' => 'required', 'plan_audit_detail_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                    $checkId = new SignAuditSummary();
                    return $checkId->updateSignAuditSummary($request);

                }
                else
                {
                    $templateQuery = new SignAuditSummary();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-strength
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryStrength(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'strenghts' => 'required', 'plan_audit_detail_id' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->abbr=='update')
                {
                   
                    $checkId = new SignAuditSummaryStrength();
                    return $checkId->updateSignAuditSummaryStrength($request);

                }
                else
                {
                    $templateQuery = new SignAuditSummaryStrength();
                    return $templateQuery->add($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-auditFinding
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryAuditFinding(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'audit_id'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {
                                $deleteQuery = new SignAuditSummaryAuditFinding();
                                return $deleteQuery->deleteAuditFinding($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new SignAuditSummaryAuditFinding();
                                return $checkId->updateAuditFinding($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new SignAuditSummaryAuditFinding();
                                return $view->viewAuditFinding($request);
                            }
                            else if($request->abbr == 'uploadImage')
                            {
                   
                              $AddQuery = new SignAuditSummaryAuditFinding();
                              return $AddQuery->upload($request);    
                            }                              
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditSummaryAuditFinding();
                      return $AddQuery->add($request);    
                    }
                  
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditSummaryAuditFinding();
                    return $indexQuery->indexAuditFinding($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit-summary-audit-improvement
     * For Update
     * @param integer $id
     * @return response
     */
    public function SignAuditSummaryAuditImprovement(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','audit_id' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {

                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                            if($request->abbr=='delete')
                            {
                                $deleteQuery = new SignAuditSummaryAuditImprovement();
                                return $deleteQuery->deleteAuditImprovement($request);
                            }
                            else if($request->abbr=='update')
                            {
                                $checkId = new SignAuditSummaryAuditImprovement();
                                return $checkId->updateAuditImprovement($request);
                            }
                            else if($request->abbr=='view')
                            {

                                $view = new SignAuditSummaryAuditImprovement();
                                return $view->viewAuditImprovement($request);
                            }
                            else 
                            {
                                return response()
                                    ->json(array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    //'data' => 'Null',
                                    'message' => 'Something is wrong Please try Again..!',
                                    'errors' => null,
                                ) , RESPONSE_CODE_SUCCESS_OK);
                            }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditSummaryAuditImprovement();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                        return response()
                            ->json(array(
                            'status' => true,
                            'code' => RESPONSE_CODE_SUCCESS_OK,
                            //'data' => 'Null',
                            'message' => 'Something is wrong Please try Again..!',
                            'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditSummaryAuditImprovement();
                    return $indexQuery->indexAuditImprovement($request);
                }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function SignAuditQuestionCompliant(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','audit_id'=>'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new SignAuditQuestionCompliant();
                            return $deleteQuery->deleteQuestionCompliant($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new SignAuditQuestionCompliant();
                            return $checkId->updateQuestionCompliant($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new SignAuditQuestionCompliant();
                            return $view->view($request);
                        }
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditQuestionCompliant();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditQuestionCompliant();
                    return $indexQuery->indexQuestionCompliant($request);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 

    /**
     * Question CRUD
     * For Add
     * @param int audit_type,question_group,parent_question,parent_answer,sub_question & sub_answer 
     * @param String question &abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function SignAuditCorrectiveActions(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
             
                if ($request->has('abbr'))
                {
                    if($request->has('id'))
                    {
                        if($request->abbr=='delete')
                        {

                            $deleteQuery = new SignAuditCorrectiveActions();
                            return $deleteQuery->deleteCorrectiveActions($request);
                        }
                        else if($request->abbr=='update')
                        {
                            $checkId = new SignAuditCorrectiveActions();
                            return $checkId->updateCorrectiveActions($request);
                        }
                        else if($request->abbr=='view')
                        {

                            $view = new SignAuditCorrectiveActions();
                            return $view->view($request);
                        }
                        else if($request->abbr=='get_last_id')
                        {

                            $view = new SignAuditCorrectiveActions();
                            return $view->getLastId($request);
                        }                        
                        else 
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add')
                    {
                      
                      $AddQuery = new SignAuditCorrectiveActions();
                      return $AddQuery->add($request);    
                    }
                    else
                    {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else
                {
                    $indexQuery = new SignAuditCorrectiveActions();
                    return $indexQuery->indexCorrectiveActions($request);
                }
            }
            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }  

    /*Team List*/
    public function TeamList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
           $list = Team::join('users', 'tbl_team.created_by', '=', 'users.id')->where('users.company_id' ,'=',$user->company_id)->pluck('tbl_team.username', 'tbl_team.id');

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }  
    public function PlanScheduleList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
       
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
         /* FULL details */       
        /*    $audit_info = DB::table('tbl_audit_plan_audit_details')
            ->select("tbl_audit_plan_audit_details.*","tbl_audit_plan_schedule.id as schedule_id","tbl_audit_plan_schedule.planned_date","tbl_audit_plan_schedule.start_date","tbl_audit_plan_schedule.completed_date","tbl_audit_plan_schedule.close_date","tbl_plan_audit_responsibility.id as responsibility_id","tbl_plan_audit_responsibility.auditee","tbl_plan_audit_responsibility.lead_auditor","tbl_plan_audit_responsibility.auditor","tbl_department.department_name")
            ->join('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->join('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
           // ->join('tbl_sign_audit_summary', 'tbl_sign_audit_summary.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_sign_audit_summary_strength', 'tbl_sign_audit_summary_strength.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_sign_audit_update_status', 'tbl_sign_audit_update_status.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            //->join('tbl_audit_sign', 'tbl_audit_sign.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            ->join('users', 'tbl_audit_plan_audit_details.created_by', '=', 'users.id')
            ->join('tbl_department', 'tbl_department.id', '=', 'tbl_audit_plan_audit_details.department')
            ->where('users.company_id' ,'=',$user->company_id)
            ->get();

            foreach($audit_info as $val){
                    $audit_id=$val->id;
            $allover_summary =DB::table('tbl_sign_audit_summary')
                        ->where('tbl_sign_audit_summary.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();  
            $summary_strength =DB::table('tbl_sign_audit_summary_strength')
                        ->where('tbl_sign_audit_summary_strength.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();    
            $audit_update_status =DB::table('tbl_sign_audit_update_status')
                        ->where('tbl_sign_audit_update_status.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();  
            $audit_signup =DB::table('tbl_audit_sign')
                        ->where('tbl_audit_sign.plan_audit_detail_id' ,'=',$audit_id)
                        ->first();                 
            $audit_finding =DB::table('tbl_sign_audit_summary_audit_finding')
                        ->where('tbl_sign_audit_summary_audit_finding.audit_summary_id' ,'=',$audit_id)
                        ->get();
                        
            $audit_improvement =DB::table('tbl_sign_audit_summary_audit_improvement')
                        ->where('tbl_sign_audit_summary_audit_improvement.audit_summary_id' ,'=',$audit_id)
                        ->get();

            $audit_corrective_actions =DB::table('tbl_sign_audit_corrective_actions')
                        ->where('tbl_sign_audit_corrective_actions.sign_audit_id' ,'=',$audit_id)
                        ->get();                         
            $list[] = array('sign_audit_info' =>$val ,'allover_summary'=>$allover_summary,'summary_strength'=>$summary_strength,'audit_update_status'=>$audit_update_status,'$audit_signup'=>$audit_signup,'audit_finding' =>$audit_finding ,'audit_improvement' =>$audit_improvement ,'audit_corrective_actions' =>$audit_corrective_actions);                    
            }*/
            if(empty($list)){
            $list = DB::table('tbl_audit_plan_audit_details')
            ->select("tbl_audit_plan_audit_details.*","tbl_audit_plan_schedule.id as schedule_id","tbl_audit_plan_schedule.planned_date","tbl_audit_plan_schedule.start_date","tbl_audit_plan_schedule.completed_date","tbl_audit_plan_schedule.close_date","tbl_plan_audit_responsibility.id as responsibility_id","tbl_plan_audit_responsibility.auditee","tbl_plan_audit_responsibility.lead_auditor","tbl_plan_audit_responsibility.auditor","tbl_department.department_name")
            ->leftJoin('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->leftJoin('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')
            ->join('users', 'tbl_audit_plan_audit_details.created_by', '=', 'users.id')
            ->join('tbl_department', 'tbl_department.id', '=', 'tbl_audit_plan_audit_details.department')
            ->where('users.company_id' ,'=',$user->company_id)
            ->where('tbl_audit_plan_audit_details.created_by' ,'=',$request->user_id)
            ->get();
            }

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    public function AuditSignList(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
            $list = DB::table('tbl_audit_plan_audit_details')
            ->join('tbl_audit_plan_schedule', 'tbl_audit_plan_audit_details.id', '=', 'tbl_audit_plan_schedule.plan_audit_detail_id')
            ->join('tbl_plan_audit_responsibility', 'tbl_plan_audit_responsibility.plan_audit_detail_id', '=', 'tbl_audit_plan_audit_details.id')->get();

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }    

    public function TeamList1(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
            $list = Team::pluck('username', 'id');

            if (count($list) > 0)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $list,
                    'message' => 'Data found.!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Data not Found..!',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }

    /**
     * Add/Update sign-audit
     * For Update
     * @param integer $id
     * @return response
     */
    public function AuditSign(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'username' => 'required', 'password' => 'required', 'user_sign_as' => 'required', 'plan_audit_detail_id' => 'required|unique:tbl_audit_sign',

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
             $CheckUser = Users::with('hasType')->where('email', '=', $request->username)->first();

             if (!empty($CheckUser)) {
                $id= $CheckUser->id;
                if (password_verify($request->password, $CheckUser->password)) {  
                    
                    $checkId = new AuditSign();
                    return $checkId->add($request,$id);
                        if($CheckUser)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => $checkId,
                                    'message' => 'User logged successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                     else {

                        if ($CheckUser->status_enum == ENUM_STATUS_INACTIVE) {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'User status Inactive. Please contact admin.',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                        }
                    }
                }
                else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Incorrect password',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                }
              }
            else {
                return response()->json(
                    array(
                        'status' => false,
                        'code' => RESPONSE_CODE_ERROR_BAD,
                        'data' => null,
                        'message' => 'Email not registered',
                        'errors' => null,
                    ), RESPONSE_CODE_ERROR_BAD
                );
            }              
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 
    /**
     * Delete Plan Schedule
     * For Delete
     * @param Request $Request
     * @return response
     */
    public function DeletePlanSchedule(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'id' => 'required', 

        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
             $audit_detail = AuditPlanAuditDetail::where('id', '=', $request->id)->first();
             if(!empty($audit_detail)){
                $planSchudule = AuditPlanSchedule::where('plan_audit_detail_id', '=', $request->id)->first();
                if($planSchudule){
                   $audit_detail->delete();                    
                }
                $responsibility = AuditPlanResponsibility::where('plan_audit_detail_id', '=', $request->id)->first();
                if($responsibility){
                    $responsibility->delete();          
                }
                $planSchudule->delete();
                $data =1;
             }
             else{
                $data = 0;
             }
             if($data==1)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => null,
                                    'message' => 'Delete Successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                    else 
                    {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'Try Again..!',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                    }
            }
                else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Invalid Login',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                   }
            } 
    }

    /**
     * View
     *
     * @return response
     */
    public function ViewAuditPlan(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required', 'abbr' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();

            if (!empty($user))
            {
                if ($user->token == $request->token)
                {
                        if ($request->abbr == 'audit_detail')
                        {
                            $query = AuditPlanAuditDetail::all();
                            
                            if (empty($query))
                            {
                                $data = 0;
                            }

                        }
                        else if ($request->abbr == 'schedule')
                        {
                            $query = AuditPlanSchedule::all();

                            if (empty($query))
                            {

                                $data = 0;
                            }
                        }

                        else if ($request->abbr == 'responsibilty')
                        {
                            $query = AuditPlanResponsibility::all();
                            if (empty($query))
                            {

                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'summary')
                        {
                            $query = SignAuditSummary::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'summary_strength')
                        {
                            $query = SignAuditSummaryStrength::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_status')
                        {
                            $query = SignAuditUpdateStatus::where('plan_audit_detail_id',$request->audit_id)->first();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_sign')
                        {
                            $query = AuditSign::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_finding')
                        {
                            $query = SignAuditSummaryAuditFinding::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_improvement')
                        {
                            $query = SignAuditSummaryAuditImprovement::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_question_compliant')
                        {
                            $query = SignAuditQuestionCompliant::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr == 'audit_corrective_actions')
                        {
                            $query = SignAuditCorrectiveActions::all();
                            if (empty($query))
                            {
                                $data = 0;
                            }
                        }
                        else if ($request->abbr != '')
                        {

                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => null,
                                'message' => 'Table not found.',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                        if (isset($data))
                        {
                            return response()->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => 'null',
                                'message' => 'Data not Found!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                        else
                        {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                'data' => $query,
                                'message' => 'Data  Found!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }
                    }
                
            }
            else
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    } 
    /**
     * Send Mail
     *
     * @return mixed
     */
    public function SendMail(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','subject' =>'required','sent_to' =>'required','content' =>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
            /* 
            *   Send Mail 
            */             
                $subject = $request->subject;
                Mail::send([], array(
                    'yourValue' => $subject
                ) , function ($message) use ($request)
                {
                    $body = file_get_contents('../mail.html');
                    $body = str_replace('$$content$$', $request->content, $body);
                    $body = str_replace('$$user_name$$', $request->sent_to, $body);
                    //$body = str_replace('$$form$$', $form, $body);
                    if($request->attachment){
                       $message->attachData($request->attachment, 'File',[ 'as' => 'file.zip','mime' => 'application/pdf/doc']);
                    }
                    $message->setBody($body, 'text/html');
                    $message->to($request->sent_to);
                    if($request->cc){
                        $message->cc($request->cc);
                    }
                    if($request->bcc){
                        $message->bcc($request->bcc);
                    }   
                   
                  $message->subject('Mail');
                   
                    
                });
                if (empty($message->errors))
                {
                    $data = 1;
                }
                else
                {
                    $data = 0;
                }
          $send_mail = new SendMail();
          $send_mail->add($request,$user->email);      
              
            if ($data = 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => $send_mail,
                    'message' => 'Email Send Successfully..!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
          }
        else {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                    'data' => null,
                    'message' => 'Invalid Login',
                    'errors' => null,
                    ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                );
            }
        }
    } 

    /**
     * CURD Audit Source
     * For Add
     * @param name abbr= add
     * For Update
     * @param id,abbr=update
     * For View Particular
     * @param id,abbr = view
     * For Delete 
     * @param id ,abbr = delete
     * 
     * @return mixed
     */
    public function AuditPlanPerformQA(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
            if ($user->token == $request->token)
            {
                if ($request->has('abbr')){
                    if($request->has('id')){
                        if($request->abbr=='delete'){

                            $deleteQuery = new AuditPlanPerformQA();
                            return $deleteQuery->deleteAuditPlanPerformQA($request);
                        }
                        else if($request->abbr=='update'){
                            $checkId = new AuditPlanPerformQA();
                            return $checkId->updateAuditPlanPerformQA($request);
                        }
                        else if($request->abbr=='view'){

                            $view = new AuditPlanPerformQA();
                            return $view->view($request);
                        }
                        else {
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                        }       
                    }
                    else if($request->abbr == 'add'){
                      
                      $AddQuery = new AuditPlanPerformQA();
                      return $AddQuery->add($request);    
                    }
                    else{
                            return response()
                                ->json(array(
                                'status' => true,
                                'code' => RESPONSE_CODE_SUCCESS_OK,
                                //'data' => 'Null',
                                'message' => 'Something is wrong Please try Again..!',
                                'errors' => null,
                            ) , RESPONSE_CODE_SUCCESS_OK);
                    }
                }
                else{
                    $indexQuery = new AuditPlanPerformQA();
                    return $indexQuery->indexAuditPlanPerformQA($request,$user->company_id);
                }
            }                

            else
            {
                return response()
                    ->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    //'data' => 'Null',
                    'message' => 'Invalid Login!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }
        }
    }


    /*
        Desc : to route a request based on different audit type
        created_by : sv 10122019
    */

    public function downloadPDF()
    {
        $type = request()->segment(3);
        $user_id = request()->segment(4);

        $request = new \Illuminate\Http\Request();

        $request->replace(['user_id' => $user_id, 'download' => 'pdf']);

            if($type == 'auditSource') {

                return $this->pdfAuditSource($request);

            } elseif($type == 'auditStatus') {

                return $this->pdfAuditStatus($request);

            } elseif($type == 'auditSite') {

                return $this->pdfAuditSite($request);

            } elseif($type == 'auditType') {

                return $this->pdfAuditType($request);

            } elseif($type == 'auditDepartment') {

                return $this->pdfAuditDepartment($request);

            } else {

                return response()
                    ->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Oops! Something went wrong, please try later.',
                    'errors' => '' ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }

    }                          

    public function pdfAuditSource(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_source', 'tbl_audit_plan_audit_details.audit_source', '=', 'tbl_audit_source.id')
                    ->where('tbl_audit_plan_audit_details.created_by', $request->user_id)
                    //->where('tbl_audit_source.created_by', $request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_audit_source.name as source_name')
                    ->get();
                 
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_source');
                    return $pdf->download('pdf_audit_source.pdf');
                }
        }
    } 

    public function pdfAuditStatus(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->where('tbl_audit_plan_audit_details.created_by', $request->user_id)
                    ->select('tbl_audit_plan_audit_details.*')
                    ->orderBy('tbl_audit_plan_audit_details.audit_status', 'Asc')
                    ->get();
                   
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_status');
                    return $pdf->download('pdf_audit_status.pdf');
                }
              
        }
    }


    public function pdfAuditSite(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
    
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_type', 'tbl_audit_plan_audit_details.audit_QA', '=', 'tbl_audit_type.id')
                    ->join('tbl_sites', 'tbl_sites.id', '=', 'tbl_audit_type.site_name')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_sites.site_name as site_name')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_site');
                    return $pdf->download('pdf_audit_site.pdf');
                }
            
        }
    }  

    public function pdfAuditType(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
    
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_audit_type', 'tbl_audit_plan_audit_details.audit_QA', '=', 'tbl_audit_type.id')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_audit_type.name as audit_type')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_type');
                    return $pdf->download('pdf_audit_type.pdf');
                }
        }
    }

    public function pdfAuditDepartment(Request $request)
    {
        $validator = Validator::make($request->all() , ['user_id' => 'required','download'=>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        { 
        
                $items = DB::table('tbl_audit_plan_audit_details')
                    ->join('tbl_department', 'tbl_audit_plan_audit_details.department', '=', 'tbl_department.id')
                    ->where('tbl_audit_plan_audit_details.created_by',$request->user_id)
                    ->select('tbl_audit_plan_audit_details.*', 'tbl_department.department_name as department_name')
                    ->orderBy('tbl_audit_plan_audit_details.id', 'desc')
                    ->get();
                    
                view()->share('items',$items);
                if($request->has('download')){
                    $pdf = PDF::loadView('pdf_audit_department');
                    return $pdf->download('pdf_audit_department.pdf');
                }
            }
        }
    /**
     * Send Mail
     *
     * @return mixed
     */
    public function SendReport(Request $request)
    {

        $validator = Validator::make($request->all() , ['user_id' => 'required', 'token' => 'required','subject' =>'required','sent_to' =>'required','type' =>'required'
        ]);
        if ($validator->fails())
        {
            return response()
                ->json(array(
                'status' => false,
                'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                'data' => null,
                'message' => 'Required Data Missing',
                'errors' => $validator->errors() ,
            ) , RESPONSE_CODE_ERROR_MISSINGDATA);

        }
        else
        {
            Log::emergency($request);
            $user = Users::where('id', '=', $request->user_id)
                ->where('status_enum', ENUM_STATUS_ACTIVE)
                ->first();
                
               
            if ($user->token == $request->token)
            {
            /* 
            *   Send Mail 
            */
            
            $getPdfPath = New  SendMail();
             $pdffile = $getPdfPath->getPdfPath($request);
             $pdf_path =  asset('pdf').'/'.$pdffile;      
             $subject = $request->subject;
                Mail::send([], array(
                    'yourValue' => $subject
                ) , function ($message) use ($request,$pdf_path)
                {
                    $body = file_get_contents('../sendmail.html');
                    $body = str_replace('$$content$$', $request->content, $body);
                    $body = str_replace('$$user_name$$', $request->sent_to, $body);
                    $body = str_replace('$$file_name$$', $pdf_path, $body);
                   
                    //$body = str_replace('$$form$$', $form, $body);
                    if($request->attachment){
                       $message->attachData($pdfpath, 'File',[ 'as' => 'file.zip','mime' => 'application/pdf/doc']);
                    }
                    $message->setBody($body, 'text/html');
                    $message->to($request->sent_to);
                    if($request->cc){
                        $message->cc($request->cc);
                    }
                    if($request->bcc){
                        $message->bcc($request->bcc);
                    }   
                   
                  $message->subject($request->subject);
                   
                    
                });
                if (empty($message->errors))
                {
                    $data = 1;
                }
                else
                {
                    $data = 0;
                }
                $send_mail = new SendMail();
                $send_mail->add($request,$user->email);      
              
            if ($data = 1)
            {
                return response()->json(array(
                    'status' => true,
                    'code' => RESPONSE_CODE_SUCCESS_OK,
                    'data' => "",
                    'message' => 'Email Send Successfully..!',
                    'errors' => null,
                ) , RESPONSE_CODE_SUCCESS_OK);
            }

            else
            {
                return response()->json(array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors() ,
                ) , RESPONSE_CODE_ERROR_MISSINGDATA);

            }
          }
        else {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                    'data' => null,
                    'message' => 'Invalid Login',
                    'errors' => null,
                    ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                );
            }
        }
    }
    public function test(){
        echo "test";
    }    
}

>>>>>>> .theirs
