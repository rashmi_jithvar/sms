<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\AddPages;
use App\Pages;
use App\AddSubMenus;

class AddPagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageList = Pages::get();
        $submenuList = AddSubMenus::get();
        $tableName = "Pages";
        return view('cms_mangment.addpagesdetails', compact('pageList', 'tableName','submenuList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pageName' => 'required',
            'pageTitle' => 'required',
            'subMenuName' => '',
            'pageDescription' => '',
            'metaTitle' => '',
            'metaKeyword' => '',
            'metaDescription' => ''
        ]);
            $posted = new AddPages;
            $posted->pageName = $request->pageName;
            $posted->subMenuName = $request->subMenuName;
            $posted->pageTitle = $request->pageTitle;
            $posted->pageDescription = $request->editor;
            $posted->metaTitle = $request->metaTitle;
            $posted->metaDescription = $request->metaDescription;
            $posted->metaKeyword = $request->metaKeyword;
            $posted->save();

            return response()->json([
                "message" => "Records Inserted Successfully!"
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AddPages  $addPages
     * @return \Illuminate\Http\Response
     */
    public function show(AddPages $addPages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AddPages  $addPages
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pageInfo = AddPages::where('id', $request->id)->first();
                        
        $pageList = Pages::get();
        
        $submenuList = AddSubMenus::get();
        
        return view('cms_mangment.editPagesDetails', compact('pageInfo', 'pageList','submenuList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AddPages  $addPages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddPages $addPages)
    {
        $request->validate([
            'pageName' => 'required',
            'pageTitle' => 'required',
            'subMenuName' => '',
            'pageDescription' => '',
            'metaTitle' => '',
            'metaKeyword' => '',
            'metaDescription' => ''
        ]);

        $pageChk = AddPages::where('id', $request->recordId)->first();
        if(!empty($pageChk))
        {
            $form_data = array(
                'pageName' => $request->pageName,
                'subMenuName' => $request->subMenuName,
                'pageTitle' => $request->pageTitle,
                'pageDescription' => $request->editor,
                'metaTitle' => $request->metaTitle,
                'metaKeyword' => $request->metaKeyword,
                'metaDescription' => $request->metaDescription
            );
  
            AddPages::whereId($request->recordId)->update($form_data);
            return response()->json([
                "message" => "Records Updated Successfully!"
            ], 201);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AddPages  $addPages
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddPages $addPages)
    {
        //
    }
}
