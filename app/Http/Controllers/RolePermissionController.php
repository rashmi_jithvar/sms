<<<<<<< HEAD
<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Illuminate\Http\RedirectResponse;

use Illuminate\Support\Facades\Validator;

use App\RolePermission;

use App\Permission;

use App\Roles;

use App\PermissionGroup;

use Auth;



class RolePermissionController extends Controller

{





  /*  public function index(){

        $roles = DB::table('tbl_role_permission')

            ->select('*')

            ->join('tbl_permission', 'tbl_role_permission.permission_id', '=', 'tbl_permission.id')

            ->join('tbl_roles', 'tbl_role_permission.role_id', '=', 'tbl_roles.id')

           // 

            ->get();

    	//$roles = DB::select('select * from tbl_role_permission');

        $permission = Permission::with('permission_action')->get();



        ///print_r($permission);exit;



       // $user = User::all();

        return view('role_permission.index',compact('roles','permission'));

    }

    */

 

    public function create(Request $request){

       // echo "reached";exit;

         //$permissiondropdown = PermissionGroup::pluck('name', 'id');

         $pergroup =   PermissionGroup::with('permission')->get();

         //echo "<pre>";

        // $pergroup = PermissionGroup::find(4)->permission();

         //print_r($pergroup);exit;

         $role=request('role');

         $roledropdown = Roles::pluck('name', 'id');

        if($role){

            $rolepermission = DB::table('tbl_role_permission')->select('permission_id')->where('role_id', $role)->get();

        return view('role_permission.create',compact('pergroup','roledropdown','rolepermission','role'));

        }

         

           return view('role_permission.create',compact('pergroup','roledropdown','role'));



      

    }

 

    public function store(Request $request){ 

        $validatedData = $request->validate([

                    'permission_id' => 'required|max:255',

                    'role_id' => 'required|max:255',

                   // 'status'  => 'required',

        ]);

    $permission=request('permission_id');

    $role=request('role_id');

        if($role){

            DB::table('tbl_role_permission')->select('permission_id')->where('role_id', $role)->get();

            DB::table('tbl_role_permission')->where('role_id', $role)->delete();

        }

        if (count($permission) > 0) {

            foreach ($permission as $key => $value) {

                $roles = new RolePermission();

                $roles->permission_id = $value;

                $roles->role_id = request('role_id');

                $roles->status = '1';

                $roles->created_by = Auth::user()->id;;

                $roles->save();

            }

        }

       return redirect('/role_permission/create')->with('message', 'Update  Successfully!');;

 

    }



/*        public function edit($id)

    {

        $permission = RolePermission::where(['id'=>$id])->first();

     //   return view('member.edit',compact('user'));

        return view('role_permission.edit', compact('permission', 'id'));

    }



        public function destroy($id)

    {

        $ticket = RolePermission::find($id);

        $ticket->delete();



        return redirect('/role_permission')->with('success', 'Roles has been deleted!!');

    }





        public function update(Request $request, $id)

    {

        $user = new RolePermission();

        $data = $this->validate($request, [

            'permission_id' => 'required|max:255',

            'role_id' => 'required|max:255',

            'status'  => 'required',

        ]);

        $data['id'] = $id;

        $user->updateRolePermission($data);

        return redirect('/role_permission')->with('success', 'New support user has been updated!!');

    }

*/



}

=======
<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\RolePermission;
use App\Permission;
use App\Roles;
use App\PermissionGroup;
use Auth;

class RolePermissionController extends Controller
{


  /*  public function index(){
        $roles = DB::table('tbl_role_permission')
            ->select('*')
            ->join('tbl_permission', 'tbl_role_permission.permission_id', '=', 'tbl_permission.id')
            ->join('tbl_roles', 'tbl_role_permission.role_id', '=', 'tbl_roles.id')
           // 
            ->get();
    	//$roles = DB::select('select * from tbl_role_permission');
        $permission = Permission::with('permission_action')->get();

        ///print_r($permission);exit;

       // $user = User::all();
        return view('role_permission.index',compact('roles','permission'));
    }
    */
 
    public function create(Request $request){
       // echo "reached";exit;
         //$permissiondropdown = PermissionGroup::pluck('name', 'id');
         $pergroup =   PermissionGroup::with('permission')->get();
         //echo "<pre>";
        // $pergroup = PermissionGroup::find(4)->permission();
         //print_r($pergroup);exit;
         $role=request('role');
         $roledropdown = Roles::pluck('name', 'id');
        if($role){
            $rolepermission = DB::table('tbl_role_permission')->select('permission_id')->where('role_id', $role)->get();
        return view('role_permission.create',compact('pergroup','roledropdown','rolepermission','role'));
        }
         
           return view('role_permission.create',compact('pergroup','roledropdown','role'));

      
    }
 
    public function store(Request $request){ 
        $validatedData = $request->validate([
                    'permission_id' => 'required|max:255',
                    'role_id' => 'required|max:255',
                   // 'status'  => 'required',
        ]);
    $permission=request('permission_id');
    $role=request('role_id');
        if($role){
            $rolepermission = DB::table('tbl_role_permission')->select('permission_id')->where('role_id', $role)->get();
            DB::table('tbl_role_permission')->where('role_id', $role)->delete();
        }
        if (count($permission) > 0) {
            foreach ($permission as $key => $value) {
                $roles = new RolePermission();
                $roles->permission_id = $value;
                $roles->role_id = request('role_id');
                $roles->status = '1';
                $roles->created_by = Auth::user()->id;;
                $roles->save();
            }
        }
       return redirect('/role_permission/create')->with('message', 'Update  Successfully!');;
 
    }

/*        public function edit($id)
    {
        $permission = RolePermission::where(['id'=>$id])->first();
     //   return view('member.edit',compact('user'));
        return view('role_permission.edit', compact('permission', 'id'));
    }

        public function destroy($id)
    {
        $ticket = RolePermission::find($id);
        $ticket->delete();

        return redirect('/role_permission')->with('success', 'Roles has been deleted!!');
    }


        public function update(Request $request, $id)
    {
        $user = new RolePermission();
        $data = $this->validate($request, [
            'permission_id' => 'required|max:255',
            'role_id' => 'required|max:255',
            'status'  => 'required',
        ]);
        $data['id'] = $id;
        $user->updateRolePermission($data);
        return redirect('/role_permission')->with('success', 'New support user has been updated!!');
    }
*/

}
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
