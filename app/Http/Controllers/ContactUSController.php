<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ContactUS;
use DB;

class ContactUSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function contactUS()
   {
       return view('contact-us');
   }

   public function getToken(){
        echo csrf_token();
        // return csrf_token();
    }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    public function callRequest(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-CSRF-TOKEN");

        $name = $request->get('name');
        $email= $request->get('email');
        $subject = $request->get('subject');
        $message = $request->get('message');

            $datainserted = DB::table('callback')->insert(
                [
                    'name' => $name, 
                    'email' => $email,
                    'subject' => $subject,
                    'message' => $message,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
             if($datainserted)
             {                 
                $response = array('status' => 1, 'msg' => 'Thanks, Your message has been sent successfully!');
                echo json_encode($response);
             }
    }
    public function contactSaveData(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-CSRF-TOKEN");
        header("Content-Type: application/json; charset=UTF-8");

        $name = $request->get('name');
        $email= $request->get('email');
        $phone = $request->get('phone');
        $subject = $request->get('subject');
        $message = $request->get('message');

            $datainserted = DB::table('contactus')->insert(
                [
                    'name' => $name, 
                    'email' => $email,
                    'phone' => $phone,
                    'subject' => $subject,
                    'message' => $message,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
             if($datainserted)
             { 
                \Mail::send('contactus',
                array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'subject' => $subject,
                    'user_message' => $message
                ), function($message) use ($request)
                {
                    $message->from($request->get('email'));
                    $message->to('arun.pradhan@jithvar.com', 'Admin')->subject($request->get('subject'));
        
                });
    
                \Mail::send('contactresponse',
                array(
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'subject' => $subject,
                    'user_message' => $message
                ), function($message) use ($request)
                {
                    $message->from('noreply@jithvar.com');
                    $message->to($request->get('email'),$request->get('name'))->subject('Inquiry to Jithvar');        
                });
                
                // return redirect('contact-us')->with('success', 'Thanks for contacting us!'); 
                
                $response = array('status' => 1, 'msg' => 'Thanks, Your message has been sent successfully!');
                echo json_encode($response);
             }
        }

    public function callBack(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-CSRF-TOKEN");
        header("Content-Type: application/json; charset=UTF-8");

        $name = $request->get('name');
        $email= $request->get('email');
        $subject = $request->get('subject');
        $message = $request->get('message');

            $datainserted = DB::table('callback')->insert(
                [
                    'name' => $name, 
                    'email' => $email,
                    'subject' => $subject,
                    'message' => $message,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
             if($datainserted)
             { 
                \Mail::send('callbackpage',
                array(
                    'name' => $name,
                    'email' => $email,
                    'subject' => $subject,
                    'user_message' => $message
                ), function($message) use ($request)
                {
                    $message->from($request->get('email'));
                    $message->to('arun.pradhan@jithvar.com', 'Admin')->subject($request->get('subject'));
        
                });
    
                \Mail::send('callbackpageresponse',
                array(
                    'name' => $name,
                    'email' => $email,
                    'subject' => $subject,
                    'user_message' => $message
                ), function($message) use ($request)
                {
                    $message->from('noreply@jithvar.com');
                    $message->to($request->get('email'),$request->get('name'))->subject('Inquiry to Jithvar');        
                });
                
                // return redirect('contact-us')->with('success', 'Thanks for contacting us!'); 
                
                $response = array('status' => 1, 'msg' => 'Thanks, Your message has been sent successfully!');
                echo json_encode($response);
             }
        }

    public function subscribe(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-CSRF-TOKEN");
        header("Content-Type: application/json; charset=UTF-8");

        $email= $request->get('email');

            $datainserted = DB::table('subscribe')->insert(
                [
                    'email' => $email,
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
             if($datainserted)
             { 
                 /*
                \Mail::send('subscriberMailPage',
                array(
                    'email' => $email
                ), function($message) use ($request)
                {
                    $message->from('noreply@jithvar.com');
                    $message->to($request->get('email'),$request->get('name'))->subject('Subscribe With JCS Pvt. Ltd.'); 
        
                });
                */
                // return redirect('contact-us')->with('success', 'Thanks for contacting us!'); 
                
                $response = array('status' => 1, 'msg' => 'Thanks, For Subscribe With Us!');
                echo json_encode($response);
             }
        }

        public function getaquote(Request $request)
        {
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: PUT, GET, POST");
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-CSRF-TOKEN");
            header("Content-Type: application/json; charset=UTF-8");
    
            $name = $request->get('name');
            $email= $request->get('email');
            $country = $request->get('country');
            $phone = $request->get('phone');
            $service = $request->get('service');
            $budget = $request->get('budget');
            $message = $request->get('message');
    
                $datainserted = DB::table('getaquote')->insert(
                    [
                        'name' => $name, 
                        'email' => $email,
                        'country' => $country,
                        'phone' => $phone,
                        'service' => $service,
                        'budget' => $budget,
                        'message' => $message,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                );
                 if($datainserted)
                 { 
                     /*
                    \Mail::send('subscriberMailPage',
                    array(
                        'email' => $email
                    ), function($message) use ($request)
                    {
                        $message->from('noreply@jithvar.com');
                        $message->to($request->get('email'),$request->get('name'))->subject('Subscribe With JCS Pvt. Ltd.'); 
            
                    });
                    */
                    // return redirect('contact-us')->with('success', 'Thanks for contacting us!'); 
                    
                    $response = array('status' => 1, 'msg' => 'Thanks, Your request has been sent successfully!');
                    echo json_encode($response);
                 }
            }

        
     }