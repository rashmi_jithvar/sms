<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Illuminate\Http\RedirectResponse;

use Illuminate\Support\Facades\Validator;

use App\Permission;

use App\PermissionGroup;

use Auth;



class PermissionController extends Controller

{

    public function index(){

    	// DB::select('select * from tbl_permission');      

       $roles= DB::table('tbl_permission as t1')

         ->select("t1.name","t1.id","t1.group_id","t1.controller_name","t1.action_name","t2.permission_name","t1.status")      

         ->join("tbl_permission_group AS t2", "t1.group_id", "=", "t2.id")

         ->get();

         

        //$pergroup = PermissionGroup::find($value->group_id)->permission();

       // $user = User::all();

        return view('permission.index',compact('roles'));

    }

 

    public function create(){

       // $groupdropdown = DB::table('tbl_permission_group')->get();

        $groupdropdown = PermissionGroup::pluck('permission_name', 'id');

        return view('permission.create' ,compact('groupdropdown'));

    }

 

    public function store(Request $request){

 	    $validatedData = $request->validate([

            'group_id' => 'required',

            'name' =>'required',

            'controller_name' => 'required|max:50',

            'action_name' => 'required|max:50',

            'name' => 'required|max:50',

            'status'  => 'required',
        ]);

           
        $permission_exit = Permission::where('controller_name', 'like', '%'.$request->controller_name.'%')->where('action_name', 'like', '%'.$request->action_name.'%')->first();     
        
        if(!empty($permission_exit)){
         return redirect('/permission')->with('message', 'Permissions already exist.');
        }

        $permission = new Permission();

        $permission->group_id = request('group_id');

        $permission->name = request('name');

        $permission->controller_name = request('controller_name');

        $permission->action_name = request('action_name');

        $permission->created_by  = Auth::user()->id;;

        $permission->status = request('status');

        $permission->save();

        return redirect('/permission')->with('message', 'Permission Create Successfully!');
      }



        public function edit($id)

    {

        $permission = Permission::where(['id'=>$id])->first();

         $groupdropdown = PermissionGroup::pluck('permission_name', 'id');

               // return view('admin.newhood', compact('citiesDropDown'));

     //   return view('member.edit',compact('user'));

        return view('permission.edit', compact('permission', 'id','groupdropdown'));

    }



        public function destroy($id)

    {

        $ticket = Permission::find($id);

        $ticket->delete();



        return redirect('/permission')->with('success', 'Roles has been deleted!!');

    }





        public function update(Request $request, $id)

    {

        $data = $this->validate($request, [

            'group_id' => 'max:3',

            'name' =>'max:150',

            'controller_name' => 'max:50',

            'action_name' => 'max:50',

            'name' => 'max:50',

            'status'  => 'max:1',

        ]);


        $permission_exit = Permission::where('controller_name', 'like', '%'.$request->controller_name.'%')->where('action_name', 'like', '%'.$request->action_name.'%')->first();     
        
        if(!empty($permission_exit)){
         return redirect('/permission')->with('message', 'Permissions already exist.');
        }

        $data['id'] = $id;

        $user = new Permission();
        
        $user->updatePermission($data);

        return redirect('/permission')->with('message', 'Permission has been updated!!');

    }





}

