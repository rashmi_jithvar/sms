<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;

use App\Teams;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teamList = Teams::get();
        $tableName = "teams";
        return view('cms_mangment.addTeamPage', compact('teamList', 'tableName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->HasFile('profilePhoto')) {
            $imageName = time().'.'.request()->profilePhoto->getClientOriginalExtension();
            request()->profilePhoto->move(public_path('team'), $imageName);
        } else {
            $imageName = NULL;
        }
        $posted = new Teams;
            $posted->name = $request->name;
            $posted->position = $request->position;
            $posted->profilePhoto = $imageName;
            $posted->facebook = $request->facebook;
            $posted->linkedin = $request->linkedin; 
            $posted->twitter = $request->twitter;
            $posted->gplus = $request->gplus;
            $posted->save();

            return response()->json([
                "message" => "Records Inserted Successfully!"
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function show(Teams $teams)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Teams $teams)
    {
        $memberInfo = Teams::where('id', $request->id)
                        ->first();

        return view('cms_mangment.editMemberPage', compact('memberInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teams $teams)
    {
        $Chk = Teams::where('id', $request->recordId)->first();
        if(!empty($Chk))
        {
            if($request->HasFile('profilePhoto')) {

                if(\File::exists(public_path('team/'.$request->oldPic))){

                    \File::delete(public_path('team/'.$request->oldPic));
                
                  }
                
                $imageName = time().'.'.request()->profilePhoto->getClientOriginalExtension();
                request()->profilePhoto->move(public_path('team'), $imageName);
            } else {
                $imageName = $request->oldPic;
            }
            $form_data = array(
            'name' => $request->name,
            'position' => $request->position,
            'profilePhoto' => $imageName,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'gplus' => $request->gplus
            );
  
            Teams::whereId($request->recordId)->update($form_data);
            return response()->json([
                "message" => "Records Updated Successfully!"
            ], 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teams $teams)
    {
        //
    }
}
