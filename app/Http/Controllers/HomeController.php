<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentEnquiry;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_student = Student::where('status','1')->get();
        $total_enquiry = StudentEnquiry::where('status','1')->get();
       // $manage_fees =FeesMaster::where('status','1')->get();
        return view('home',compact('total_student','total_enquiry'));
    }
}
