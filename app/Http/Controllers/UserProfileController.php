<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $webMaster = Setting::where('id', '1')->first();
        return view('profile.showProfile', compact('webMaster'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function passwordEdit()
    {
        return view('profile.changePassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        
        return redirect()->route('change-password')->with('message','Password change successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebsiteMaster  $websiteMaster
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteMaster $websiteMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebsiteMaster  $websiteMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteMaster $websiteMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebsiteMaster  $websiteMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteMaster $websiteMaster)
    {
        $Chk = WebsiteMaster::where('id', $request->recordId)->first();
        if(!empty($Chk))
        {
            $form_data = array(
            'phoneFirst' => $request->phoneFirst, 
            'phoneSecond' => $request->phoneSecond, 
            'primaryEmail' => $request->primaryEmail, 
            'secondaryEmail' => $request->secondaryEmail, 
            'noreplyEmail' => $request->noreplyEmail, 
            'facebook' => $request->facebook, 
            'linkedin' => $request->linkedin, 
            'twitter' => $request->twitter, 
            'gplus' => $request->gplus, 
            'address' => $request->address, 
            'employeeLogin' => $request->employeeLogin, 
            'clientLogin' => $request->clientLogin
            );
  
            WebsiteMaster::whereId($request->recordId)->update($form_data);
        }

        return response()->json([
            "message" => "Records Updated Successfully!"
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebsiteMaster  $websiteMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteMaster $websiteMaster)
    {
        //
    }
}
