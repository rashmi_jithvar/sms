<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;



use Illuminate\Http\RedirectResponse;

use Illuminate\Support\Facades\Validator;

use App\User;

use App\Roles;

use Auth;



class UserController extends Controller

{
    public function __construct()
    {
      $this->middleware('auth'); 
    }
    
    public function index(){
    	  $user= DB::table('users as t1')
         ->select("t1.*","t2.name as role_name")      
         ->join("tbl_roles AS t2", "t1.role_id", "=", "t2.id")
         ->get();

       // $user = User::all();

        // $role_id = Auth::user()->role_id;

        // $roles = DB::select("SELECT * FROM `tbl_role_permission` as role_permission inner join tbl_permission as permission on role_permission.permission_id=permission.id where role_id='$role_id' and controller_name='UserController'");

        //     foreach ($roles as $key => $value) {

        //       //$routeArray[] =$value->controller_name .'@'.$value->action_name;

        // }

        return view('user.index',compact('user'));

    }

 

    public function create(){

        $roledropdown = Roles::where('name','!=','Super Admin')->pluck('name', 'id');

        //$clientdropdown = Client::select(DB::raw("CONCAT(first_name,':',' ',email) AS name"),'id')->pluck('name', 'id');

        return view('user.create',compact('roledropdown'));

    }



    public function view($id)

    {

        $user = User::where(['id'=>$id])->first();

        $role_name = Roles::where(['id'=>$user->role_id])->first();

        return view('user.view', compact('user', 'id','role_name'));

    }

 

    public function storeUser(Request $request){

 	

        $user = new User();

            $validatedData = $request->validate([

            'email' => 'bail|required|unique:users|max:255',

            'name'  => 'required|max:255',

            'password' => 'min:6|required_with:confirm_password|same:confirm_password',

            'confirm_password' => 'min:6',

            'role_id' => 'required|not_in:-- Select RoleId --',

            'client_id' => 'min:1',

            'status' => 'required|not_in:-- Select Status --',



        ]);

        $user->name = request('name');

        $user->email = request('email'); 
       
        $user->password = \Hash::make(request('password'));

        $user->remember_token = request('_token');

        $user->status ='1';

        $user->role_id =request('role_id');

        $user->save();

        return redirect('/user')->with('message', 'User add Successfully!');;

 

    }



        public function edit($id)

    {

        $user = User::where(['id'=>$id])->first();

        $roledropdown = Roles::pluck('name', 'id');

     //   return view('member.edit',compact('user'));

        return view('user.edit', compact('user', 'id','roledropdown'));

    }



        public function destroy($id)

    {

        $ticket = User::find($id);

        $ticket->delete();





        return redirect('/user')->with('message', 'User has been deleted!!');

    }





        public function update(Request $request, $id)

    {

        $user = new User();

        $data = $this->validate($request, [

            'email' => 'bail|required|max:255',

            'name'  => 'required|max:255',

            'password' => 'required_with:confirm_password|same:confirm_password',

            //'confirm_password' => 'min:6',
            'role_id' => 'required',

            'status' => 'required',



        ]);

        $data['id'] = $id;

        $user->updateUser($data);



        return redirect('/user')->with('message', 'User has been updated!!');

    }

    

    

       public  function get_user(Request $request)

    {

     $role_id = $request->get('role_id');



     $data = DB::table('users')

       ->where('role_id', $role_id)

       //->groupBy('')

       ->get();

     if(count($data) !=0){

            $output = '<option value="">Select User</option>';

             foreach($data as $row)

             {

              $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';

             }

     }  

     else{

          $output =0;

     }

     return $output;

    }





}

