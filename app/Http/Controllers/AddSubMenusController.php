<?php

namespace App\Http\Controllers;

use App\AddSubMenus;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pages;

class AddSubMenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageList = Pages::get();
        $submenuList = AddSubMenus::get();
        $tableName = "add_sub_menuses";
        return view('cms_mangment.addSubMenuPages', compact('pageList', 'submenuList', 'tableName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posted = new AddSubMenus;
            $posted->pageNameId = $request->pageName;
            $posted->subMenuName = $request->subMenuName;
            $posted->save();

            return response()->json([
                "message" => "Records Inserted Successfully!"
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AddSubMenus  $addSubMenus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $submenu = AddSubMenus::where('pageNameId', $request->id)->get();
        $menuArray = '<option value="">-- SELECT --</option>';
        foreach($submenu as $sm) {
            $menuArray .= '<option value="'.$sm->id.'">'.$sm->subMenuName.'</option>';
        }
        return $menuArray;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AddSubMenus  $addSubMenus
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $submenuInfo = AddSubMenus::where('id', $request->id)
                        ->first();
        $pageList = Pages::get();

        return view('cms_mangment.editSubMenuPage', compact('submenuInfo','pageList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AddSubMenus  $addSubMenus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AddSubMenus $addSubMenus)
    {
        $Chk = AddSubMenus::where('id', $request->recordId)->first();
        if(!empty($Chk))
        {
            $form_data = array(
            'subMenuName' => $request->subMenuName,
            'pageNameId' => $request->pageName
            );
  
            AddSubMenus::whereId($request->recordId)->update($form_data);
            return response()->json([
                "message" => "Records Updated Successfully!"
            ], 201);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AddSubMenus  $addSubMenus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddSubMenus $addSubMenus)
    {
        //
    }
}
