<?php



namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Illuminate\Http\RedirectResponse;

use Illuminate\Support\Facades\Validator;

use App\PermissionGroup;

use Auth;



class PermissionGroupController extends Controller

{





    public function index(){

    	$roles = DB::select('select * from tbl_permission_group');

       // $user = User::all();

        return view('permission_group.index',compact('roles'));

    }

 

    public function create(){

        return view('permission_group.create');

    }

 

    public function store(Request $request){

 	

        $roles = new PermissionGroup();

            $validatedData = $request->validate([

            'name' => 'required|max:255',

            //'status'  => 'required',
        ]);

        $permission_exit = PermissionGroup::where('permission_name', 'like', '%'.$request->name.'%')->first();     
        
        if(!empty($permission_exit)){
         return redirect()->back()->with('message', 'Permissions already exist.');
        }     

        $roles->permission_name = request('name');

        $roles->status = '1';

        $roles->save();

        return redirect('/permission_group')->with('message', 'Permission Group Created Successfully!');;

 

    }



        public function edit($id)

    {

        $permission = PermissionGroup::where(['id'=>$id])->first();



     //   return view('member.edit',compact('user'));

        return view('permission_group.edit', compact('permission', 'id'));

    }



        public function destroy($id)

    {

        $ticket = PermissionGroup::find($id);

        $ticket->delete();

        DB::table("tbl_permission")->where("group_id", $id)->delete();

        return redirect('/permission_group')->with('message', 'Permission group has been deleted!!');

    }





        public function update(Request $request, $id)

    {

        $user = new PermissionGroup();

        $data = $this->validate($request, [

            'name' => 'required|max:255',

            'status'  => 'required',

        ]);
        
        /*$permission_exit = PermissionGroup::where('permission_name', 'like', '%'.$request->name.'%')->first();     
        
        if(!empty($permission_exit)){
         return redirect()->back()->with('message', 'Permissions already exist.');
        }*/

        $user = new PermissionGroup();
        $data['id'] = $id;

        $user->updatePermissionGroup($data);

        return redirect('/permission_group')->with('message', 'Permission Group has been updated!!');

    }





}

