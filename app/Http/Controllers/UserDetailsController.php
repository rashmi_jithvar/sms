<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class UserDetailsController extends Controller
{
    public function userDetails(Request $req)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    
    foreach($req->params as $postdata)
    {
      $datainserted = DB::table('contactus')->insert(
      [
        'name' => $postdata['updates'][0]['value'], 
        'email' => $postdata['updates'][1]['value'],
        'phone' => $postdata['updates'][2]['value'],
        'subject' => $postdata['updates'][3]['value'],
        'message' => $postdata['updates'][4]['value']
      ]
        );
      if($datainserted)
      {
        return response()->json("Data Added Successfully");
        // $response = array('status' => 1, 'msg' => 'Thanks, Your message has been sent successfully!');
        //         echo json_encode($response);
      }
    }
    
  }
}