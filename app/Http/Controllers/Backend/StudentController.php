<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Models\Common;
use App\Models\ManageFees;
use App\Models\GuardianEnquiry;
use App\Models\StudentEnquiry;
use App\Models\Student;
use App\Models\FeesMaster;
use App\Models\StudentFeesTemp;
use App\Models\Guardian;
use App\Models\Setting;
use App\Models\StudentMonthlyFees;
use App\Models\StudentFees;
use DB;
use Auth;
use Session;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        return $this->title = 'Student Detail';
    } 

    public function index()
    {


      $title = $this->title;  
      $student = Student::with('hasClass','hasCategory')->get();
      if(request()->get('abbr')!=''){
        $student_ddl = Common::Studentddl();       
        $classddl = Common::Masterddl('class');
        $monthddl = Common::Monthddl();
        $categoryddl = Common::Masterddl('category');        
        $abbr = request()->get('abbr');
        $title = "Addmission Report";
         return view('addmission.index',compact('student','title','abbr','student_ddl','classddl','monthddl','categoryddl'));
      }      
      return view('addmission.index',compact('student','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $title = $this->title;
        $user_id = Auth::user()->id;
        $addmission = new Student();
        $value=time().$user_id;
        if (!$request->session()->exists('fee_temp_id')) {
                // user value cannot be found in session
                 Session::put('fee_temp_id', $value);
            }         
        $student_enquiry = StudentEnquiry::with('hasCategory','hasClass')->where('id',$id)->first();
        $section = Common::Masterddl('section');
        $fees_type = Common::FeesTypeddl($student_enquiry->admission_seeking,$student_enquiry->category);
        $currency_id = Auth::user()->currency_id;
        $currency_name = Master::GetCurrent($currency_id);        
        return view('addmission.create',compact('student_enquiry','section','fees_type','currency_name','addmission','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'section'=> 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
               
                $study = Student::updatecreate($request);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    

                return redirect('student')->with('message', 'Added Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect('student')->with('message', 'Added Successfully!');      
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $title = $this->title;
      $user_id = Auth::user()->id; 
      $student = Student::with('hasClass','hasCategory','hasSection')->where('id',$id)->first();
      $father = Guardian::where('student_id',$id)
                        ->where('relation','father')
                        ->first(); 

        $mother = Guardian::where('student_id',$id)
                            ->where('relation','mother')
                            ->first();  
        $monthly_fees = StudentMonthlyFees::where('student_id',$id)
                            ->get(); 

        $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');

        if(!empty($student)){

         return view('addmission.show',compact('student','father','mother','monthly_fees','month','title'));   
        }
        return view('addmission');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $student = Student::where('id',$id)
                              ->first();                                                                        
        $class = Common::Masterddl('class','',$student->admission_seeking);
        $category = Common::Masterddl('category','',$student->category);
       
<<<<<<< HEAD
         return view('student_enquiry.edit',compact('student','class','category'));   
=======
         return view('student_enquiry.edit',compact('student','class','category','father','mother'));   
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

        $rules = array(
            'section'=> 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = Student::updatecreate($request,$id);
                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('student-enquiry')->with('message', 'Updated Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->with('message','Something Went Wrong..!!')->back();         
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master = Student::where('id',$id)
                                ->first();
        if(!empty($master)){
                $fees_type = GuardianEnquiry::where('student_id',$master->id)->delete();
                if(empty($fees_type)){
                    $master->delete();
                }
        }
        if($master->delete()){
           return redirect('student')->with('message', 'Deleted Successfully!');
        }
         return redirect()->back(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getFees(Request $request)
    {   

        $master = Master::
                         select('master.name','tbl_fee_type.value','tbl_label_type.label','tbl_label_type.id as label_id')
                        ->join('tbl_fee_type','tbl_fee_type.fees_type','=','master.id')
                        ->join('tbl_label_type','tbl_label_type.id','=','master.parant_id')
                        ->join('tbl_fees_master','tbl_fees_master.id','=','tbl_fee_type.fees_master_id')
                        ->where('tbl_fees_master.class',$request->class)
                        ->where('tbl_fees_master.category',$request->category)
                        ->where('master.id',$request->fees_type)
                        ->where('master.abbr','fees')
                        ->where('tbl_label_type.abbr','frequency')
                        ->first();
                        
          if(!empty($master)){
                     return $master;
          }
          else{
            return "0";
          }
    }

    public function addFeesTemp(Request $request){

        $key_id = request('fee_temp_id');
        $addFeesTemp =StudentFeesTemp::updatecreate($request,$key_id);
        return $addFeesTemp;
    }

    public function loadFees(Request $request) {
      $key_id = request('fee_temp_id');    
      $data = StudentFeesTemp::loadFees($key_id);    
      return $data;

    }

    public function deleteFeesTemp($id){
        $delete = StudentFeesTemp::find($id); 
        if(!empty($delete)){
            $delete->delete();
        }
        if(empty($delete->delete())){
            return '1';
        }
        else{
            return '0';
        }
    }

    public function parent(){
     return $this->hasMany(Guardian::class,'student_id','id');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewInvoice($id)
    {
        $title = 'Fees Detail';
        $user_id = Auth::user()->id;
        $student_monthly_fees = StudentMonthlyFees::
                                        with('studentInfo.student_parent','studentInfo.hasClass','studentInfo.hasCategory','studentInfo.hasSection')
                                        ->where('id',$id)
                                        ->first(); 
        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first(); 

                                        //echo count($student_monthly_fees->studentInfo->student_parent);exit;                     
        $student_fee_item = StudentFees::with('getLabel','getFees')->where('fee_id',$id)
                                        ->get();
          $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');                                    
       return view('addmission.fee-invoice',compact('student_fee_item','student_monthly_fees','month','title','schoolInfo'));                             
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function paymentMonthly(Request $request)
    {

        DB::beginTransaction();
        try
            {           
                $payment = StudentMonthlyFees::where('id',$request->id)->first();
                $paid_amount =  $payment->paid_amount + $request->paid_amount;
                $payment->paid_amount = $paid_amount;
                if($request->due_amount =='0' ||  $request->due_amount!=''){
                    $payment->due_amount = $request->due_amount;
                }
                if($request->due_amount =='0'){
                    $payment->status = '1';
                }
                //$payment->final_amount = $paid_amount;
                $payment->save();
                DB::commit();    
                    return redirect()->url('master',$request->id)->with('message', 'Payment Successfully..!!');
                }
                catch(\Exception $e)
                {
                    DB::rollBack();
                    \Log::emergency($e);
                    \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
                }
                return redirect()->back();                    
    }

    public function totalFees($id)
    {
        $title = 'Invoice Detail';
        $user_id = Auth::user()->id;
        $student_monthly_fees = StudentMonthlyFees::where('student_id',$id) 
                                                    ->get(); 
        $student_info = Student::with('student_parent','hasClass','hasSection')
                                ->where('id',$id) 
                                ->first();


        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first(); 

                                        //echo count($student_monthly_fees->studentInfo->student_parent);exit; 
                                         $total_fees = $discount = $paid_fees = $due_fees= 0;                    
        foreach ($student_monthly_fees as $key => $value) {
            $total_fees += $value->total_fees;
            $discount += $value->discount;
            $paid_fees += $value->paid_amount;  
            $due_fees += $value->due_amount;
          # code...
        }
      
          $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');                                    
       return view('addmission.total-fees-invoice',compact('student_monthly_fees','month','title','schoolInfo','student_info','total_fees','discount','paid_fees','due_fees'));                             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function printMonthlyInvoice($id)
    {
        $title = 'Fees Detail';
        $user_id = Auth::user()->id;
        $student_monthly_fees = StudentMonthlyFees::
                                        with('studentInfo.student_parent','studentInfo.hasClass','studentInfo.hasCategory','studentInfo.hasSection')
                                        ->where('id',$id)
                                        ->first(); 
        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first(); 

                                        //echo count($student_monthly_fees->studentInfo->student_parent);exit;                     
        $student_fee_item = StudentFees::with('getLabel','getFees')->where('fee_id',$id)
                                        ->get();
          $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');                                    
       return view('addmission.print-monthly-invoice',compact('student_fee_item','student_monthly_fees','month','title','schoolInfo'));                             
    }

    public function printReport($id)
    {
        $title = 'Invoice Detail';
        $user_id = Auth::user()->id;
        $student_monthly_fees = StudentMonthlyFees::where('student_id',$id) 
                                                    ->get(); 
        $student_info = Student::with('student_parent','hasClass','hasSection')
                                ->where('id',$id) 
                                ->first();


        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first(); 

                                        //echo count($student_monthly_fees->studentInfo->student_parent);exit; 
                                         $total_fees = $discount = $paid_fees = $due_fees= 0;                    
        foreach ($student_monthly_fees as $key => $value) {
            $total_fees += $value->total_fees;
            $discount += $value->discount;
            $paid_fees += $value->paid_amount;  
            $due_fees += $value->due_amount;
          # code...
        }
      
          $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');                                    
       return view('addmission.print-report-invoice',compact('student_monthly_fees','month','title','schoolInfo','student_info','total_fees','discount','paid_fees','due_fees'));                             
    }

    public function searchAddmissionReport(Request $request){
      
        $varStart = $request->start_date;
        $dateStart = str_replace('/', '-', $varStart);
        $newStart= date('Y-m-d', strtotime($dateStart));
        $varEnd = $request->end_date;
        $dateEnd = str_replace('/', '-', $varEnd);
    
        $newEnd= date('Y-m-d', strtotime($dateEnd));         
        $data = Student::with('student_parent')->whereBetween('created_at', [$newStart, $newEnd])->get();
        //print_r($data);exit;
        $table = "";
        if (count($data) !='0') {

            $i = 0;
            $total = 0;

            foreach ($data as $row) {

              foreach ($row->student_parent as $key => $value) {
                 $mobile = $value ? $value->mobile:'';
              }
              foreach ($row->student_parent as $key => $val1) {
                 $email = $val1 ? $val1->email:'';
              } 
              if($row->status =='1'){
                $status = ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status = '<label class="badge badge-danger">Inactive</label>';
              }             
              $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$row->first_name."</td>
                  <td>" .$row->gender."</td>
                   <td>" .$row->dob. "</td>
                   <td>" .$mobile. "</td>
                   <td>" .$email . "</td>
                   <td>" . $status . "</td>
                   <td></td>                    
                  </tr>";
        
                }
        } 
        else {
            $table = "
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
        return $table;     
    }    
/* Due Fees Report */
    public function dueFeeReport()
    {
      $title = 'Due Fees Report';  
      $student = DB::table('tbl_student_monthly_fee')
                    ->select('tbl_student.*','master.name as class_name', DB::raw('sum(tbl_student_monthly_fee.due_amount) as due_amount'),DB::raw('sum(tbl_student_monthly_fee.total_fees) as total_amount'))
                    ->join('tbl_student', 'tbl_student_monthly_fee.student_id', '=', 'tbl_student.id')
                    ->join('master', 'master.id', '=', 'tbl_student.class')
                    ->groupBy('tbl_student_monthly_fee.student_id')
                    ->get();
      $student_ddl = Common::Studentddl();       
      $classddl = Common::Masterddl('class');
      $categoryddl = Common::Masterddl('category');
      $monthddl = Common::Monthddl();

      return view('addmission.due_fees_report',compact('student','title','student_ddl','classddl','monthddl','categoryddl'));
    }

/* search Due amount */
  public function searchDueAmount(Request $request){
 
    $data =DB::table('tbl_student_monthly_fee')
                    ->select('tbl_student.*','master.name as class_name','tbl_student_monthly_fee.due_amount as due_amount','tbl_student_monthly_fee.total_fees as total_amount','tbl_student_monthly_fee.month_id')
                    ->join('tbl_student', 'tbl_student_monthly_fee.student_id', '=', 'tbl_student.id')
                    ->join('master', 'master.id', '=', 'tbl_student.class')
                    ->where('tbl_student_monthly_fee.due_amount','!=','0.00')
                    ->where('tbl_student_monthly_fee.status','0');
                              
        if($request->student!=0){
          $data = $data->where('tbl_student_monthly_fee.student_id',$request->student);              
        }

        if($request->gender){
          $data = $data->where('tbl_student.gender','=',$request->gender);              
        }

        if($request->category!=0){
          $data = $data->where('tbl_student.category',$request->category);              
        }

        if($request->roll_no!=0){
          $data = $data->where('tbl_student.roll_no',$request->roll_no); 
        }

        if($request->class!=0){
          $data = $data->where('tbl_student.class',$request->class); 
        }

        if($request->month!=0){
          $data = $data->where('tbl_student_monthly_fee.month_id',$request->month); 
        }                

        /* html  table */
        $result = $data->get();

        /* month  */

        $table = "";
        if (count($result) !="0") {
            $i = 0;
            $due_amount = 0;
            $total_amount = 0;            
        $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');

            foreach ($result as $row) {
              $i++;

              foreach($month as $key => $mon) {
                if($row->month_id == $key){
                   $new_mon =  $mon;
                }
              }
              
              if($row->status =='1'){
                $status = ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status = '<label class="badge badge-danger">Inactive</label>';
              }                                              
              $category = Master::where('id',$row->category)->first();
              $category = $category ?   $category->name:'';                                        
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$row->first_name."&nbsp;" .$row->last_name."</td>
                  <td>" .$row->roll_no."</td>
                  <td>" .$row->class_name. "</td>
                  <td>" .$row->gender. "</td>
                  <td>" .$category. "</td>
                  <td>" .$row->due_amount . "</td>
                  <td>".$new_mon."</td>
                  <td>" .$row->total_amount . "</td>
                  <td>".$status."</td>
                  <td></td>                    
                  </tr>";
                $due_amount+= $row->due_amount;
                $total_amount+= $row->total_amount;

                }

                $table.= "<tr>
                          <th colspan='7' class='text-right'>Total Due Amount: &nbsp; &nbsp; &nbsp;".$due_amount."</th>
                          <th colspan='2' class='text-left'>Total Due Amount: &nbsp; &nbsp; &nbsp;".$total_amount."</th> 
                          <th></th>
                          <th></th> 
                     </tr>";
        } 
        else {
            $table = "
                  <tr>
                  <td></td>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
       
        return $table;           
  } 

/* Due Fees Report */
    public function paidFeeReport()
    {
      $title = 'Due Fees Report';  
      $student = DB::table('tbl_student_monthly_fee')
                    ->select('tbl_student.*','master.name as class_name', DB::raw('sum(tbl_student_monthly_fee.paid_amount) as paid_amount'),DB::raw('sum(tbl_student_monthly_fee.total_fees) as total_amount'))
                    ->join('tbl_student', 'tbl_student_monthly_fee.student_id', '=', 'tbl_student.id')
                    ->join('master', 'master.id', '=', 'tbl_student.class')
                    ->where('tbl_student_monthly_fee.paid_amount','!=','0.00')
                    //->where('tbl_student_monthly_fee.status','1')                  
                    ->groupBy('tbl_student_monthly_fee.student_id')
                    ->get();

      $student_ddl = Common::Studentddl();       
      $classddl = Common::Masterddl('class');
      $monthddl = Common::Monthddl();
      $categoryddl = Common::Masterddl('category');

      return view('addmission.paid_fees_report',compact('student','title','student_ddl','classddl','monthddl','categoryddl'));
    } 

/* search Due amount */
  public function searchPaidAmount(Request $request){
    
    $data =DB::table('tbl_student_monthly_fee')
                    ->select('tbl_student.*','master.name as class_name','tbl_student_monthly_fee.paid_amount as paid_amount','tbl_student_monthly_fee.total_fees as total_amount','tbl_student_monthly_fee.month_id')
                    ->join('tbl_student', 'tbl_student_monthly_fee.student_id', '=', 'tbl_student.id')
                    ->join('master', 'master.id', '=', 'tbl_student.class')
                    ->where('tbl_student_monthly_fee.paid_amount','!=','0.00');
                    //->where('tbl_student_monthly_fee.status','1');
                                       
        if($request->student!=0){
          $data = $data->where('tbl_student_monthly_fee.student_id',$request->student);              
        }

        if($request->roll_no!=0){
          $data = $data->where('tbl_student.roll_no',$request->roll_no); 
        }

        if($request->gender){
          $data = $data->where('tbl_student.gender','=',$request->gender);              
        }

        if($request->category!=0){
          $data = $data->where('tbl_student.category',$request->category);              
        }        

        if($request->class!=0){
          $data = $data->where('tbl_student.class',$request->class); 
        }

        if($request->month!=0){
          $data = $data->where('tbl_student_monthly_fee.month_id',$request->month); 
        }                

        /* html  table */
        $result = $data->get();


        /* month  */

        $table = "";
        if (count($result) !="0") {
            $i = 0;
        $month = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');

            foreach ($result as $row) {
              $i++;

              foreach($month as $key => $mon) {
                if($row->month_id == $key){
                   $new_mon =  $mon;
                }
              }
              
              if($row->status =='1'){
                $status = ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status = '<label class="badge badge-danger">Inactive</label>';
              }

              $category = Master::where('id',$row->category)->first();
              $category = $category ?   $category->name:'';                                                                                                
                $table.= "<tr>
                  <td>" . $i . "</td>
                  <td>" .$row->first_name."&nbsp;" .$row->last_name."</td>
                  <td>" .$row->roll_no."</td>
                  <td>" .$row->class_name. "</td>
                  <td>" .$row->gender. "</td>
                  <td>" .$category. "</td>                  
                  <td>" .$row->paid_amount . "</td>
                  <td>".$new_mon."</td>
                  <td>" .$row->total_amount . "</td>
                  <td>".$status."</td>                  
                  <td></td>
                  </tr>";
        
                }
        } 
        else {
            $table = "
                  <tr>
                  <td></td>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
       
        return $table;           
  }  

/* search Addmission Report */
  public function searchAddmission(Request $request){

      $varStart = $request->start_date;
      $dateStart = str_replace('/', '-', $varStart);
      $newStart= date('Y-m-d', strtotime($dateStart));
      $varEnd = $request->end_date;
      $dateEnd = str_replace('/', '-', $varEnd);
      $newEnd= date('Y-m-d', strtotime($dateEnd));         

      $data = Student::with('hasClass','hasCategory'); 
    
        if($request->student!=0){
          $data = $data->where('tbl_student.id',$request->student);              
        }

        if($request->gender){
          $data = $data->where('tbl_student.gender','=',$request->gender);              
        }

        if($request->category!=0){
          $data = $data->where('tbl_student.category',$request->category);              
        }

        if($request->roll_no!=0){
          $data = $data->where('tbl_student.roll_no',$request->roll_no); 
        }

        if($request->class!=0){
          $data = $data->where('tbl_student.class',$request->class); 
        } 

        if($request->end_date ){
          $data = $data->whereBetween('created_at', [$newStart, $newEnd]); 
        }
        /* html  table */
        $result = $data->get();

        /* month  */

        $table = "";
        if (count($result) !="0") {
            $i = 0;
            foreach ($result as $row) {
              $i++;
              
              if($row->status =='1'){
                $status = ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status = '<label class="badge badge-danger">Inactive</label>';
              }                                              
              $category = Master::where('id',$row->category)->first();
              $category = $category ?   $category->name:'';                                        
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$row->first_name."&nbsp;" .$row->last_name."</td>
                  <td>" .$row->roll_no."</td>
                  <td>".date("d-m-Y ",strtotime($row ? $row->dob:'' ))."</td>
                 
                  <td>" .$row->gender. "</td>
                   <td>" .$row->hasClass->name. "</td>
                  <td>" .$row->hasCategory->name. "</td>
                  <td>".date("d-m-Y ",strtotime($row ? $row->created_at:'' ))."</td>
                  <td>".$status."</td>
                  <td></td>                    
                  </tr>";
                      if($row->gender =='male'){
                                $total_male[] = $row->gender;
                      }
                      if($row->gender =='female'){
                                  $total_female[] = $row->gender;
                      }  
                }
                      if(isset($total_male)){
                        $total_male = count($total_male);
                      }
                      else{
                          $total_male = "0";
                      }
                      if(isset($total_female)){
                        $total_female =count($total_female);
                      } 
                      else{
                        $total_female = "0";
                      }                 

                $table.= "<tr>
                          <th colspan='7' class='text-right'>Total Male: &nbsp; &nbsp; &nbsp;".$total_male."</th>
                          <th colspan='2' class='text-left'>Total Female: &nbsp; &nbsp; &nbsp;".$total_female."</th> 
                          <th></th>
                          <th></th> 
                     </tr>";
        } 
        else {
            $table = "
                  <tr>
                  <td></td>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
       
        echo $table;           
  } 


}
