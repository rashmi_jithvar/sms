<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Models\Common;

class SubPropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
      $properties = Master::with('parent')->where('abbr','subproperty_type')->get();
      return view('subproperty_type.index',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $property_type = Common::Masterddl('property_type');
        return view('subproperty_type.create',compact('property_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'parant_id'=> 'required|integer',
            'name' => 'required|max:255',
            'status'  => 'required',
        ]);
        
        $permission_exit = Master::where('name', 'like', '%'.$request->name.'%')->first();

        if(!empty($permission_exit)){
           return redirect()->back()->with('message', 'Property already exist.');
         }
            

        $request->merge(['abbr'=>'subproperty_type']);
        $master = Master::updatecreate($request);  
        
        return redirect('subproperty_type')->with('message', 'Property Create Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $property_exist = Master::find($id);
        
        if(!empty($property_exist)){
         return view('subproperty_type.edit',compact('property_exist'));   
        }
        return view('subproperty_type.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $property_exist = Master::find($id);
        $property_type = Common::Masterddl('property_type',$property_exist->parant_id);
        if(!empty($property_exist)){
         return view('subproperty_type.edit',compact('property_exist','property_type'));   
        }
        return view('subproperty_type.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
         $validatedData = $request->validate([
            'parant_id'=> 'required|integer',
            'name' => 'required|max:255',
            'status'  => 'required',
        ]);
        

        $permission_exit = Master::where('name', 'like', '%'.$request->name.'%')->first();
        

        if(!empty($permission_exit)){
            if($request->name != $permission_exit->name){
             return redirect()->back()->with('message', 'Property already exist.');
            }
        }
        $request->merge(['abbr'=>'subproperty_type']);
        $master = Master::updatecreate($request,$id);
        //dd($master);
        if($master){  
          return redirect('subproperty_type')->with('message', 'Property Create Successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
