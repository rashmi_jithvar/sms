<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudentResultReport;
use App\Models\Common;
use App\Models\MarksMaster;
use App\Models\SubjectHasMarks;
use App\Models\StudentObtainedMarksTemp;
use App\Models\StudentObtainedMarks;
use App\Models\Setting;
use Session;
use DB;

class StudentResultReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        return $this->title = 'Manage Student Result';
    } 

    public function index()
    {

      $title = $this->title;
      $list = StudentResultReport::with('studentInfo.hasClass','studentInfo.hasSection','hasExamType')->get();

      return view('student_result.index',compact('list','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = $this->title;
        $marks = new StudentResultReport();
        $exam_type = Common::Masterddl('exam-type');
        $section = Common::Masterddl('section');
        $class = Common::Masterddl('class');
        $user_id = Auth::user()->id;
        $value=time().$user_id.'rr';
        if (!$request->session()->exists('result_key')) {
                 Session::put('result_key', $value);
        }     
        return view('student_result.create',compact('class','title','exam_type','marks','section'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $rules = array(
            'exam_type'=> 'required|not_in:0',
            'class' => 'required|not_in:0',
            'section'=> 'required|not_in:0',
            'student' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $session = 54;
                $study = StudentResultReport::updatecreate($request,$session);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('student-result-report');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;      
     //echo count($fees_type);exit;
      $marks = StudentResultReport::with('studentInfo.hasClass','studentInfo.hasSection','hasExamType')                          ->where('id',$id)
                                  ->first();  

        $exam_type = Common::Masterddl('exam-type','',$marks->exam_type);
        $section = Common::Masterddl('section','',$marks->section_id);
        $class = Common::Masterddl('class','',$marks->class_id);
        $student = Common::StudentddlList($marks->class_id,$marks->section_id,$marks->student_id);

        $subject_marks = StudentObtainedMarks::where('student_result_id',$marks->id)->get();
    
            if (count($subject_marks) > 0) {
                foreach ($subject_marks as $k => $item) {

                    $check = StudentObtainedMarksTemp::where('key_id',$item->student_result_id)
                                            ->where('subject_id',$item->subject_id)
                                            ->where('total_max_marks',$item->total_max_marks)
                                            ->where('total_obtained_marks',$item->total_obtained_marks)
                                            ->first();

                    if(empty($check)){
                        $temp = new StudentObtainedMarksTemp();
                        $temp->subject_id = $item->subject_id;
                        $temp->internal_marks = $item->internal_marks;
                        $temp->obtained_marks = $item->obtained_marks;
                        $temp->external_marks = $item->external_marks;
                        $temp->total_max_marks = $item->total_max_marks;
                        $temp->total_obtained_marks = $item->total_obtained_marks;
                        $temp->key_id = $item->student_result_id;
                        $temp->save();                        
                    }

                }
        }          
        //if(!empty($fees_master)){
<<<<<<< HEAD
         return view('student_result.edit',compact('class','marks','exam_type','section','student','title'));   
=======
         return view('student_result.edit',compact('class','marks','exam_type','section','student'));   
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
       // }
       // return view('student_enquiry.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        
        $rules = array(
            'exam_type'=> 'required|not_in:0',
            'class' => 'required|not_in:0',
            'section'=> 'required|not_in:0',
            'student' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
<<<<<<< HEAD
            $input ='';
=======
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      

                $study = StudentResultReport::updatecreate($request,'',$id);

                if($study)
                {
                    $request->session()->flash('success', 'Update Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('student-result-report');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        $title = 'Report Card'; 
        $user_id = Auth::user()->id;   
        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first();         
        $list = StudentResultReport::with('studentInfo.hasClass','studentInfo.hasSection','hasExamType','hasSubjectMarks.getSubject','getGrade')
                         ->where('id',$id)
                         ->first();              
        if(!empty($list)){
          return view('student_result.show',compact('list','title','schoolInfo'));
        }
        return view('student_result.show',compact('list','title','schoolInfo'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {   

        $title = 'Report Card'; 
        $user_id = Auth::user()->id;   
        $schoolInfo = Setting::where('user_id',$user_id)
                                        ->first();         
        $list = StudentResultReport::with('studentInfo.hasClass','studentInfo.hasSection','hasExamType','hasSubjectMarks.getSubject','getGrade')
                         ->where('id',$id)
                         ->first();              
        if(!empty($list)){
          return view('student_result.print',compact('list','title','schoolInfo'));
        }
        return view('student_result.print',compact('list','title','schoolInfo'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $master = StudentResultReport::where('id',$id)
                            ->first();
        if(!empty($master)){
                $marks = StudentObtainedMarks::where('student_result_id',$master->id)->delete();
                if(empty($marks)){
                    $master->delete();
                }
        }
        if($master->delete()){
            return redirect()->back()->with('message','Deleted Successfully.!'); 
        }
         return redirect()->back()->with('message','Deleted Successfully.!'); 
       
    }


/* get Subject */
    public function getSubject(Request $request)
    {

            $get_subject = Common::SubjectMarksMaster($request->class,$request->exam_type);
            if(!empty($get_subject)){
                return $get_subject;
            }
            else{
                return 0;
            }

    }

/* get Subject */
    public function getTotalSubjectMarks(Request $request)
    {

      $data = MarksMaster::where('exam_type',$request->exam_type)
                          ->where('class_id',$request->class)
                          ->first();  
                          
                     
            if(!empty($data)){
                return $data ? $data->total_marks : 0;
            }
            else{
                return 0;
            }

    }  


/* add Marks */
    public function addStudentMarks(Request $request){

        $key = request('result_key');
        $fees_master =StudentObtainedMarksTemp::updatecreate($request,$key);
        return $fees_master;
    }



/* Load Marks */
    public function LoadStudentMarks(Request $request) {
      $key = request('result_key');
      $data = StudentObtainedMarksTemp::loadMarks($key);    
      return $data;

    }

/* delete temp marks */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteStudentMarks($id)
    {

        $delete = StudentObtainedMarksTemp::find($id); 
        if(!empty($delete)){
            $delete->delete();
        }
        if(empty($delete->delete())){
            return '1';
        }
        else{
            return '0';
        }
    }

  /* Student List */  
  public function getStudentList(Request $request){
      $class = $request->class;
      $section = $request->section;
      $student_ddl = Common::StudentddlList($class,$section);
      if(!empty($student_ddl)){
        return $student_ddl;
      }
      else{
        return 0;
      }
  }  
/* get Marks*/
  public function getMarks(Request $request){
    $request->class;
    $request->exam_type;
    $request->subject;
            $data = MarksMaster::
                            with('subjectHasMarks.getSubject')
                            ->select('tbl_subject_has_marks.*')
                            ->join('tbl_subject_has_marks','tbl_subject_has_marks.marks_master_id','=','tbl_marks_master.id')
                            ->where('exam_type',$request->exam_type)
                            ->where('class_id',$request->class)
                            ->where('tbl_subject_has_marks.subject_master_id',$request->subject)
                            ->first();
      if(!empty($data)) {
        return $data;
      }               
      else{
        return 0;
      }
  }     
}
