<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Models\Common;
use App\Models\GradeMaster;
use App\Models\GuardianEnquiry;
use App\Models\StudentEnquiry;
use App\Models\Student;
use App\Models\FeesMaster;
use App\Models\StudentFeesTemp;
use App\Models\Guardian;
use App\Models\Setting;
use App\Models\StudentMonthlyFees;
use App\Models\StudentFees;
use DB;
use Auth;
use Session;

class GradeMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        return $this->title = 'Grade Master';
    } 

    public function index()
    {


      $title = $this->title;  
      $list = GradeMaster::with('gradeName')->get();  

      return view('grade_master.index',compact('list','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = $this->title; 
        $grade = Common::Masterddl('grade');

        return view('grade_master.create',compact('title','grade'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'min_percent'=> 'required|numeric',
            'max_percent'=>'required|numeric',
            'grade' =>'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
               
                $study = GradeMaster::updatecreate($request);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    

                return redirect('grade-master')->with('message', 'Added Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                \Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect('grade-master')->with('message', 'Added Successfully!');      
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title; 
        $list = GradeMaster::with('gradeName')->where('id',$id)->first();
        $grade = Common::Masterddl('grade','',$list->grade);
       
         return view('grade_master.edit',compact('list','grade','title'));   
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

        $rules = array(
            'min_percent'=> 'required|numeric',
            'max_percent'=>'required|numeric',
            'grade' =>'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = GradeMaster::updatecreate($request,$id);
                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('grade-master')->with('message', 'Updated Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                \Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->with('message','Something Went Wrong..!!')->back();         
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master = GradeMaster::where('id',$id)
                                ->first();
        if(!empty($master)){
                    $master->delete();
        }
        if($master->delete()){
           return redirect('grade-master')->with('message', 'Deleted Successfully!');
        }
         return redirect()->back(); 
    }
}