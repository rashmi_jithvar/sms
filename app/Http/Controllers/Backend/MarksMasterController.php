<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubjectMaster;
use App\Models\Common;
use App\Models\MarksMaster;
use App\Models\SubjectHasMarks;
use App\Models\SubjectHasMarksTemp;
use Session;
use DB;

class MarksMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        return $this->title = 'Masks Master';
    } 

    public function index()
    {

      $title = $this->title;
      $list = MarksMaster::with('subjectHasMarks.getSubject','className','examType')->get();

      return view('marks_master.index',compact('list','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = $this->title;
        $marks = new MarksMaster();
        $exam_type = Common::Masterddl('exam-type');
        $class = Common::Masterddl('class');
        $user_id = Auth::user()->id;
        $value=time().$user_id.'m';
        if (!$request->session()->exists('subject_key')) {
                // user value cannot be found in session
                 Session::put('subject_key', $value);
        }     
        return view('marks_master.create',compact('class','title','exam_type','marks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

        $rules = array(
            'class'=> 'required|not_in:0',
            'exam_type' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = MarksMaster::updatecreate($request);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('marks-master');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;      
     //echo count($fees_type);exit;
        $marks = MarksMaster::with('subjectHasMarks.getSubject','className','examType')
                         ->where('id',$id)
                          ->first();  

        $class = Common::Masterddl('class','',$marks->class_id);
        $exam_type = Common::Masterddl('exam-type','',$marks->exam_type); 

        $subject_marks = SubjectHasMarks::where('marks_master_id',$marks->id)->get();
       
            if (count($subject_marks) > 0) {
                foreach ($subject_marks as $k => $item) {

                    $check = SubjectHasMarksTemp::where('key_id',$item->marks_master_id)
                                            ->where('subject_master_id',$item->subject_master_id)
                                            ->first();

                    if(empty($check)){
                        $temp = new SubjectHasMarksTemp();
                        $temp->subject_master_id = $item->subject_master_id;
                        //$temp->max_mark = $item->max_mark;
                        $temp->internal_marks = $item->internal_marks;
                        $temp->is_external = $item->is_external;
                        $temp->external_marks = $item->external_marks;
                        $temp->total_marks = $item->total_marks;
                        $temp->key_id = $item->marks_master_id;
                        $temp->save();                        
                    }

                }
        }          
        //if(!empty($fees_master)){
<<<<<<< HEAD
         return view('marks_master.edit',compact('class','marks','exam_type','title'));   
=======
         return view('marks_master.edit',compact('class','marks','exam_type'));   
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
       // }
       // return view('student_enquiry.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        
        $rules = array(
            'class'=> 'required|not_in:0',
            'exam_type' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = MarksMaster::updatecreate($request,$id);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('marks-master');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        $title = $this->title;    
        $list = MarksMaster::with('subjectHasMarks.getSubject','className','examType')
                         ->where('id',$id)
                          ->first();                        
        if(!empty($list)){
          return view('marks_master.show',compact('list','title'));
        }
        return view('marks_master.show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $master = MarksMaster::where('id',$id)
                            ->first();
        if(!empty($master)){
                $marks = SubjectHasMarks::where('marks_master_id',$master->id)->delete();
                if(empty($marks)){
                    $master->delete();
                }
        }
        if($master->delete()){
            return redirect()->back()->with('message','Deleted Successfully.!'); 
        }
         return redirect()->back()->with('message','Deleted Successfully.!'); 
       
    }

    public function getSubject(Request $request)
    {
            $get_subject = Common::SubjectMaster($request->class);
            if(!empty($get_subject)){
                return $get_subject;
            }
            else{
                return 0;
            }

    }

/* add Marks */
    public function addMarks(Request $request){
        $key = request('subject_key');
        $fees_master =SubjectHasMarksTemp::updatecreate($request,$key);
        return $fees_master;
    }



/* Load Marks */
    public function LoadMarks(Request $request) {
      $key = request('subject_key');
      $data = SubjectHasMarksTemp::loadMarks($key);    
      return $data;

    }

/* delete temp marks */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteSubjectHasMarks($id)
    {
        $delete = SubjectHasMarksTemp::find($id); 
        if(!empty($delete)){
            $delete->delete();

        }
        
        if(empty($delete->delete())){
            return '1';
        }
        else{
            return '0';
        }
    }    
}
