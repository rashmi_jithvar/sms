<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubjectMaster;
use App\Models\Common;
use App\Models\Master;
use Session;
use DB;

class SubjectMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        return $this->title = 'Subject Master';
    } 

    public function index()
    {

      $title = $this->title;
      $list = Master::with('SubjectMaster.getSubject')
                          ->where('abbr','class')
                          //->where('class_has_subject','1')
                          ->get();
      return view('subject_master.index',compact('list','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = $this->title;
        $fees_master = new SubjectMaster();
        $subject = Common::Masterddl('subject');
        $class = Common::Masterddl('class');     
        return view('subject_master.create',compact('class','title','subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

        $rules = array(
            'class_id'=> 'required|not_in:0',
            'subject_id' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = SubjectMaster::updatecreate($request);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('subject-master');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;      
     //echo count($fees_type);exit;
        $list = Master::with('SubjectMaster.getSubject')
                          ->where('abbr','class')
                          ->where('id',$id)
                          ->first(); 

        $class = Common::Masterddl('class','',$list->id);
        $subject = Master::where('abbr','subject')->pluck('name','id');    
        $fees = Common::Masterddl('fees');   
   
        //if(!empty($fees_master)){
<<<<<<< HEAD
         return view('subject_master.edit',compact('class','fees','list','title','subject'));   
=======
         return view('subject_master.edit',compact('class','category','fees','list','title','subject'));   
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
       // }
       // return view('student_enquiry.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        
        $rules = array(
            'class_id'=> 'required|not_in:0',
            'subject_id' => 'required|not_in:0',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = SubjectMaster::updatecreate($request,$id);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('subject-master');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $master = SubjectMaster::where('class_id',$id)
                                ->get();
            if(!empty($master)){
              foreach ($master as $key => $value) {
                $value->delete();
              }     
               return redirect()->back()->with('message','Deleted Successfully.!'); 
            }
      
         return redirect()->back()->with('message','SomeThing Went Wrong.!'); 
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFeesTypeTemp($id)
    {
        $delete = FeeTypeTemp::find($id); 
        if(!empty($delete)){
            $delete->delete();

        }
        
        if(empty($delete->delete())){
            return '1';
        }
        else{
            return '0';
        }
    }

}
