<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;
use App\Models\Common;

class MasterController extends Controller
{

    public function __construct()
    {
        $abbr = request()->get('abbr');
        $common = New Common();
        $getInfo = $common->getAbbr($abbr); 
        return $this->abbr = $getInfo;
    } 

    /* Index Function */
    public function index(Request $request){
      if(request()->get('abbr') =='fees'){

        $properties = Master::with('getLabel')->where('abbr',$request->abbr)->get();
      }
      else{
        $properties = Master::with('parent')->where('abbr',$request->abbr)->get();
      }
    
      $title =  $this->abbr['title']; 
      $subtitle = $this->abbr['subtitle'];                         
      return view('master.index',compact('properties','title','subtitle'));
    }

    /* Create Function */
    public function create(Request $request){
        $property_type = $this->abbr['property_type']; 
        $title = $this->abbr['title'];  
        $subtitle = $this->abbr['subtitle'];        
    	return view('master.create',compact('property_type','title','subtitle'));
    }

    /* Edit Function */ 
    public function edit(Request $request){
    	$property_exist = Master::find($request->id);
        
          if(empty($property_exist)){
      	  return redirect()->back();	
      	}

        if(request()->get('abbr') =='fees'){
         
            $property_type =  $property_type = Common::Labelddl('frequency',$property_exist->parant_id);
        
        }
        else{
             $property_type = $this->abbr['property_type']; 
        }
       
        $title = $this->abbr['title'];  
        $subtitle = $this->abbr['subtitle'];
    	return view('master.edit',compact('property_exist','property_type','subtitle','title'));
    }

    /* Store Function */
    public function store(Request $request){
    	  
        $validation = $this->validationList($request);
       // $permission_exit = Master::where('name', 'like', '%'.$request->name.'%')->first();
        $permission_exit = Master::where("name", 'REGEXP' ,'^[ankit]')->first();
        //
        if(!empty($permission_exit)){

            if($request->name == $permission_exit->name){
             return redirect()->back()->with('message', 'Property already exist.');
            }
        }
        $request->merge(['abbr'=>$request->abbr]);
<<<<<<< HEAD
        Master::updatecreate($request);
=======
        $master = Master::updatecreate($request);
>>>>>>> af13eede2432353eba5e6b9be1fc7669cb247cab
        
       // if($master){  
          return redirect()->route('master',['abbr'=>$request->abbr])->with('message', 'Added Successfully!');
        //}
    }

    /* Update Function */
    public function update(Request $request){

      $validation = $this->validationList($request);
	    //$permission_exit = Master::where('name', 'like', '%'.$request->name.'%')->first();
	    $permission_exit = Master::where('name', $request->name)->where('abbr', $request->abbr)->first();
   
	    if(!empty($permission_exit)){
	        if($request->name != $permission_exit->name){
	         return redirect()->back()->with('message', 'Name already exist.');
	        }
        }
        $request->merge(['abbr'=>$request->abbr]);
        $master = Master::updatecreate($request,$request->id);
        
        if($master){  
          return redirect()->route('master',['abbr'=>$request->abbr])->with('message', 'Update Successfully!');
        }
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        //
        $properties = Master::findorfail($request->id);
        $child_properties = Master::where('parant_id',$request->id)->first();
        
        if($child_properties){
         $child_properties->parant_id = null;
         $child_properties->save();
        } 

        if($properties->delete()){
            return redirect()->route('master',['abbr'=>$request->abbr])->with('message','Deleted successfully');
        }
    }

    /**
     * validate List.
     *
     * @param $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validationList(Request $request)
    {
        if($request->abbr=='class' || $request->abbr=='section' || $request->abbr=='subject' || $request->abbr=='session' || $request->abbr=='fees' || $request->abbr=='grade' && $request->abbr=='category' || $request->abbr=='discount' ||  $request->abbr=='exam-type' ){
            $validatedData = $request->validate([
                'name' => 'required',
                //'status'  => 'required',
            ]);
        }else{
            $validatedData = $request->validate([
                //'parant_id' => 'required|int',
                'name' => 'required',
                //'status'  => 'required',
            ]); 
        }

        return $validatedData;
    } 

}