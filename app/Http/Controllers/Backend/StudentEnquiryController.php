<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Models\Common;
use App\Models\StudentEnquiry;
use App\Models\GuardianEnquiry;
use DB;
use Auth;

class StudentEnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {

        return $this->title = 'Student Enquiry';
    } 

    public function index()
    {
      $title = $this->title;
      $student = StudentEnquiry::with('parent','hasClass','hasCategory')->get();
      $abbr = request()->get('abbr');
      if(request()->get('abbr')!=''){
        $abbr = request()->get('abbr');
        $title = "Enquiry Report";
        $student_ddl = Common::Studentddl();       
        $classddl = Common::Masterddl('class');
        $monthddl = Common::Monthddl();
        $categoryddl = Common::Masterddl('category');            
         return view('student_enquiry.index',compact('student','title','abbr','student_ddl','classddl','monthddl','categoryddl'));
      }
      return view('student_enquiry.index',compact('student','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = $this->title;
        $class = Common::Masterddl('class');
        $category = Common::Masterddl('category');
        return view('student_enquiry.create',compact('class','category','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'first_name'=> 'required|alpha',
            'last_name' => 'required|alpha',
            'gender' => 'required_if:gender, !=, ""',
            'dob' => 'required',
            'category' => 'required|not_in:0',
            'admission_seeking' => 'required|not_in:0',
            //'father_name' => 'required',
            'father_contact_no' => 'required_without:mother_contact_no',
            //'father_email' => 'required|email',            
            //'occupation_father' => 'required',
           // 'mother_name' => 'required',
            'mother_contact_no' => 'required_without:father_contact_no',
            //'mother_email' => 'required|email',
            //'occupation_mother' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required|min:6|numeric',           
            'address' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
               
                $study = StudentEnquiry::updatecreate($request);

                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('student-enquiry')->with('message', 'Added Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect('student-enquiry')->with('message', 'Added Successfully!');    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $title = $this->title;
      $user_id = Auth::user()->id; 
      $student = StudentEnquiry::with('hasClass','hasCategory','hasSection')->where('id',$id)->first();
      $father = GuardianEnquiry::where('student_id',$id)
                        ->where('relation','father')
                        ->first(); 

      $mother = GuardianEnquiry::where('student_id',$id)
                            ->where('relation','mother')
                            ->first();  

        if(!empty($student)){

         return view('student_enquiry.show',compact('student','father','mother','title'));   
        }
        return view('addmission');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;
        $student = StudentEnquiry::
                                    where('id',$id)
                                    ->first();
        $father = GuardianEnquiry::
                                    where('student_id',$id)
                                    ->where('relation','father')
                                    ->first(); 

        $mother = GuardianEnquiry::
                                    where('student_id',$id)
                                    ->where('relation','mother')
                                    ->first();                                                                         
        $class = Common::Masterddl('class','',$student->admission_seeking);
        $category = Common::Masterddl('category','',$student->category);
       
         return view('student_enquiry.edit',compact('student','class','category','father','mother','title'));   
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

        $rules = array(
            'first_name'=> 'required|alpha',
            'last_name' => 'required|alpha',
            'gender' => 'required_if:gender, !=, ""',
            'dob' => 'required',
            'category' => 'required|not_in:0',
            'admission_seeking' => 'required|not_in:0',
            //'father_name' => 'required',
            'father_contact_no' => 'required_without:mother_contact_no',
            //'father_email' => 'required|email',            
            //'occupation_father' => 'required',
           // 'mother_name' => 'required',
            'mother_contact_no' => 'required_without:father_contact_no',
            //'mother_email' => 'required|email',
            //'occupation_mother' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required|min:6|numeric',           
            'address' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return  back() ->withErrors($validator)->withInput();
            $input = input::all();
        } 
        else 
        {
            DB::beginTransaction();
            try
            {      
                $study = StudentEnquiry::updatecreate($request,$id);
                if($study)
                {
                    $request->session()->flash('success', 'Added Successfully. ');
                }
                else
                {
                    $request->session()->flash('error', 'Some Error Occoured. Please Try Again.');
                }
            DB::commit();    
                return redirect('student-enquiry')->with('message', 'Updated Successfully!');
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::emergency($e);
                \Session::flash('error', 'Unable to process request.Error:'.json_encode($e->getMessage(), true));
            }
            return redirect()->back();         
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master = StudentEnquiry::where('id',$id)
                                ->first();
        if(!empty($master)){
                $fees_type = GuardianEnquiry::where('student_id',$master->id)->delete();
                if(empty($fees_type)){
                    $master->delete();
                }
        }
        if($master->delete()){
            return redirect('student-enquiry')->with('message', 'Deleted Successfully!');
        }
         return redirect('student-enquiry')->with('message', 'Deleted Successfully!');
    }

    public function searchReport(Request $request){
      
        $varStart = $request->start_date;
        $dateStart = str_replace('/', '-', $varStart);
        $newStart= date('Y-m-d', strtotime($dateStart));
        $varEnd = $request->end_date;
        $dateEnd = str_replace('/', '-', $varEnd);
    
        $newEnd= date('Y-m-d', strtotime($dateEnd));         
        $data = StudentEnquiry::with('parent')->whereBetween('created_at', [$newStart, $newEnd])->get();
        $table = "";
        if (count($data) !='0') {

            $i = 0;
            $total = 0;

            foreach ($data as $row) {
              foreach ($row->parent as $key => $value) {
                 $mobile = $value ? $value->mobile:'';
              }
              foreach ($row->parent as $key => $val1) {
                 $email = $val1 ? $val1->email:'';
              } 
              if($row->status =='1'){
                $status =  ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status =   '<label class="badge badge-danger">Inactive</label>';
              }             
              $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$row->first_name."</td>
                  <td>" .$row->gender."</td>
                   <td>" .$row->dob. "</td>
                   <td>" .$mobile. "</td>
                   <td>" .$email . "</td>
                   <td>" . $status . "</td>
                   <td></td>                    
                  </tr>";
        
                }
            $table.= "
                 ";
        } 
        else {
            $table = "
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
        return $table;     
    }

/* search Addmission Report */
  public function searchEnquiry(Request $request){

      $varStart = $request->start_date;
      $dateStart = str_replace('/', '-', $varStart);
      $newStart= date('Y-m-d', strtotime($dateStart));
      $varEnd = $request->end_date;
      $dateEnd = str_replace('/', '-', $varEnd);
      $newEnd= date('Y-m-d', strtotime($dateEnd));         

      $data = StudentEnquiry::with('hasClass','hasCategory','parent');
                              //->join('tbl_guardian_enquiry','tbl_guardian_enquiry.student_id','=','tbl_student_enquiry.id'); 

        if($request->student!=0){
          $data = $data->where('tbl_student_enquiry.id',$request->student);              
        }


        if($request->gender){
          $data = $data->where('tbl_student_enquiry.gender','=',$request->gender);              
        }

        if($request->category!=0){
          $data = $data->where('tbl_student_enquiry.category',$request->category);              
        }

        if($request->roll_no!=0){
          $data = $data->where('tbl_student_enquiry.roll_no',$request->roll_no); 
        }

        if($request->class!=0){
          $data = $data->where('tbl_student_enquiry.admission_seeking',$request->class); 
        } 

        if($request->phone!=0){
          $data = $data->where('tbl_guardian_enquiry.mobile',$request->phone); 
        }

        if($request->email!=0){
          $data = $data->where('tbl_guardian_enquiry.email',$request->email); 
        }         

        if($request->end_date ){
          $data = $data->whereBetween('tbl_student_enquiry.created_at', [$newStart, $newEnd]); 
        }
        /* html  table */
        $result = $data->get();

        /* month  */

        $table = "";
        if (count($result) !="0") {
            $i = 0;
            foreach ($result as $row) {
              $i++;
            
              if(isset($row->parent[0])){
               $phone = $row->parent[0]->mobile;
               $email = $row->parent[0]->email;
              } 
              else if(isset($row->parent[1])){
                $phone = $row->parent[1]->mobile;
                $email = $row->parent[0]->email;
              } 
              else{
                  $phone = "";
                  $email = "";
                  
              }


              if($row->status =='1'){
                $status = ' <label class="badge badge-success">Active</label>';
              }
              else{
                 $status = '<label class="badge badge-danger">Inactive</label>';
              }                                              
              $category = Master::where('id',$row->category)->first();
              $category = $category ?   $category->name:'';                                        
                $table.= "<tr>
                <td>" . $i . "</td>
                <td>" .$row->first_name."&nbsp;" .$row->last_name."</td>
                <td>".date("d-m-Y ",strtotime($row ? $row->dob:'' ))."</td>
                <td>" .$row->gender. "</td>
                <td>" .$row->hasClass->name. "</td>
                <td>" .$row->hasCategory->name. "</td>
                <td>".$phone."</td>
                <td>".$email."</td>
                <td>".date("d-m-Y ",strtotime($row ? $row->created_at:'' ))."</td>
                <td>".$status."</td>
                <td></td>                    
                </tr>";
                      if($row->gender =='male'){
                                $total_male[] = $row->gender;
                      }
                      if($row->gender =='female'){
                                  $total_female[] = $row->gender;
                      }  
                }
                      if(isset($total_male)){
                        $total_male = count($total_male);
                      }
                      else{
                          $total_male = "0";
                      }
                      if(isset($total_female)){
                        $total_female =count($total_female);
                      } 
                      else{
                        $total_female = "0";
                      }                 

                $table.= "<tr>
                          <th colspan='7' class='text-right'>Total Male: &nbsp; &nbsp; &nbsp;".$total_male."</th>
                          <th colspan='2' class='text-left'>Total Female: &nbsp; &nbsp; &nbsp;".$total_female."</th> 
                          <th></th>
                          <th></th> 

                     </tr>";
        } 
        else {
            $table = "
                  <tr>
                  <td></td>
                      <td colspan='10'>No records found</td>
                   </tr>
                   ";
        }
       
        echo $table;           
  } 



}
