<?php

namespace App\Http\Controllers;

use App\Pages;
use App\AddSubMenus;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageList = Pages::get();        
        $tableName = "pages";
        return view('cms_mangment.addmenupage', compact('pageList', 'tableName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $posted = new Pages;
            $posted->pageName = $request->pageName;
            $posted->subMenu = $request->subMenuName;
            $posted->pageLink = str_replace(" ", "-", strtolower($request->pageName));
            $posted->pageStatus = $request->status;
            $posted->save();

            return response()->json([
                "message" => "Records Inserted Successfully!"
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function show(Pages $pages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $menuDetails = Pages::where('id', $request->id)
                        ->first();
        $pageList = Pages::get();        

        return view('cms_mangment.addmenuedit', compact('menuDetails', 'pageList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'pageName' => 'required',
            'status' => 'required',
        ]);

        $menuChk = Pages::where('id', $request->recordId)->first();
        if(!empty($menuChk))
        {
            $form_data = array(
            'pageName' => $request->pageName,
            'subMenu' => $request->subMenuName,
            'pageLink' => str_replace(" ", "-", strtolower($request->pageName)),
            'pageStatus' => $request->status
            );
  
            Pages::whereId($request->recordId)->update($form_data);
        }

        return response()->json([
            "message" => "Records Updated Successfully!"
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // $posted = Pages::find($request->rowid);
        // $posted->delete();
        // DB::enableQuerylog();
        $datainserted = DB::table($request->tableName)->whereId($request->rowid)->delete();
        // dd(DB::getQuerylog());
        return response()->json([
            "message" => "Records Deleted Successfully!"
        ], 201);
    }
}