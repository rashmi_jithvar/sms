<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Roles;
use App\Models\Common;
use App\Models\Master;
use App\Models\Property;
use App\Models\PropertyImage;
use App\Models\PropertyAdditionalFeatures;
use App\Models\PropertyFurniture;
use App\Models\PropertyParking;

class PropertyController extends Controller
{

    public function index(){
        $properties = Property::with('customerDetail','PropertyType','SubPropertyType')->get();       
        return view('property.index',compact('properties'));
    }


    public function create(){
        $property_type = Common::Masterddl('property-type');
        $property_sub_type = Common::Masterddl('subproperty-type');
        $property_sub_sub_type = Common::Masterddl('resi-commer-type');
        $bathroom_type = Common::Masterddl('bathroom-type');
        $bedroom_type = Common::Masterddl('room-type');
        $facing_type = Common::Masterddl('facing-type');    
        $flooring_type = Common::Masterddl('flooring-type');
        $kitchen_type = Common::Masterddl('kitchen-type');
        $bathroom = Common::Masterddl('bathroom');   
        $floor_type = Common::Masterddl('floor-type');
        $furnished_type = Common::Masterddl('furnished-type');
        $unit = Common::Masterddl('unit');
        $state = Common::Masterddl('state');    
        $additional_feauture = Common::MasterAbbrData('additional-feature-type'); 
        $other_room = Common::MasterAbbrData('other-rooms-type');  
        $additional_sub_feature = Master::with('children')->where('abbr','additional-feature-type')->get();
       
        return view('property.step-form',compact('property_type','property_sub_type','property_sub_sub_type','bathroom_type','bedroom_type','facing_type','flooring_type','kitchen_type','bathroom','floor_type','furnished_type','unit','state','additional_feauture','additional_sub_feature','other_room'));
    }

    public function storeBasicProperty(Request $request){
      
        $property_id = null;

        if($request->has('property_id') !=0){
            $property_id = $request->property_id;  
        }
        if($request->has('images')){

            return $image = PropertyImage::updateCreate($request);

        }

        if($request->has('additional_features')){
            $features = PropertyAdditionalFeatures::updateCreate($request);
        }

        if($request->has('furniture_type')){

           $image = PropertyFurniture::updateCreate($request);
        } 

        if($request->has('close_parking')){
            $parking = $request->close_parking;
            $type ="close_parking";
            $image = PropertyParking::updateCreate($parking,$type,$request);
        } 

        if($request->has('open_parking')){
            $parking = $request->open_parking;
            $type ="open_parking";            
            $image = PropertyParking::updateCreate($parking,$type,$request);
        } 
        
        $property = Property::updatecreate($request,$property_id);
          if($property){
            return $property;
          }


    } 

    public function getFurniture(Request $request){
        //$parent_id = null;
        $furniture = Master::where('abbr',$request->abbr)
                            ->where('parant_id',$request->parent_id)
                            ->get(); 
        $html = "";                    
        foreach ($furniture as $key => $value) {
          $html .= "<div class='col-md-2'>
                        <label>".$value->name."
                            <input type='number' name='fans' id='furniture_type' style='width: 100px;' max='100' min='1' class='form-control' data-id='".$value->id."'/>
                                </label>
                    </div>";

        }
        //$html = "<div class='col-md-2'></div>";
        return $html;
    } 
  
    public function getMaster(Request $request){

    $parent_id = null;
       $abbr =  $request->abbr;
        if($request->has('parent_id')){
             $parent_id = $request->parent_id;
           }
            $master = Common::Masterddl($abbr,$parent_id);
            return $master;
    } 

    public function getOtherRoom(Request $request){
        $other_rooms = Master::where('abbr',$request->abbr)
                            ->where('parant_id',$request->parent_id)
                            ->get(); 
        $html = "";                    
        foreach ($other_rooms as $key => $value) {
          $html .= "<div class='col-md-3'>
                        <label>
                            <input type='checkbox' id='cn-p-1' name='other_room' id='other_room'>&nbsp;&nbsp;&nbsp;&nbsp;".$value->name."</label>
                    </div>";

        }
        //$html = "<div class='col-md-2'></div>";
        return $html;
    }  
    public function getTypeLabel(Request $request){

            if($request->has('type')){
                 $master_type = Master::where('id',$request->type)->first();
               }
                if(!empty($master_type)){
                   $data = $master_type->name;
                }
                return $data;
    }

    public function getMasterAbbr(Request $request){

            if($request->has('property_type')){

                $masterAbbr = Master::where('parant_id',$request->property_type)
                                        ->where('abbr',$request->abbr)
                                        ->first(); 
                                
                    if(!empty($masterAbbr)){
                       $data = 1;
                    }
                    else{
                        $data = 0;
                    }
                    return $data;                                        
               }

    }

}    


