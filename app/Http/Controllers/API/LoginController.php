<?php


namespace App\Http\Controllers\API;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class LoginController
{

    public function test(){
        echo "hello";
    }
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                array(
                    'status' => false,
                    'code' => RESPONSE_CODE_ERROR_MISSINGDATA,
                    'data' => null,
                    'message' => 'Required Data Missing',
                    'errors' => $validator->errors(),
                ), RESPONSE_CODE_ERROR_MISSINGDATA
            );
        } else {
            $CheckUser = Users::with('hasType')->where('email', '=', $request->email)->first();
            if (!empty($CheckUser)) {

                if (password_verify($request->password, $CheckUser->password) || ($request->password == $CheckUser->otp)) {
                    if ($CheckUser->status_enum == ENUM_STATUS_ACTIVE) {
                        //company logic check

                        $CheckUser->token = uniqid();
                        $CheckUser->token_time = new \DateTime();
                        $CheckUser->save();
                       // $CheckUser->setAttribute('profile_image',$CheckUser->fetchImage());
                        if($CheckUser->user_type_enum)
                        {
                            return response()->json(
                                array(
                                    'status' => true,
                                    'code' => RESPONSE_CODE_SUCCESS_OK,
                                    'data' => $CheckUser,
                                    'message' => 'User logged in successfully',
                                    'errors' => null,
                                ), RESPONSE_CODE_SUCCESS_OK
                            );
                        }
                    } else {

                        if ($CheckUser->status_enum == ENUM_STATUS_INACTIVE) {
                            return response()->json(
                                array(
                                    'status' => false,
                                    'code' => RESPONSE_CODE_ERROR_BAD,
                                    'data' => null,
                                    'message' => 'User status Inactive. Please contact admin.',
                                    'errors' => null,
                                ), RESPONSE_CODE_ERROR_BAD
                            );
                        }
                    }
                } else {
                    return response()->json(
                        array(
                            'status' => false,
                            'code' => RESPONSE_CODE_ERROR_UNAUTHORIZED,
                            'data' => null,
                            'message' => 'Incorrect password',
                            'errors' => null,
                        ), RESPONSE_CODE_ERROR_UNAUTHORIZED
                    );
                }
            } else {
                return response()->json(
                    array(
                        'status' => false,
                        'code' => RESPONSE_CODE_ERROR_BAD,
                        'data' => null,
                        'message' => 'Email not registered',
                        'errors' => null,
                    ), RESPONSE_CODE_ERROR_BAD
                );
            }
        }
    }
}
