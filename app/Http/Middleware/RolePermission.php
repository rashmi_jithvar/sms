<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class RolePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
         if(auth()->user()->role_id !=User::Admin){
             $currentAction = \Route::currentRouteAction();
             list($controller, $method) = explode('@', $currentAction);
             $controller = preg_replace('/.*\\\/', '', $controller);   
             $currentRoute = $controller.'@'.$method ;

             $routeArray = User::permission();
             $routeArray = (!empty($routeArray))? $routeArray : [];
             if(in_array($currentRoute,$routeArray )){
              return $next($request);  
             }
          }else{
            return $next($request);
          }
         return redirect()->back();
    }
  }
