<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddPages extends Model
{
    public $table = 'add_pages';
    public $fillable = ['pageName','pageTitle','pageDescription','metaTitle','metaDescription','metaKeyword'];
}
