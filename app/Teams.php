<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    protected $table = 'teams';

    protected $fillable = ['name', 'position', 'facebook', 'linkedin', 'twitter', 'gplus'];
}
