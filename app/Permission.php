<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

	protected $table = 'tbl_permission';
	  
    public function updatePermission($data)
	{
        $roles = $this->find($data['id']);
        $roles->group_id = $data['group_id'];
        $roles->name = $data['name'];
        $roles->controller_name = $data['controller_name'];
        $roles->action_name = $data['action_name'];
        //$permission->created_by ='1';
        if($data['status']!=''){
            $permission->status = $data['status'];
        }
        

        $roles->save();
        return 1;
	}
}
