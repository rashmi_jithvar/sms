<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    public $table = 'sliders';
    public $fillable = ['image', 'content'];
}
