<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
 
class Pageinfo {
    
    public static function get_pagename($page_id) {
        $user = DB::table('pages')->where('id', $page_id)->first();
         
        return (isset($user->pageName) ? $user->pageName : '');
    }
}