<?php
use App\User; 

if (!function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder)
    {
        $directory = $folder;
        $handle = opendir($directory);
        $directory_list = [$directory];

        while (false !== ($filename = readdir($handle))) {
            if ($filename != '.' && $filename != '..' && is_dir($directory.$filename)) {
                array_push($directory_list, $directory.$filename.'/');
            }
        }

        foreach ($directory_list as $directory) {
            foreach (glob($directory.'*.php') as $filename) {
                require $filename;
            }
        }
    }
}

//
if(!function_exists('permissionGate')){
  function permissionGate($action){
     
     $currentAction = \Route::currentRouteAction();
     if(empty($currentAction)){
      return false;
     }
     
     list($controller, $method) = explode('@', $currentAction);
     $controller = preg_replace('/.*\\\/', '', $controller);   
     $currentRoute = $controller.'@'.$action ;

     $routeArray = User::permission();
     $routeArray = (!empty($routeArray))? $routeArray : [];
     if(in_array($currentRoute,$routeArray )){
      return true;  
     }else{
      return false;  
     }
  }
}