<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use App\Models\FeeTypeTemp;
use App\Models\FeesType;
use DB;

class LabelType extends Model
{
    protected $table = 'tbl_label_type';
    public $timestamps = false;

}
