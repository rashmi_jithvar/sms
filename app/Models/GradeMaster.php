<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use DB;

class GradeMaster extends Model
{
    protected $table = 'tbl_grade_master';
    //public $timestamps = false;

    protected function updatecreate($request,$id=null){
 
        if($id){
          $master = Self::find($id);
        }else{
          $master = new GradeMaster();
        }

       DB::beginTransaction();
       try{ 
          
        $master->min_percent = $request->min_percent;
        $master->max_percent = $request->max_percent;
        $master->grade = $request->grade;
        $master->remark = $request->remark;
        $master->created_by = auth()->user()->id;
        if($request->status =='on'){
            $master->status = '1'; 
        }
        else{
            $master->status = '0';
        }  
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Property type :'.$e->getMessage());
        return false;	
     } 
    }  

    public function gradeName(){
     return $this->hasOne(Master::class,'id','grade');	
    }  

   protected function checkGrade($percent){
      $get_grade = GradeMaster::where('min_percent','<=',$percent)
                                ->where('max_percent','>',$percent)
                                ->first();
        if(!empty($get_grade)){
          return $get_grade;
        }
                      
   }   
}
