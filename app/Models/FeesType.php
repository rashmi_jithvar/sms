<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FeesType;
use DB;

class FeesType extends Model
{
    protected $table = 'tbl_fee_type';
    public $timestamps = false;


    public function hasFeeName(){
     return $this->hasOne(Master::class,'id','fees_type'); 
    } 

    public function getFees(){
     return $this->hasMany(Master::class,'id','fees_type');   
    }    
}
