<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\SubjectMaster;

class Master extends Model
{
	use SoftDeletes;

    protected $table = 'master';

    protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];

    protected function updatecreate($request,$id=null){
     
        if($id){
          $master = Self::find($id);
        }else{
          $master = new Master();
        }

       DB::beginTransaction();
       try{ 
        
        if($request->parant_id){	
         $master->parant_id = $request->parant_id;
        } 
        
        $master->name = $request->name;
        $master->abbr = $request->abbr;
        $master->created_by = auth()->user()->id;
        $master->status = $request->status;
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Property type :'.$e->getMessage());
        return false;	
     } 
    }

    public function parent(){
     return $this->hasMany(self::class,'id','parant_id');	
    }

    public function children(){
     return $this->hasMany(self::class,'parant_id','id'); 
    } 
     protected function GetCurrent($currency_id){
     return Master::where('id',$currency_id)->first();
    }  

    public function getLabel(){
      return $this->hasMany(LabelType::class,'id','parant_id'); 
    }   

    public function SubjectMaster(){
      return $this->hasMany(SubjectMaster::class,'class_id','id'); 
    }      


}
