<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use App\Models\FeeTypeTemp;
use App\Models\FeesType;
use App\Models\StudentFees;
use App\Models\Guardian;
use DB;
use Session;
use Auth;

class Student extends Model
{
    protected $table = 'tbl_student';
    public $timestamps = false;

    protected function updatecreate($request,$id=null){
     
        if($id){
          $master = Self::find($id);
        }else{
       	  $enquiry = StudentEnquiry::where('id',$request->student_id)->first();		 	
          $master = new Student();
        }
       DB::beginTransaction();
       try{ 
        $master->first_name = $enquiry->first_name;
        $master->last_name = $enquiry->last_name;
        $master->dob = $enquiry->dob;            
        $master->gender = $enquiry->gender;
        $master->category = $enquiry->category;
        $master->class = $enquiry->admission_seeking;
        $master->section = $request->section;        
        $master->city = $enquiry->city;
        $master->state = $enquiry->state;
        $master->address = $enquiry->address;
        $master->pincode = $enquiry->pincode;
        $master->created_by = auth()->user()->id;
        $master->created_at = date('Y-m-d');
        DB::commit();
        $master->save();
           
        if($master->save()){
            $enquiry->is_copied = '1';
            $enquiry->save();

            $no = strlen((string)$master->id);
            if ($no == '1') {
                $enrollment_no = '000' .$master->id;
            } else if ($no == '2') {
                $enrollment_no = '00' . $master->id;
            } else if ($no == '3') {
                $enrollment_no = '0' . $master->id;
            } else {
                $enrollment_no = $master->id;
            }
            /* roll No. */  
            $master->enrollment_no =   $enrollment_no;
            $master->save();  

            $check_class = Student::select('id','roll_no')
                                    ->where('class',$master->class)
                                    ->where('id', '!=' , $master->id)
                                    ->orderBy('id','DESC')
                                    ->first(); 
                              
                if(!empty($check_class)){
                    $roll_id =  $check_class->roll_no + '1';
                }
                else{
                    $roll_id= '1';

                }

                $roll = strlen((string)$roll_id);
                if ($no == '1') {
                    $roll_no = '000' .$roll_id;
                } else if ($no == '2') {
                    $roll_no = '00' . $roll_id;
                } else if ($no == '3') {
                    $roll_no = '0' . $roll_id;
                } else {
                    $roll_no = '1' . $roll_id;
                }  

                $master->roll_no =   $roll_no;
                $master->save();

        	$GuardianEnquiry =GuardianEnquiry::where('student_id',$request->student_id)
        										->get();
            if (count($GuardianEnquiry) > 0) {
                foreach ($GuardianEnquiry as $k => $parent) {
                    $guardian = new Guardian();
                    $guardian->name = $parent->name;
                    $guardian->relation = $parent->relation;
                    $guardian->mobile = $parent->mobile;
                    $guardian->email = $parent->email;
                    $guardian->occupation = $parent->occupation;
                    $guardian->student_id = $master->id;
                    $guardian->save();
                } 
            }

            if (Session::get('fee_temp_id') == null) {
                $fees_temp = StudentFeesTemp::where(['fee_temp_id' => $id])->get();
                $check_fee_type = StudentFees::where(['fees_id' => $id])->get();

                    if(count($check_fee_type) > 0){
                       
                            foreach ($check_fee_type as $key => $value) {
                                $value->delete();
                            }
                        } 
                        
                               
            } else {
                    $user_id = Auth::user()->id;
                    $setting = Setting::where('user_id',$user_id)->first();
                    $setting->session_start_month =4;
                    $month = Master::where('abbr','month')->get(); 
                     
                    for ($x = 1; $x <= 12; $x++) {
                            $setting->session_start_month = $setting->session_start_month <= 12 ? $setting->session_start_month : 1;
                            "<br />";

                        $student_monthly_fees = New StudentMonthlyFees();  
                        $student_monthly_fees->student_id = $master->id;
                        $student_monthly_fees->month_id =  $setting->session_start_month; 
                        $student_monthly_fees->created_by =auth()->user()->id;
                        $student_monthly_fees->save();                            
                        if($student_monthly_fees->save()){
                                $invoice_no =0;
                                $noofdigit = strlen((string)$student_monthly_fees->id);
                                if ($noofdigit == '1') {
                                    $invoice_no = '0000000' . $student_monthly_fees->id;
                                } 
                                else if ($noofdigit == '2') {
                                    $invoice_no = '000000' . $student_monthly_fees->id;
                                } 
                                else if ($noofdigit == '3') {
                                    $invoice_no = '00000' . $student_monthly_fees->id;
                                }
                                else if ($noofdigit == '4') {
                                    $invoice_no = '0000' . $student_monthly_fees->id;
                                } 
                                else if ($noofdigit == '5') {
                                    $invoice_no = '000' . $student_monthly_fees->id;
                                } 
                                else if ($noofdigit == '6') {
                                    $invoice_no = '00' . $student_monthly_fees->id;
                                }  
                                else if ($noofdigit == '7') {
                                    $invoice_no = '0' . $student_monthly_fees->id;
                                }                                        
                                else {
                                    $invoice_no = $student_monthly_fees->id;
                                } 
                                if($invoice_no){
                                   $student_monthly_fees->invoice_no =  "BSASV".$invoice_no;
                                   $student_monthly_fees->save(); 
                                }
                                
                        }
                        $fees_temp = StudentFeesTemp::where(['fee_temp_id' => Session::get('fee_temp_id') ])->get();
                        if (count($fees_temp) > 0) {
                        $total = 0;
                        $discount = 0;
                            foreach ($fees_temp as $k => $item) {
                                $fees = new StudentFees();
                                $fees->fees_type_id = $item->fees_type_id;
                                $fees->frequency_id =  $item->frequency_id;
                                $fees->fee = $item->fee;
                                $fees->discount = $item->discount;
                                $fees->total = $item->total;
                                $fees->fee_id = $student_monthly_fees->id;

                                if($x == 4 || $x == 10){
                                        if($fees->frequency_id == '3' || $fees->frequency_id =='1'){
                                            $total+= $item->total;
                                            $discount+= $item->discount; 
                                            $fees->save();

                                        }
                                }
                                else if($x == 7){
                                    if($fees->frequency_id == '2' || $fees->frequency_id == '3' || $fees->frequency_id =='1'){
                                        $total+= $item->total;
                                        $discount+= $item->discount;
                                        $fees->save();
                                    }
                                }
                                else if($x == 1){
                                    $total+= $item->total;
                                    $discount+= $item->discount;
                                    $fees->save();
                                }
                                else{
                                    if($fees->frequency_id == '1'){
                                            $total+= $item->total;
                                            $discount+= $item->discount;
                                            $fees->save();
                                    }
                                }
                                $student_monthly_fees->total_fees = $total;
                                $student_monthly_fees->due_amount = $total;                                
                                $student_monthly_fees->discount = $discount;
                                $student_monthly_fees->save();                                        
                            }                                               
                         $setting->session_start_month++;
                    }
                }
            }                 
                if (Session::get('fee_temp_id') == null) {
                    DB::table('tbl_student_fees_temp')->where('fee_temp_id', $id)->delete();
                }
                else{
                    DB::table('tbl_student_fees_temp')->where('fee_temp_id', Session::get('fee_temp_id'))->delete();
                }
                Session::forget('fee_temp_id');
             }           
        return $master->id;
       }
       catch(\Exception $e){
        DB::rollback();
        \Log::debug('Student :'.$e->getMessage());
        return false;	
     } 
    }

    public function hasClass(){
     return $this->hasOne(Master::class,'id','class');  
    }

    public function hasCategory(){
     return $this->hasOne(Master::class,'id','category'); 
    }
    public function hasSection(){
     return $this->hasOne(Master::class,'id','section'); 
    }
     public function parent(){
     return $this->belongsTo(GuardianEnquiry::class,'student_id','id');	
    }
     public function student_parent(){
     return $this->hasMany(Guardian::class,'student_id','id'); 
    }                    
}
