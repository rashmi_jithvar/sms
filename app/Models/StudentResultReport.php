<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\Student;
use App\Models\StudentObtainedMarksTemp;
use App\Models\StudentObtainedMarks;
use App\Models\GradeMaster;
use Session;
class StudentResultReport extends Model
{

    protected $table = 'tbl_student_result_report';
    //public $timestamps = false;
   // protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];
    protected function updatecreate($request,$session,$id=null){

         if($id){
         
          $master = Self::find($id);
        }else{
        	
          $master = new StudentResultReport();
        }

       DB::beginTransaction();
       try{ 
        $master->student_id = $request->student;
        $master->exam_type = $request->exam_type;
        $master->class_id = $request->class;
        $master->section_id = $request->section; 
        $master->total_obtained_marks = $request->total;   
        $master->total_max_marks = $request->all_subject_total_marks;
        $master->session = $session;  
        $master->percent = ($master->total_obtained_marks / $master->total_max_marks) * 100;
        $master->created_by = auth()->user()->id;
        DB::commit();
        $master->save();

        if($master->save()){
            /* get Grade Value */
            //$percent = number_format($master->percent,2);
            $get_grade = GradeMaster::checkGrade($master->percent);

            if(!empty($get_grade)){
                $master->grade = $get_grade->grade;
                $master->remark = $get_grade->remark;
                $master->save();
            }
            if (Session::get('result_key') == null) {
                $marks_temp = StudentObtainedMarksTemp::where(['key_id' => $id])->get();
                $check_marks = StudentObtainedMarks::where(['student_result_id' => $id])->get();
                    if(count($check_marks) > 0){
                            foreach ($check_marks as $key => $value) {
                                $value->delete();
                            }
                        }        
            } else {
                $marks_temp = StudentObtainedMarksTemp::where(['key_id' => Session::get('result_key') ])->get();

            }
            if (count($marks_temp) > 0) {
                foreach ($marks_temp as $k => $item) {
                    $marks = new StudentObtainedMarks();
                    $marks->subject_id = $item->subject_id;
                    $marks->internal_marks = $item->internal_marks;
                    $marks->external_marks = $item->external_marks;
                    $marks->obtained_marks = $item->obtained_marks ;
                    $marks->total_max_marks = $item->total_max_marks ;  
                    $marks->total_obtained_marks = $item->total_obtained_marks;             
                    $marks->student_result_id = $master->id;                    
                    $marks->save();
                }

                if (Session::get('result_key') == null) {
                    DB::table('tbl_student_obtained_marks_temp')->where('key_id', $id)->delete();
                }
                else{
                    DB::table('tbl_student_obtained_marks_temp')->where('key_id', Session::get('result_key'))->delete();
                }
                Session::forget('result_key');
             }           
            }
        //return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Student Marks :'.$e->getMessage());
        return false; 
     } 
    }

    public function studentInfo(){
     return $this->hasOne(Student::class,'id','student_id'); 
    } 

    public function hasExamType(){
     return $this->hasOne(Master::class,'id','exam_type'); 
    } 

    public function studentDetail(){
     return $this->hasMany(Student::class,'id','student_id'); 
    }  
    public function hasSubjectMarks(){
     return $this->hasMany(StudentObtainedMarks::class,'student_result_id','id'); 
    }

    public function getGrade(){
     return $this->hasOne(Master::class,'id','grade'); 
    } 
}
