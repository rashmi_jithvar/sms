<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use App\Models\LabelType;
use DB;
use Auth;

class StudentFeesTemp extends Model
{


    protected $table = 'tbl_student_fees_temp';
    public $timestamps = false;

    protected $fillable = ['fees_type_id','fees','fee_temp_id','discount','total'];

    protected function updatecreate($request,$fee_temp_id){
     
       DB::beginTransaction();
       try{ 
        $master = new StudentFeesTemp();
        $master->fees_type_id = $request->fees_type;
        $master->frequency_id = $request->frequency_id;
        $master->fee = $request->fee;
        $master->discount = $request->discount;
        $master->total = $request->total;
        $master->fee_temp_id = $fee_temp_id;
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Student Fees Temp :'.$e->getMessage());
        return '0';	
     } 
    }


    protected function loadFees($fee_temp_id){
        $currency_id = Auth::user()->currency_id;
        $currency_name = Master::GetCurrent($currency_id);       
        if($currency_name ==''){
           $currency_name ='';
        }
     $item = StudentFeesTemp::with('getFees','getLabel')
                         ->where('fee_temp_id',$fee_temp_id)
                         ->get();    
        $table = "";
        if ($item) {
            $table.= "
        <thead>
              <tr>
                  <th><strong>S.no.</strong></th>
                  <th><strong>Fees Type</strong></th>
                  <th><strong>Frequency</strong></th>
                  <th><strong>Fee &nbsp;(".$currency_name->name.")</strong></th>
                  <th><strong>Discount &nbsp;(".$currency_name->name.")</strong></th>
                  <th><strong>Total &nbsp;(".$currency_name->name.")</strong></th>
                  <th></th>
              </tr>
        </thead>
        <tbody id='cart-body'>";
            $i = 0;
            $total = 0;
            foreach ($item as $row) {

              $fees_type = Master::select('name')->where(['id'=>$row->fees_type])->first();  
                $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$row->getFees->name."</td>
                  <td>" .$row->getLabel->label."</td>
                   <td>" . number_format($row->fee,2) . "</td>
                   <td>" . number_format($row->discount,2) . "</td>
                   <td>" . number_format($row->total,2) . "</td>
                     <td><a href='javascript:void(0)' onclick='removeCart(\"" . Url('delete-fees-temp/' . $row->id) . "\");'><i class='fa fa-trash' aria-hidden='true'></i></a></td>
                    </tr>";
                        $total+= $row->total; 
                }
            $table.= "</tbody>
                   <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>

                            <th class='text-right'>Total:</th>
                            <th class='text-left' id='total' ><input type='hidden' name='total' value=" . $total . ">".$currency_name->name."&nbsp;&nbsp;&nbsp;&nbsp;" . number_format($total,2) . " </th>
                        </tr>
                    </tfoot>";
        } 
        else {
            $table = "
                    <thead>
                        <tr>
                        <th><strong>S.no.</strong></th>
                        <th><strong>Fees Type</strong></th>
                        <th><strong>Frequency</strong></th>
                        <th><strong>Fee &nbsp;(".$currency_name->name.")</strong></th>
                        <th><strong>Discount &nbsp;(".$currency_name->name.")</strong></th>
                        <th><strong>Total &nbsp;(".$currency_name->name.")</strong></th>
                        <th></th>
                        </tr>
                    </thead>
                     <tbody id='cart-body'>
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class='text-right' id='total'></th>
                            <th></th>
                        </tr>
                    </tfoot>
                   ";
        }
        return $table;  
    }
  public function getFees(){
     return $this->hasOne(Master::class,'id','fees_type_id');   
  }
  public function getLabel(){
     return $this->hasOne(LabelType::class,'id','frequency_id');   
  }

}
