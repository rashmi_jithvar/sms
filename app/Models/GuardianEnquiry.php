<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use DB;

class GuardianEnquiry extends Model
{
    protected $table = 'tbl_guardian_enquiry';
    public $timestamps = false;

}
