<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\Master;

class SubjectMaster extends Model
{
	//use SoftDeletes;

    protected $table = 'tbl_subject_master';
    protected $fillable = ['class_id','subject'];
    public $timestamps = false;

    protected function updatecreate($request,$id=null){
    
       DB::beginTransaction();
       try{ 
        if($id){
           $check_subject = SubjectMaster::where('class_id',$id)->get();
           if(!empty($check_subject)){
             foreach ($check_subject as $key => $value) {
                    $value->delete();
              }            
           }
          if($request->has('subject_id')){
              foreach ($request->subject_id as $key => $value) {
                              $master = new SubjectMaster();
                              $master->class_id = $request->class_id;
                              $master->subject_id = $value;
                              if($request->status =='on'){
                                  $master->status = '1'; 
                              }
                              else{
                                  $master->status = '0';
                              }  
                      $master->save();                         
              }
          }              
        }
        else{
          if($request->has('subject_id')){
              foreach ($request->subject_id as $key => $value) {
                              $master = new SubjectMaster();
                              $master->class_id = $request->class_id;
                              $master->subject_id = $value;
                              if($request->status =='on'){
                                  $master->status = '1'; 
                              }
                              else{
                                  $master->status = '0';
                              }  
                      $master->save();                         
              }
          }          
        }

        DB::commit(); 
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Property type :'.$e->getMessage());
        return false;	
     } 
    }

    public function getSubject(){
      return $this->hasOne(Master::class,'id','subject_id'); 
    } 


    public function SubjectMaster(){
      return $this->hasMany(Master::class,'class_id','id'); 
    }  

    public function getSubjectName(){
      return $this->hasMany(Master::class,'id','subject_id'); 
    }  


}
