<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\Student;

class StudentMonthlyFees extends Model
{

    protected $table = 'tbl_student_monthly_fee';
    public $timestamps = false;
   // protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];
 
    public function studentInfo(){
     return $this->hasOne(Student::class,'id','student_id'); 
    } 

    public function studentDetail(){
     return $this->hasMany(Student::class,'id','student_id'); 
    }  


}
