<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;

class SubjectHasMarks extends Model
{
	//use SoftDeletes;

    protected $table = 'tbl_subject_has_marks';
    protected $fillable = ['subject_master_id'];
    public $timestamps = false;

    public function getSubject(){
      return $this->hasOne(Master::class,'id','subject_master_id'); 
    }     
}
