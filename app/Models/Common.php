<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Master;
use App\Models\LabelType;
use App\Models\MarksMaster;

class Common extends Model
{
  const STATUS_ACTIVE = '1';
  const STATUS_DEACTIVE = '0'; 

  protected function Masterddl($abbr=null,$parant_id=null,$select_id=null){


    if($parant_id!=null){

        $data = Master::where('abbr',$abbr)
           ->where('status',self::STATUS_ACTIVE)
           ->where('parant_id',$parant_id)
           ->get();  

      }
      else{
          $data = Master::where('abbr',$abbr)
                 ->where('status',self::STATUS_ACTIVE)
                 //->orwhere('parant_id',$parant_id)
                 ->get();        
      }

    $html = '<option value="0" >Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->name."</option>";
       }
     } 
    
     return $html;
  }

  protected function Studentddl($select_id=null){

    
      $data = Student::select(
             DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
                ->where('status',self::STATUS_ACTIVE)
                ->get();

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->name."</option>";
       }
     } 
    
     return $html;
  } 
/* Subject Master */
  protected function SubjectMaster($class_id,$select_id=null){

    
      $data = SubjectMaster::with('getSubjectName')
                            ->where('class_id',$class_id)
                            ->get();  

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {   
            foreach ($row->getSubjectName as $key => $value) {
              $selected = $select_id==$value->id ? 'selected' :'' ;
              $html .= "<option value='".$value->id."' ".$selected." data-name=".$value->name.">".$value->name."</option>";
          }

       }
     } 
    
     return $html;
  }

/* Subject Marks Master */
  protected function SubjectMarksMaster($class_id,$exam_type,$select_id=null){

    
      $data = MarksMaster::with('subjectHasMarks.getSubject')
                            ->where('exam_type',$exam_type)
                            ->where('class_id',$class_id)
                            ->get();  
    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {   
            foreach ($row->subjectHasMarks as $key => $value) {
              $selected = $select_id==$value->id ? 'selected' :'' ;
              $html .= "<option value='".$value->getSubject->id."' ".$selected." data-name=".$value->getSubject->name.">".$value->getSubject->name."</option>";
          }

       }
     } 
    
     return $html;
  }     
/* month ddl  */
  protected function Monthddl($select_id=null){

    $data = array('1' => 'January','2'=> 'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');
    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$key ? 'selected' :'' ;
         
         $html .= "<option value='".$key."' ".$selected." data-name=".$row.">".$row."</option>";
       }
     } 
    
     return $html;
  }     
  protected function FeesTypeddl($class,$category,$select_id=null){

        $data = DB::table('tbl_fees_master')
                            ->select('master.*')
                            ->join('tbl_fee_type','tbl_fee_type.fees_master_id','=','tbl_fees_master.id')
                            ->join('master','tbl_fee_type.fees_type','=','master.id')
                            ->where('tbl_fees_master.class',$class)
                            ->where('tbl_fees_master.category',$category)
                            ->get(); 

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->name."</option>";
       }
     } 
    
     return $html;
  }
  protected function Labelddl($abbr=null,$select_id=null){


    $data = LabelType::where('abbr',$abbr)
                      ->where('status',self::STATUS_ACTIVE)
                      ->get();  

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->label."</option>";
       }
     } 
    
     return $html;
  }  
  protected function MasterAbbrData($abbr=null,$parant_id=null){
    if($parant_id!=null){
        $data = Master::where('abbr',$abbr)
           ->where('status',self::STATUS_ACTIVE)
           ->where('parant_id',$parant_id)
           ->get();  

    } 
    else{
      $data = Master::where('abbr',$abbr)
             ->where('status',self::STATUS_ACTIVE)
             ->get();      
    }   

     return $data;
  }

/* student Enquiry ddl */
  protected function StudentEnquiryddl($select_id=null){

    
      $data = StudentEnquiry::select(
             DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
                ->where('status',self::STATUS_ACTIVE)
                //->where('deleted_at','==','0')
                ->get();

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->name."</option>";
       }
     } 
    
     return $html;
  }

  protected function StudentddlList($class,$section,$select_id=null){

    
      $data = Student::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
                      ->where('class',$class)
                      ->where('section',$section)
                      ->where('status',self::STATUS_ACTIVE)
                      ->get();

    $html = '<option value="0">Select</option>';
     if(!empty($data)){
       foreach ($data as $key => $row) {
         
         $selected = $select_id==$row->id ? 'selected' :'' ;
         
         $html .= "<option value='".$row->id."' ".$selected." data-name=".$row->name.">".$row->name."</option>";
       }
     } 
    
     return $html;
  } 


  public function getAbbr($abbr){
      $property_type = null ;
      $title = '';
      $subtitle = '';
      /* Class */
      if($abbr =='class'){
         $title = 'Class';
      } 

      /* Subject */       
      if($abbr =='subject'){
         $title = 'Subject';
         //$subtitle = 'Property Type';
      }

      /* Session */
      if($abbr =='session'){
         $title = 'Session';
        // $subtitle = 'Sub Property Type';
      } 

      /* Section */     
      if($abbr =='section'){
         $title = 'Section';
         //$title = 'Room Type';
      } 

      /*  */
      if($abbr =='grade'){
         $title = 'Grade';
      }


      /*  */
      if($abbr =='exam-type'){
         $title = 'Exam Type';
      }


      /* Kitchen Type */
      if($abbr =='fees'){
        $property_type = Common::Labelddl('frequency');
        $title = 'Fees';
        $subtitle = 'Frequency';
      }

      if($abbr =='category'){
         $title = 'Category';
      } 
      if($abbr =='discount'){
         $title = 'Discount Type';
      } 

      /* Furnished Type */     
      if($abbr =='fees-type'){
         $property_type = Common::Masterddl('fees');
         $title = 'Fee Type';
         $subtitle = 'Fees';
      }  
                  
      return $arrayName = array('title' =>$title ,'subtitle' =>$subtitle,'property_type'=>$property_type);
  } 



}
