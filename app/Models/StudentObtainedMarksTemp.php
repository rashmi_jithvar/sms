<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\Student;

class StudentObtainedMarksTemp extends Model
{

    protected $table = 'tbl_student_obtained_marks_temp';
    public $timestamps = false;
   // protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];
 
    public function studentInfo(){
     return $this->hasOne(Student::class,'id','student_id'); 
    } 

    public function studentDetail(){
     return $this->hasMany(Student::class,'id','student_id'); 
    }  

    protected function updatecreate($request,$key_id){
     
       DB::beginTransaction();
       try{ 
        $master = new StudentObtainedMarksTemp();
        $master->subject_id = $request->subject;
       // $master->max_mark = $request->max_marks;
        $master->internal_marks = $request->internal_marks;
        if($request->has('external_marks')!=0){
          $master->external_marks = $request->external_marks;
        } 
        $master->obtained_marks = $request->obtained_marks;
        $master->total_max_marks = $request->total_max_marks;
        $master->total_obtained_marks = $request->total_obtained_marks;     
        $master->key_id = $key_id;
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Student Result Add Marks :'.$e->getMessage());
        return '0'; 
      } 
    }


/* load  Student Result Marks */ 
    public function getsubjectName(){
     return $this->hasOne(Master::class,'id','subject_id')->where('abbr','subject');   
    }

    protected function loadMarks($key_id){

     $item = StudentObtainedMarksTemp::with('getsubjectName')
                         ->where('key_id',$key_id)
                         ->get(); 
                      
        $table = "";
        if ($item) {
            $table.= "
        <thead>
              <tr>
                  <th><strong>S.no.</strong></th>
                  <th><strong>Subject Name</strong></th>
                  <th><strong>Internal Marks</strong></th>
                  <th><strong>External Marks</strong></th>
                  <th><strong>Total Max Marks</strong></th>
                  <th><strong>Obtained Marks</strong></th>
                  
                  <th></th>
                  <th></th>
                  <th></th>
              </tr>
        </thead>
        <tbody id='cart-body'>";
            $i = 0;
            $total = 0;
            $total_max_mark = 0;
            foreach ($item as $row) {
            	
              $subject = Master::select('name')->where(['id'=>$row->subject_id])->first();
             
                //$total_marks = $row->max_mark + $row->internal_marks + $row->external_marks
                $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$subject->name."</td>
                  <td>" .$row->internal_marks."</td>
                  <td>" .$row->external_marks."</td>
                  <td>" . number_format($row->total_max_marks,2) . "</td>
                  <td>" .$row->obtained_marks."</td>
                  
                  <td><a href='javascript:void(0)' onclick='removeMarks(\"" . Url('delete-student-marks-temp/' . $row->id) . "\");'><i class='fa fa-trash' aria-hidden='true'></i></a></td>
                  <td></td>
                  <td></td>
                    </tr>";
                        $total+= $row->total_obtained_marks;
                        $total_max_mark+= $row->total_max_marks; 
                }
            $table.= "</tbody>
                   <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                            <th></th>
                            <th></th>
                            <th>Total:</th>
                            <th class='text-right' id='total' ><input type='hidden' name='total' value=" . $total . "><input type='hidden' name='total_max_mark' value=" . $total_max_mark . ">" . number_format($total,2) . " </th>
                        </tr>
                    </tfoot>";
        } 
        else {
            $table = "
                    <thead>
		              <tr>
		                  <th><strong>S.no.</strong></th>
		                  <th><strong>Subject Name</strong></th>
		                  <th><strong>External Marks</strong></th>
		                  <th><strong>Internal Marks</strong></th>
		                  <th><strong>Total Max Marks</strong></th>
		                  <th><strong>Obtained Marks</strong></th>
		                  <th>Total</th>
		                  <th></th>
		              </tr>
                    </thead>
                     <tbody id='cart-body'>
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th> 
                            <th></th>
                            <th></th>
                            <th class='text-right' id='total'></th>
                            <th></th>
                        </tr>
                    </tfoot>
                   ";
        }
        echo $table;  
    }

}
