<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FeeTypeTemp;
use App\Models\FeesType;
use App\Models\Master;
use Session;
use DB;

class FeesMaster extends Model
{
	use SoftDeletes;
    protected $table = 'tbl_fees_master';
    public $timestamps = false;

    //protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];

    protected function updatecreate($request,$id=null){
     
        if($id){
          $master = Self::find($id);
        }else{
          $master = new FeesMaster();
        }

       DB::beginTransaction();
       try{ 
        
        $master->class = $request->class;
        $master->category = $request->category;
        $master->total_fees = $request->total;
        $master->created_by = auth()->user()->id;
        DB::commit();
        $master->save();

        if($master->save()){
            if (Session::get('key_id') == null) {

                $fees_temp = FeeTypeTemp::where(['key_id' => $id])->get();

                $check_fee_type = FeesType::where(['fees_master_id' => $id])->get();
                //print_r($check_fee_type);exit;

                    if(count($check_fee_type) > 0){
                       
                            foreach ($check_fee_type as $key => $value) {
                                $value->delete();
                            }
                        } 
                        
                               
            } else {
                $fees_temp = FeeTypeTemp::where(['key_id' => Session::get('key_id') ])->get();

            }
          
            if (count($fees_temp) > 0) {
                foreach ($fees_temp as $k => $item) {
                    $fees = new FeesType();
                    $fees->fees_type = $item->fees_type;
                    $fees->value = $item->value;
                    $fees->fees_master_id = $master->id;
                    $fees->save();
                }
                if (Session::get('key_id') == null) {
                    DB::table('tbl_fee_type_temp')->where('key_id', $id)->delete();
                }
                else{
                    DB::table('tbl_fee_type_temp')->where('key_id', Session::get('key_id'))->delete();
                }
                Session::forget('key_id');
             }           
            }
        //return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Property type :'.$e->getMessage());
        return false;	
     } 
    }

    public function hasFeeType(){
     return $this->hasMany(FeesType::class,'id','fees_master_id');	
    }

    public function hasClass(){
     return $this->hasOne(Master::class,'id','class');  
    }

    public function hasCategory(){
     return $this->hasOne(Master::class,'id','category'); 
    } 

    public function hasFeeName(){
     return $this->hasOne(Master::class,'id','fees_type'); 
    } 

}
