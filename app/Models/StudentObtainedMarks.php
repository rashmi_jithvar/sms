<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;

class StudentObtainedMarks extends Model
{
	//use SoftDeletes;

    protected $table = 'tbl_student_obtained_marks';
    protected $fillable = ['subject_id','internal_marks','obtained_marks','total_max_marks','total_obtained_marks','student_result_id'];
    public $timestamps = false;

    public function getSubject(){
      return $this->hasOne(Master::class,'id','subject_id'); 
    }     
}
