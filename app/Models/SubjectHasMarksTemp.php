<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;

class SubjectHasMarksTemp extends Model
{
	//use SoftDeletes;

    protected $table = 'tbl_subject_has_marks_temp';
    protected $fillable = ['subject_master_id'];
    public $timestamps = false;


    protected function updatecreate($request,$key_id){

       DB::beginTransaction();
       try{ 
        $master = new SubjectHasMarksTemp();
        $master->subject_master_id = $request->subject;
       // $master->max_mark = $request->max_marks;
        $master->internal_marks = $request->internal_marks;
        if($request->has('external_marks')!=0){
          $master->external_marks = $request->external_marks;
        } 
        if($request->has('is_external')!=0){
          $master->is_external = $request->is_external;
        } 
        
        $master->total_marks = $request->external_marks + $request->internal_marks;        
        $master->key_id = $key_id;
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Subject Has Marks type :'.$e->getMessage());
        return '0'; 
      } 
    }
/* load   Marks */ 
    public function getsubjectName(){
     return $this->hasOne(Master::class,'id','subject_id')->where('abbr','subject');   
    }

    protected function loadMarks($key_id){

     $item = SubjectHasMarksTemp::with('getsubjectName')
                         ->where('key_id',$key_id)
                         ->get();    
        $table = "";
        if ($item) {
            $table.= "
        <thead>
              <tr>
                  <th><strong>S.no.</strong></th>
                  <th><strong>Subject Name</strong></th>
                  <th><strong>External Marks</strong></th>
                  <th><strong>Internal Marks</strong></th>
                  
                  <th>Total</th>
                  <th></th>
              </tr>
        </thead>
        <tbody id='cart-body'>";
            $i = 0;
            $total = 0;
            foreach ($item as $row) {
              $subject = Master::select('name')->where(['id'=>$row->subject_master_id])->first();
                //$total_marks = $row->max_mark + $row->internal_marks + $row->external_marks
                $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$subject->name."</td>
                  <td>" .$row->external_marks."</td>
                  <td>" .$row->internal_marks."</td>
                 
                  <td>" . number_format($row->total_marks,2) . "</td>
                  <td><a href='javascript:void(0)' onclick='removeMarks(\"" . Url('delete-subject-has-marks-temp/' . $row->id) . "\");'><i class='fa fa-trash' aria-hidden='true'></i></a></td>
                    </tr>";
                        $total+= $row->total_marks; 
                }
            $table.= "</tbody>
                   <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                           
                            <th>Total:</th>
                            <th class='text-right' id='total' ><input type='hidden' name='total' value=" . $total . ">" . number_format($total,2) . " </th>
                        </tr>
                    </tfoot>";
        } 
        else {
            $table = "
                    <thead>
                        <tr>
                        <th><strong>S.no.</strong></th>
                        <th><strong>Subject Name</strong></th>
                        <th><strong>Max Marks</strong></th>
                        <th><strong>External Marks</strong></th>
                        <th><strong>Internal Marks</strong></th>
                        
                        <th></th>
                        <th></th>
                        </tr>
                    </thead>
                     <tbody id='cart-body'>
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class='text-right' id='total'></th>
                            <th></th>
                        </tr>
                    </tfoot>
                   ";
        }
        echo $table;  
    }

}
