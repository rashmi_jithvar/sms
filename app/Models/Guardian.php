<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use DB;

class Guardian extends Model
{
    protected $table = 'tbl_guardian';
    public $timestamps = false;

}
