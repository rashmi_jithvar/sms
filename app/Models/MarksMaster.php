<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\LabelType;
use App\Models\SubjectHasMarks;
use App\Models\SubjectHasMarksTemp;
use Session;

class MarksMaster extends Model
{
	//use SoftDeletes;

    protected $table = 'tbl_marks_master';
    protected $fillable = ['class_id','exam_type','total_marks'];
    //public $timestamps = false;

    
    protected function updatecreate($request,$id=null){
         if($id){
          $master = Self::find($id);
        }else{
          $master = new MarksMaster();
        }

       DB::beginTransaction();
       try{ 
        
        $master->class_id = $request->class;
        $master->exam_type = $request->exam_type;
        $master->total_marks = $request->total;
        $master->created_by = auth()->user()->id;
        DB::commit();
        $master->save();

        if($master->save()){
            if (Session::get('subject_key') == null) {
                $marks_temp = SubjectHasMarksTemp::where(['key_id' => $id])->get();
                $check_marks = SubjectHasMarks::where(['marks_master_id' => $id])->get();
                    if(count($check_marks) > 0){
                            foreach ($check_marks as $key => $value) {
                                $value->delete();
                            }
                        } 
                        
                               
            } else {
                $marks_temp = SubjectHasMarksTemp::where(['key_id' => Session::get('subject_key') ])->get();

            }
            if (count($marks_temp) > 0) {
                foreach ($marks_temp as $k => $item) {
                    $marks = new SubjectHasMarks();
                    $marks->subject_master_id = $item->subject_master_id;
                    $marks->internal_marks = $item->internal_marks;
                    $marks->external_marks = $item->external_marks;
                    $marks->is_external = $item->is_external ;
                    $marks->total_marks = $item->total_marks ;          
                    $marks->marks_master_id = $master->id;                    
                    $marks->save();
                }
                if (Session::get('subject_key') == null) {
                    DB::table('tbl_subject_has_marks_temp')->where('key_id', $id)->delete();
                }
                else{
                    DB::table('tbl_subject_has_marks_temp')->where('key_id', Session::get('subject_key'))->delete();
                }
                Session::forget('subject_key');
             }           
            }
        //return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Marks Master :'.$e->getMessage());
        return false; 
     } 
    }

    public function getSubject(){
      return $this->hasOne(Master::class,'id','subject_id'); 
    } 


    public function SubjectMaster(){
      return $this->hasMany(Master::class,'class_id','id'); 
    }  

    public function getSubjectName(){
      return $this->hasMany(Master::class,'id','subject_id'); 
    }  

    public function subjectHasMarks(){
      return $this->hasMany(SubjectHasMarks::class,'marks_master_id','id'); 
    }  
    public function className(){
      return $this->hasOne(Master::class,'id','class_id'); 
    } 
    public function examType(){
      return $this->hasOne(Master::class,'id','exam_type'); 
    } 

    
}
