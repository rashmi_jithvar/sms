<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use DB;
use Auth;

class FeeTypeTemp extends Model
{


    protected $table = 'tbl_fee_type_temp';
    public $timestamps = false;

    protected $fillable = ['fees_type','value','key_id'];

    protected function updatecreate($request,$key_id){
     
       DB::beginTransaction();
       try{ 
        $master = new FeeTypeTemp();
        $master->fees_type = $request->fees_type;
        $master->value = $request->value;
        $master->key_id = $key_id;
        DB::commit();
        $master->save();
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Fees Master type :'.$e->getMessage());
        return '0';	
     } 
    }

    public function getFees(){
     return $this->hasMany(Master::class,'id','fees_type');   
    }

    protected function loadCart($key_id){
        $currency_id = Auth::user()->currency_id;
        $currency_name = Master::GetCurrent($currency_id);       
        if($currency_name ==''){
           $currency_name ='';
        }
     $item = FeeTypeTemp::with('getFees')
                         ->where('key_id',$key_id)
                         ->get();    
        $table = "";
        if ($item) {
            $table.= "
        <thead>
              <tr>
                  <th><strong>S.no.</strong></th>
                  <th><strong>Fees Type</strong></th>
                  <th><strong>Fee &nbsp;(".$currency_name->name.")</strong></th>
                  <th></th>
              </tr>
        </thead>
        <tbody id='cart-body'>";
            $i = 0;
            $total = 0;
            foreach ($item as $row) {
              $fees_type = Master::select('name')->where(['id'=>$row->fees_type])->first();  
                $i++;
                $table.= "<tr>
                 <td>" . $i . "</td>
                  <td>" .$fees_type->name."</td>
                   <td>" . number_format($row->value,2) . "</td>
                     <td><a href='javascript:void(0)' onclick='removeCart(\"" . Url('delete-fees-type-temp/' . $row->id) . "\");'><i class='fa fa-trash' aria-hidden='true'></i></a></td>
                    </tr>";
                        $total+= $row->value; 
                }
            $table.= "</tbody>
                   <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Total:</th>
                            <th class='text-right' id='total' ><input type='hidden' name='total' value=" . $total . ">".$currency_name->name."&nbsp;&nbsp;&nbsp;&nbsp;" . number_format($total,2) . " </th>
                        </tr>
                    </tfoot>";
        } 
        else {
            $table = "
                    <thead>
                        <tr>
                          <th><strong>S.no.</strong></th>
                          <th><strong>Fees Type</strong></th>
                          <th><strong>Fee (".$currency_name->name.")</strong></th>
                          <th></th>
                        </tr>
                    </thead>
                     <tbody id='cart-body'>
                  <tr>
                      <td colspan='10'>No records found</td>
                   </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th class='text-right' id='total'></th>
                            <th></th>
                        </tr>
                    </tfoot>
                   ";
        }
        return $table;  
    }

}
