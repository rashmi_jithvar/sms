<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Master;
use App\Models\LabelType;
use App\Models\Student;
use DB;
use Auth;

class StudentFees extends Model
{
    protected $table = 'tbl_student_fees';
    public $timestamps = false;
    protected $fillable = ['fees_type_id','fees','fee_id','discount','total'];

  public function getFees(){
     return $this->hasOne(Master::class,'id','fees_type_id');   
  }
  public function getLabel(){
     return $this->hasOne(LabelType::class,'id','frequency_id');   
  }      
}
