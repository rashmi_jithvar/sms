<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\GuardianEnquiry;
use DB;

class StudentEnquiry extends Model
{
	use SoftDeletes;

    protected $table = 'tbl_student_enquiry';
    //public $timestamps = false;

    //protected $fillable = ['parant_id','name','abbr','created_by','created_at','updated_at','deleted_at','status'];

    protected function updatecreate($request,$id=null){
    
        if($id){
          $master = Self::find($id);
        }else{
          $master = new StudentEnquiry();
        }

       DB::beginTransaction();
       try{  
        $master->first_name = $request->first_name;
        $master->last_name = $request->last_name;
        if($request->has('dob')){
            $var = $request->dob;
            $date = str_replace('/', '-', $var);
            $new= date('Y-m-d', strtotime($date));
            $master->dob = $new;            
        }

        $master->gender = $request->gender;
        $master->category = $request->category;
        $master->admission_seeking = $request->admission_seeking;        
        $master->city = $request->city;
        $master->state = $request->state;
        $master->address = $request->address;
        $master->pincode = $request->pincode;
        $master->created_by = auth()->user()->id;
        $master->created_at = date('Y-m-d');
        DB::commit();
        $master->save();

        if($master->save()){

            $father = new GuardianEnquiry();
            if($request->has('father_id')){
                if($request->father_id !=''){
                      $father =  GuardianEnquiry::where('id',$request->father_id)->first();
               }
            }
            if($request->has('father_name')){
                    $father->name = $request->father_name;             
            }

            if($request->has('father_contact_no')){
                
                    $father->mobile = $request->father_contact_no;
                         
            }
            if($request->has('father_email')){
                
                    $father->email =  $request->father_email;
                         
            }
            if($request->has('occupation_father')){
                
                    $father->occupation = $request->occupation_father;
                            
            }
                
            $father->relation = 'father';
            $father->student_id = $master->id;

            $father->save();  
            
            $mother = new GuardianEnquiry();
            if($request->has('mother_id')){
                if($request->mother_id !=''){
                $mother =  GuardianEnquiry::where('id',$request->mother_id)->first();
                }
            }            
            if($request->has('mother_name')){
                $mother->name = $request->mother_name;             
            }

            if($request->has('mother_contact_no')){
                
                    $mother->mobile = $request->mother_contact_no;
                         
            }
            if($request->has('mother_email')){
                
                    $mother->email =  $request->mother_email;
                         
            }
            if($request->has('occupation_mother')){
                
                    $mother->occupation = $request->occupation_mother;
                            
            }
            $mother->relation = 'mother';
            $mother->student_id = $master->id;
                              
            $mother->save();                                               
        }
        return $master->id;
       }catch(\Exception $e){
        DB::rollback();
        \Log::debug('Student type :'.$e->getMessage());
        return false;	
     } 
    }

    public function parent(){
     return $this->hasMany(GuardianEnquiry::class,'student_id','id');	
    }
    public function children(){
     return $this->hasMany(self::class,'parant_id','id'); 
    } 

    public function hasClass(){
     return $this->hasOne(Master::class,'id','admission_seeking');  
    }

    public function hasCategory(){
     return $this->hasOne(Master::class,'id','category'); 
    }     

    public function hasSection(){
     return $this->hasOne(Master::class,'id','section'); 
    }

        
}
