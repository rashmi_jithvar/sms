<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteMaster extends Model
{
    protected $table = 'website_masters';

    protected $fillable = ['phoneFirst', 'phoneSecond', 'primaryEmail', 
    'secondaryEmail', 'noreplyEmail', 'facebook', 'linkedin', 'twitter', 'gplus', 'address', 'employeeLogin', 'clientLogin'];
}
