<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddSubMenus extends Model
{
    protected $table = 'add_sub_menuses';

    protected $fillable = ['subMenuName', 'pageNameId'];
}
