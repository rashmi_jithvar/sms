<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;
use App\PermissionGroup;

class PermissionGroup extends Model
{

	protected $table = 'tbl_permission_group';
	  
    public function updatePermissionGroup($data)
	{
        $roles = $this->find($data['id']);
        $roles->permission_name = $data['name'];
        $roles->status = $data['status'];
        $roles->save();
        return 1;
	}

	public function permission()
    {
        return $this->hasMany(Permission::class, 'group_id', 'id');
    }




}
