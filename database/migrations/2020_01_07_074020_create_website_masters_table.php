<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phoneFirst')->nullable();
            $table->string('phoneSecond')->nullable();
            $table->string('primaryEmail')->nullable();
            $table->string('secondaryEmail')->nullable();
            $table->string('noreplyEmail')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('gplus')->nullable();
            $table->string('address')->nullable();
            $table->string('employeeLogin')->nullable();
            $table->string('clientLogin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_masters');
    }
}
