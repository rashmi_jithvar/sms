<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pageName');
            $table->string('subMenuName')->nullable();
            $table->string('pageTitle');
            $table->longtext('pageDescription');
            $table->string('metaTitle');
            $table->string('metaDescription');
            $table->string('metaKeyword');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_pages');
    }
}
