<?php 
     Route::get('fees-master','FeesMasterController@index')->name('fees-master');
     Route::get('add-fees','FeesMasterController@create')->name('add-fees');  
     Route::post('add-cart','FeesMasterController@addCart')->name('add-cart');

     Route::get('load-cart','FeesMasterController@loadCart')->name('load-cart');  
     Route::get('delete-fees-type-temp/{id}','FeesMasterController@deleteFeesTypeTemp')->name('delete-fees-type-temp'); 
       
     Route::post('fees-type/fees-store','FeesMasterController@store')->name('fees-type.fees-store'); 
     Route::post('fees-type/update-fees-store/{id}','FeesMasterController@update')->name('fees-type.update-fees-store'); 
     Route::get('fees-type/edit/{id}','FeesMasterController@edit')->name('fees-type.edit'); 
     Route::get('fees-type/delete/{id}','FeesMasterController@destroy')->name('fees-type.delete'); 
     Route::get('fees-type/show/{id}','FeesMasterController@show')->name('fees-type.show'); 
?>