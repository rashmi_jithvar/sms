<?php 
     Route::get('student','StudentController@index')->name('student');
     Route::get('student/add-student/{id}','StudentController@create')->name('student.add-student');  
     Route::post('student/store','StudentController@store')->name('student.store'); 	
     Route::get('load-fees','StudentController@loadFees')->name('load-fees');
     Route::post('get-fees/','StudentController@getFees')->name('get-fees'); 

     Route::post('add-fees-temp/','StudentController@addFeesTemp')->name('add-fees-temp'); 
     Route::get('delete-fees-temp/{id}','StudentController@deleteFeesTemp')->name('delete-fees-temp');
     Route::get('student/edit/{id}','StudentController@edit')->name('student.edit'); 
     Route::get('student/delete/{id}','StudentController@destroy')->name('student.delete'); 
     Route::get('student/show/{id}','StudentController@show')->name('student.show');      
     Route::get('student/view-invoice/{id}','StudentController@viewInvoice')->name('student.view-invoice');
     Route::get('student/payment-monthly','StudentController@paymentMonthly')->name('student.payment-monthly');
     Route::get('student/total-fees/{id}','StudentController@totalFees')->name('student.total-fees');  
     Route::get('student/print-monthly-invoice/{id}','StudentController@printMonthlyInvoice')->name('print-monthly-invoice');  
     Route::get('student/print-report/{id}','StudentController@printReport')->name('print-report');  

     Route::get('enquiry-addmission-report','StudentController@index')->name('enquiry-addmission-report');

     Route::post('search-addmission-report','StudentController@searchAddmissionReport')->name('search-addmission-report');  
     Route::get('due-fee-report','StudentController@dueFeeReport')->name('due-fee-report');  
     Route::post('search-due-amount','StudentController@searchDueAmount')->name('search-due-amount');  
     Route::get('paid-fee-report','StudentController@paidFeeReport')->name('paid-fee-report');   
     Route::post('search-paid-amount','StudentController@searchPaidAmount')->name('search-paid-amount');   
     Route::post('search-addmission','StudentController@searchAddmission')->name('search-addmission');       
?>