<?php 


Route::get('contact-us', 'ContactUSController@contactUS')->name('contact-us');
// Route::get('contact-us/store', 'ContactUSController@contactSaveData')->name('store');
Route::get('contact-us/callBack', 'ContactUSController@callBack')->name('callBack');
Route::get('contact-us/subscribe', 'ContactUSController@subscribe')->name('subscribe');
Route::get('contact-us/getaquote', 'ContactUSController@getaquote')->name('getaquote');



// Route::post('contact-us/save', 'ContactUSController@contactSave')->name('save');
Route::get('contact-us/token', 'ContactUSController@getToken')->name('token');

Route::post("/adduserdetails", "UserDetailsController@userDetails");


// User Managment 
Route::get('menu', 'PagesController@index')->name('pages');
Route::post('/menu/store', 'PagesController@store')->name('pages.store');
Route::get('/menu/edit/{id}','PagesController@edit')->name('pages.edit');
Route::post('/menu/destroy', 'PagesController@destroy')->name('pages.destroy');
Route::post('/menu/update', 'PagesController@update')->name('pages.update');

//  Sub menu operation
Route::get('add-sub-menus', 'AddSubMenusController@index')->name('add-sub-menus');
Route::post('/add-sub-menus/store', 'AddSubMenusController@store')->name('add-sub-menus.store');
Route::get('/add-sub-menus/edit/{id}','AddSubMenusController@edit')->name('add-sub-menus.edit');
Route::post('/add-sub-menus/destroy', 'AddSubMenusController@destroy')->name('add-sub-menus.destroy');
Route::post('/add-sub-menus/update', 'AddSubMenusController@update')->name('add-sub-menus.update');
Route::post('/add-sub-menus/show', 'AddSubMenusController@show')->name('add-sub-menus.show');

//  pages operation
Route::get('page', 'AddPagesController@index')->name('add-pages');
Route::post('/page/store', 'AddPagesController@store')->name('add-pages.store');
Route::get('/page/edit/{id}','AddPagesController@edit')->name('add-pages.edit');
Route::post('/page/update', 'AddPagesController@update')->name('add-pages.update');
//  pages list
Route::get('page-list', 'PageListsController@index')->name('page-list');

//  team operation
Route::get('teams', 'TeamsController@index')->name('teams');
Route::post('/teams/store', 'TeamsController@store')->name('teams.store');
Route::get('/teams/edit/{id}','TeamsController@edit')->name('teams.edit');
Route::post('/teams/destroy', 'TeamsController@destroy')->name('teams.destroy');
Route::post('/teams/update', 'TeamsController@update')->name('teams.update');

//  SLIDER
Route::get('sliders', 'SliderController@index')->name('sliders');
Route::post('/sliders/store', 'SliderController@store')->name('sliders.store');
Route::get('/sliders/edit/{id}','SliderController@edit')->name('sliders.edit');
Route::post('/sliders/destroy', 'SliderController@destroy')->name('sliders.destroy');
Route::post('/sliders/update', 'SliderController@update')->name('sliders.update');

