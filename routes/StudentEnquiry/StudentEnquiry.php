<?php 
     Route::get('student-enquiry','StudentEnquiryController@index')->name('student-enquiry');
     Route::get('enquiry-report','StudentEnquiryController@index')->name('enquiry-report');
     Route::post('search-enquiry','StudentEnquiryController@searchEnquiry')->name('search-enquiry');     
     Route::post('search-report','StudentEnquiryController@searchReport')->name('search-report');
     Route::get('add-student-enquiry','StudentEnquiryController@create')->name('add-student-enquiry');  
     Route::post('enquiry/store','StudentEnquiryController@store')->name('enquiry.store');    
     Route::get('enquiry/edit/{id}','StudentEnquiryController@edit')->name('enquiry.edit'); 
     Route::get('enquiry/delete/{id}','StudentEnquiryController@destroy')->name('enquiry.delete'); 
     Route::get('enquiry/show/{id}','StudentEnquiryController@show')->name('enquiry.show');
     Route::post('enquiry/update/{id}','StudentEnquiryController@update')->name('enquiry.update'); 
        
?>