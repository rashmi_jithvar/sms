<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'validToken'], function () {
	//Route::get('/forgot-password', 'ApiController@forgotPassword');
});
Route::group(['namespace' => 'Login','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/API/Login/');
});
Route::get('/test', 'ApiController@test');