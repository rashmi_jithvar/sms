<?php

Route::get('/roles', 'RolesController@index');
Route::get('/roles/create', 'RolesController@create');
Route::post('/roles/store', 'RolesController@store');
Route::get('/edit/roles/{id}','RolesController@edit');
Route::post('/edit/roles/{id}','RolesController@update');
Route::get('/delete/roles/{id}','RolesController@destroy');
Route::get('/permission_group', 'PermissionGroupController@index');
Route::get('/permission_group/create', 'PermissionGroupController@create');
Route::post('/permissiongroup/store', 'PermissionGroupController@store');
Route::get('/edit/permission_group/{id}','PermissionGroupController@edit');
Route::post('/edit/permission_group/{id}','PermissionGroupController@update');
Route::get('/delete/permission_group/{id}','PermissionGroupController@destroy');
//permission
Route::get('/permission', 'PermissionController@index');
Route::get('/permission/create', 'PermissionController@create');
Route::post('/permission/store', 'PermissionController@store');
Route::get('/edit/permission/{id}','PermissionController@edit');
Route::post('/edit/permission/{id}','PermissionController@update');
Route::get('/delete/permission/{id}','PermissionController@destroy');
//role Permission
Route::get('/role_permission', 'RolePermissionController@index');
Route::get('/role_permission/create', 'RolePermissionController@create');
Route::post('/role_permission/store', 'RolePermissionController@store');
Route::get('/edit/role_permission/{id}','RolePermissionController@edit');
Route::post('/edit/role_permission/{id}','RolePermissionController@update');
Route::get('/delete/role_permission/{id}','RolePermissionController@destroy');
//User
Route::get('/user', 'UserController@index');
Route::get('/user/create', 'UserController@create');
Route::post('/user/store', 'UserController@storeUser');
Route::get('/edit/user/{id}','UserController@edit');
Route::post('/edit/user/{id}','UserController@update');
Route::get('/delete/user/{id}','UserController@destroy');
Route::get('/user/view/{id}','UserController@view');