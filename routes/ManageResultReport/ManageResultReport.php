<?php 
     Route::get('student-result-report','StudentResultReportController@index')->name('student-result-report');
     Route::get('add-student-result','StudentResultReportController@create')->name('add-student-result');  
     Route::post('add-student-marks','StudentResultReportController@addStudentMarks')->name('add-student-marks');

     Route::get('load-student-marks','StudentResultReportController@LoadStudentMarks')->name('load-student-marks');  

     Route::get('student-result/edit/{id}','StudentResultReportController@edit')->name('student-result.edit'); 

     Route::post('student-result/update/{id}','StudentResultReportController@update')->name('student-result.update');      
     Route::get('student-result/delete/{id}','StudentResultReportController@destroy')->name('student-result.delete'); 
     Route::get('student-result/show/{id}','StudentResultReportController@show')->name('student-result.show');       

     Route::post('get-student','StudentResultReportController@getStudentList')->name('get-student');
     Route::post('get-class-subject','StudentResultReportController@getSubject')->name('get-class-subject');
     Route::post('get-total-subject-marks','StudentResultReportController@getTotalSubjectMarks')->name('get-total-subject-marks');     
      
     Route::post('get-marks','StudentResultReportController@getMarks')->name('get-marks'); 
     Route::get('delete-student-marks-temp/{id}','StudentResultReportController@deleteStudentMarks')->name('delete-student-marks-temp'); 
     //Route::get('fees-type/show/{id}','FeesMasterController@show')->name('fees-type.show'); 
     Route::get('student-result/print/{id}','StudentResultReportController@print')->name('student-result.print');
     Route::post('student-marks/store','StudentResultReportController@store')->name('student-marks.store');
?>