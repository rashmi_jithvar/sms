<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//  change password
Route::get('change-password', 'UserProfileController@passwordEdit')->name('change-password');
Route::post('/change-password/store', 'UserProfileController@store')->name('change-password.store');

//  master details
Route::get('profile', 'UserProfileController@index')->name('website-master');
Route::post('/profile/update', 'UserProfileController@update')->name('website-master.update');

//CMS Management
Route::group(['prefix'=>'cms_mangment','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/CMS/');
});

//User Management
Route::group(['middleware' => ['auth','Roleauth']], function () {
   includeRouteFiles(__DIR__.'/UserManagement/');
});

 //Property Type
Route::group(['namespace' => 'Backend','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/MasterManagement/');
});

Route::group(['namespace' => 'Backend','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/StudentEnquiry/');
});

Route::group(['namespace' => 'Backend','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/FeesMaster/');
});


Route::group(['namespace' => 'Backend','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/Student/');
});

Route::group(['namespace' => 'Backend','middleware' => ['auth','Roleauth']], function () {
  includeRouteFiles(__DIR__.'/ManageResultReport/');
});



