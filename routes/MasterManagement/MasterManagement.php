<?php

     Route::get('master','MasterController@index')->name('master');    
	 Route::get('mastermaster/create/','MasterController@create')->name('master.create');
	 Route::post('master/store/','MasterController@store')->name('master.store');
	 Route::get('master/edit/','MasterController@edit')->name('master.edit');
	 Route::patch('master/update/','MasterController@update')->name('master.update');
	 Route::delete('master/delete','MasterController@delete')->name('master.delete');
/* Subject Master*/      
	 Route::get('subject-master','SubjectMasterController@index')->name('subject-master');
	 Route::get('add-subject-master','SubjectMasterController@create')->name('add-subject-master');
	 Route::post('subject-master/store','SubjectMasterController@store')->name('subject-master.store');
	 Route::post('subject-master/update/{id}','SubjectMasterController@update')->name('subject-master.update');
     Route::get('subject-master/delete/{id}','SubjectMasterController@destroy')->name('subject-master.delete'); 
     Route::get('subject-master/edit/{id}','SubjectMasterController@edit')->name('subject-master.edit');
/* Marks Master*/       
     Route::get('marks-master','MarksMasterController@index')->name('marks-master');  
     Route::get('add-marks-master','MarksMasterController@create')->name('add-marks-master');
     Route::post('add-marks','MarksMasterController@addMarks')->name('add-marks');
     Route::get('load-marks','MarksMasterController@loadMarks')->name('load-marks');
     Route::post('get-subject','MarksMasterController@getSubject')->name('get-subject');
    Route::get('delete-subject-has-marks-temp/{id}','MarksMasterController@deleteSubjectHasMarks')->name('delete-subject-has-marks-temp'); 
    Route::post('marks-master/store','MarksMasterController@store')->name('marks-master.store');
    Route::get('marks-master/show/{id}','MarksMasterController@show')->name('marks-master.show');
    Route::get('marks-master/edit/{id}','MarksMasterController@edit')->name('marks-master.edit'); 
    Route::post('marks-master/update/{id}','MarksMasterController@update')->name('marks-master.update');
    Route::get('marks-master/delete/{id}','MarksMasterController@destroy')->name('marks-master.delete');

/* grade Master */    
     Route::get('grade-master','GradeMasterController@index')->name('grade-master');  
     Route::get('add-grade','GradeMasterController@create')->name('add-grade');
     Route::post('grade-master/store','GradeMasterController@store')->name('grade-master.store');
     Route::get('grade-master/delete/{id}','GradeMasterController@destroy')->name('grade-master.delete'); 
     Route::get('grade-master/edit/{id}','GradeMasterController@edit')->name('grade-master.edit');  
     Route::post('grade-master/update/{id}','GradeMasterController@update')->name('grade-master.update');        
     